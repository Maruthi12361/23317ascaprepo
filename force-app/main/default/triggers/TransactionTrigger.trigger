/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer               Description                                                 *
*   ----------  -----------------       ------------------------------------------------------------*
*   2019-04-30  Xede Consulting Group   Initial Creation                                            *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This trigger manages the distribution of work and the order of operations for events
* against the Transaction_object.
* @author Xede Consulting Group
* @date 2019-04-30
*/
trigger TransactionTrigger on Transaction__c (before insert, before update, after insert, after update) {

    //Verify that trigger is enabled
    string triggerName = ASCAPUtils.getExecutableName();
    if (ASCAPUtils.triggerDisabled(triggerName)) {
        system.debug('INFO: ' + triggerName + ' has been disabled.  To enable it, update the DisableTrigger Custom Setting');
        return;
    }

    //capture current state of constraints and send to debug log.
    System.debug(triggerName + ': Number of Queries used in this apex code so far: ' + Limits.getQueries());
    System.debug(triggerName + ': Number of rows queried in this apex code so far: ' + Limits.getDmlRows());
    System.debug(triggerName + ': Number of DML statements in this apex code so far: ' + Limits.getDmlStatements());
    System.debug(triggerName + ': CPU time (in ms) used in this apex code so far: ' + Limits.getCpuTime());

    if (trigger.isBefore){
        TransactionHelper.useTableFormat = boolean.valueOf(label.Use_Table_Format);
        if (trigger.isInsert){
            TransactionHelper.createRateScheduleFeeDetail(trigger.new, null);
            TransactionHelper.createMusicUseFeeDetail(trigger.new, null);
        }
        else if (trigger.isUpdate){
            TransactionHelper.createRateScheduleFeeDetail(trigger.new, trigger.oldMap);
            TransactionHelper.createMusicUseFeeDetail(trigger.new, trigger.oldMap);
        }
    }
    if (trigger.isAfter){
        if (trigger.isInsert){
            //TransactionHelper.updateLicensePolicyMergeFields(trigger.new, null);

        }
        if (trigger.isUpdate){
            //TransactionHelper.updateLicensePolicyMergeFields(trigger.new, trigger.oldMap);
        }
    }
}