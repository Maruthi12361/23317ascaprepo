/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer           Description                                                     *
*   ----------  -----------------   ----------------------------------------------------------------*
*     Xede Consulting Group  	Initial Creation                                                *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This trigger manages the distribution of work and the order of operations for events
* against the Opportunity object.
* @author Xede Consulting Group
* @date 
*/
trigger OpportunityTrigger on Opportunity (after insert, before update, after update) {
    //Verify that trigger is enabled
    string triggerName = ASCAPUtils.getExecutableName();
    if (ASCAPUtils.triggerDisabled(triggerName)) {
        system.debug('INFO: ' + triggerName + ' has been disabled.  To enable it, update the DisableTrigger Custom Setting');
        return;
    }

    //capture current state of constraints and send to debug log.
    System.debug(triggerName + ': Number of Queries used in this apex code so far: ' + Limits.getQueries());
    System.debug(triggerName + ': Number of rows queried in this apex code so far: ' + Limits.getDmlRows());
    System.debug(triggerName + ': Number of DML statements in this apex code so far: ' + Limits.getDmlStatements());
    System.debug(triggerName + ': CPU time (in ms) used in this apex code so far: ' + Limits.getCpuTime());

    if (trigger.isAfter){
        if (trigger.isInsert){
            OpportunityFeeCalcHelper.createLicensePolicyTransaction(trigger.new, null);
            OpportunityHelper.recalcOpportunityCounts(Trigger.new, null);
        }
        else if (trigger.isUpdate){
            if (OpportunityFeeCalcHelper.isFirstTime){
                OpportunityFeeCalcHelper.isFirstTime = false;
                OpportunityFeeCalcHelper.deleteRequestOnLicenseChange(trigger.new, trigger.oldMap);
            }
            OpportunityFeeCalcHelper.createLicensePolicyTransaction(trigger.new, trigger.oldMap);
            if (OpportunityHelper.ownerAssignmentRan == false) {
                OpportunityHelper.assignOpportunityOwner(trigger.new, trigger.oldMap);
            }
            OpportunityHelper.recalcOpportunityCounts(Trigger.new, Trigger.oldMap);
        }
    }
    if (trigger.isBefore && trigger.isUpdate){
        OpportunityFeeCalcHelper.assignEncryptedRecordId(trigger.new, trigger.oldMap);
    }

}