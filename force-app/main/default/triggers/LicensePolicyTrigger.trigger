/**
 * Created by johnreedy on 2019-06-06.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer           Description                                                     *
*   ----------  -----------------   ----------------------------------------------------------------*
*     Xede Consulting Group     Initial Creation                                                *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This trigger manages the distribution of work and the order of operations for events
* against the License_Policy__c object.
* @author Xede Consulting Group
* @date 
*/
trigger LicensePolicyTrigger on License_Policy__c (after insert, after update,before update) {

    if (trigger.isAfter){
        if (trigger.isInsert){
            LicensePolicyHelper.createRetroTimePeriods(Trigger.new, null);
            LicensePolicyHelper.publishDocGenEvent(trigger.new, null);
        }
        if (trigger.isUpdate){
            LicensePolicyHelper.createRetroTimePeriods(Trigger.new, Trigger.oldMap);
            LicensePolicyHelper.publishDocGenEvent(trigger.new, trigger.oldMap);
            LicenseAgreementHelper.pollForLicensePolicy(trigger.new, trigger.oldMap);
        }
    }
    if(trigger.isBefore && trigger.isUpdate){
        LicensePolicyHelper.populateDDPFlags(trigger.new, trigger.oldMap);
    }

}