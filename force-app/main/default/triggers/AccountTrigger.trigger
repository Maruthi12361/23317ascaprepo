/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer        Description                                                        *
*   ----------  ---------------  -------------------------------------------------------------------*
*   2019-10-14  Xede Consulting  Initial Creation    										    	*
*
*   Code Coverage: 90%
*                                                                                                   *
*****************************************************************************************************
*/

/**
 * @description Trigger for the account object
 * @author Xede Consulting Group
 * @date 2019-10-14
 */
trigger AccountTrigger on Account (after insert, after update) {
    string triggerName = 'AccountTrigger'; //ASCAPUtils.getExecutableName();
    
    //Verify that trigger is enabled
    if (ASCAPUtils.triggerDisabled(triggerName)) {
        system.debug('INFO: ' + triggerName + ' has been disabled.  To enable it, update the DisableTrigger Custom Setting');
        return;
    }
    if (AccountHelper.isInheritingVipDesignation) {
        System.debug('INFO: ' + triggerName + ' is being skipped because VIP designation is currently being inherited');
        return;
    }

    // capture current state of constraints and send to debug log.
    System.debug(triggerName + ': Number of Queries used in this apex code so far: ' + Limits.getQueries());
    System.debug(triggerName + ': Number of rows queried in this apex code so far: ' + Limits.getDmlRows());
    System.debug(triggerName + ': Number of DML statements in this apex code so far: ' + Limits.getDmlStatements());
    System.debug(triggerName + ': CPU time (in ms) used in this apex code so far: ' + Limits.getCpuTime());

    if (Trigger.isAfter) {
        if (Trigger.isInsert) {
            AccountHelper.validateAddress(Trigger.new, null);
        }
        else if (Trigger.isUpdate) {
            if (AccountHelper.isFirstTime) {
                AccountHelper.isFirstTime = false;

                AccountHelper.inheritVipDesignation(Trigger.new, Trigger.oldMap);

                if (AccountHelper.FutureMethodCalled == False) {
                    AccountHelper.validateAddress(Trigger.new, Trigger.oldMap);
                }
                else {
                    AccountHelper.checkForAddressChange(trigger.new, trigger.oldMap);
                }
            }
            else {
                AccountHelper.checkForAddressChange(trigger.new, trigger.oldMap);
            }
        }
    }
}