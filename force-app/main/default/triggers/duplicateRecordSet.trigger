trigger duplicateRecordSet on DuplicateRecordSet (after insert, after update) {
    List<duplicateRecordSet> drs = Trigger.new;
    system.debug('drs----'+drs);
    QueueSObject QueueID = [Select Queue.Id, Queue.Name, Queue.Type from QueueSObject WHERE Queue.Type ='Queue' AND Queue.Name = 'Qualification' Limit 1];
    List<deDupe__c> deDupeList = new List<deDupe__c>();
    for(duplicateRecordSet d : drs){
        	//DuplicateRecordItem lead = [SELECT id, RecordId FROM DuplicateRecordItem where DuplicateRecordSetId = :d.id Limit 1];
            deDupe__c deDupe = new deDupe__c();
            deDupe.Name = 'Check these dupes';
            deDupe.originalId__c = d.id;
        	//deDupe.originalId__c = lead.RecordId;
        
        	deDupeList.add(deDupe);
        	deDupe.OwnerId = QueueID.Queue.Id;
    }
    if(deDupeList.size()>0)
        insert deDupeList;
}