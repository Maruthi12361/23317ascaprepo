/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer        Description                                                        *
*   ----------  ---------------  -------------------------------------------------------------------*
*   2018-11-21  Xede Consulting  Initial Creation    										    	*
*   2018-01-29	Xede (fkindle)	 Update adding "after delete" mergedLeadChatter.postChatter			*
*   2018-02-07	Xede Consulting	 Added new method callout createRelatedRecordsOnConversion			*
*   2019-10-02  Xede Consulting Group   Added LeadHelper.assignRegionByZipcode to before insert, before update  *
*
*   Code Coverage: 93%
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This trigger manages the distribution of work and the order of operations for events
* against the Lead object.
* @author Xede Consulting Group
* @date 2018-11-21
*/

trigger LeadTrigger on Lead (before insert, before update, after insert, after update, after delete) {

    //Verify that trigger is enabled
    string triggerName = ASCAPUtils.getExecutableName();
    if (ASCAPUtils.triggerDisabled(triggerName)) {
        system.debug('INFO: ' + triggerName + ' has been disabled.  To enable it, update the DisableTrigger Custom Setting');
        return;
    }

    //capture current state of constraints and send to debug log.
    System.debug(triggerName + ': Number of Queries used in this apex code so far: ' + Limits.getQueries());
    System.debug(triggerName + ': Number of rows queried in this apex code so far: ' + Limits.getDmlRows());
    System.debug(triggerName + ': Number of DML statements in this apex code so far: ' + Limits.getDmlStatements());
    System.debug(triggerName + ': CPU time (in ms) used in this apex code so far: ' + Limits.getCpuTime());

    //If trigger is enabled, execute the below 'before' methods.
    if (trigger.isBefore && trigger.isInsert){
        LeadHelper.setBypassDuplicateRulesFlag(trigger.new);
        LeadHelper.applyKnockoutCriteria(trigger.new);
        LeadHelper.assignRegionByZipcode(Trigger.new, null);
    }

    if (trigger.isBefore && trigger.isUpdate){
        LeadHelper.assignEncryptedRecordId(trigger.new, trigger.oldMap);
        LeadHelper.beforeConversionUpdates(trigger.new, trigger.oldMap);
        LeadHelper.assignRegionByZipcode(Trigger.new, Trigger.oldMap);
    }

    //If trigger is enabled, execute the below 'after' methods.
    if (trigger.isAfter){
        if( trigger.isInsert) {
            LeadHelper.removeBypassDuplicateRulesFlag(trigger.new);
            LeadHelper.validateAddress(trigger.new, null);

        }
        else if(trigger.isDelete){
            Lead[] lead = Trigger.old;
            //mergedLeadChatter.postChatter(lead);
        }
        else if (trigger.isUpdate) {
            if (LeadHelper.isFirstTime && LeadHelper.FutureMethodCalled == False) {
                LeadHelper.isFirstTime = False;
                LeadHelper.validateAddress(trigger.new, trigger.oldMap);
                LeadFeeCalcHelper.deleteRequestOnLicenseChange(trigger.new, trigger.oldMap);
            }
            if (LeadFeeCalcHelper.isFirstTime){
                LeadFeeCalcHelper.isFirstTime = False;
                LeadFeeCalcHelper.deleteRequestOnLicenseChange(trigger.new, trigger.oldMap);
            }
            LeadConversionHelper.createRelatedRecordsOnConversion(trigger.new, trigger.oldMap);
            LeadHelper.checkForAddressChange(trigger.new, trigger.oldMap);
        }
    }
}