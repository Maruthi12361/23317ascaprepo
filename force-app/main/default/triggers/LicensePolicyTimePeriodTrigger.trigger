/**
 * Created by johnreedy on 2019-10-28.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer           Description                                                     *
*   ----------  -----------------   ----------------------------------------------------------------*
*   2019-10-28  Xede Consulting   	Initial Creation                                                *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This trigger manages the distribution of work and the order of operations for events
* against the License_Policy_Time_Period__c object.
* @author Xede Consulting Group
* @date 
*/
trigger LicensePolicyTimePeriodTrigger on License_Policy_Time_Period__c (before insert, before update) {
    //Verify that trigger is enabled
    string triggerName = ASCAPUtils.getExecutableName();
    if (ASCAPUtils.triggerDisabled(triggerName)) {
        system.debug('INFO: ' + triggerName + ' has been disabled.  To enable it, update the DisableTrigger Custom Setting');
        return;
    }

    //capture current state of constraints and send to debug log.
    System.debug(triggerName + ': Number of Queries used in this apex code so far: ' + Limits.getQueries());
    System.debug(triggerName + ': Number of rows queried in this apex code so far: ' + Limits.getDmlRows());
    System.debug(triggerName + ': Number of DML statements in this apex code so far: ' + Limits.getDmlStatements());
    System.debug(triggerName + ': CPU time (in ms) used in this apex code so far: ' + Limits.getCpuTime());

    if (trigger.isBefore){
        if (trigger.isInsert || trigger.isUpdate){
            LicensePolicyTimePeriodHelper.setRelatedLookupFields(trigger.new);
        }
    }

}