/**
 * Created by Xede Consulting Group (Jason Burke) on 11/27/2019.
 */

/**
 * @description Schedules the MonthlyFixedFeeBatch job
 */
public with sharing class MonthlyFixedFeeScheduler implements Schedulable {

    /**
     * @description Starts the MonthlyFixedFeeBatch job for the next month
     *
     * @param sc
     */
    public void execute(SchedulableContext sc) {
        Date batchDate = Date.today().addMonths(1);
        Database.executeBatch(new MonthlyFixedFeeBatch(batchDate.month(), batchDate.year()));
    }
}