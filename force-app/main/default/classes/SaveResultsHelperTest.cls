/**
 * Created by johnreedy on 2019-01-21.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-01-21   Xede Consulting Group 		Initial Creation                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This test class exercises the methods in the SaveResultsHelper class.
* @author Xede Consulting Group
* @date 2019-01-21 
*/
@isTest
private class SaveResultsHelperTest {

    @isTest
    static void printToDebug() {
        List<Database.SaveResult> saveResultList = TestDataHelper.collectResults();

        Test.startTest();
            SaveResultsHelper resultsUtility = new SaveResultsHelper(saveResultList);
        Test.stopTest();
    }

    @isTest
    static void printResultToDebug() {
        Database.SaveResult saveResult = TestDataHelper.collectResult();

        Test.startTest();
            SaveResultsHelper resultsUtility = new SaveResultsHelper(saveResult);
        Test.stopTest();
    }

    @isTest
    static void returnErrorMessages() {
        List<Database.SaveResult> saveResultList = TestDataHelper.collectResults();

        Test.startTest();
            SaveResultsHelper resultsUtility = new SaveResultsHelper(saveResultList);
            List<String> resultList = resultsUtility.returnErrorMessages();
        Test.stopTest();

        System.assert(resultList[0].contains('<ERROR>'));
    }
}