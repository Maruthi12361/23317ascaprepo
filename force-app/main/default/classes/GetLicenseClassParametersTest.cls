/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer               Description                                                 *
*   ----------  ---------------------   ------------------------------------------------------------*
*   2019-4-2  Xede Consulting Group 	Initial Creation                                            *                                 *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This test class exercises code that performs operations on the DSL
* @author Xede Consulting Group
* @date 2018-11-26
*/

@IsTest (seeAllData = False)
private class GetLicenseClassParametersTest {
    @isTest static void testGetLicenseClassInfo() {
        LicenseClassConfig returnClassConfig = GetLicenseClassParameters.GetLicenseClassInfo('Music_in_Business_Per_Location_License_Agreement');
        System.assert(returnClassConfig != null,'Data is returned by web service');
        LicenseClassConfig_V2 returnClass2Config = GetLicenseClassParameters.GetLicenseClassConfigV2Info('Music_in_Business_Per_Location_License_Agreement');
        System.assert(returnClass2Config != null,'Data is returned by web service');

        LicenseClassFeeResponse feeResponse = GetLicenseClassParameters.GetLicenseClassFeeResult('TEST');
        System.assert(feeResponse != null,'Fee is retuened');

    }
}