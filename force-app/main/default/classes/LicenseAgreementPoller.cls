/**
 * Created by johnreedy on 2019-07-02.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  -----------------  ---------------------------------------------------------------- *
*   2019-07-02  Xede Consulting    Initial Creation                                                 *
*   2019-10-25  Xede Consulting    Added condition to check for DDP Finish date and URL so that     *
*                                  process only runs for Self-serve and added time-out at 3 mins.   8
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This queueable class polls the Content Link object for an new record, and if it is not found,
* it queues up another instance that waits a specified number of seconds and retries until the link is available.
* @author Xede Consulting Group
* @see LicenseAgreementPollerTest
* @date 2019-07-02 
*/
public with sharing class LicenseAgreementPoller implements Queueable, Database.AllowsCallouts{

    private Static Datetime DocumentFinishDate;
    private String licensePolicyId;
    private String createdByUserName;
    private String documentLink;
    private Boolean foundLink = false;

    /**
     * @description This is the class constructor.
     * @param licensePolicyToPoll
     */

    public LicenseAgreementPoller(String licensePolicyToPoll) {
        licensePolicyId = licensePolicyToPoll;

        system.debug('INFO: Getting Self_Service_DDP_Finish_Date_Time__c for License Policy> ' + licensePolicyId);

        //get the finish date
        License_Policy__c currLicensePolicy = [
            SELECT id, Self_Service_DDP_Finish_Date_Time__c, LastModifiedDate, CreatedBy.Name, Self_Service_License_Agreement_Link__c
              FROM License_Policy__c
             WHERE id = :licensePolicyId
             LIMIT 1];
        createdByUserName = currLicensePolicy.CreatedBy.Name;
        DocumentFinishDate = currLicensePolicy.Self_Service_DDP_Finish_Date_Time__c;
        documentLink = currLicensePolicy.Self_Service_License_Agreement_Link__c;
        system.debug('INFO: DocumentFinishDate for License Policy> ' + DocumentFinishDate);

    }
    /**
     * @description This is the execute method.  It checks for the presence of the related linked document, and if NOT
     * found, it re-queues until either it is found, or time limit expires.
     * Note:  The Document Finished Date and URL are only used for Self-service.
     * @param qc
     */
    public void execute(QueueableContext qc) {
        ErrorLogger logger = new ErrorLogger();
        string message='';

        //if the finish datetime is greater than 3 minutes ago and the url is not blank exit
        if(DocumentFinishDate <= System.Now().addMinutes(-3) && string.isNotBlank(documentLink)){
            message = 'Document Finish greater than 5 minutes ago and url is not blank.\n User:' + createdByUserName +'.\n Queueable method not executed.';
            logger.log(ASCAPUtils.getExecutableName(),message).save();
            return;
        }

        //check for presence of link records for this policy
        integer linkRecordsFound = getDocumentStatus(licensePolicyId);

        //if linkRecordsFound, then call method to make link document publicly available, if not re-queue
        if (linkRecordsFound > 0) {
            foundLink = true;
            LicenseAgreementHelper.makeLicenseAgreementPublic(licensePolicyId, DocumentFinishDate) ;
        } else if (linkRecordsFound == 0 && foundLink == False){
            //get current datetime and pause for 5 seconds
            DateTime start = System.Now();
            while(System.Now().getTime() < start.getTime() + 5000){}

            //re-queue job
            system.debug('INFO: Re-queueing Job for License Policy> ' + licensePolicyId);
            System.enqueueJob(new LicenseAgreementPoller(licensePolicyId));
        }
        else{
            message = 'Unhandled Exception.';
            logger.log(ASCAPUtils.getExecutableName(),message).save();
        }
    }

    /**
     * @description This method checks for existence of new ContentDocumentLink
     * @param licensePolicyId
     * @return contentLinkListSize
     */
    public static integer getDocumentStatus(string licensePolicyId){
        system.debug('INFO: Checking for new Content Document Link for Policy> ' + licensePolicyId);

        list<ContentDocumentLink> contentLinkList = [
                SELECT Id, ContentDocumentId, LinkedEntityId
                  FROM ContentDocumentLink
                 WHERE LinkedEntityId = :LicensePolicyId
                   AND SystemModstamp >=: DocumentFinishDate];

        return contentLinkList.size();
    }
}