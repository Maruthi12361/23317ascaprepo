/**
 * Created by Xede Consulting Group (Jason Burke) on 10/21/2019.
 */

@IsTest
private class MonthlyTmlcFeeBatchTest {

    @testSetup
    static void setup() {
        TestDataHelper.createAscapGlobalProperties(true, 1);
        TestDataHelper.createLicenseClass(GlobalConstant.TMLC_LICENSE_CLASS_NAME, GlobalConstant.INDUSTRY_TYPE, true);
    }

    @isTest
    static void testHappyPath() {
        License_Class__c tmlcLicenseClass = [SELECT Id FROM License_Class__c];

        Account licensee = TestDataHelper.createAccount('Test Licensee', 'Legal Entity', true);
        Account reporter = TestDataHelper.createAccount('Reporting Entity', 'Representative Body', false);
        reporter.Account_Classification__c = 'Vendor';
        insert reporter;

        List<License_Policy__c> policyList = new List<License_Policy__c>();
        License_Policy__c policy0 = TestDataHelper.createLicensePolicy(tmlcLicenseClass.Id, Date.today().addYears(-1).toStartOfMonth(), null, false);
        policy0.Licensee__c = licensee.Id;
        policy0.Reporting_Entity__c = null;
        policy0.Licensing_Area__c = GlobalConstant.BROADCAST_TELEVISION_LITERAL;
        policy0.License_Type__c = GlobalConstant.BLANKET_TYPE;
        policy0.Allocated_Fee_Amount__c = 1000.0;
        policy0.Alternative_Blanket_Fee_Amount__c = 575.0;
        policy0.Allocated_Fee_Type__c = GlobalConstant.YEARLY_ALLOCATION_LITERAL;
        policy0.Supplemental_Fee__c = 100.0;
        policy0.LNSPA_Percentage__c = 50.0;
        policyList.add(policy0);

        License_Policy__c policy1 = TestDataHelper.createLicensePolicy(tmlcLicenseClass.Id, Date.today().addYears(-1).toStartOfMonth(), null, false);
        policy1.Licensee__c = licensee.Id;
        policy1.Reporting_Entity__c = reporter.Id;
        policy1.Licensing_Area__c = GlobalConstant.BROADCAST_TELEVISION_LITERAL;
        policy1.License_Type__c = GlobalConstant.ALT_BLANKET_TYPE;
        policy1.Allocated_Fee_Amount__c = 1000.0;
        policy1.Alternative_Blanket_Fee_Amount__c = 575.0;
        policy1.Allocated_Fee_Type__c = GlobalConstant.YEARLY_ALLOCATION_LITERAL;
        policy1.Supplemental_Fee__c = 75.0;
        policy1.LNSPA_Percentage__c = 50.0;
        policyList.add(policy1);

        License_Policy__c policy2 = TestDataHelper.createLicensePolicy(tmlcLicenseClass.Id, Date.today().addYears(-1).toStartOfMonth(), null, false);
        policy2.Licensee__c = licensee.Id;
        policy2.Reporting_Entity__c = reporter.Id;
        policy2.Licensing_Area__c = GlobalConstant.BROADCAST_TELEVISION_LITERAL;
        policy2.License_Type__c = GlobalConstant.PER_PROGRAM_TYPE;
        policy2.Supplemental_Fee__c = 125.0;
        policyList.add(policy2);
        insert policyList;

        License_Policy_Time_Period__c timePeriod1 = TestDataHelper.createLicensePolicyTimePeriod(policy1.Id, Date.today().addMonths(1).toStartOfMonth(),
                                                    Date.today().addMonths(2).toStartOfMonth().addDays(-1), false);
        timePeriod1.Is_Batch_Created__c = true;
        insert timePeriod1;

        Transaction__c tran1 = TestDataHelper.createTransaction(timePeriod1.Id, 1, false);
        tran1.Is_Batch_Created__c = true;
        insert tran1;

        License_Policy_Time_Period__c timePeriod2 = TestDataHelper.createLicensePolicyTimePeriod(policy2.Id, Date.today().addMonths(1).toStartOfMonth(),
                Date.today().addMonths(2).toStartOfMonth().addDays(-1), false);
        timePeriod2.Is_Batch_Created__c = true;
        insert timePeriod2;

        Transaction__c tran2 = TestDataHelper.createTransaction(timePeriod2.Id, 1, false);
        tran2.Is_Batch_Created__c = true;
        insert tran2;

        Test.startTest();
        new MonthlyTmlcFeeScheduler().execute(null);
        Test.stopTest();

        License_Policy_Time_Period__c[] timePeriodList = [
                SELECT License_Policy__c, License_Policy__r.Reporting_Entity__c, Billing_Entity__c, Financially_Responsible__c, Reporting_Entity__c
                FROM License_Policy_Time_Period__c
        ];
        System.assertEquals(3, timePeriodList.size());
        for (License_Policy_Time_Period__c timePeriod : timePeriodList) {
            System.assertEquals(licensee.Id, timePeriod.Billing_Entity__c);
            System.assertEquals(licensee.Id, timePeriod.Financially_Responsible__c);
            if (timePeriod.License_Policy__c == policy0.Id) {
                System.assertEquals(null, timePeriod.Reporting_Entity__c);
            }
            else {
                System.assertEquals(reporter.Id, timePeriod.Reporting_Entity__c);
            }
        }


        Transaction__c[] tranList = [
                SELECT Amount__c, Transaction_Description__c, License_Policy_Time_Period__r.License_Policy__c,
                        License_Policy_Time_Period__r.License_Policy__r.License_Type__c,
                        License_Policy_Time_Period__r.License_Policy__r.Allocated_Fee_Amount__c,
                        License_Policy_Time_Period__r.License_Policy__r.Alternative_Blanket_Fee_Amount__c,
                        License_Policy_Time_Period__r.License_Policy__r.Supplemental_Fee__c
                FROM Transaction__c
        ];
        System.debug(tranList);

        System.assertEquals(5, tranList.size());

        Map<Id, Integer> feeMap = new Map<Id, Integer> {
                policy0.Id => 0,
                policy1.Id => 0,
                policy2.Id => 0
        };
        Map<Id, Integer> suppMap = new Map<Id, Integer> {
                policy0.Id => 0,
                policy1.Id => 0,
                policy2.Id => 0
        };

        for (Transaction__c tran : tranList) {
            if (tran.Transaction_Description__c != null && tran.Transaction_Description__c.startsWith(GlobalConstant.SUPPLEMENTAL_FEE_LITERAL)) {
                System.assertEquals(tran.License_Policy_Time_Period__r.License_Policy__r.Supplemental_Fee__c, tran.Amount__c);
                Integer num = suppMap.get(tran.License_Policy_Time_Period__r.License_Policy__c);
                suppMap.put(tran.License_Policy_Time_Period__r.License_Policy__c, num + 1);
            }
            else {
                System.assertNotEquals(GlobalConstant.PER_PROGRAM_TYPE, tran.License_Policy_Time_Period__r.License_Policy__r.License_Type__c);
                if (tran.License_Policy_Time_Period__r.License_Policy__r.License_Type__c == GlobalConstant.BLANKET_TYPE) {
                    System.assertEquals(tran.License_Policy_Time_Period__r.License_Policy__r.Allocated_Fee_Amount__c, tran.Amount__c);
                }
                else if (tran.License_Policy_Time_Period__r.License_Policy__r.License_Type__c == GlobalConstant.ALT_BLANKET_TYPE) {
                    System.assertEquals(tran.License_Policy_Time_Period__r.License_Policy__r.Alternative_Blanket_Fee_Amount__c, tran.Amount__c);
                }

                Integer num = feeMap.get(tran.License_Policy_Time_Period__r.License_Policy__c);
                feeMap.put(tran.License_Policy_Time_Period__r.License_Policy__c, num + 1);
            }
        }

        System.assertEquals(1, suppMap.get(policy0.Id));
        System.assertEquals(1, suppMap.get(policy1.Id));
        System.assertEquals(1, suppMap.get(policy2.Id));

        System.assertEquals(1, feeMap.get(policy0.Id));
        System.assertEquals(1, feeMap.get(policy1.Id));
        System.assertEquals(0, feeMap.get(policy2.Id));

    }

    @isTest
    static void testPerProgramChange() {
        License_Class__c tmlcLicenseClass = [SELECT Id FROM License_Class__c];
        Account reporter = TestDataHelper.createAccount('Reporting Entity', 'Representative Body', false);
        reporter.Account_Classification__c = 'Vendor';
        insert reporter;

        License_Policy__c policy = TestDataHelper.createLicensePolicy(tmlcLicenseClass.Id, Date.today().addYears(-1), null, false);
        policy.Reporting_Entity__c = reporter.Id;
        policy.Licensing_Area__c = GlobalConstant.BROADCAST_TELEVISION_LITERAL;
        policy.License_Type__c = GlobalConstant.PER_PROGRAM_TYPE;
        insert policy;

        License_Policy_Time_Period__c timePeriod = TestDataHelper.createLicensePolicyTimePeriod(policy.Id, Date.today().toStartOfMonth(),
                Date.today().addMonths(1).toStartOfMonth().addDays(-1), false);
        timePeriod.Is_Batch_Created__c = true;
        timePeriod.Stage__c = GlobalConstant.INITIAL_REPORT_LITERAL;
        timePeriod.Next_Report_Due_Date__c = timePeriod.End_Date__c.addDays(45);
        insert timePeriod;

        policy.License_Type__c = GlobalConstant.BLANKET_TYPE;
        update policy;

        Test.startTest();
        Database.executeBatch(new MonthlyTmlcFeeBatch(Date.today().month(), Date.today().year(), new Id[] { policy.Id }, null, null, null, null, null, false));
        Test.stopTest();

        timePeriod = [SELECT Stage__c, Next_Report_Due_Date__c FROM License_Policy_Time_Period__c WHERE Id = :timePeriod.Id];
        System.assertEquals(null, timePeriod.Stage__c);
        System.assertEquals(null, timePeriod.Next_Report_Due_Date__c);
    }

    @isTest
    static void testFutureLicenseType() {
        License_Class__c tmlcLicenseClass = [SELECT Id FROM License_Class__c];
        Date today = Date.today();

        Account reporter1 = TestDataHelper.createAccount('Reporting Entity 1', 'Representative Body', false);
        reporter1.Account_Classification__c = 'Vendor';
        insert reporter1;
        List<License_Policy__c> policyList = new List<License_Policy__c>();
        License_Policy__c policy0 = TestDataHelper.createLicensePolicy(tmlcLicenseClass.Id, Date.newInstance(2019, 1, 1), null, false);
        policy0.Reporting_Entity__c = reporter1.Id;
        policy0.Licensing_Area__c = GlobalConstant.BROADCAST_TELEVISION_LITERAL;
        policy0.License_Type__c = GlobalConstant.PER_PROGRAM_TYPE;
        policy0.Future_License_Type__c = GlobalConstant.ALT_BLANKET_TYPE;
        policy0.Future_Reporting_Entity__c = null;
        policy0.Future_License_Type_Date__c = today.toStartOfMonth();
        policy0.Allocated_Fee_Amount__c = 1000.0;
        policy0.Alternative_Blanket_Fee_Amount__c = 575.0;
        policy0.LNSPA_Percentage__c = 50.0;
        policyList.add(policy0);
        insert policyList;

        Test.startTest();
        Database.executeBatch(new MonthlyTmlcFeeBatch(today.month(), today.year()));
        Test.stopTest();

        Transaction__c[] tranList = [
                SELECT License_Policy_Time_Period__r.License_Policy__r.License_Type__c,
                        License_Policy_Time_Period__r.License_Policy__r.Future_License_Type__c,
                        License_Policy_Time_Period__r.License_Policy__r.Future_Reporting_Entity__c,
                        License_Policy_Time_Period__r.License_Policy__r.Future_License_Type_Date__c,
                        License_Policy_Time_Period__r.License_Policy__r.License_Policy_Changes__c,
                        License_Policy_Time_Period__r.License_Policy__r.Reporting_Entity__c,
                        Amount__c
                FROM Transaction__c
        ];

        System.assertEquals(1, tranList.size());
        System.assertEquals(GlobalConstant.ALT_BLANKET_TYPE, tranList[0].License_Policy_Time_Period__r.License_Policy__r.License_Type__c);
        System.assertEquals(null, tranList[0].License_Policy_Time_Period__r.License_Policy__r.Future_License_Type__c);
        System.assertEquals(null, tranList[0].License_Policy_Time_Period__r.License_Policy__r.Future_Reporting_Entity__c);
        System.assertEquals(null, tranList[0].License_Policy_Time_Period__r.License_Policy__r.Future_License_Type_Date__c);
        System.assertEquals(1, tranList[0].License_Policy_Time_Period__r.License_Policy__r.License_Policy_Changes__c);
        System.assertEquals(null, tranList[0].License_Policy_Time_Period__r.License_Policy__r.Reporting_Entity__c);
        System.assertEquals(policy0.Alternative_Blanket_Fee_Amount__c, tranList[0].Amount__c);

    }

    @isTest
    static void testIsVenueOffAir() {
        License_Class__c tmlcLicenseClass = [SELECT Id FROM License_Class__c];

        Account licensee = TestDataHelper.createAccount('Test Licensee', 'Legal Entity', true);
        Account venue = TestDataHelper.createAccount('Test Venue', 'Station', false);
        venue.Is_Off_Air__c = true;
        insert venue;

        License_Policy__c policy0 = TestDataHelper.createLicensePolicy(tmlcLicenseClass.Id, Date.today().addYears(-1), null, false);
        policy0.Licensee__c = licensee.Id;
        policy0.Venue__c = venue.Id;
        policy0.Licensing_Area__c = GlobalConstant.BROADCAST_TELEVISION_LITERAL;
        policy0.License_Type__c = GlobalConstant.BLANKET_TYPE;
        policy0.Allocated_Fee_Amount__c = 1000.0;
        policy0.Alternative_Blanket_Fee_Amount__c = 575.0;
        policy0.LNSPA_Percentage__c = 50.0;
        insert policy0;

        Test.startTest();
        Database.executeBatch(new MonthlyTmlcFeeBatch());
        Test.stopTest();

        License_Policy_Time_Period__c[] timePeriodList = [
                SELECT Is_Off_Air__c, Total_Amount__c
                FROM License_Policy_Time_Period__c
        ];
        System.assertEquals(1, timePeriodList.size());
        System.assertEquals(true, timePeriodList[0].Is_Off_Air__c);
        System.assertEquals(0, timePeriodList[0].Total_Amount__c);
    }

    @isTest
    static void testErrorConditions() {
        License_Class__c tmlcLicenseClass = [SELECT Id FROM License_Class__c];

        List<License_Policy__c> policyList = new List<License_Policy__c>();
        License_Policy__c policy1 = TestDataHelper.createLicensePolicy(tmlcLicenseClass.Id, Date.today().addYears(-1), null, false);
        policy1.Licensing_Area__c = GlobalConstant.BROADCAST_TELEVISION_LITERAL;
        policy1.License_Type__c = GlobalConstant.BLANKET_TYPE;
        policy1.Allocated_Fee_Amount__c = 1000.0;
        policy1.LNSPA_Percentage__c = 50.0;
        policyList.add(policy1);

        License_Policy__c policy2 = TestDataHelper.createLicensePolicy(tmlcLicenseClass.Id, Date.today().addYears(-1), null, false);
        policy2.Licensing_Area__c = GlobalConstant.BROADCAST_TELEVISION_LITERAL;
        policy2.License_Type__c = GlobalConstant.ALT_BLANKET_TYPE;
        policy2.Allocated_Fee_Amount__c = 1000.0;
        policy2.LNSPA_Percentage__c = 50.0;
        policyList.add(policy2);
        insert policyList;

        List<License_Policy_Time_Period__c> timePeriodList = new List<License_Policy_Time_Period__c>();
        License_Policy_Time_Period__c timePeriod11 = TestDataHelper.createLicensePolicyTimePeriod(policy1.Id, Date.today().toStartOfMonth().addDays(3),
                                                        Date.today().addMonths(1).toStartOfMonth().addDays(-3), false);
        timePeriodList.add(timePeriod11);

        License_Policy_Time_Period__c timePeriod21 = TestDataHelper.createLicensePolicyTimePeriod(policy2.Id, Date.today().toStartOfMonth(),
                                                        Date.today().addMonths(1).toStartOfMonth().addDays(-1), false);
        timePeriod21.Is_Batch_Created__c = true;
        timePeriodList.add(timePeriod21);
        insert timePeriodList;

        List<Transaction__c> tranList = new List<Transaction__c>();
        Transaction__c tran211 = TestDataHelper.createTransaction(timePeriod21.Id, 100, false);
        tran211.Is_Batch_Created__c = true;
        tranList.add(tran211);

        Transaction__c tran212 = TestDataHelper.createTransaction(timePeriod21.Id, 100,false);
        tran212.Is_Batch_Created__c = true;
        tranList.add(tran212);

        Transaction__c tran213 = TestDataHelper.createTransaction(timePeriod21.Id, 100,false);
        tran213.Is_Batch_Created__c = true;
        tranList.add(tran213);
        insert tranList;

        Test.startTest();
        Database.executeBatch(new MonthlyTmlcFeeBatch());
        Test.stopTest();

        System.assertEquals(2, [SELECT COUNT() FROM Error_Log__c]);
    }
}