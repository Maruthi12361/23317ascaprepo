/**
 * Created by Xede
 */

/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer   Description                                                             *
*   ----------  ----------- ------------------------------------------------------------------------*
*   2018-10-06  Xede  		Initial Creation                                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description Returned by the metadata web service call and provides the structure of the outbound request
* @author Xede Consulting Group
* @see LicenseClassFieldTest for test class
* @date 2018-10-06
*/
public class LicenseClassField {
    //Constructor method
    public LicenseClassField(String inApiName, String inLabel,boolean inRequired, String inType,String containerName) {
        this.name = inApiName;
        this.Label = inLabel;
        this.Required = inRequired;
        this.data_type = inType;
        this.container_name = containerName;
        
    }

    @AuraEnabled
    public Integer lowerBound{ get;set; }

    @AuraEnabled
    public Integer upperBound{ get;set; }

    @AuraEnabled
    public String guid { get;set; }

    @AuraEnabled
    public String licenseClassName { get;set; }

    @AuraEnabled
    public String list_name { get;set; }

    @AuraEnabled
    public String container_name { get;set; }
    
    @AuraEnabled
    public String name { get;set; }
    
    @AuraEnabled
    public String Label { get;set; }
    
    @AuraEnabled
    public Boolean Required { get;set; }
    
    @AuraEnabled
    public String data_type { get; set; }
    
    @AuraEnabled
    public String input_value { get; set; }
}