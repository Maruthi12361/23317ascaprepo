/*
 * Created by Xede
 */

/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer   Description                                                             *
*   ----------  ----------- ------------------------------------------------------------------------*
*   2018-10-06  Xede Consulting  Initial Creation                                                   *
*   2019-01-31  Xede Consulting  Added constants to support address standardization processes.      *
*   2019-02-08  Xede Consulting  Added constants to support the LeadConversion proess.               *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description Stores the common constants
* @author Xede Consulting Group
* @see NA for test class
* @date 2018-10-06
*/
public with sharing class GlobalConstant {

    //Object name constants
    public static final string OBJ_ACCOUNT= 'Account';
    public static final string OBJ_LEAD= 'Lead';
    public static final string OBJ_Contact = 'Contact';
    public static final string OBJ_OPPORTUNITY= 'Opportunity';
    public static final string OBJ_LICENSE_CLASS = 'License_Class__c';
    public static final string OBJ_LICENSE_POLICY = 'License_Policy__c';
    public static final string OBJ_LICENSE_POLICY_TIME_PERIOD = 'License_Policy_Time_Period__c';
    public static final string OBJ_LICENSE_POLICY_TRANSACTION = 'Transaction__c';

    //Object parent id field names
    public static final string ACCOUNT_PARENT_ID_FIELD= 'ParentId';
    public static final string ACCOUNT_ID_FIELD= 'AccountId';
    public static final string ACCOUNT_DUPLICATE_BYPASS_FIELD= 'Bypass_Duplicate_Rules__c';


    //Lead Status literals
    public static final String LEAD_STATUS_OPEN ='Open';
    public static final String LEAD_STATUS_NEW ='New';

    public static final String YES_STRING_LITERAL ='Yes';
    public static final String NO_STRING_LITERAL ='No';
    public static final String LICENSE_TYPE_LITERAL ='License_Type';
    public static final String UNDERSCORE_COUNT_LITERAL ='_count';
    public static final String UNDERSCORE_AMOUNT_LITERAL ='_Amount';
    public static final String UNDERSCORE_LIMIT_LITERAL ='_Limit';
    public static final String UNDERSCORE_TYPE_LITERAL ='_Type';
    public static final String LOWERCASE_TRUE_STRING ='true';
    public static final String LOWERCASE_FALSE_STRING ='false';
    public static final boolean BOOLEAN_FALSE = False;
    public static final boolean BOOLEAN_TRUE = True;
    public static final String CORE_REGION_LITERAL = 'Core';
    public static final String ACTIVE_LITERAL = 'Active';
    public static final String FORECAST_CLOSED_CATEGORY = 'Closed';
    public static final String FORECAST_OMITTED_CATEGORY = 'Omitted';

    //Melissa Data/Address Standardization literals
    public static final String MELISSA_DATA_ERROR_PREFIX ='AE';
    public static final String INVALID_LEAD_ADDRESS_STATUS ='Invalid';
    public static final String HTTP_GET_METHOD ='GET';
    public static final String QUEUE_NAME_STRING ='Queue';
    public static final String BILLING_ADDR_TYPE = 'Billing Address';
    public static final String BUSINESS_ADDR_TYPE = 'Business Address';
    public static final String ENTITY_ADDR_TYPE = 'Entity Address';
    public static final String PREMISE_ADDR_TYPE = 'Premise Address';
    public static final String USA_REGEX = '(?i)usa?|united\\s*states(?:\\s*of\\s*america)?';

    //Queue name literals
    public static final String QUEUE_WEBLEAD='WebLead';
    public static final String QUEUE_SELF_SERVICING='Self_Licensing_Prospects';

    //Various data type literals
    public static final String CURRENCY_DATA_TYPE='CURRENCY';
    public static final String DOUBLE_DATA_TYPE='DOUBLE';
    public static final String PERCENT_DATA_TYPE='PERCENT';
    public static final String DATE_DATA_TYPE='DATE';
    public static final String DATETIME_DATA_TYPE='DATETIME';
    public static final String BOOLEAN_DATA_TYPE='BOOLEAN';
    public static final String INTEGER_DATA_TYPE='INTEGER';
    public static final String STRING_DATA_TYPE='STRING';
    public static final String NUMBER_DATA_TYPE='NUMBER';
    public static final String PICKLIST_DATA_TYPE='PICKLIST';

    //Fee Calculation Literals
    public static final String FEE_CALC_PARAMETER_TYPE='fee_calc';

    //Frequency Literals
    public static final String YEARLY_LITERAL='yearly';
    public static final String MONTHLY_LITERAL='monthly';
    public static final String QUARTERLY_LITERAL='yearly';
    public static final String SEMI_ANNUAL_LITERAL='semi-annual';

    //Various static literals
    public static final string LICENSE_AGREEMENT_STRING = 'License Agreement';
    public static final String RATE_SCHEDULE_LITERAL ='Rate Schedule';
    public static final String PARAMETERS_LITERAL ='Parameters';
    public static final String KEY_CONTACT_LITERAL ='Key Contact';
    public static final String SYSTEM_EXCEPTION_LITERAL ='System Exception';
    public static final String USER_DEFINED_LITERAL ='User Defined';
    public static final String COMPLETE_STATUS_LITERAL ='Complete';
    public static final String PER_PROGRAM_TYPE_LITERAL ='Per-Program';
    public static final String INITIAL_REPORT_LITERAL ='Initial Report';
    public static final String SUCCESS_LITERAL ='SUCCESS';
    public static final String ERROR_LITERAL ='ERROR';

    //Guest user
    public static final string GUEST_USER = 'Guest';
    public static final String NO_LIMIT_LITERAL='No Limit';

    // License Policy Record Type Developer Names
    public static final String FIXED_TYPE = 'Fixed';
    public static final String FORM_TYPE = 'Form';
    public static final String INDUSTRY_TYPE = 'Industry';
    public static final String NEGOTIATED_TYPE = 'Negotiated';

    //License Policy TMLC
    public static final String TMLC_LICENSE_CLASS_NAME = 'TMLC Industry Agreement';
    public static final String BLANKET_TYPE = 'Blanket';
    public static final String ALT_BLANKET_TYPE = 'Alternative Blanket';
    public static final String PER_PROGRAM_TYPE ='Per-Program';
    public static final String TERMINATED_LITERAL = 'Terminated';
    public static final String BROADCAST_TELEVISION_LITERAL = 'Broadcast Television';
    public static final String PAYMENT_LITERAL = 'Payment';
    public static final String CREDIT_LITERAL = 'Credit';
    public static final String INITIAL_QUOTE_LITERAL = 'Initial Quote';
    public static final String FINAL_LITERAL = 'Final';
    public static final String BILLING_ITEM_LITERAL = 'Billing Item';
    public static final String YEARLY_ALLOCATION_LITERAL = 'Yearly Allocation';
    public static final String MINIMUM_FEE_LITERAL = 'Minimum Fee';
    public static final String SUPPLEMENTAL_FEE_LITERAL = 'Supplemental Fee';
}