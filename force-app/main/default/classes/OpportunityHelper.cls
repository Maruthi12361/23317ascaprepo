/**
 * Created by johnreedy on 2019-08-19.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  -----------------------  --------------------------------------------------------   *
*   2019-08-19   Xede Consulting Group 	 Initial Creation                                           *
*   2019-08-19   Xede Consulting Group 	 Remove threshold bands and replaced with Authorized        *
*                                        Dollar Limit.                                              *
*   2019-09-22   Xede Consulting Group   Added check to set last assignment long to 1 when null     *
*   2019-09-26   Xede Consulting Group   Added 'Core' team functionality.                           *
*                                                                                                   *
*  Code Coverage: 97.2%                                                                             *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This helper class supports operations against the Opportunity object.
* @author Xede Consulting Group
* @see OpportunityHelperTest
* @date 2019-08-19 
*/
public with sharing class OpportunityHelper {
    Public Static Boolean ownerAssignmentRan = False;

    /**
     * @description This method assigns opportunities (converted by a lead specialist) to a Lead Specialist based on the
     * 1) region, 2) value of the Opportunity, 3) License Class, and 4) Last Updated DateTime (long).
     * @param opptyList List of opportunities
     * @param oldMap Map of previous values of Opportunities passed in via trigger
     */
    public static void assignOpportunityOwner(list<Opportunity> opptyList, map<id,Opportunity> oldMap) {

        //build list of related Converted Leads
        set<string> validConvertedProfiles = new set<string>{'Lead Identification Specialist'};
        List<Lead> convertedLeadList = [
                SELECT ConvertedOpportunityId, Owner.Profile.Name, License_Class_Relationship__r.Name, Estimated_Value__c, Region__c
                  FROM Lead
                 WHERE ConvertedOpportunityId in:opptyList
                   AND Owner.Profile.Name =: label.Lead_Identification_Specialist_Profile_Name];

        //create map of OpportunityIds and OwnerProfile from related leads
        Map<string,string> opportunityProfileMap = new Map<string,string>();
        for (Lead each: convertedLeadList){
            opportunityProfileMap.put(each.ConvertedOpportunityId, each.Owner.Profile.Name);

            //write lead info to debug log to facilitate researching assignment questions
            system.debug('Debug:OpportunityHelper.assignOpportunityOwner Lead License Class>' + each.License_Class_Relationship__r.Name);
            system.debug('\nDebug:OpportunityHelper.assignOpportunityOwner Lead Region>' + each.Region__c);
            system.debug('\nDebug:OpportunityHelper.assignOpportunityOwner Lead Amount>' + each.Estimated_Value__c);
        }

        /*
        Iterate over list of Opportunities passed-in and add them to list to process if they are a new Opportunity and the region
        is not blank, or they are an updated opportunity where the region has changed
        */
        list<Opportunity> opptyAssignmentList = new list<Opportunity>();
        for (Opportunity eachOppty: opptyList){
            if ((oldMap == null && string.isNotBlank(eachOppty.Region__c))
                    || (string.isNotBlank(eachOppty.Region__c) && eachOppty.Amount != null && eachOppty.Amount != oldMap.get(eachOppty.Id).Amount)){
                if (opportunityProfileMap.containsKey(eachOppty.Id) && validConvertedProfiles.contains(opportunityProfileMap.get(eachOppty.Id))) {
                    opptyAssignmentList.add(eachOppty);
                }
            }
        }
        //if there are no Opportunities to process, then return
        if (opptyAssignmentList.size() == 0){
            return;
        }
        //set ownerAssignmentRan to avoid possible recursive trigger
        ownerAssignmentRan = true;

        //get list of team leads and build map of regions with team lead id
        list<User> teamLeadList = [
                SELECT Id, Name, UserRoleId, UserRole.DeveloperName
                  FROM User
                 WHERE UserRole.DeveloperName
                  LIKE '%Team_Lead'
                   AND IsActive = True];
        Map<String,String> regionteamLeadMap = new Map<String,String>();

        for (User each: teamLeadList){
            string region = each.UserRole.DeveloperName.replaceAll('_Team_Lead','');
            regionTeamLeadMap.put(region,each.Id);
        }
        system.debug('\nDebug:OpportunityHelper.assignOpportunityOwner Region Team Lead>' + regionTeamLeadMap);

        //get list of all Opportunity Assignment Matrix records
        List<Opportunity_Assignment_Matrix__c> opptyAssignMatrixList = [
                SELECT Assignment_Region__c, Authorized_Dollar_Limit__c, Certified__c, License_Specialist__c, Status__c
                     , Average_Open_Opportunity_Days__c, Active_Opportunity_Count__c, Last_Assigned_Datetime_long__c
                  FROM Opportunity_Assignment_Matrix__c
                 ORDER BY Assignment_Region__c, Authorized_Dollar_Limit__c, License_Specialist__c, Last_Assigned_Datetime_long__c DESC];

        //build set of specialistIds
        set<id> certifiedSpecialistIds = new set<id>();
        for (Opportunity_Assignment_Matrix__c eachSpecialist: opptyAssignMatrixList){
            certifiedSpecialistIds.add(eachSpecialist.License_Specialist__c);
        }

        //get opportunity count and average days open by license specialist
        map<id,AggregateResult> arOpportunityCounts = new map<id,AggregateResult>([
                SELECT OwnerId Id, Count(Id) OpportunityCount, AVG(Days_Open__c) AverageDaysOpen
                  FROM Opportunity
                 WHERE OwnerId in: certifiedSpecialistIds
                   AND ForecastCategory !=: GlobalConstant.FORECAST_CLOSED_CATEGORY
                 GROUP BY OwnerId]);

        //assign current average days open and oppty count
        for (Opportunity_Assignment_Matrix__c each: opptyAssignMatrixList){
            //if Aggregate Result contains the specialist, set the Average Open days, else set to zero
            if (arOpportunityCounts.containsKey(each.License_Specialist__c) ) {
                each.Average_Open_Opportunity_Days__c = (decimal)arOpportunityCounts.get(each.License_Specialist__c).get('AverageDaysOpen');
            }
            else{
                each.Average_Open_Opportunity_Days__c = 0;
            }

            //if Aggregate Result contains the specialist, set the Opportunity Cpunt, else set to zero
            if (arOpportunityCounts.containsKey(each.License_Specialist__c) && (decimal)arOpportunityCounts.get(each.License_Specialist__c).get('OpportunityCount') != null) {
                each.Active_Opportunity_Count__c = (decimal)arOpportunityCounts.get(each.License_Specialist__c).get('OpportunityCount');
            }
            else{
                each.Active_Opportunity_Count__c = 0;
            }
        }
        //build map of license class id to short name
        map<id,License_Class__c> licenseClassMap = new map<id,License_Class__c>([
                SELECT id, License_Class_Short_Name__c
                  FROM License_Class__c
                 WHERE Active__c = True]);

        /*
        Iterate over list of Opportunities eligible for assignment
         */
        list<Opportunity> updateOpportunityList = new list<Opportunity>();
        for (Opportunity eachOppty: opptyAssignmentList){

            //build region assignment list
            list<Opportunity_Assignment_Matrix__c> regionAssignmentList = new list<Opportunity_Assignment_Matrix__c>();
            for (Opportunity_Assignment_Matrix__c eachAssignment: opptyAssignMatrixList) {
                if (eachAssignment.Assignment_Region__c == eachOppty.Region__c || eachAssignment.Assignment_Region__c == GlobalConstant.CORE_REGION_LITERAL) {
                    if (eachAssignment.Last_Assigned_Datetime_long__c == null) {
                        eachAssignment.Last_Assigned_Datetime_long__c = 1;
                    }
                    regionAssignmentList.add(eachAssignment);
                }
            }
            system.debug('Debug:OpportunityHelper.assignOpportunityOwner - Regional List of Eligible Specialist (including core)>' + regionAssignmentList);

            //narrow list to those where the Opportunity Amount is below their Authorized Dollar Limit
            list<Opportunity_Assignment_Matrix__c> thresholdAssignmentList = new list<Opportunity_Assignment_Matrix__c>();
            for (Opportunity_Assignment_Matrix__c eachRegion: regionAssignmentList){
                if (string.isNotBlank(eachRegion.Authorized_Dollar_Limit__c) && (eachRegion.Authorized_Dollar_Limit__c.toLowerCase() == GlobalConstant.NO_LIMIT_LITERAL.toLowerCase() || eachOppty.Amount < decimal.valueOf(eachRegion.Authorized_Dollar_Limit__c.replace('$','')))){
                    thresholdAssignmentList.add(eachRegion);
                }
            }
            system.debug('Debug:OpportunityHelper.assignOpportunityOwner - Threshold List of Eligible Specialist>' + thresholdAssignmentList);

            string[] classesRequiringCertList = label.Requires_Certified_Resource.split(';');

            //narrow the list to those who are certified for the opportunity's License Class
            list<Opportunity_Assignment_Matrix__c> licenseAssignmentList = new list<Opportunity_Assignment_Matrix__c>();
            //check for null license class id
            for (Opportunity_Assignment_Matrix__c eachThresh: thresholdAssignmentList){
                //if the Opportunities License Class DOES NOT require a certified resource, add the resource to the list, otherwise add only those certified
                if (classesRequiringCertList.contains(licenseClassMap.get(eachOppty.License_Class_Relationship__c).License_Class_Short_Name__c) == False) {
                    licenseAssignmentList.add(eachThresh);
                }
                else {
                    if (string.isNotBlank(eachThresh.Certified__c) && eachThresh.Certified__c.contains(licenseClassMap.get(eachOppty.License_Class_Relationship__c).License_Class_Short_Name__c)){
                        licenseAssignmentList.add(eachThresh);
                    }
                }
            }
            system.debug('Debug:OpportunityHelper.assignOpportunityOwner - License Class List of Eligible Specialist>' + licenseAssignmentList);

            //Apply exclusion filters
            list<Opportunity_Assignment_Matrix__c> availableSpecialistList = new list<Opportunity_Assignment_Matrix__c>();
            for (Opportunity_Assignment_Matrix__c eachLC: licenseAssignmentList){
                //add to availableSpecialistList if the specialist has less than 20 open Opportunities, is Active, and whose Average Open Day count is less than 50
                if (eachLC.Average_Open_Opportunity_Days__c <= integer.valueOf(label.Open_Opportunity_Avg_Days_Max)
                        && eachLC.Active_Opportunity_Count__c <= integer.valueOf(label.Open_Opportunity_Count) && eachLC.Status__c == GlobalConstant.ACTIVE_LITERAL ){
                    availableSpecialistList.add(eachLC);
                }
            }
            system.debug('Debug:OpportunityHelper.assignOpportunityOwner - Available List of Eligible Specialist>' + availableSpecialistList);

            //sort list by last update datetime long
            List<licenseSpecialistSort> specialistSortList = new list<licenseSpecialistSort>();
            if (availableSpecialistList.size() > 0) {
                for (Opportunity_Assignment_Matrix__c each : availableSpecialistList) {
                    licenseSpecialistSort newlss = new licenseSpecialistSort(each.License_Specialist__c, each.Last_Assigned_Datetime_long__c);
                    specialistSortList.add(newlss);
                }
                specialistSortList.sort();

                Opportunity updatedOpportunity = new Opportunity();
                updatedOpportunity.id = eachOppty.Id;
                updatedOpportunity.OwnerId = (id) specialistSortList[0].licenseSpecialist;
                updateOpportunityList.add(updatedOpportunity);

                for (Opportunity_Assignment_Matrix__c each : opptyAssignMatrixList) {
                    if (each.License_Specialist__c == specialistSortList[0].licenseSpecialist) {
                        DateTime dt = DateTime.now();
                        Long currDateTimeLong = dt.getTime();
                        each.Last_Assigned_Datetime_long__c = currDateTimeLong;
                        each.Active_Opportunity_Count__c = each.Active_Opportunity_Count__c == null ? 1 : each.Active_Opportunity_Count__c + 1;
                        each.Last_Assigned_Datetime__c = datetime.Now();
                    }
                }
                Database.SaveResult[] saveResultList = Database.update(opptyAssignMatrixList, false);
                SaveResultsHelper saveResults = new SaveResultsHelper(saveResultList);
            }
            else{
                //If no eligible specialist found, assign opportunity to team lead
                system.debug('Debug:OpportunityHelper.assignOpportunityOwner - No Eligible Specialist Found, Assign to Team Lead -' + regionTeamLeadMap.get(eachOppty.Region__c));
                Opportunity updatedOpportunity = new Opportunity();
                updatedOpportunity.id = eachOppty.Id;
                updatedOpportunity.OwnerId = regionTeamLeadMap.get(eachOppty.Region__c);
                updateOpportunityList.add(updatedOpportunity);
            }
        }
        Database.SaveResult[] saveResultList = Database.update(updateOpportunityList, false);
        SaveResultsHelper saveResults = new SaveResultsHelper(saveResultList);
    }
    public static void recalcOpportunityCounts(list<Opportunity> opptyList, map<id,Opportunity> oldMap) {

        list<Opportunity> opptysToProcess = new List<Opportunity>();
        for (Opportunity eachOppty: opptyList){
            if (oldMap == null || ((eachOppty.IsClosed && !oldMap.get(eachOppty.Id).isClosed) || eachOppty.OwnerId != oldMap.get(eachOppty.Id).OwnerId)){
                opptysToProcess.add(eachOppty);
            }
        }
        if (opptysToProcess.size() == 0){
            return;
        }
        //get list of all Opportunity Assignment Matrix records
        List<Opportunity_Assignment_Matrix__c> opptyAssignMatrixList = [
                SELECT License_Specialist__c, Status__c, Average_Open_Opportunity_Days__c, Active_Opportunity_Count__c
                  FROM Opportunity_Assignment_Matrix__c];


        set<id> certifiedSpecialistIds = new set<id>();
        for (Opportunity_Assignment_Matrix__c each: opptyAssignMatrixList){
            certifiedSpecialistIds.add(each.License_Specialist__c);
        }

        //get opportunity count and average days open by license specialist
        map<id,AggregateResult> arOpportunityCounts = new map<id,AggregateResult>([
                SELECT OwnerId Id, Count(Id) OpportunityCount, AVG(Days_Open__c) AverageDaysOpen
                  FROM Opportunity
                 WHERE OwnerId in: certifiedSpecialistIds
                   AND isClosed = FALSE
                 GROUP BY OwnerId]);

        for (Opportunity_Assignment_Matrix__c each: opptyAssignMatrixList){
            if (arOpportunityCounts.containsKey(each.License_Specialist__c)) {
                each.Average_Open_Opportunity_Days__c = (decimal) arOpportunityCounts.get(each.License_Specialist__c).get('AverageDaysOpen');
                each.Active_Opportunity_Count__c = (decimal)arOpportunityCounts.get(each.License_Specialist__c).get('OpportunityCount');
            }
            else{
                each.Average_Open_Opportunity_Days__c = 0;
                each.Average_Open_Opportunity_Days__c = 0;
            }
        }
        Database.SaveResult[] saveResultList = Database.update(opptyAssignMatrixList, false);
        SaveResultsHelper saveResults = new SaveResultsHelper(saveResultList);

    }
    /**
     * @description This wrapper class implements comparable and sorts the list of License Specialists passed in.
     * @return Returns a list of sorted License Specialist based on Last Update Datetime (long)
     */
    public class licenseSpecialistSort implements Comparable {
        public String licenseSpecialist {get;set;}
        public decimal lastAssignment {get; set;}

        public licenseSpecialistSort(String licenseSpecialistId, decimal lastAssignmentDateLong) {
            licenseSpecialist = licenseSpecialistId;
            lastAssignment = lastAssignmentDateLong;
        }

        public Integer compareTo(Object ObjToCompare) {
            licenseSpecialistSort compareTo = (licenseSpecialistSort)ObjToCompare;

            if (lastAssignment == compareTo.lastAssignment) return 0;
            if (lastAssignment > compareTo.lastAssignment) return 1;
            return -1;
        }
    }

}