/**
 * Created by Anand on 2019-09-24.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-09-24  Xede Consulting Group       Initial Creation                                        *

*****************************************************************************************************
*/
/**
* @description This class contains the callout methods to the Docusign service for self-licensing.
* @author Xede Consulting Group
* @see DocusignWebServiceCalloutTest
* @date 2019-09-24 
*/
public class DocusignWebServiceCallout {

}