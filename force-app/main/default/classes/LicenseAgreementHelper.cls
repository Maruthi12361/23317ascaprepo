/**
 * Created by johnreedy on 2019-07-02.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-07-02   Xede Consulting Group 		Initial Creation                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This helper class support operations related to the License Agreement generation.
* @author Xede Consulting Group
* @see LicenseAgreementHelperTest
* @date 2019-07-02 
*/
public with sharing class LicenseAgreementHelper {
    public static boolean FirstTime = true;

    /**
     * @description This method polls for a new content version record related to the license policy that is passed in.
     * @param licensePolicyList
     * @param oldMap
    */
    public static void pollForLicensePolicy(List<License_Policy__c> licensePolicyList, map<id,License_Policy__c> oldMap){
        if (licensePolicyList.size() > 1){
            return;
        }
        else{
            if (licensePolicyList[0].Self_Service_DDP_Finish_Date_Time__c <> oldMap.get(licensePolicyList[0].id).Self_Service_DDP_Finish_Date_Time__c) {
                //delete previous versions
                deleteExistingLicenseAgreements(licensePolicyList[0].id);


                system.debug('INFO: Job Initial Queued> ' + datetime.now());

                Id jobID = System.enqueueJob(new LicenseAgreementPoller(licensePolicyList[0].id));

                system.debug('INFO: Initial Job Id> ' + jobID);
            }
        }
    }

    /**
     * @description This method creates the Content Distribution record that makes the file public.
     * @param licensePolicyId
    */
    public static void makeLicenseAgreementPublic(string licensePolicyId, DateTime documentFinishDateTime){

        //get ContentDocumentLink using licensePolicyId
        list<ContentDocumentLink> contentLinkList = [
            SELECT Id, ContentDocumentId, LinkedEntityId
              FROM ContentDocumentLink
             WHERE LinkedEntityId = :LicensePolicyId
               AND SystemModstamp >: documentFinishDateTime];

        //get the latest content version using contentDocumentId
        list<ContentVersion> contentVersionList = [
            SELECT id, ContentDocumentId, IsLatest, SystemModstamp
              FROM ContentVersion
             WHERE ContentDocumentId = :contentLinkList[0].ContentDocumentId
               AND IsLatest = True];

        //create ContentDistribution record
        system.debug('INFO: Creating Content Distribution for License Policy> ' + licensePolicyId);

        if (contentVersionList.size() > 0) {
            ContentDistribution newDistribution = new ContentDistribution();
                newDistribution.ContentVersionId = contentVersionList[0].id;
                newDistribution.Name = 'License Agreement Documentation';
                newDistribution.PreferencesAllowOriginalDownload = False;
                newDistribution.PreferencesAllowPDFDownload = True;
                newDistribution.PreferencesAllowViewInBrowser = True;
                newDistribution.PreferencesLinkLatestVersion = True;
                newDistribution.RelatedRecordId = licensePolicyId;

            try{
                insert newDistribution;
            }
            catch(Exception ex){
                system.debug('ERROR: An Error occurred while creating the new ContentDistribution> ' + ex.getMessage());
            }

            ContentDistribution publicLink = [SELECT Id, DistributionPublicUrl FROM ContentDistribution WHERE Id = :newDistribution.Id limit 1];
            system.debug('INFO: PublicLink>' + publicLink);
            //update policy with this link
            system.debug('INFO: Making Content Document Publicly Available for License Policy> ' + licensePolicyId);

            //update license policy with new public link
            License_Policy__c updLicnesPolicy = new License_Policy__c();
            updLicnesPolicy.id = licensePolicyId;
            updLicnesPolicy.Self_Service_License_Agreement_Link__c = publicLink.DistributionPublicUrl;

            try{
                update updLicnesPolicy;
            }
            catch(Exception ex){
                system.debug('ERROR: An Error occurred while updating the License Policy\'s new Self_Service_License_Agreement_Link__c> ' + ex.getMessage());
            }

            system.debug('INFO: License Agreement Available DateTime> ' + datetime.now());
        }
    }
    /**
     * @description This method performs cleanup functions, it deletes any existing Licnese Agreement files, and initializes
     * the Self_Service_License_Agreement_Link__c
     * @param licensePolicyId
    */
    public static void deleteExistingLicenseAgreements(string licensePolicyId){
        //get content links then related content versions, can use subquery?
        list<ContentDocumentLink> documentLinksList = [
            SELECT Id, ContentDocumentId, LinkedEntityId
              FROM ContentDocumentLink
             WHERE LinkedEntityId = :licensePolicyId];

        set<id> licenseAgreementIdSet = new set<id>();
        for (ContentDocumentLink each: documentLinksList) {
            licenseAgreementIdSet.add(each.ContentDocumentId);
        }
        list<ContentDocument> delContentDocumentList = new list<ContentDocument> ();

        for (ContentDocument each: [Select id, Title from ContentDocument where id in: licenseAgreementIdSet]){
            if (each.Title.contains(GlobalConstant.LICENSE_AGREEMENT_STRING)){
                delContentDocumentList.add(new ContentDocument(Id = each.Id));
            }
        }

        //delete list of old License Agreements
        try{
            delete delContentDocumentList;
        }
        catch(Exception ex){
            system.debug('ERROR: An Error occurred while deleting list of Old License Agreements> ' + ex.getMessage());
        }


        License_Policy__c updLicensePolicy = new License_Policy__c();
        updLicensePolicy.id = licensePolicyId;
        updLicensePolicy.Self_Service_License_Agreement_Link__c = '';

        try{
            update updLicensePolicy;
        }
        catch(Exception ex){
            system.debug('ERROR: An Error occurred while updating the License Policy\'s  Self_Service_License_Agreement_Link__c> ' + ex.getMessage());
        }

        system.debug('INFO: Old License Agreement Files Deleted for License Policy> ' + licensePolicyId);
    }
}