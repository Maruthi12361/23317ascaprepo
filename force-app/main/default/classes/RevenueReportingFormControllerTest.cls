/**
 * Created by johnreedy on 2019-12-16.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  ------------------- ----------------------------------------------------------------*
*   2019-12-16  Xede Consulting     Initial Creation                                                *
*   2020-01-07  Xede Consulting     Added Start Date as argument to sendForm method call.           *
*   2020-01-17  Xede Consulting     Added a License Class Field Info record with a deduction        *
*                                   Dependent field.                                                *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This test class exercises the methods in the RevenueReportingFormController class.
* @author Xede Consulting Group
* @date 2019-12-16 
*/
@IsTest
private class RevenueReportingFormControllerTest {
    static Account accountObj;
    static License_Policy__c policyObj;
    static License_Class__c licenseClassObj;
    static License_Class_Field_Info__c licenseClassFieldInfo1Obj;
    static License_Class_Field_Info__c licenseClassFieldInfo2Obj;
    static License_Class_Field_Info__c licenseClassFieldInfo3Obj;
    static License_Class_Field_Info__c licenseClassFieldInfo4Obj;
    static License_Class_Response_Info__c licenseClassResponseInfo1Obj;
    static License_Class_Response_Info__c licenseClassResponseInfo2Obj;
    static License_Class_Response_Info__c licenseClassResponseInfo3Obj;
    static List<License_Class_Response_Info__c> licenseClassResponseList = new List<License_Class_Response_Info__c>();
    static string feeRequest = '';
    static string feeResponse = '';
    static string timePeriod = '';
    static string fieldMetadataString;

    /**
    * @ description This method creates the test data used to exercise the controller's methods.
    */
    static void init() {
        TestDataHelper.createAscapGlobalProperties(false, 0);

        //create an Account
        accountObj = TestDataHelper.createAccount('Television Network', 'Broadcast Television', true);

        //create a license class
        licenseClassObj = TestDataHelper.createLicenseClass('Revenue Reporting', GlobalConstant.FORM_TYPE, false);
        licenseClassObj.DSLLicenseTypeName__c = 'Revenue_Reporting_License_Agreement';
        insert licenseClassObj;

        //create a License Policy
        Date policyStartDate = System.Today().toStartOfMonth();
        Date policyEndDate = policyStartDate.addYears(1).addDays(-1);
        policyObj = TestDataHelper.createLicensePolicy(licenseClassObj.Id, policyStartDate, policyEndDate, false);
        policyObj.Revenue_Reported__c    = 'Advertising Revenue';
        policyObj.Deductions_Reported__c = 'Advertising Agency Commissions';
        policyObj.Status__c              = GlobalConstant.ACTIVE_LITERAL;
        policyObj.End_Date__c            = System.Today().addYears(1);
        policyObj.Content_Type_s__c      = 'Sports';
        insert policyObj;

        //create License Class Field Info fields
        licenseClassFieldInfo1Obj = new License_Class_Field_Info__c(
                License_Class__c = licenseClassObj.id
                , Internal_Field_Label__c = 'Advertising Revenue'
                , DSL_Parameter_Name__c = 'Advertising_Revenue_Amount'
                , Field_Value_Type__c = 'Number'
                , Deduction_Maximum_Percentage__c = 0.0
                , Order__c = 1
                , Help_Text__c = ''
        );
        insert licenseClassFieldInfo1Obj;

        licenseClassFieldInfo2Obj = new License_Class_Field_Info__c(
                License_Class__c = licenseClassObj.id
                , Internal_Field_Label__c = 'Advertising Agency Commissions'
                , DSL_Parameter_Name__c = 'Advertising_Commission_Amount'
                , Field_Value_Type__c = 'Number'
                , Deduction_Maximum_Percentage__c = 0.0
                , Order__c = 2
                , Help_Text__c = ''
        );
        insert licenseClassFieldInfo2Obj;

        licenseClassFieldInfo3Obj = new License_Class_Field_Info__c(
                License_Class__c = licenseClassObj.id
                , Internal_Field_Label__c = 'Content Type'
                , DSL_Parameter_Name__c = 'Predominant_Programming_Type'
                , Field_Value_Type__c = 'Picklist'
                , Deduction_Maximum_Percentage__c = 0.0
                , Order__c = 0
                , Help_Text__c = 'Content Type'
        );
        insert licenseClassFieldInfo3Obj;

        licenseClassFieldInfo4Obj = new License_Class_Field_Info__c(
                License_Class__c = licenseClassObj.id
                , Internal_Field_Label__c = 'Bad Debt (not to exceed 1%  of Gross Revenue)'
                , DSL_Parameter_Name__c = 'Bad_Debt_Deduction_Amount'
                , Field_Value_Type__c = 'Number'
                , Deduction_Maximum_Percentage__c = 1.0
                , Order__c = 10
                , Help_Text__c = 'Bad Debt'
                , Deduction_Field_Dependency__c = 'Total Gross Revenue'
        );
        insert licenseClassFieldInfo4Obj;

        //create the License Class Response Field Info fields
        licenseClassResponseInfo1Obj = new License_Class_Response_Info__c(License_Class__c = licenseClassObj.Id
                , Printable_Name__c = 'Total Gross Revenue Amount', Response_Field_Name__c = 'Total_Gross_Revenue_Amount');
        licenseClassResponseList.add(licenseClassResponseInfo1Obj);

        licenseClassResponseInfo2Obj = new License_Class_Response_Info__c(License_Class__c = licenseClassObj.Id
                , Printable_Name__c = 'Total Deduction Amount', Response_Field_Name__c = 'Total_Deduction_Amount');
        licenseClassResponseList.add(licenseClassResponseInfo2Obj);

        licenseClassResponseInfo3Obj = new License_Class_Response_Info__c(License_Class__c = licenseClassObj.Id
                , Printable_Name__c = 'Net Revenue Amount', Response_Field_Name__c = 'Net_Revenue_Amount');
        licenseClassResponseList.add(licenseClassResponseInfo3Obj);

        insert licenseClassResponseList;

        //create a test fee request string
        feeRequest = '{'
                + '"License_Policy":{'
                + '"Id": "a051g000001DJP4AAO",'
                + '"Name": "Revenue Report",'
                + '"License_Type": "Revenue_Reporting_License_Agreement",'
                + '"Predominant_Programming_Type": "Sports",'
                + '"Advertising_Revenue_Amount": 100000,'
                + '"Advertising_Agency_Commissions_Deduction_Amount": 50000'
                + '},'
                + '"Time_Period": {'
                + '"type": "Annual",'
                + '    "period": "2019-11-01"'
                + '}'
                + '}';

        //create a test time period string
        timePeriod = '{"attributes":{"type":"License_Policy_Time_Period__c"},"Start_Date__c":"2019-11-01","End_Date__c":"2019-11-30"}';

        //test fee response string
        feeResponse = '{'
                + '"lfaftp": 385.0,'
                + '"lfaftp_prorated": 0.0,'
                + '"fee_detail_dict": {'
                + '"Total_Gross_Revenue_Amount": 300000,'
                + '"Total_Deduction_Amount": 20000.0,'
                + '"Net_Revenue_Amount": 280000.0,'
                + '"Warning_Message": ""'
                + '},'
                + '"other_related_info_dict": {'
                + '"lfaftp_not_subject_to_cpi_adjustment": 0.0,'
                + '"admin_fee_list": [],'
                + '"fees_subject_to_proration": 0.0,'
                + '"prorated_fees": 0.0,'
                + '"minimum_annual_fee_amount": 0.0,'
                + '"maximum_annual_fee_amount": null'
                + '}'
                + '}';

        //test field metadata string
        fieldMetadataString = '['
                + '{'
                + '"max_discount": 0,'
                + '"fieldValue": "100000",'
                + '"field_type": "Number",'
                + '"field_order": 10,'
                + '"field_label": "Advertising Revenue",'
                + '"field_help_text": "Total Advertising Revenue",'
                + '"field_api_name": "Advertising_Revenue_Amount"'
                + '},'
                + '{'
                + '"selectedValue": "Sports",'
                + '"possible_values": ['
                + '"Sports"'
                + '],'
                + '"field_type": "Picklist",'
                + '"field_order": 0,'
                + '"field_label": "Content Type",'
                + '"field_help_text": "Predominant Programming Type",'
                + '"field_api_name": "Predominant_Programming_Type"'
                + '},'
                + '{'
                + '"max_discount": 15,'
                + '"fieldValue": "0100",'
                + '"field_type": "Number",'
                + '"field_order": 20,'
                + '"field_label": "Advertising Agency Commissions (not to exceed 15% of Advertising Revenue)",'
                + '"field_help_text": "Advertising Agency Commissions (not to exceed 15% of Advertising Revenue)",'
                + '"field_api_name": "Advertising_Agency_Commissions_Amount"'
                + '}'
                + ']';

    }
    /**
    * @description This test class method exercises the various methods in the RevenueReportingController
    */
    static testMethod void testRevenueReportingController() {
        init();

        //get the license class configuration (fields to be placed dynamically on form)
        RevenueReportingFormController.getLicenseClassConfiguration(policyObj.id);

        //Submitting the fee request
        RevenueReportingFormController.sendForm(policyObj.id, policyObj.Name, licenseClassObj.Id
                , 'Revenue_Reporting_License_Agreement', fieldMetadataString, timePeriod, System.Today().addYears(1), System.Today().addYears(-1));

        //Save the fee response to the time period and transaction objects
        RevenueReportingFormController.saveReport(policyObj.id, '1000.00', feeRequest, feeResponse, timePeriod);

        //update an existing time period and create a new transaction
        RevenueReportingFormController.saveReport(policyObj.id, '1200.00', feeRequest, feeResponse, timePeriod);

    }

    /**
    * @description This test class method verifies the various exceptions in the Sendform method.
    */
    static testMethod void testSendFormExceptions() {
        init();

        /*
        Given a start date that is not the first of the month, verify exception is thrown indicating start date
        must be first of a month.
        */
        timePeriod = '{"attributes":{"type":"License_Policy_Time_Period__c"},"Start_Date__c":"2019-11-06","End_Date__c":"2019-11-30"}';

        try {
            RevenueReportingFormController.sendForm(policyObj.id, policyObj.Name, licenseClassObj.Id
                    , 'Revenue_Reporting_License_Agreement', fieldMetadataString, timePeriod, System.Today().addYears(1), System.Today().addYears(-1));
        } catch (Exception ex) {
            System.assert(true, ex.getMessage().contains('Start Date must be the first of the month'));
        }

        /*
        Given an End date that is not the last of the month, verify exception is thrown indicating end date
        must be last of a month.
        */
        timePeriod = '{"attributes":{"type":"License_Policy_Time_Period__c"},"Start_Date__c":"2019-11-01","End_Date__c":"2019-11-26"}';

        try {
            RevenueReportingFormController.sendForm(policyObj.id, policyObj.Name, licenseClassObj.Id
                    , 'Revenue_Reporting_License_Agreement', fieldMetadataString, timePeriod, System.Today().addYears(1), System.Today().addYears(-1));
        } catch (Exception ex) {
            System.assert(true, ex.getMessage().contains('End Date must be the last day of the Reporting Period'));
        }

        /*
        Given an End date that is before the start date, verify exception is thrown indicating end date must be greater
        than the start date.
        */
        timePeriod = '{"attributes":{"type":"License_Policy_Time_Period__c"},"Start_Date__c":"2019-11-01","End_Date__c":"2019-10-31"}';

        try {
            RevenueReportingFormController.sendForm(policyObj.id, policyObj.Name, licenseClassObj.Id
                    , 'Revenue_Reporting_License_Agreement', fieldMetadataString, timePeriod, System.Today().addYears(1), System.Today().addYears(-1));
        } catch (Exception ex) {
            System.assert(true, ex.getMessage().contains('End Date must be greater than the Reporting period\'s Start Date'));
        }

        /*
        Given a submission where the revenue is zero, throw an error.
        */
        fieldMetadataString = fieldMetadataString.replace('"fieldValue": "100000",', '"fieldValue": "0",');
        feeRequest = feeRequest.replace('100000','0');
        timePeriod = '{"attributes":{"type":"License_Policy_Time_Period__c"},"Start_Date__c":"2019-11-01","End_Date__c":"2019-11-30"}';

        try {
            RevenueReportingFormController.sendForm(policyObj.id, policyObj.Name, licenseClassObj.Id
                    , 'Revenue_Reporting_License_Agreement', fieldMetadataString, timePeriod, System.Today().addYears(1), System.Today().addYears(-1));
        } catch (Exception ex) {
            System.assert(true, ex.getMessage().contains('Total Revenue reported must be greater than $0'));
        }

        //Save the fee response to the time period and transaction objects
        try {
            RevenueReportingFormController.saveReport(policyObj.id, '1000.00', feeRequest, feeResponse, timePeriod);
        } catch (Exception ex) {
            System.assert(true, ex.getMessage().contains('Start Date must be the first of the month'));
        }

        //given an End Date that exceeds the policy end date, throw an error
        timePeriod = '{"attributes":{"type":"License_Policy_Time_Period__c"},"Start_Date__c":"2019-11-01","End_Date__c":"2045-11-30"}';
        system.debug('Debug-jwr: timePeriod>' + timePeriod);
        try {
            RevenueReportingFormController.sendForm(policyObj.id, policyObj.Name, licenseClassObj.Id
                    , 'Revenue_Reporting_License_Agreement', fieldMetadataString, timePeriod, System.Today().addYears(1), System.Today().addYears(-1));
        } catch (Exception ex) {
            System.assert(true, ex.getMessage().contains('The Reporting period End Date exceeds Policy\'s End Date.'));
        }

        /*
        Given an Start Date that is greater than the End Date, verify exception is thrown indicating sttart date
        must be less than end date.
        */
        timePeriod = '{"attributes":{"type":"License_Policy_Time_Period__c"},"Start_Date__c":"2020-11-01","End_Date__c":"2019-11-30"}';

        try {
            RevenueReportingFormController.sendForm(policyObj.id, policyObj.Name, licenseClassObj.Id
                    , 'Revenue_Reporting_License_Agreement', fieldMetadataString, timePeriod, System.Today().addYears(1), System.Today().addYears(-1));
        }
        catch (Exception ex) {
            System.assert(true, ex.getMessage().contains('The Reporting period Start Date exceeds the End Date.'));
        }

        /*
        Given an Start Date that is greater than the Policy End Date, verify exception is thrown indicating start date
        must be less than policy end date.
        */
        timePeriod = '{"attributes":{"type":"License_Policy_Time_Period__c"},"Start_Date__c":"2045-11-01","End_Date__c":"2019-11-30"}';

        try {
            RevenueReportingFormController.sendForm(policyObj.id, policyObj.Name, licenseClassObj.Id
                    , 'Revenue_Reporting_License_Agreement', fieldMetadataString, timePeriod, System.Today().addYears(1), System.Today().addYears(-1));
        }
        catch (Exception ex) {
            System.assert(true, ex.getMessage().contains('The Reporting period Start Date exceeds Policy\'s End Date.'));
        }

        /*
        Given an Start Date that precedes Policy Start Date, verify exception is thrown indicating start date
        must be greater than the policy Start Date.
        */
        timePeriod = '{"attributes":{"type":"License_Policy_Time_Period__c"},"Start_Date__c":"2012-11-01","End_Date__c":"2019-11-30"}';

        try {
            RevenueReportingFormController.sendForm(policyObj.id, policyObj.Name, licenseClassObj.Id
                    , 'Revenue_Reporting_License_Agreement', fieldMetadataString, timePeriod, System.Today().addYears(1), System.Today().addYears(-1));
        }
        catch (Exception ex) {
            System.assert(true, ex.getMessage().contains('The Reporting period Start Date precedes Policy\'s Start Date'));
        }

    }

    /**
    * @description This test class method verifies the various exceptions in the getLicenseClassConfiguration method.
    */
    static testMethod void testConfigurationExceptions() {
        init();

        policyObj.Status__c = GlobalConstant.TERMINATED_LITERAL;
        update policyObj;
        test.startTest();
            //Save the fee response to the time period and transaction objects
            try {
                RevenueReportingFormController.getLicenseClassConfiguration(policyObj.id);
            } catch (Exception ex) {
                System.assert(true, ex.getMessage().contains('This license policy is not in an \'Active\' status'));
            }
        test.stopTest();
    }
    /**
    * @description This test class method verifies the various exceptions in the SaveReport method.
    */
    static testMethod void testSaveReportExceptions() {
        init();

        RevenueReportingFormController.saveReport(policyObj.id, '1000.00', feeRequest, feeResponse, timePeriod);
        RevenueReportingFormController.saveReport(policyObj.id, '900.00', feeRequest, feeResponse, timePeriod);

        //Save the fee response to the time period and transaction objects
        try {
            RevenueReportingFormController.saveReport(policyObj.id, '1100.00', feeRequest, feeResponse, timePeriod);
        } catch (Exception ex) {
            System.assert(true, ex.getMessage().contains('Limit of 2 revisions per reporting period'));
        }
    }
}