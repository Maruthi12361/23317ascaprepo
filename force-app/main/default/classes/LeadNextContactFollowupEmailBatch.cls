/**
 * Created by johnreedy on 2019-02-14.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer       Description                                                         *
*   ----------  --------------- --------------------------------------------------------------------*
*   2019-02-14  Xede Consulting Initial Creation                                                    *
*   2019-10-01  Xede Consulting Qualified OrgWideEmail query to use custom label.                   *
*                                                                                                   *
*  Code Coverage: 100.0%                                                                            *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This batch process finds all leads where the next contact date is today, and sends the
* owner an email.
* @author Xede Consulting Group
* @see LeadNextContactFollowupEmailTest
* @date 2019-02-14 
*/
global class LeadNextContactFollowupEmailBatch implements Database.Batchable<sObject> {
    String query;

    /**
    * @description This constructor accepts a custom query.
    * @param  queryString
    */
    global LeadNextContactFollowupEmailBatch(string queryString) {
        query = queryString;
        system.debug('INFO: Query String passed in> ' + query);
    }

    /**
    * @description This is the default constructor and constructs a query string to get all Self-service leads
    * abandoned in the last 30 minutes.
    */
    global LeadNextContactFollowupEmailBatch() {
        query = 'Select Id, FirstName, LastName, Email, Owner.Email, Owner.Name, Company_Name__c, Next_Step_Comments__c ' +
                '  From Lead' +
                ' Where Next_Contact_Date__c = Today' +
                '   And isConverted = False';

        system.debug('INFO: Default Constructor\'s query> ' + query);
    }

    /**
    * @description This method returns a Database.QueryLocator object that is passed to the execute method.
    * @params Batchable Context
    * @return Query Locator
    */
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    /**
    * @description This method executes for each set of records returned by the query.
    * @params Batchable Context
    * @params Scope of records returned by query
    */
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        //Get Abandon Email Template
        EmailTemplate followupEmailTemplate = [
                SELECT id, Name, DeveloperName
                  FROM EmailTemplate
                 WHERE DeveloperName =: label.Lead_Followup_Email_Template_DeveloperName
                 LIMIT 1];

        //Get Organization Wide wDefault Email Address
        OrgWideEmailAddress organizationWideEmail = [
                SELECT Id, Address, DisplayName
                  FROM OrgWideEmailAddress
                WHERE DisplayName =: label.Licensing_OrgWide_Email
                 LIMIT 1];

        /*
        Iterate over list of leads returned in scope query and send them and abandon email with link to form.
        */
        List<Messaging.SingleEmailMessage> followupEmailList = new List<Messaging.SingleEmailMessage>();
        for (sObject s: scope){
            Lead each = (Lead)s;

            /*
            Because we are using a template, and the email is not going to the lead (target object), a list of the
            one owner email must be added to the email list with the toAddresses
            */
            list<string> userEmailList = new list<string>();
            userEmailList.add(each.Owner.Email);

            //Instantiate new email message and set values
            Messaging.SingleEmailMessage followupEmail = new Messaging.SingleEmailMessage();
            followupEmail.setTreatTargetObjectAsRecipient(false);
            followupEmail.setOrgWideEmailAddressId(organizationWideEmail.Id);
            followupEmail.setTargetObjectId(each.id);
            followupEmail.setToAddresses(userEmailList);
            followupEmail.setTemplateID(followupEmailTemplate.Id);
            followupEmail.saveAsActivity = false;
            followupEmailList.add(followupEmail);
        }
        //Send emails to list of Leads who abandoned self-service form
        try {
            Messaging.SendEmail(followupEmailList);
        }
        catch(Exception ex){
            system.debug('ERROR: An Error occurred while trying to send list of emails.');
            system.debug('ERROR: Email List> ' + followupEmailList);
            system.debug('ERROR MESSAGE: ' + ex.getMessage());
        }
    }

    /**
    * @description This method executes after all batches are processed.
    * @params Batchable Context
    */
    global void finish(Database.BatchableContext BC) {

    }
}