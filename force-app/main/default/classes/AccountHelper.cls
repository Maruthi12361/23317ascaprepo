/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer   Description                                                             *
*   ----------  ----------- ------------------------------------------------------------------------*
*   2019-10-14  Xede Consulting Group   Initial Creation                                            *
*
*   Code Coverage: 100%
*                                                                                                   *
*****************************************************************************************************
*/

/**
 * @description Contains methods called by the AccountTrigger
 * @author Xede Consulting Group
 * @date 2019-10-14
 */
public with sharing class AccountHelper {
    public static boolean isFirstTime = True;
    public static boolean FutureMethodCalled = False;
    public static Boolean isInheritingVipDesignation = false;
    private static Integer UPDATE_SIZE_LIMIT = 1000;
    private static Integer MAP_SIZE_LIMIT = 10000;

    /**
     * @description finds the accounts with changed addresses and sends them to MelissaDataWebServiceCallout
     * to be validated
     *
     * @param acctList the updated accounts
     * @param oldMap the old version of the updated accounts
     */
    public static void validateAddress (List<Account> acctList, Map<Id, Account> oldMap) {
        new AccountAddressHelper().validateAddresses(acctList, oldMap);
    }

    public static void checkForAddressChange(List<Account> acctList, Map<Id, Account> oldMap) {
        new AccountAddressHelper().checkForAddressChanges(acctList, oldMap);
    }

    public static void inheritVipDesignation(List<Account> acctList, Map<Id, Account> oldMap) {
        Map<Id, Account> changedVipAcctMap = new Map<Id, Account>();
        for (Account acct : acctList) {
            Account oldAcct = oldMap != null ? oldMap.get(acct.Id) : null;
            if (oldAcct == null || acct.VIP__c != oldAcct.VIP__c) {
                changedVipAcctMap.put(acct.Id, acct);
            }
        }

        if (changedVipAcctMap.isEmpty()) {
            return;
        }

        updateDescendantAccounts(changedVipAcctMap);
    }

    private static void updateDescendantAccounts(Map<Id, Account> parentAcctMap) {
        List<Account> updateAcctList = new List<Account>();
        Map<Id, Account> childAcctMap = new Map<Id, Account>();

        for (Account childAcct : [SELECT ParentId FROM Account WHERE ParentId IN :parentAcctMap.keySet()]) {
            Account parentAcct = parentAcctMap.get(childAcct.ParentId);
            childAcct.VIP__c = parentAcct.VIP__c;
            childAcct.Serviced_By__c = parentAcct.VIP__c ? parentAcct.Serviced_By__c : null;

            updateAcctList.add(childAcct);
            if (updateAcctList.size() >= UPDATE_SIZE_LIMIT) {
                System.enqueueJob(new AccountUpdaterQueueable(updateAcctList, true));
                updateAcctList.clear();
            }

            childAcctMap.put(childAcct.Id, childAcct);
            if (childAcctMap.size() >= MAP_SIZE_LIMIT) {
                updateDescendantAccounts(childAcctMap);
                childAcctMap.clear();
            }
        }

        if (!updateAcctList.isEmpty()) {
            System.enqueueJob(new AccountUpdaterQueueable(updateAcctList, true));
        }

        if (!childAcctMap.isEmpty()) {
            updateDescendantAccounts(childAcctMap);
        }
    }
}