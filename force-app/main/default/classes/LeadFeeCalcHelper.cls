/**
 * Created by johnreedy on 2019-06-21.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-06-21   Xede Consulting Group 		Initial Creation                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This method contains some helper methods supporting the Fee Calculation Application and business process.
* @author Xede Consulting Group
* @see LeadFeeCalcHelperTest
* @date 2019-06-21 
*/
public with sharing class LeadFeeCalcHelper {
    public static Boolean isFirstTime = True;

    /**
    * @description This method removes the value in the Fee_Calculation_Request__c if the License Class Relationship changes.
    * @param opportunityList
    */
    public static void deleteRequestOnLicenseChange(List<Lead> leadList, Map<Id,Lead> oldMap){
        List<Lead> updateLeadList = new List<Lead>();

        //iterate over list of leads and if th e license class relationship changes, set the Fee_Calculation_Request__c to an empty string.
        for (Lead each: leadList){
            if (each.License_Class_Relationship__c != oldMap.get(each.Id).License_Class_Relationship__c){
                Lead currLead = new Lead();
                currLead.Id = each.Id;
                currLead.Fee_Calculation_Request__c = '';
                currLead.Fee_Calculation_Response__c = '';
                currLead.Estimated_Value__c = null;
                updateLeadList.add(currLead);
            }
        }

        //update the lead
        Database.SaveResult[] saveResultList = Database.update(updateLeadList, false);
        SaveResultsHelper saveResults = new SaveResultsHelper(saveResultList);
    }
}