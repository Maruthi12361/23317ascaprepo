/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer               Description                                                 *
*   ----------  ---------------------   ------------------------------------------------------------*
*   2018-11-27  Xede Consulting Group 		Initial Creation                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This test class exercises the methods in the ASCAPUtils class.
* @author Xede Consulting Group
* @date 2018-11-27
*/
@IsTest (seeAllData=false)
private class ASCAPUtilsTest {
    static testMethod void testDisableTrigger() {
        //create an API User and set it API Indicator to True
        User APIUser = TestDataHelper.createAdminUser('AdminUser', 'Andy', false);
        APIUser.API_User_Indicator__c = True;
        insert APIUser;

        //create a disabled trigger custom setting record for the Lead Trigger and enable it
        DisableTrigger__c disableTriggerObj = new DisableTrigger__c(Name = 'LeadTrigger', Disabled__c = false);
        insert disableTriggerObj;

        /*
        Attempt to insert Lead with 'Liquor' Lead in the name by an API User, should save and Disqualified Reason
        should be properly set.
        */
        system.runAs(APIUser) {
            Lead leadObj = TestDataHelper.createLead('Liquor', 'FirstName', 'companyName', false);
            leadObj.LeadSource = 'Liquor List';
            insert leadObj;

            Lead savedLeadObj = [SELECT Id, Disqualification_Reason_Type__c FROM Lead WHERE Id =: leadObj.Id LIMIT 1];
            system.assertEquals('Knockout Criteria', savedLeadObj.Disqualification_Reason_Type__c);
        }

        //disable the Lead trigger
        disableTriggerObj = [SELECT id FROM DisableTrigger__c WHERE id =: disableTriggerObj.Id];
        disableTriggerObj.Disabled__c = true;
        update disableTriggerObj;

        /*
        Attempt to insert the same 'Liquor' lead as above as an API user, but with the trigger disabled.  This time
        the insert should be successful.
        */
        system.runAs(APIUser) {
            Lead leadObj2 = TestDataHelper.createLead('Liquor 2', 'FirstName', 'companyName', false);
            insert leadObj2;

            Lead savedLeadObj2 = [SELECT Id, Disqualification_Reason_Type__c FROM Lead WHERE Id =: leadObj2.Id LIMIT 1];
            system.assertEquals(null, savedLeadObj2.Disqualification_Reason_Type__c);
        }
    }

    @isTest
    static void testGetAllFieldNamesSelectString() {
        Account acct = TestDataHelper.createAccount('Test Account', 'Legal Entity', true);
        String query = 'SELECT ' + ASCAPUtils.getAllFieldNamesSelectString('Account') + ' FROM Account';
        Account[] accts = Database.query(query);
        System.assertEquals(1, accts.size());
    }
}