/**
 * Created by Xede Consulting Group (Jason Burke) on 2/4/2020.
 */

public with sharing class AccountUpdaterQueueable implements Queueable {
    private List<Account> acctUpdateList;
    private Boolean isInheritingVipDesignation;

    public AccountUpdaterQueueable(Account[] acctUpdateList, Boolean isInheritingVipDesignation) {
        this.acctUpdateList = new List<Account>(acctUpdateList);
        this.isInheritingVipDesignation = isInheritingVipDesignation;
    }

    public void execute(QueueableContext qc) {
        ErrorLogger logger = new ErrorLogger();

        AccountHelper.isInheritingVipDesignation = isInheritingVipDesignation;

        Database.SaveResult[] resultList = Database.update(acctUpdateList, false);
        for (Database.SaveResult result : resultList) {
            if (!result.isSuccess()) {
                Database.Error[] errorList = result.getErrors();
                for (Database.Error error : errorList) {
                    logger.log('AccountUpdaterQueueable', 'execute', 'Account Id: "' + result.getId() + '" : ' + error.getMessage());
                }
            }
        }

        logger.save();
    }
}