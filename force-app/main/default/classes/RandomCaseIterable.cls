/**
 * Created by Xede Consulting Group (Jason Burke) on 12/12/2019.
 */
/**
 * @description Custom Iterable for use in ClosedCaseReviewBatch
 * @author Xede (Jason Burke)
 * @date 2019-12-12
 */
public with sharing class RandomCaseIterable implements Iterable<Case> {

    private Integer month;
    private Integer year;
    private Decimal percent;

    public RandomCaseIterable(Integer month, Integer year, Decimal percent) {
        this.month = month;
        this.year = year;
        this.percent = percent;
    }

    public Iterator<Case> iterator() {
        return new RandomCaseIterator(month, year, percent);
    }
}