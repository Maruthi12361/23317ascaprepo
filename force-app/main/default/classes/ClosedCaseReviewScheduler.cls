/**
 * Created by Xede Consulting Group (Jason Burke) on 12/13/2019.
 */
/**
 * @description Schedulable class for ClosedCaseReviewBatch, runs the batch for the previous month.
 * Percentage of cases to select is stored in the custom label "Closed Case Review Percentage"
 * @author Xede (Jason Burke)
 * @date 2019-12-12
 */
public with sharing class ClosedCaseReviewScheduler implements Schedulable {

    public void execute(SchedulableContext sc) {
        Date lastMonth = Date.today().addMonths(-1);
        Decimal percentMarked = Decimal.valueOf(Label.Closed_Case_Review_Percentage) * 0.01;
        Database.executeBatch(new ClosedCaseReviewBatch(lastMonth.month(), lastMonth.year(), percentMarked));
    }
}