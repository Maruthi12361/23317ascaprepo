/**
 * Created by johnreedy on 2019-10-28.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer           Description                                                     *
*   ----------  ------------------  ----------------------------------------------------------------*
*   2019-10-28   Xede Consulting  	Initial Creation                                                *
*                                                                                                   *
*   Code Coverage: 100%                                                                             *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This helper class supports operations against the License_Policy_Time_Period__c object.
* @author Xede Consulting Group
* @see LicensePolicyTimePeriodHelperTest
* @date 2019-10-28 
*/
public with sharing class LicensePolicyTimePeriodHelper {

    public static void setRelatedLookupFields(List<License_Policy_Time_Period__c> timePeriodList){
        //build set of related license policy records
        Set<Id> licensePolicyIdSet = new Set<Id>();
        for (License_Policy_Time_Period__c each: timePeriodList){
            licensePolicyIdSet.add(each.License_Policy__c);
        }

        //build map of related license policy records with their Financially Responsible and Reporting Entity
        Map<Id, License_Policy__c> licensePolicyMap = new Map<Id, License_Policy__c>([
                SELECT id, Billing_Entity__c, Financially_Responsible__c, Reporting_Entity__c, Licensee__c
                  FROM License_Policy__c
                 WHERE Id in :licensePolicyIdSet
        ]);

        /*
        Iterate over list of time period records passed in, and set the Financially Responsible and Reporting Entity fields
        */
        for (License_Policy_Time_Period__c each: timePeriodList){
            //if related license policy exists continue, otherwise throw user defined error
            if (licensePolicyMap.containsKey(each.License_Policy__c)) {
                License_Policy__c policy = licensePolicyMap.get(each.License_Policy__c);

                //if Billing Entity lookup exists on License Policy, then set on Time Period record, otherwise use Licensee
                if (String.isBlank(each.Billing_Entity__c)) {
                    if (String.isNotBlank(policy.Billing_Entity__c)) {
                        each.Billing_Entity__c = policy.Billing_Entity__c;
                    }
                    else if (String.isNotBlank(policy.Licensee__c)) {
                        each.Billing_Entity__c = policy.Licensee__c;
                    }
                }

                //if Financially Responsible lookup exists on License Policy, then set on Time Period record, otherwise use Licensee
                if (String.isBlank(each.Financially_Responsible__c)) {
                    if (String.isNotBlank(policy.Financially_Responsible__c)) {
                        each.Financially_Responsible__c = policy.Financially_Responsible__c;
                    }
                    else if (String.isNotBlank(policy.Licensee__c)) {
                        each.Financially_Responsible__c = policy.Licensee__c;
                    }
                }

                //if reporting Entity is present on License Policy then set on Time Period
                if (String.isBlank(each.Reporting_Entity__c)) {
                    if (String.isNotBlank(policy.Reporting_Entity__c)) {
                        each.Reporting_Entity__c = policy.Reporting_Entity__c;
                    }
                }
            }
        }
    }
}