/**
 * Created by johnreedy on 2019-11-20.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer           Description                                                     *
*   ----------  ------------------  ----------------------------------------------------------------*
*   2019-11-20  Xede Consulting 	Initial Creation                                                *
*   2020-01-15  Xede Consulting     Added Content Type/Predominant Programming as a picklist. Also  *
*                                   added deduction dependent field functionality                   *
*                                                                                                   *
*   Code Coverage: 87%                                                                              *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This Controller class supports the Revenue Reporting form.
* @author Xede Consulting Group
* @see RevenueReportingFormControllerTest
* @date 2019-11-20 
*/
public with sharing class RevenueReportingFormController {

    /**
     * @description This AuraEnabled method returns the fields to display on the form
     * @param recordId
     * @return FieldMetadataWrapper
     */
    @AuraEnabled
    public static String getLicenseClassConfiguration(Id recordId) {
        ErrorLogger logger = new ErrorLogger();

        //get current license policy properties
        License_Policy__c currPolicy = new License_Policy__c();
        try {
             currPolicy = [
                SELECT Id
                     , Name
                     , License_Class_Type__c
                     , License_Class_Type__r.Name
                     , License_Class_Type__r.DSLLicenseTypeName__c
                     , Licensee_Type__c
                     , License_Type__c
                     , Revenue_Reported__c
                     , Deductions_Reported__c
                     , Content_Type_s__c
                     , Status__c
                     , Start_Date__c
                     , End_Date__c
                  FROM License_Policy__c
                 WHERE Id = :recordId
                   AND Status__c = :GlobalConstant.ACTIVE_LITERAL
                 LIMIT 1
            ];
        }
        catch(Exception ex){
            if (ex.getMessage().toLowerCase().contains('list has no rows for assignment to sobject')){
                logger.log('RevenueReportingFormController:getLicenseClassConfiguration', 'This license policy (' + currPolicy.Id + ') is not in an \'Active\' status.  Please select an Active Policy and resubmit your report.').save();
                throw new AscapException('This license policy is not in an \'Active\' status.  Please select an Active Policy and resubmit your report.');
            }
        }

        //get related license class field info properties
        list<License_Class_Field_Info__c> licenseClassFieldList = [
                SELECT Id
                     , Name
                     , License_Class__c
                     , Internal_Field_Label__c
                     , DSL_Parameter_Name__c
                     , Field_Value_Type__c
                     , Help_Text__c
                     , Reporting_Category__c
                     , Deduction_Maximum_Percentage__c
                     , Order__c
                  FROM License_Class_Field_Info__c
                 WHERE License_Class__r.Name = :currPolicy.License_Class_Type__r.Name
                    OR License_Class__r.DSLLicenseTypeName__c = :currPolicy.License_Class_Type__r.Name];

        //populate licenseClassFieldInfoMap
        Map<String,License_Class_Field_Info__c> licenseClassFieldMap = new Map<String,License_Class_Field_Info__c>();
        for (License_Class_Field_Info__c eachField: licenseClassFieldList){
            licenseClassFieldMap.put(eachField.Internal_Field_Label__c, eachField);
        }

        //populate column metadata wrapper object
        FieldMetadataWrapper cmw = new FieldMetadataWrapper();
        cmw.timePeriod = new License_Policy_Time_Period__c();

        cmw.licenseClassId           = currPolicy.License_Class_Type__c;
        cmw.licenseClassName         = currPolicy.License_Class_Type__r.Name;
        cmw.LicensePolicyName        = currPolicy.Name;
        cmw.LicenseClassConfig       = currPolicy.License_Class_Type__r.DSLLicenseTypeName__c;

        cmw.contentType              = currPolicy.Content_Type_s__c;
        cmw.policyStartDate          = currPolicy.Start_Date__c;
        cmw.policyEndDate            = currPolicy.End_Date__c;
        cmw.timePeriod.Start_Date__c = System.Today().addMonths(-1).toStartOfMonth();
        cmw.timePeriod.End_Date__c   = System.Today().toStartOfMonth().addDays(-1);
        cmw.hasRateScheduleFees      = False;

        //parse Reported Revenue Picklist for required Revenue fields
        String[] revenueColumns = currPolicy.Revenue_Reported__c.split(';');
        String[] deductionColumns = currPolicy.Deductions_Reported__c != null?currPolicy.Deductions_Reported__c.split(';'):null;
        String[] allFields = new String[]{};

        //add all revenue fields and deduction fields to a single list
        allFields.AddAll(revenueColumns);

        //deductions maybe optional on some agreements, so if not found ignore
        if (deductionColumns != null){
            allFields.AddAll(deductionColumns);
        }
        allFields.add('Content Type');

        //iterate over list of revenue columns and create field metadata record and add it to the wrapper class
        for (String eachItem: allFields){
            FieldMetadata cm = new FieldMetadata();
            cm.field_label     = eachItem;
            if (licenseClassFieldMap.containsKey(eachItem)) {
                if (licenseClassFieldMap.get(eachItem).Field_Value_Type__c.toLowerCase() == GlobalConstant.PICKLIST_DATA_TYPE.toLowerCase() && eachItem == 'Content Type'){
                    String picklistValues = currPolicy.Content_Type_s__c.replaceAll(';',',');
                    cm.field_api_name  = licenseClassFieldMap.get(eachItem).DSL_Parameter_Name__c;
                    cm.field_help_text = licenseClassFieldMap.get(eachItem).Help_Text__c;
                    cm.field_type      = licenseClassFieldMap.get(eachItem).Field_Value_Type__c;
                    cm.field_order     = licenseClassFieldMap.get(eachItem).Order__c;
                    cm.possible_values = picklistValues != null? ASCAPUtils.getLicenseClassPicklistValue(picklistValues):null;
                    cm.selectedValue   = cm.possible_values[0];
                }
                else {
                    cm.field_api_name = licenseClassFieldMap.get(eachItem).DSL_Parameter_Name__c;
                    cm.field_help_text = licenseClassFieldMap.get(eachItem).Help_Text__c;
                    cm.field_type = licenseClassFieldMap.get(eachItem).Field_Value_Type__c;
                    cm.field_order = licenseClassFieldMap.get(eachItem).Order__c;
                    cm.fieldValue = 0.00;
                    cm.max_discount = licenseClassFieldMap.get(eachItem).Deduction_Maximum_Percentage__c == null ? 0.00 : licenseClassFieldMap.get(eachItem).Deduction_Maximum_Percentage__c;
                }
                cmw.field_level_metadata.add(cm);

            }
            else{
                throw new AscapException(eachItem + ' is Not found on License Class Field Info record related to ' + currPolicy.License_Class_Type__r.Name);
            }
        }

        //sort the fields being rendered on the form according to sort order
        cmw.field_level_metadata.sort();

        //return field metadata wrapper with list of field data
        return JSON.serialize(cmw, true);
    }

    /**
    * @description This AuraEnabled method returns the fee to the form for display.
    * @param recordId
    * @return ReturnWrapper
    */
    @AuraEnabled
    public static String sendForm(String recordId, String LicensePolicyName, String licenseClassId, String LicenseClassConfig, String licensePolicy, String policyTimePeriod, Date policyEndDate, Date policyStartDate) {
        //todo review for hard coded values and replace
        System.debug('RevenueReportingFormController.sendForm');
        System.debug('recordId: ' + recordId);
        System.debug('licensePolicy: ' + licensePolicy);
        System.debug('LicensePolicyName: ' + LicensePolicyName);
        System.debug('LicenseClassId:' + licenseClassId);
        System.debug('LicenseClassConfig: ' + LicenseClassConfig);
        System.debug('policyTimePeriod:' + policyTimePeriod);
        System.debug('policyStartDate:' + policyStartDate);
        System.debug('policyEndDate:' + policyEndDate);

        ReturnWrapper returnWrapper;


        Map<String, Object> timePeriodMap = (Map<String, Object>) JSON.deserializeUntyped(policyTimePeriod);
        date reportingStartDate = Date.valueOf(String.valueOf(timePeriodMap.get('Start_Date__c')));
        date reportingEndDate = Date.valueOf(String.valueOf(timePeriodMap.get('End_Date__c')));

        validateDateParameters(reportingStartDate, reportingEndDate, policyStartDate, policyEndDate);

        //build list of field metadata from the fields submitted (displayed) on the form
        List<FieldMetadata> licensePolicySubmitted = null;
        if (licensePolicy != null && !String.isEmpty(licensePolicy)) {
            licensePolicySubmitted = (List<FieldMetadata>) JSON.deserialize(licensePolicy, List<FieldMetadata>.class);
        }

        //validate that revenue submitted is greater than zero
        boolean revenueSubmitted = False;
        for (FieldMetadata eachField: licensePolicySubmitted){
            if (eachField.field_api_name.toLowerCase().contains('revenue') && eachField.fieldValue > 0){
                revenueSubmitted = True;
                break;
            }
        }
        if (revenueSubmitted == false){
            throw new AscapException('Total Revenue reported must be greater than $0.');
        }

        //Build JSON Fee Request
        String feeRequestJSONString = buildDSLRequest(licensePolicySubmitted, recordId, LicensePolicyName, LicenseClassConfig, timePeriodMap);

        //sendEmail('json', feeRequestJSONString);
        LicenseClassFeeResponse objFeeResponse = new LicenseClassFeeResponse();
        Decimal feeAmount = 0.0;

        //get license class fee amount and set fee amount
        try{
            objFeeResponse = GetLicenseClassParameters.GetLicenseClassFeeResult(feeRequestJSONString);
            HttpResponse response = objFeeResponse.response;
            if(response != null && response.getStatusCode() != 200){
                returnWrapper = new ReturnWrapper(GlobalConstant.ERROR_LITERAL,'Error processing request. Status Code='+objFeeResponse.response.getStatusCode());
                return JSON.serialize(returnWrapper, true);
            }else{
                feeAmount = objFeeResponse.lfaftp_prorated != null && objFeeResponse.lfaftp_prorated > 0 ? objFeeResponse.lfaftp_prorated : objFeeResponse.lfaftp;
            }
        }
        catch (Exception ex) {
            returnWrapper = new ReturnWrapper('ERROR',ex.getMessage());
            return ('ERROR: An error occurred while trying to retrieve the fee from the DSL in the sendForm method of the FeeCalculationFormController.\n' + ex.getMessage());
        }

        //instantiate new returnWrapper and populate fields
        returnWrapper = new ReturnWrapper(GlobalConstant.SUCCESS_LITERAL,'Fee Amount is $' + feeAmount);
        returnWrapper.feeAmount           = String.valueOf(feeAmount);
        returnWrapper.feeRequest          = feeRequestJSONString;  //this is being returned so that they can be stored on the transaction record to see the request that calculated the fee
        returnWrapper.feeResponse         = objFeeResponse.response.getBody(); //this is being returned so that they can be stored on the transaction record to see the request that calculated the fee
        returnWrapper.hasRateScheduleFees = True;

        //serialize wrapper class and return
        String returnValue = JSON.serialize(returnWrapper, true);
        return returnValue;
    }

    /**
    * @description This AuraEnabled method creates the time period and transaction records.
    * @param recordId
    * @return jsonString
    */
    @AuraEnabled
    public static String saveReport(String recordId, String feeAmount, String feeRequest, String feeResponse, String policyTimePeriod) {
        //todo review for hard coded values and replace
        ErrorLogger logger = new ErrorLogger();

        list<Transaction_Codes__mdt> transactionCodeList = [
                SELECT Id, Label, Transaction_Code__c, Transaction_Description__c
                From Transaction_Codes__mdt];

        Map<String,Transaction_Codes__mdt> transactionCodeMap = new Map<String,Transaction_Codes__mdt>();
        for (Transaction_Codes__mdt each: transactionCodeList){
            transactionCodeMap.put(each.Label,each);
        }

        System.debug('RevenueReportingFormController.saveReport');
        System.debug('recordId: ' + recordId);
        System.debug('feeAmount: ' + feeAmount);
        System.debug('feeRequest: ' + feeRequest);
        System.debug('feeResponse: ' + feeResponse);
        System.debug('policyTimePeriod: ' + policyTimePeriod);

        Map<String, Object> timePeriodMap = (Map<String, Object>) JSON.deserializeUntyped(policyTimePeriod);

        //convert time period dates passed in from string to date
        Date startDate = Date.valueOf(String.valueOf(timePeriodMap.get('Start_Date__c')));
        Date endDate = Date.valueOf(String.valueOf(timePeriodMap.get('End_Date__c')));

        //get the responsible parties from the license policy so that they can be stored on the time period for historical purposes
        License_Policy__c licensePolicy =[
                SELECT Id, Financially_Responsible__c, Billing_Entity__c, Reporting_Entity__c
                  FROM License_Policy__c
                 WHERE Id =: recordId
                 LIMIT 1];

        //find any existing time periods for the same reporting period if they exist
        List<License_Policy_Time_Period__c> existingTimePeriod =[
                SELECT Id, License_Policy_Fee_Amount__c, Start_Date__c, End_Date__c
                     , Fee_Calculation_Request_String__c, Fee_Calculation_Response_String__c, Total_Amount__c
                     , Transaction_Count__c
                  FROM License_Policy_Time_Period__c
                 WHERE License_Policy__r.Id =: recordId
                   AND Start_Date__c = : startDate
                   AND End_Date__c =: endDate];

        //get content type from new transaction/fee request
        string contentType = string.valueOf(feeRequest).substringBetween('Predominant_Programming_Type',',').replaceAll('"','').replaceAll(':','').trim();
        system.debug('Info: Content Type submitted as part of Fee Request>' + contentType);

        //if the time period already exists, create a new transaction that is the delta between the new and previous value
        system.debug('Debug-jwr: feeRequest>' + feeRequest);
        Savepoint sp = Database.setSavepoint();
        if (existingTimePeriod.size() > 0){
            list<Transaction__c> existingTransactionList = [
                SELECT Fee_Calculation_Request_String__c, Amount__c
                  FROM Transaction__c
                 WHERE License_Policy_Time_Period__c =: existingTimePeriod[0].Id
            ];
            
            boolean revision = false;
            integer transactionCount = 0;
            decimal totalAmount = 0.0;

            /*
            Iterate over existing transactions to see if an existing transacton for the same content type exists, so
            we can manage revisions, and use the cumulative amount for a given content type to create the delta billing item.
            */
            for (Transaction__c each: existingTransactionList){
                if (each.Fee_Calculation_Request_String__c.contains(contentType)){
                    revision = true;
                    transactionCount += 1;
                    totalAmount += each.Amount__c;
                }
            }

            if (transactionCount >= integer.valueOf(Label.Maximum_Number_of_VMVPD_Revisions) + 1){
                throw new AscapException('Limit of 2 revisions per reporting period has been met. \n You can not submit and additional report for this reporting period.');
            }
            License_Policy_Time_Period__c timePeriod = new License_Policy_Time_Period__c();
            timePeriod.Id                                 = existingTimePeriod[0].Id;
            timePeriod.Fee_Calculation_Request_String__c  = feeRequest;
            timePeriod.Fee_Calculation_Response_String__c = feeResponse;
            timePeriod.Billing_Entity__c                  = licensePolicy.Billing_Entity__c;
            timePeriod.Financially_Responsible__c         = licensePolicy.Financially_Responsible__c;
            timePeriod.Reporting_Entity__c                = licensePolicy.Reporting_Entity__c;
            try {
                update timePeriod;
            }
            catch(Exception ex){
                logger.log(ex).save();
                throw new AscapException('An error occurred while attempting to submit your report, please contact Customer Service, or your ASCAPOne Administrator for further assistance.');
            }

            Transaction__c trans = new Transaction__c();
            trans.License_Policy_Time_Period__c      = existingTimePeriod[0].Id;
            trans.Amount__c                          = Decimal.valueOf(feeAmount.replaceAll(',','')) - totalAmount;
            if (trans.Amount__c > 0){
                trans.Transaction_Type__c            = GlobalConstant.BILLING_ITEM_LITERAL;
            }
            else{
                trans.Transaction_Type__c            = GlobalConstant.CREDIT_LITERAL;
            }
            trans.Transaction_Date__c                = Date.Today();
            trans.Transaction_Code__c                = transactionCodeMap.get('License Fee Adjustment').Transaction_Code__c;
            trans.Transaction_Description__c         = transactionCodeMap.get('License Fee Adjustment').Transaction_Description__c;
            trans.Fee_Calculation_Response_String__c = feeResponse;
            trans.Fee_Calculation_Request_String__c  = feeRequest;

            try {
                insert trans;
            }
            catch(Exception ex){
                Database.rollback(sp);
                logger.log(ex).save();
                throw new AscapException('An error occurred while attempting to sumbit your report, please contact Customer Service, or your ASCAPOne Administrator for further assistance.');
            }
        }
        else{
            //create new time period record
            License_Policy_Time_Period__c timePeriod = new License_Policy_Time_Period__c();
            timePeriod.Start_Date__c                      = startDate;
            timePeriod.End_Date__c                        = endDate;
            timePeriod.License_Policy__c                  = recordId;
            timePeriod.Fee_Calculation_Request_String__c  = feeRequest;
            timePeriod.Fee_Calculation_Response_String__c = feeResponse;
            timePeriod.Billing_Entity__c                  = licensePolicy.Billing_Entity__c;
            timePeriod.Financially_Responsible__c         = licensePolicy.Financially_Responsible__c;
            timePeriod.Reporting_Entity__c                = licensePolicy.Reporting_Entity__c;
            try {
                insert timePeriod;
            }
            catch(Exception ex){
                logger.log(ex).save();
                throw new AscapException('An error occurred while attempting to submit your report, please contact Customer Service, or your ASCAPOne Administrator for further assistance.');
            }

            //create new transaction record
            Transaction__c trans = new Transaction__c();
            trans.License_Policy_Time_Period__c      = timePeriod.Id;
            trans.Transaction_Date__c                = Date.Today();
            trans.Transaction_Type__c                = GlobalConstant.BILLING_ITEM_LITERAL;
            trans.Transaction_Code__c                = transactionCodeMap.get('License Fee').Transaction_Code__c;
            trans.Transaction_Description__c         = transactionCodeMap.get('License Fee').Transaction_Description__c;
            trans.Fee_Calculation_Response_String__c = feeResponse;
            trans.Fee_Calculation_Request_String__c  = feeRequest;
            trans.Amount__c                          = decimal.valueOf(feeAmount.replaceAll(',',''));

            try {
                insert trans;
            }
            catch(Exception ex){
                Database.rollback(sp);
                logger.log(ex).save();
                throw new AscapException('An error occurred while attempting to submit your report, please contact Customer Service, or your ASCAPOne Administrator for further assistance.');
            }
        }
        return '{"msg": "Records saved"}';

    }
    /**
     * @description This method builds the JSON request submitted to the DSL in order to get the license fee amount
     * @param licensePolicySubmitted
     * @param recordId
     * @param policyName
     * @param licenseClass
     * @param timePeriodMap
     * @return feeRequest
     */
    private static String buildDSLRequest(List<FieldMetadata> licensePolicySubmitted, String recordId, String policyName, String licenseClass, Map<String, Object> timePeriodMap ){
        //get current license policy properties
        License_Policy__c currPolicy = [
                SELECT Id
                     , Name
                     , License_Class_Type__c
                     , License_Class_Type__r.Name
                     , License_Class_Type__r.DSLLicenseTypeName__c
                     , Licensee_Type__c
                     , License_Type__c
                     , Revenue_Reported__c
                     , Deductions_Reported__c
                     , Content_Type_s__c
                  FROM License_Policy__c
                 WHERE Id = :recordId
                   AND Status__c = :GlobalConstant.ACTIVE_LITERAL
                 LIMIT 1];

        //get license class field info records related to current policy
        list<License_Class_Field_Info__c> licenseClassFieldList = [
               SELECT Id
                    , Name
                    , License_Class__c
                    , Internal_Field_Label__c
                    , DSL_Parameter_Name__c
                    , Field_Value_Type__c
                    , Help_Text__c
                    , Reporting_Category__c
                    , Deduction_Maximum_Percentage__c
                    , Deduction_Field_Dependency__c
                    , Order__c
               FROM License_Class_Field_Info__c
              WHERE License_Class__r.Name = :currPolicy.License_Class_Type__r.Name
                 OR License_Class__r.DSLLicenseTypeName__c = :currPolicy.License_Class_Type__r.Name];

        //build request string header
        String feeRequestString =
                '{"License_Policy":{'
                + ' "Id":"' + recordId + '",'
                + ' "Name":"' + policyName + '",'
                + '"License_Type":"' + licenseClass + '",';

        //Build map of submitted fields and percent overrides (e.g. Advertising Commission deduction not to exceed 15%), so they can be passed to the DSL
        //TODO is there a way to combine number and stringfield maps?
        Map<String,Decimal> percentOverrideMap = new Map<String,Decimal>();
        Map<String, Decimal> submittedNumberFieldMap = new Map<String,Decimal>();
        Map<String, String> submittedStringFieldMap = new Map<String,String>();
        Map<String, String> dependentFieldMap = new Map<String,String>();
        //todo remove dead code, do not think this is needed.  Leaving for now, in case testing identifies the need.
        //dependentFieldMap.put('Bad_Debt_Deduction_Type','');
        //dependentFieldMap.put('Advertising_Agency_Commissions_Deduction_Type','');

        for (FieldMetadata eachField: licensePolicySubmitted){
            if (eachField.field_type.toLowerCase() == GlobalConstant.PICKLIST_DATA_TYPE.toLowerCase()){
                submittedStringFieldMap.put(eachField.field_api_name, eachField.selectedValue);
            }
            else if (eachField.field_type.toLowerCase() == GlobalConstant.NUMBER_DATA_TYPE.toLowerCase()){
                submittedNumberFieldMap.put(eachField.field_api_name, eachField.fieldValue);
            }
            for (License_Class_Field_Info__c lcfi: licenseClassFieldList){
                //if there is a percent override, add the limit field and the dependent field if it exists
                if (lcfi.Internal_Field_Label__c == eachField.field_label && (lcfi.Deduction_Maximum_Percentage__c != 0 && lcfi.Deduction_Maximum_Percentage__c != null)) {
                    percentOverrideMap.put(eachField.field_api_name.replace(GlobalConstant.UNDERSCORE_AMOUNT_LITERAL, GlobalConstant.UNDERSCORE_LIMIT_LITERAL), lcfi.Deduction_Maximum_Percentage__c / 100);
                    //if Bad_Debt_Deduction_Field_Dependency__c is not null, add it, if not add empty string
                    string dependentField = lcfi.Deduction_Field_Dependency__c != null? lcfi.Deduction_Field_Dependency__c: '';
                    dependentFieldMap.put(eachField.field_label, eachField.field_api_name.replace(GlobalConstant.UNDERSCORE_AMOUNT_LITERAL,GlobalConstant.UNDERSCORE_TYPE_LITERAL)+':'+ dependentField );
                }
            }
        }

        /*
        Initialize field map and add all API field names/DSL Parameter Names, and if the field has a corresponding deduction field, add it with its override limit.
        Otherwise, set its limit to zero.
        */
        Map<String, Decimal> fieldMap = new Map<String,Decimal>();
        for (License_Class_Field_Info__c lpfi: licenseClassFieldList){
            if (lpfi.Field_Value_Type__c.toLowerCase() != GlobalConstant.PICKLIST_DATA_TYPE.toLowerCase()) {
                fieldMap.put(lpfi.DSL_Parameter_Name__c, 0);
                if (percentOverrideMap.size() > 0 && percentOverrideMap.containsKey(lpfi.DSL_Parameter_Name__c.replace(GlobalConstant.UNDERSCORE_AMOUNT_LITERAL, GlobalConstant.UNDERSCORE_LIMIT_LITERAL))) {
                    fieldMap.put(lpfi.DSL_Parameter_Name__c.replace(GlobalConstant.UNDERSCORE_AMOUNT_LITERAL, GlobalConstant.UNDERSCORE_LIMIT_LITERAL), percentOverrideMap.get(lpfi.DSL_Parameter_Name__c.replace(GlobalConstant.UNDERSCORE_AMOUNT_LITERAL, GlobalConstant.UNDERSCORE_LIMIT_LITERAL)));
                }
                else {
                    fieldMap.put(lpfi.DSL_Parameter_Name__c.replace(GlobalConstant.UNDERSCORE_AMOUNT_LITERAL, GlobalConstant.UNDERSCORE_LIMIT_LITERAL), 0.00);
                }
            }
        }

        /*
        If field is part of submitted field list, then add the field name to the request, along with the amount submitted;
        otherwise, just add it with the default value.
        */
        for (String apiName: submittedStringFieldMap.keySet()){
            if (submittedStringFieldMap.containsKey(apiName)){
                feeRequestString += '"' + apiName + '" : "'  + submittedStringFieldMap.get(apiName) + '",';
            }
        }

        //add dependent field to request
        for (FieldMetadata eachField: licensePolicySubmitted){
            //todo remove dead code, do not think this is needed.  Leaving for now, in case testing identifies the need.
            /*if (dependentFieldMap.size() == 0){
                feeRequestString += '"Advertising_Agency_Commissions_Type": "",';
                feeRequestString += '"Bad_Debt_Deduction_Type": "",';
            }*/
            if (dependentFieldMap.containsKey(eachField.field_label)){
                if (dependentFieldMap.get(eachField.field_label).contains(':')) {
                    string[] mapParts = dependentFieldMap.get(eachField.field_label).split(':');
                    if (mapParts.size() == 2) {
                        feeRequestString += '"' + mapParts[0] + '" : "' + mapParts[1].toLowerCase() + '",';
                    }
                }
            }
        }

        /*
        iterate over licenseClassFieldList and if a deduction type field is missing (because it is not required for this particular policy),
        add it as these fields are still required by the DSL even if they are empty.
        */
        for (License_Class_Field_Info__c each: licenseClassFieldList){
            if (each.DSL_Parameter_Name__c.containsIgnoreCase('Deduction')){
                string deductionFieldName = each.DSL_Parameter_Name__c.replace(GlobalConstant.UNDERSCORE_AMOUNT_LITERAL,GlobalConstant.UNDERSCORE_TYPE_LITERAL);
                if (feeRequestString.contains(deductionFieldName) == false){
                    feeRequestString += '"' + deductionFieldName + '": "",';

                }
            }
        }
        
        //add numeric fields to json string
        for (String apiName: fieldMap.keySet()){
            if (submittedNumberFieldMap.containsKey(apiName)){
                feeRequestString += '"' + apiName + '" : '  + submittedNumberFieldMap.get(apiName) + ',';
            }
            else {
                feeRequestString += '"' + apiName + '" : ' + fieldMap.get(apiName) + ',';
            }
        }

        //remove the trailing comma (,) and replace it with closing curly brace
        feeRequestString = feeRequestString.subStringBeforeLast(',') + '},';

        //add the time period property to the request
        feeRequestString +=
                '"Time_Period":{'
                +  '"type": "Annual",'
                +  '"period": "' + timePeriodMap.get('Start_Date__c') + '"}}';

        /*
        You can enable the below sendEmail to debug issues related to the DSL in case the there is a json request issue
        Cut-n-paste the json string into postMan, or insomnia to validate that the DSL is not raising an error.
        */
        //sendEmail('feeRequestString', feeRequestString);
        return feeRequestString;
    }

    private static void validateDateParameters(Date reportingStartDate, Date reportingEndDate, Date policyStartDate, Date policyEndDate ){
        //validate reporting start date is first of month
        integer startDateDayOfMonth = Date.valueOf(String.valueOf(reportingStartDate)).day();
        if (startDateDayOfMonth != 1){
            throw new AscapException('The Reporting period Start Date must be the first of the month.');
        }

        //validate that reporting end date is last of month
        Date lastDayOfReportingEndDateMonth = reportingEndDate.addMonths(1).toStartOfMonth().addDays(-1);
        if (reportingEndDate != lastDayOfReportingEndDateMonth){
            throw new AscapException('The Reporting period\'s End Date must be the last day of the Reporting Period\'s Month.');
        }

        //validate that reporting end date does not exceed policy end date
        if (reportingEndDate > policyEndDate){
            throw new AscapException('The Reporting period End Date exceeds Policy\'s End Date.  Please correct \n and resubmit your report');
        }

        //validate that reporting start date does not exceed policy end date
        if (reportingStartDate > policyEndDate){
            throw new AscapException('The Reporting period Start Date exceeds Policy\'s End Date.  Please correct \n and resubmit your report');
        }

        //validate that reporting start date does not precede policy start date
        if (reportingStartDate < policyStartDate){
            throw new AscapException('The Reporting period Start Date precedes Policy\'s Start Date.  Please correct \n and resubmit your report');
        }

        //validate that Reporting Period's end date is greater than the start date.
        if (reportingEndDate < reportingStartDate){
            throw new AscapException('The Reporting period\'s End Date must be greater than the Reporting period\'s Start Date');
        }

    }
    /**
    * @description This wrapper class represents the column definitions passed to the lightning component
    */
    public class FieldMetadataWrapper {
        private String licenseClassId;
        private String licenseClassName;
        private String LicenseClassConfig;
        private String LicensePolicyName;
        private String contentType;
        private Date policyStartDate;
        private Date policyEndDate;
        private List<FieldMetadata> field_level_metadata;
        private License_Policy_Time_Period__c timePeriod { get; set; }
        public Boolean hasRateScheduleFees { get;set; }


        public FieldMetadataWrapper(){
            //initialize time period and list
            timePeriod = new License_Policy_Time_Period__c();
            field_level_metadata = new list<FieldMetadata>();
        }

    }
    /**
     * @description This class describes the fields required in order to define a field to be rendered on the page
    */
    public class FieldMetadata implements Comparable {
        private String field_label;
        private String field_api_name;
        private String field_help_text;
        private String field_type;
        private Decimal field_order;
        private Decimal max_discount;
        public Decimal fieldValue { get; set; }
        public String selectedValue { get; set; }
        public List<String> possible_values{ get;set; }

        public Integer compareTo(Object objToCompare) {
            FieldMetadata that = (FieldMetadata)objToCompare;
            if (this.field_order == that.field_order) return 0;
            if (this.field_order == null) return 1;
            if (that.field_order == null) return -1;
            if (this.field_order > that.field_order) return 1;
            return -1;

        }
    }
    /**
     * @description This wrapper class contains the fields returned to the revenue reporting form once the fee has been calculated.
    */
    private class ReturnWrapper {
        private String message {get;set;}
        private String code {get;set;}
        private String feeAmount {get;set;}
        private Boolean hasRateScheduleFees {get;set;}
        private String feeRequest {get;set;}
        private String feeResponse {get;set;}

        public ReturnWrapper(String errorCode, String  errorMessage){
            message = errorMessage;
            code = errorCode;
        }
    }
    /**
     * @description this method is currently used for testing to pass large strings via email so they can be verified.
     * it should be remove after testing.
    */
    public static void sendEmail(String subject, String emailBody){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'jreedy@ascap.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject(subject);
        mail.setPlainTextBody(emailBody);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }

}