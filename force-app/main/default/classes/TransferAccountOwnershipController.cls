/**
 * Created by Xede Consulting Group (Jason Burke) on 10/23/2019.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-10-28  Xede Consulting Group       Initial Creation                                        *
*   2019-10-30  Xede Consulting Group       incorporated feedback from biz checkpoint,              *
*                                               see commit log for details                          *
*   2019-11-20  Xede Consulting Group       added per-program transfer                              *
*                                                                                                   *
*   Code Coverage: 87%                                                                              *
*                                                                                                   *
*****************************************************************************************************/
/**
 * @description Controller for the TransferAccountOwnership Lightning component.
 * @author Xede Consulting Group
 * @date 2019-10-28
 */
public with sharing class TransferAccountOwnershipController {
    private static ErrorLogger logger = new ErrorLogger();

    /**
     * @description Performs SOQL query to get the current active TMLC License Policy of the venue
     *
     * @param venueId
     *
     * @return the License Policy, null if not found
     */
    @AuraEnabled
    public static License_Policy__c getTmlcLicensePolicy(Id venueId) {
        License_Policy__c[] policyList = [
                SELECT Venue__r.Name, Licensee__c, License_Class_Type__c, License_Type__c, Allocated_Fee_Amount__c, Alternative_Blanket_Fee_Amount__c, LNSPA_Percentage__c
                FROM License_Policy__c
                WHERE Venue__c = :venueId
                AND RecordType.DeveloperName = :GlobalConstant.INDUSTRY_TYPE
                AND Status__c = :GlobalConstant.ACTIVE_LITERAL
                AND Start_Date__c <= TODAY
                AND (End_Date__c = null OR End_Date__c >= TODAY)
                ORDER BY Start_Date__c DESC
        ];

        if (policyList.isEmpty()) {
            throw new AscapException('A current, active, TMLC License Policy was not found for this Account');
        }

        return policyList[0];
    }

    /**
     * @description queries for all of the License Classes
     *
     * @return the License Classes
     */
    @AuraEnabled
    public static License_Class__c[] getLicenseClasses() {
        return [SELECT Id, Name FROM License_Class__c ORDER BY Name];
    }

    /**
     * @description Transfers ownership of a venue-type Account from one Legal Entity to another.  Handles
     * cancellation and creation of TMLC license policies for the old an new owners, respectively.
     * First, it creates new Transactions for the original owner's license policy's future time periods, in order to zero-balance them.
     * Second, it updates the end dates on the original owner's license policy.
     * Third, it changes the parent of the venue Account to the new Legal Entity owner.
     * Fourth, it creates a new TMLC License Policy for the new owner and links it to the venue Account and Legal Entity.
     * Fifth, it executes the MonthlyTmlcFeeBatch job to create time periods and transactions for the new License Policy.
     *
     * @param venueId the Id of the venue-type Account being transferred
     * @param parentId the Id of the new Legal Entity Account venue owner
     * @param effectiveDate the date the transfer took place, i.e. the start date for the new policy
     * @param offAirDateList the dates for the months that the station was off-the-air, does not create Transactions for those months
     * @param licenseType the license type of the new license policies (Blanket, Alternative Blanket, Per Program)
     * @param policyId the Id of the existing License Policy of the old owner
     */
    @AuraEnabled
    public static void transferOwnership(Id venueId, Id parentId, Date effectiveDate, Date[] offAirDateList, String licenseType, Id policyId) {
        // assert that all params are set correctly
        System.assert(String.isNotBlank(venueId), 'The Account Id is not set');
        System.assert(String.isNotBlank(parentId), 'The New Account Owner Id is not set');
        System.assert(effectiveDate != null, 'The Effective Date is not set');
        System.assert(effectiveDate.day() == 1, 'The Effective Date must be the 1st of the month');
        if (offAirDateList != null) {
            for (Date offAirDate : offAirDateList) {
                System.assert(offAirDate.day() == 1, 'The Off-Air Date must be the 1st of the month');
                System.assert(offAirDate >= effectiveDate, 'The Off-Air Date must be equal to or later than the Effective Date');
            }
        }
        System.assert(String.isNotBlank(licenseType), 'The License Type is not set');
        System.assert(String.isNotBlank(policyId), 'The License Policy Id is not set');

        System.debug('offAirDateList: ' + offAirDateList);

        // create savepoint so we can rollback the entire operation if there is an exception with one of the DML statements
        Savepoint sp = Database.setSavepoint();

        // query for the original license policy
        String policyQuery = 'SELECT ' + ASCAPUtils.getAllFieldNamesSelectString('License_Policy__c') +
                             ' FROM License_Policy__c WHERE Id = :policyId';
        License_Policy__c policy = Database.query(policyQuery);

        // find the affected time periods for the original owner's license policy
        String timePeriodQuery = 'SELECT ' + ASCAPUtils.getAllFieldNamesSelectString('License_Policy_Time_Period__c') +
                                ', (SELECT ' + ASCAPUtils.getAllFieldNamesSelectString('Transaction__c') + ' FROM Transaction__r)' +
                                ' FROM License_Policy_Time_Period__c' +
                                ' WHERE License_Policy__c = :policyId' +
                                ' AND Start_Date__c >= :effectiveDate' +
                                ' ORDER BY Start_Date__c ASC';
        License_Policy_Time_Period__c[] timePeriodList = Database.query(timePeriodQuery);

        // instantiate a new helper object
        TransferAccountOwnershipHelper helper = new TransferAccountOwnershipHelper(policy.License_Type__c, licenseType);

        // create map of start date to its time period
        // ASSUMPTIONS: only one time period exists with a given start date, the start date is the 1st of a month, each time period is a month long
        Map<Date, License_Policy_Time_Period__c> dateTimePeriodMap = new Map<Date, License_Policy_Time_Period__c>();
        // create new transactions to credit the affected time periods
        List<Transaction__c> newTranList = new List<Transaction__c>();
        for (License_Policy_Time_Period__c timePeriod : timePeriodList) {
            // add time period to map
            dateTimePeriodMap.put(timePeriod.Start_Date__c, timePeriod);

            Transaction__c newTran = helper.createCreditTransaction(timePeriod);
            if (newTran != null) {
                newTranList.add(newTran);
            }
        }

        // save the new transactions
        performUpsert(newTranList, 'new Transactions', sp);

        // update the original TMLC license policy's end date and status
        policy.End_Date__c = effectiveDate.addDays(-1);
        policy.Status__c = GlobalConstant.TERMINATED_LITERAL;
        performUpsert(new License_Policy__c[] { policy }, 'original License Policy', sp);

        // set the owner (parent) of the venue Account to the new Legal Entity and save
        performUpsert(new Account[] { new Account(Id = venueId, ParentId = parentId) }, 'Account', sp);

        // create a new TMLC license policy for the new venue owner
        License_Policy__c newPolicy = policy.clone();
        newPolicy.Licensee__c = parentId;
        newPolicy.Venue__c = venueId;
        newPolicy.Start_Date__c = effectiveDate;
        newPolicy.End_Date__c = null;
        newPolicy.License_Type__c = licenseType;
        newPolicy.Status__c = GlobalConstant.ACTIVE_LITERAL;
        newPolicy.Billing_Entity__c = null;
        newPolicy.Financially_Responsible__c = null;
        newPolicy.Reporting_Entity__c = null;

        // save the new license policy
        performUpsert(new License_Policy__c[] { newPolicy }, 'new License Policy', sp);

        // we are going to create time periods and their transactions for the new license policies
        // from the effective date's month up until the later of the current month or the last time period date
        // using the MonthlyTmlcFeeBatch job
        Date lastTimePeriodDate = !timePeriodList.isEmpty() ? timePeriodList.get(timePeriodList.size() - 1).Start_Date__c : null;
        Date thisMonthDate = Date.today().toStartOfMonth();
        Date lastDate = lastTimePeriodDate > thisMonthDate ? lastTimePeriodDate : thisMonthDate;
        for (Date currentDate = effectiveDate; currentDate <= lastDate; currentDate = currentDate.addMonths(1)) {
            System.debug('currentDate: ' + currentDate);

            License_Policy_Time_Period__c timePeriod = dateTimePeriodMap.get(currentDate);
            Boolean isOffAir = offAirDateList != null && offAirDateList.contains(currentDate);
            // this adds the time period internally to the helper object
            helper.addTimePeriod(currentDate, newPolicy, timePeriod, isOffAir);
        }

        try {
            // saves the internally added time periods
            helper.saveTimePeriods();
        }
        catch (DmlException e) {
            handleException(sp, e, 'An error occurred while saving the new License Policy Time Periods: ' + e.getMessage());
        }

    }

    /**
     * @description Performs an upsert on the given SObject list.
     * This method mainly exists to reduce the number of error handling code lines to improve unit test code coverage percentage.
     *
     * @param sObjList the SObject list to upsert
     * @param sp the savepoint to rollback if needed
     *
     * @return an error message if an exception occurs, null otherwise
     */
    private static void performUpsert(List<SObject> sObjList, String sObjDesc, Savepoint sp) {
        try {
            upsert sObjList;
        }
        catch (DmlException e) {
            handleException(sp, e, 'An error occurred while saving the ' + sObjDesc + ': ' + e.getMessage());
        }
    }

    /**
     * @description Handles an exception by rolling back the given savepoint, logging the exception, and throwing a new AscapException.
     *
     * @param sp the savepoint to rollback
     * @param e the exception to handle
     * @param msg the message to include in the thrown AscapException
     */
    private static void handleException(Savepoint sp, Exception e, String msg) {
        Database.rollback(sp);
        logger.log(e).save();
        throw new AscapException(msg, e);
    }
}