/**
 * Created by johnreedy on 2019-05-17.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-05-17   Xede Consulting Group 		Initial Creation                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This AuraController supports the FeeCalculationApp Aura Component
* @author Xede Consulting Group
* @see FeeCalculationAppControllerTest
* @date 2019-05-17 
*/

public with sharing class FeeCalculationAppController {

    /**
    * @description This method checks to see if the Fee_Calculation_Request__c field has been populated.
    * @params recordId
    * @returns FeeCalculationExists
    */
    @AuraEnabled
    public static Boolean checkForFeeCalc (Id recordId){

        //get sObjectType of recordId passed in
        String sObjectType = Id.valueOf(recordId).getSobjectType().getDescribe().getName();

        Boolean FeeCalculationExist = false;

        //if sObjectType is 'Lead', check if Lead.Fee_Calculation_Request__c is populated
        if (sObjectType == GlobalConstant.OBJ_LEAD) {
            Lead currentLead = [SELECT Id, Fee_Calculation_Request__c FROM Lead WHERE Id = :recordId LIMIT 1];

            if (String.isNotBlank(currentLead.Fee_Calculation_Request__c)) {
                FeeCalculationExist = true;
            }
        }
        else if (sObjectType == GlobalConstant.OBJ_OPPORTUNITY){
            Opportunity currentOpportunity = [SELECT Id, Fee_Calculation_Request__c FROM Opportunity WHERE Id = :recordId LIMIT 1];

            if (String.isNotBlank(currentOpportunity.Fee_Calculation_Request__c)) {
                FeeCalculationExist = true;
            }
        }
        else if (sObjectType == GlobalConstant.OBJ_LICENSE_POLICY){
            List<License_Policy_Time_Period__c> currentTimePeriod = [
                    SELECT Id, Fee_Calculation_Request_String__c
                      FROM License_Policy_Time_Period__c
                     WHERE License_Policy__c = :recordId];

            if (currentTimePeriod.size() > 0 && String.isNotBlank(currentTimePeriod[0].Fee_Calculation_Request_String__c)) {
                FeeCalculationExist = true;
            }
        }
        else{
            System.debug('INFO: Unable to render Fee Calculation for record type: ' + sObjectType);
        }

        return FeeCalculationExist;
    }

}