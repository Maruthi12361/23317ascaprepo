/**
 * Created by Xede Consulting Group (Jason Burke) on 10/16/2019.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-10-16  Xede Consulting Group       Initial Creation                                        *
*                                                                                                   *
*   Code Coverage: 100%                                                                             *
*                                                                                                   *
*****************************************************************************************************/
/**
 * @description Concrete subclass of AddressHelper for helping with Lead addresses.
 * @see AddressHelper
 */
public with sharing class LeadAddressHelper extends AddressHelper {
    /**
     * @description Map to indicate if the Entity Address for a given Lead Id has been updated by MelissaData yet.
     * Used by isCalloutNeeded() method.
     * @see LeadAddressHelper.isCalloutNeeded
     */
    private Map<Id, Boolean> leadIdEntityAddrUpdatedMap = new Map<Id, Boolean>();

    /**
     * @description Initializes the addrTypeList with all of the Lead address types.
     * @see AddressHelper
     */
    public LeadAddressHelper() {
        // it's important that the Entity Address type is listed AFTER the Premise Address type
        // as it is dependent on the Premise Address type validation, in case it is marked to be
        // the same as it
        addrTypeList.add(GlobalConstant.PREMISE_ADDR_TYPE);
        addrTypeList.add(GlobalConstant.ENTITY_ADDR_TYPE);
    }

    /**
     * @description Calls the no-arg constructor, then initializes the addrTypeSObjIdsMap
     * with the given Sets of Ids.
     * @see AddressHelper
     *
     * @param entityAddrLeadIdSet Lead Ids that have changed entity addresses
     * @param premiseAddrLeadIdSet Lead Ids that have changed premise addresses
     */
    public LeadAddressHelper(Set<Id> entityAddrLeadIdSet, Set<Id> premiseAddrLeadIdSet) {
        this();
        addrTypeSObjIdsMap.put(GlobalConstant.ENTITY_ADDR_TYPE, entityAddrLeadIdSet);
        addrTypeSObjIdsMap.put(GlobalConstant.PREMISE_ADDR_TYPE, premiseAddrLeadIdSet);
    }

    /**
     * @see AddressHelper.isAddrFound
     */
    public override Boolean isAddrFound(SObject sObj, String addrType) {
        if (sObj == null) {
            return false;
        }
        Lead ld = (Lead) sObj;

        // return true if any address component is not blank
        if (addrType == GlobalConstant.ENTITY_ADDR_TYPE) {
            return String.isNotBlank(ld.Street) || String.isNotBlank(ld.City) || String.isNotBlank(ld.State) || String.isNotBlank(ld.PostalCode);
        }
        if (addrType == GlobalConstant.PREMISE_ADDR_TYPE) {
            return String.isNotBlank(ld.Premise_Street__c) || String.isNotBlank(ld.Premise_City__c) || String.isNotBlank(ld.Premise_State__c) || String.isNotBlank(ld.Premise_Zip_Code__c);
        }

        return false;
    }

    /**
     * @see AddressHelper.getStreet
     */
    public override String getStreet(SObject sObj, String addrType) {
        if (sObj == null) {
            return null;
        }
        Lead ld = (Lead) sObj;

        if (addrType == GlobalConstant.ENTITY_ADDR_TYPE) {
            return ld.Street;
        }
        if (addrType == GlobalConstant.PREMISE_ADDR_TYPE) {
            return ld.Premise_Street__c;
        }

        return null;
    }

    /**
     * @see AddressHelper.getCity
     */
    public override String getCity(SObject sObj, String addrType) {
        if (sObj == null) {
            return null;
        }
        Lead ld = (Lead) sObj;

        if (addrType == GlobalConstant.ENTITY_ADDR_TYPE) {
            return ld.City;
        }
        if (addrType == GlobalConstant.PREMISE_ADDR_TYPE) {
            return ld.Premise_City__c;
        }

        return null;
    }

    /**
     * @see AddressHelper.getState
     */
    public override String getState(SObject sObj, String addrType) {
        if (sObj == null) {
            return null;
        }
        Lead ld = (Lead) sObj;

        if (addrType == GlobalConstant.ENTITY_ADDR_TYPE) {
            return ld.State;
        }
        if (addrType == GlobalConstant.PREMISE_ADDR_TYPE) {
            return ld.Premise_State__c;
        }

        return null;
    }

    /**
     * @see AddressHelper.getPostalCode
     */
    public override String getPostalCode(SObject sObj, String addrType) {
        if (sObj == null) {
            return null;
        }
        Lead ld = (Lead) sObj;

        if (addrType == GlobalConstant.ENTITY_ADDR_TYPE) {
            return ld.PostalCode;
        }
        if (addrType == GlobalConstant.PREMISE_ADDR_TYPE) {
            return ld.Premise_Zip_Code__c;
        }

        return null;
    }

    /**
     * @see AddressHelper.getCountry
     */
    public override String getCountry(SObject sObj, String addrType) {
        if (sObj == null) {
            return null;
        }
        Lead ld = (Lead) sObj;

        if (addrType == GlobalConstant.ENTITY_ADDR_TYPE) {
            return ld.Country;
        }
        if (addrType == GlobalConstant.PREMISE_ADDR_TYPE) {
            return ld.Premise_Country__c;
        }

        return null;
    }

    /**
     * @see AddressHelper.getCompany
     */
    public override String getCompany(SObject sObj, String addrType) {
        if (sObj == null) {
            return null;
        }
        Lead ld = (Lead) sObj;

        if (addrType == GlobalConstant.ENTITY_ADDR_TYPE) {
            return ld.Company;
        }
        if (addrType == GlobalConstant.PREMISE_ADDR_TYPE) {
            return ld.Company_Name__c;
        }

        return null;
    }

    /**
     * @description Checks the default implementation and if true, does an additional check to see if the address type
     * is Entity Address and if it has already been updated by MelissaData.  This can happen if the Premise Address gets
     * updated by MelissaData and is copied to the Entity Address because it is the same as the Premise Address.  In that
     * case, a separate callout for the Entity Address is not needed.
     *
     * @see AddressHelper.isCalloutNeeded
     */
    public override Boolean isCalloutNeeded(SObject sObj, String addrType) {
        if (!super.isCalloutNeeded(sObj, addrType)) {
            return false;
        }
        Lead ld = (Lead) sObj;

        if (addrType == GlobalConstant.ENTITY_ADDR_TYPE && leadIdEntityAddrUpdatedMap.get(ld.Id) == true) {
            return false;
        }

        return true;
    }

    protected override SObject initialize(SObject sObj) {
        // clone a new object preserving the ID so we can use it to update
        // since we are in an after trigger, the original SObjects are read-only
        Lead ld = (Lead) sObj.clone(true);
        if (ld.Mailing_same_as_premise_indicator__c == true) {
            ld.Street = ld.Premise_Street__c;
            ld.City = ld.Premise_City__c;
            ld.State = ld.Premise_State__c;
            ld.PostalCode = ld.Premise_Zip_Code__c;
            ld.Country = ld.Premise_Country__c;
        }

        return ld;
    }

    /**
     * @see AddressHelper.querySObjects
     */
    protected override SObject[] querySObjects(Set<Id> allSObjIdSet) {
        return [
                SELECT Street, City, State, PostalCode, Country, Company,
                        Premise_Street__c, Premise_City__c, Premise_State__c, Premise_Zip_Code__c, Premise_Country__c, Company_Name__c,
                        Mailing_same_as_premise_indicator__c
                FROM Lead
                WHERE Id IN :allSObjIdSet
        ];
    }

    /**
     * @see AddressHelper.updateSObject
     */
    protected override void updateSObject(SObject updateSObj, SObject originalSObj, String addrType, MelissaDataResponse addrResp) {
        Lead updateLead = (Lead) updateSObj;
        Lead originalLead = (Lead) originalSObj;

        // update the update SObject with info from the MelissaData response
        if (addrType == GlobalConstant.ENTITY_ADDR_TYPE || (addrType == GlobalConstant.PREMISE_ADDR_TYPE && originalLead.Mailing_same_as_premise_indicator__c == true)) {
            updateLead.Street = addrResp.Records[0].AddressLine1;
            updateLead.City = addrResp.Records[0].City;
            updateLead.State = addrResp.Records[0].State;
            updateLead.PostalCode = addrResp.Records[0].PostalCode;
            updateLead.Melissa_Data_Processed_Indicator__c = true;
            updateLead.Address_Last_Verified_Date__c = System.Now();
            updateLead.Standardized_Address_String__c = addrResp.Records[0].AddressLine1 + ' ' + addrResp.Records[0].City + ' ' + addrResp.Records[0].State + ' ' + addrResp.Records[0].PostalCode;


            // if this is the entity address, set the non-standardized field to the value entered by the user, and if
            // this is the Premise address and the Same as Premise indicator is set to true, then
            // set the non-standardized address to the value of the Premise address entered by the user.

            if (addrType == GlobalConstant.ENTITY_ADDR_TYPE) {
                updateLead.NonStandardized_Address_String__c = replaceCR(originalLead.Street) + ' ' + originalLead.City + ' ' + originalLead.State + ' ' + originalLead.PostalCode;
            }
            else {
                // must be the case that this is the premise address type and the entity (mailing) address is marked to be the same.
                // See the if statement above that got us into this block.
                updateLead.NonStandardized_Address_String__c = replaceCR(originalLead.Premise_Street__c) + ' ' + originalLead.Premise_City__c + ' ' + originalLead.Premise_State__c + ' ' + originalLead.Premise_Zip_Code__c;
            }

            if (addrResp.Records[0].Results.contains(GlobalConstant.MELISSA_DATA_ERROR_PREFIX)) {   // an error has occurred
                updateLead.Valid_Address_Indicator__c = false;
                updateLead.Address_Status__c = Label.MelissaData_Invalid_Status;
            } else {
                updateLead.Valid_Address_Indicator__c = true;
                updateLead.Address_Status__c = Label.MelissaData_Valid_Status;
            }

            updateLead.Address_Verification_Result_Codes__c = getResultCodeString(addrResp.Records[0].Results);

            // mark the entity address as having been updated
            leadIdEntityAddrUpdatedMap.put(updateLead.Id, true);
        }

        if (addrType == GlobalConstant.PREMISE_ADDR_TYPE) {
            updateLead.Premise_Street__c = addrResp.Records[0].AddressLine1;
            updateLead.Premise_City__c = addrResp.Records[0].City;
            updateLead.Premise_State__c = addrResp.Records[0].State;
            updateLead.Premise_Zip_Code__c = addrResp.Records[0].PostalCode;
            updateLead.Melissa_Data_Premise_Processed_Indicator__c = true;
            updateLead.Last_Verified_Premise_Date__c = System.Now();
            updateLead.Standardized_Premise_String__c = addrResp.Records[0].AddressLine1 + ' ' + addrResp.Records[0].City + ' ' + addrResp.Records[0].State + ' ' + addrResp.Records[0].PostalCode;
            updateLead.NonStandardized_Premise_String__c = replaceCR(originalLead.Premise_Street__c) + ' ' + originalLead.Premise_City__c + ' ' + originalLead.Premise_State__c + ' ' + originalLead.Premise_Zip_Code__c;

            if (addrResp.Records[0].Results.contains(GlobalConstant.MELISSA_DATA_ERROR_PREFIX)) {   // an error has occurred
                updateLead.Valid_Premise_Indicator__c = false;
                updateLead.Premise_Address_Status__c = Label.MelissaData_Invalid_Status;
            } else {
                updateLead.Valid_Premise_Indicator__c = true;
                updateLead.Premise_Address_Status__c = Label.MelissaData_Valid_Status;
            }

            updateLead.Premise_Verification_Result_Codes__c = getResultCodeString(addrResp.Records[0].Results);
        }
    }

    /**
     * @see AddressHelper.getAddrStatus
     */
    @TestVisible
    protected override String getAddrStatus(SObject sObj, String addrType) {
        if (sObj == null) {
            return null;
        }
        Lead ld = (Lead) sObj;

        if (addrType == GlobalConstant.ENTITY_ADDR_TYPE) {
            return ld.Address_Status__c;
        }
        if (addrType == GlobalConstant.PREMISE_ADDR_TYPE) {
            return ld.Premise_Address_Status__c;
        }

        return null;
    }

    /**
     * @see AddressHelper.setAddrStatus
     */
    @TestVisible
    protected override void setAddrStatus(SObject sObj, String addrType, String status) {
        if (sObj == null) {
            return;
        }
        Lead ld = (Lead) sObj;

        if (addrType == GlobalConstant.ENTITY_ADDR_TYPE) {
            ld.Address_Status__c = status;
        }
        if (addrType == GlobalConstant.PREMISE_ADDR_TYPE) {
            ld.Premise_Address_Status__c = status;
        }
    }

    /**
     * @see AddressHelper.buildAddrStr
     */
    @TestVisible
    protected override String buildAddrStr(SObject sObj, String addrType) {
        if (sObj == null) {
            return null;
        }
        Lead ld = (Lead) sObj;

        // build a string of all the address component fields
        if (addrType == GlobalConstant.ENTITY_ADDR_TYPE) {
            return ld.Street + ld.City + ld.State + ld.PostalCode + ld.Country;
        }
        if (addrType == GlobalConstant.PREMISE_ADDR_TYPE) {
            return ld.Premise_Street__c + ld.Premise_City__c + ld.Premise_State__c + ld.Premise_Zip_Code__c + ld.Premise_Country__c;
        }

        return null;
    }

    /**
     * @see AddressHelper.callAddrValidationService
     */
    protected override void callAddrValidationService() {
        MelissaDataWebServiceCallout.validateLeadAddress(addrTypeSObjIdsMap.get(GlobalConstant.ENTITY_ADDR_TYPE), addrTypeSObjIdsMap.get(GlobalConstant.PREMISE_ADDR_TYPE));
    }

    /**
     * @see AddressHelper.getAddrLastVerifiedDate
     */
    @TestVisible
    protected override Datetime getAddrLastVerifiedDate(SObject sObj, String addrType) {
        if (sObj == null) {
            return null;
        }
        Lead ld = (Lead) sObj;

        if (addrType == GlobalConstant.ENTITY_ADDR_TYPE) {
            return ld.Address_Last_Verified_Date__c;
        }
        if (addrType == GlobalConstant.PREMISE_ADDR_TYPE) {
            return ld.Last_Verified_Premise_Date__c;
        }

        return null;
    }

    /**
     * @see AddressHelper.getSObjectType
     */
    protected override String getSObjectType() {
        return GlobalConstant.OBJ_LEAD;
    }

    /**
     * @see AddressHelper.getNewSObject
     */
    protected override SObject getNewSObject() {
        return new Lead();
    }
}