/**
 * Created by Xede Consulting Group (Jason Burke) on 12/10/2019.
 */
/**
 * @description Controller for MarkOffAirMonths Lightning component.
 * @date 2019-12-10
 * @author Xede (Jason Burke)
 */
public with sharing class MarkOffAirMonthsController {

    /**
     * @description Splits a time period into multiple on- and off-air time periods
     * based on the dates in the offAirDateList
     *
     * @param timePeriodId The Id of the original On-Air license policy time period
     * @param offAirDateList A list of dates within the time period that were off-air.
     * They must be on the first of the month and represent the entire month being off-air.
     * Assumes that all off-air dates fall within the original On-Air time period.
     */
    @AuraEnabled
    public static void markOffAirMonths(Id timePeriodId, Date[] offAirDateList) {
        System.assert(offAirDateList != null && !offAirDateList.isEmpty(), 'No Off-Air dates are selected');

        String query = 'SELECT ' + ASCAPUtils.getAllFieldNamesSelectString('License_Policy_Time_Period__c') +
                        ' FROM License_Policy_Time_Period__c WHERE Id = :timePeriodId';
        License_Policy_Time_Period__c[] timePeriodList = Database.query(query);
        if (timePeriodList.isEmpty()) {
            throw new AscapException('No License Policy Time Period found for Id: ' + timePeriodId);
        }

        // We split the original on-air time period into a list of alternating on- and off-air time periods
        // curOnAirTimePeriod starts as the original on-air time period and curOffAirTimePeriod is the time period
        // just before that one in the list.  So curOnAirTimePeriod will always be the most recent time period.
        // We loop through the off-air dates in order, comparing them to the curOnAirTimePeriod start date.
        // If it is equal to the start date, we extend the curOffAirTimePeriod to cover that month and shorten the
        // curOnAirTimePeriod accordingly.  If it is later than the start date, we end the curOnAirTimePeriod just
        // before the off-air date, create a new curOffAirTimePeriod to cover the off-air month, and create a new
        // curOnAirTimePeriod to cover the time from after the off-air month to the end of the original time frame.

        // preserve a copy of the original on-air time period
        License_Policy_Time_Period__c origOnAirTimePeriod = timePeriodList[0].clone(true);
        // initialize the current on-air time period
        License_Policy_Time_Period__c curOnAirTimePeriod = timePeriodList[0];
        List<License_Policy_Time_Period__c> timePeriodUpsertList = new List<License_Policy_Time_Period__c>();
        timePeriodUpsertList.add(curOnAirTimePeriod);
        License_Policy_Time_Period__c curOffAirTimePeriod = null;
        // order the off-air dates from earliest to latest
        offAirDateList.sort();
        for (Date offAirDate : offAirDateList) {
            // compare the off-air date to the current on-air time period's start date
            if (offAirDate.isSameDay(curOnAirTimePeriod.Start_Date__c)) {
                // we need to extend the current off-air time period's end date to cover this off-air month
                if (curOffAirTimePeriod == null) {
                    // initialize the current off-air time period
                    curOffAirTimePeriod = curOnAirTimePeriod.clone();
                    curOffAirTimePeriod.Is_Off_Air__c = true;
                    timePeriodUpsertList.add(curOffAirTimePeriod);
                }
                // set off-air time period end date to the last day of the off-air month
                curOffAirTimePeriod.End_Date__c = offAirDate.addMonths(1).addDays(-1);
                // set on-air time period start date to the next day
                curOnAirTimePeriod.Start_Date__c = curOffAirTimePeriod.End_Date__c.addDays(1);
            }
            else if (offAirDate > curOnAirTimePeriod.Start_Date__c) {
                // we need to create a new current off-air time period to cover this off-air month
                // create the new off-air time period
                curOffAirTimePeriod = curOnAirTimePeriod.clone();
                curOffAirTimePeriod.Is_Off_Air__c = true;
                timePeriodUpsertList.add(curOffAirTimePeriod);

                // set on-air time period end date to the last day of the month prior to the off-air month
                curOnAirTimePeriod.End_Date__c = offAirDate.addDays(-1);
                // set off-air time period start date to the off-air date
                curOffAirTimePeriod.Start_Date__c = offAirDate;

                // create a new on-air current time period
                curOnAirTimePeriod = curOffAirTimePeriod.clone();
                curOnAirTimePeriod.Is_Off_Air__c = false;

                // set off-air time period end date to the last day of the off-air month
                curOffAirTimePeriod.End_Date__c = offAirDate.addMonths(1).addDays(-1);
                // set on-air time period start date to the next day
                curOnAirTimePeriod.Start_Date__c = curOffAirTimePeriod.End_Date__c.addDays(1);

                // don't save this current on-air time period if we're at the end of the time frame
                if (curOnAirTimePeriod.Start_Date__c < curOnAirTimePeriod.End_Date__c) {
                    timePeriodUpsertList.add(curOnAirTimePeriod);
                }
            }
        }

        // if the original on-air time period has been shrunk to nothing, this means every month in it is off-air
        // so we can just set the Is Off Air checkbox on the original time period instead of creating a new time period
        if (timePeriodList[0].Start_Date__c >= timePeriodList[0].End_Date__c) {
            origOnAirTimePeriod.Is_Off_Air__c = true;
            timePeriodUpsertList.clear();
            timePeriodUpsertList.add(origOnAirTimePeriod);
        }

        // calculate the prorated fee credit based on the fraction of months
        // in the original on-air time period that were marked off-air
        Decimal creditAmount = -origOnAirTimePeriod.Total_Amount__c *
                (Decimal.valueOf(offAirDateList.size()) / Decimal.valueOf(origOnAirTimePeriod.Start_Date__c.monthsBetween(origOnAirTimePeriod.End_Date__c) + 1));

        // save the time periods and credit transaction
        try {
            upsert timePeriodUpsertList;
            insert new Transaction__c(
                    License_Policy_Time_Period__c = origOnAirTimePeriod.Id,
                    Transaction_Type__c = GlobalConstant.CREDIT_LITERAL,
                    Amount__c = creditAmount,
                    Transaction_Date__c = Date.today()
            );
        }
        catch (DmlException e) {
            throw new AscapException('An error occurred while updating the License Policy Time Periods: ' + e.getMessage(), e);
        }
    }
}