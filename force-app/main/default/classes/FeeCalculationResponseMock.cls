/**
 * Created by johnreedy on 2019-11-07.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-11-07   Xede Consulting Group 		Initial Creation                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This mock class contains the fee calculation response string.
* @author Xede Consulting Group
* @see FeeCalculationResponseMockTest
* @date 2019-11-07 
*/
@isTest
public class FeeCalculationResponseMock implements HttpCalloutMock {

    public HTTPResponse respond(HTTPRequest request) {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{'
                + '"lfaftp": 6100.0,'
                + '"lfaftp_prorated": 0.0,'
                + '"fee_detail_dict": {},'
                + '"other_related_info_dict": {'
                + '"lfaftp_not_subject_to_cpi_adjustment": 0.0,'
                + '"admin_fee_list": [],'
                + '"fees_subject_to_proration": 0.0,'
                + '"prorated_fees": 0.0,'
                + '"minimum_annual_fee_amount": 0,'
                + '"maximum_annual_fee_amount": null'
                + '}'
                + '}'
        );
        response.setStatusCode(200);
        response.setStatus('OK');
        return response;
    }

}