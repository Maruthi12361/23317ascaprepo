/**
 * Created by Xede Consulting Group (Jason Burke) on 12/12/2019.
 */
/**
 * @description Processes a random percentage of all cases closed in the given month,
 * marking them as Needs Review
 * @author Xede (Jason Burke)
 * @date 2019-12-12
 */
public with sharing class ClosedCaseReviewBatch implements Database.Batchable<Case> {

    private final Integer month;
    private final Integer year;
    private final Decimal percentMarked;

    public ClosedCaseReviewBatch(Integer month, Integer year, Decimal percentMarked) {
        this.month = month;
        this.year = year;
        this.percentMarked = percentMarked;
    }

    public Iterable<Case> start(Database.BatchableContext bc) {
        return new RandomCaseIterable(month, year, percentMarked);
    }

    public void execute(Database.BatchableContext bc, List<Case> scope) {
        for (Case c : scope) {
            c.Review_Status__c = Label.Needs_Review;
        }

        update scope;
    }

    public void finish(Database.BatchableContext bc) {
    }

}