/**
 * Created by Xede Consulting Group (Jason Burke) on 12/12/2019.
 */
/**
 * @description Randomly selects a percentage of Cases that were closed in the given month.
 * An equal percentage of each Case Type is selected.
 * @author Xede (Jason Burke)
 * @date 2019-12-12
 */
public with sharing class RandomCaseIterator implements Iterator<Case> {
    /**
     * @description The list of randomly selected cases to return
     */
    private List<Case> randomCaseList = new List<Case>();
    /**
     * @description The current index of the randomCaseList
     */
    private Integer i = 0;

    /**
     * @description Constructor that queries for all Cases closed in the given month,
     * divides them by Case Type, and then selects the given percentage of each type.
     *
     * @param month The closing month
     * @param year The closing year
     * @param percent The percentage to select
     */
    public RandomCaseIterator(Integer month, Integer year, Decimal percent) {
        // create the date range for the soql query
        Date startDate = Date.newInstance(year, month, 1);
        Date endDate = startDate.addMonths(1);
        // create map of case type to list of cases with that type
        Map<String, List<Case>> typeCasesMap = new Map<String, List<Case>>();

        // query for cases and populate map
        for (Case c : [SELECT Type FROM Case WHERE ClosedDate >= :startDate AND ClosedDate < :endDate ORDER BY CaseNumber]) {
            if (!typeCasesMap.containsKey(c.Type)) {
                typeCasesMap.put(c.Type, new List<Case>());
            }
            typeCasesMap.get(c.Type).add(c);
        }

        // process the map
        for (String type : typeCasesMap.keySet()) {
//            System.debug('Case Type: ' + type);

            List<Case> caseList = typeCasesMap.get(type);
            // calculate the number of cases to select
            Integer numCases = Math.round(percent * Decimal.valueOf(caseList.size()));
            if (numCases == 0) {
                // always choose at least one case
                numCases = 1;
            }
//            System.debug('numCases = ' + numCases);

            // create set to hold previously selected list indexes
            Set<Integer> indexSet = new Set<Integer>();
            for (Integer j = 0; j < numCases; j++) {
                Integer r = getRandomIndex(caseList.size());
                while (indexSet.contains(r)) {  // get another random index until we find an unused one
                    r = getRandomIndex(caseList.size());
                }
                indexSet.add(r);

                // add the randomly selected case to the return list
                randomCaseList.add(caseList.get(r));
            }
        }
    }

    /**
     * @description Informs if the iterator has more elements to return
     *
     * @return true if there are more elements, false otherwise
     */
    public Boolean hasNext() {
        if (i < randomCaseList.size()) {
            return true;
        }

        return false;
    }

    /**
     * @description returns the next element in the list
     *
     * @return the next element in the list
     */
    public Case next() {
        return randomCaseList.get(i++);
    }

    /**
     * @description generates a random list index from 0 to listSize - 1
     *
     * @param listSize the size of the list
     *
     * @return the random index
     */
    private Integer getRandomIndex(Integer listSize) {
        Integer r = Integer.valueOf(Math.floor(Math.random() * listSize));
//        System.debug('random index = ' + r);
        return r;
    }
}