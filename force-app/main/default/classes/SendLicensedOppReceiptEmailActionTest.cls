/**
 * Created by johnreedy on 2019-10-01.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer         Description                                                       *
*   ----------  ----------------  ------------------------------------------------------------------*
*   2019-10-01  Xede Consulting   Initial Creation                                                  *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This test class exercises the SendLicensedOppReceiptEmailAction class.
* @author Xede Consulting Group
* @date 2019-10-01 
*/
@IsTest
private class SendLicensedOppReceiptEmailActionTest {
    /**
    * @description Given a new Opportunity with a related Contact Role, pass the id to the
    * SendLicensedOppReceiptEmailAction.sendLicensedOpportunityReceiptEmail method.
    */
    static testMethod void testEmailAction(){
        List<Id> idList = new list<id>();

        //create Account
        Account accountObj = TestDataHelper.createAccount('AccountName', 'Legal Entity', true);

        //create opportunity
        Opportunity opportunityObj = TestDataHelper.createOpportunity('OpportunityName', accountObj.Id, 'Form Licenses', 'New', true);

        //create Contact with email
        Contact contactObj = TestDataHelper.createContact(accountObj.Id, 'LastName', 'FirstName', true);

        //Opportunity Contact Role where Contact is Primary
        OpportunityContactRole opportunityContactRoleObj = new OpportunityContactRole(OpportunityId=opportunityObj.Id, ContactId=contactObj.Id, Role='Decision Maker', IsPrimary=true);
        insert opportunityContactRoleObj;

        //add opportunity id to list
        idList.add(opportunityObj.Id);

        //run test
        test.StartTest();
            SendLicensedOppReceiptEmailAction.sendLicensedOpportunityReceiptEmail(idList);
        test.StopTest();

    }
}