/**
 * Created by johnreedy on 2019-07-11.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-07-11   Xede Consulting Group 		Initial Creation                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description <description>
* @author Xede Consulting Group
* @see LeadConversionHelperTestTest
* @date 2019-07-11 
*/

@IsTest (seeAllData = False)
public with sharing class LeadConversionHelperTest {
    public static User apiUser;
    public static User accountManager;
    public static User leadSpecialist;

    /**
    * @description This method initializes setup data.
    */
    @testSetup
    public static void setup(){
        User apiUser = TestDataHelper.createAdminUser('AdminUser', 'Andy', false); //NOPMD - reviewed
        apiUser.API_User_Indicator__c = True;
        insert apiUser;

        User accountManager = TestDataHelper.createUser('Manager', 'Account', label.Account_Manager_Profile_Name, true);
        User leadSpecialist = TestDataHelper.createUser('Specialist', 'Lead', label.Lead_Identification_Specialist_Profile_Name, true);

    }
    /**
    * @description This method queries the data created in the setup method.
    */
    public static void init() {

        apiUser = [SELECT Id, Name, API_User_Indicator__c FROM User WHERE LastName = 'AdminUser' LIMIT 1];
        accountManager = [SELECT Id, Name, API_User_Indicator__c FROM User WHERE LastName = 'Manager' LIMIT 1];
        leadSpecialist = [SELECT Id, Name, API_User_Indicator__c FROM User WHERE LastName = 'Specialist' LIMIT 1];
    }
    /**
    * @description Convert a lead and verify that other related objects are created (venue and secondary contact).
    */
    static testMethod void testOtherRelatedObjectsOnConversion() {
        init();
        //Test.setMock(HttpCalloutMock.class, new MelissaDataValidResponseMock());

        //get record type ids needed for validation
        id physicalVenueRecordTypeId = schema.SObjectType.Account.getRecordTypeInfosByName().get('Physical Venue').getRecordTypeId();
        id legalEntityRecordTypeId = schema.SObjectType.Account.getRecordTypeInfosByName().get('Legal Entity').getRecordTypeId();

        //create test license class
        License_Class__c licenseClass = TestDataHelper.createLicenseClass('test_license_class', GlobalConstant.FORM_TYPE, true);
        Lead newLead = TestDataHelper.createGeneralLead(licenseClass.Id, false);
        newLead.Company_Name__c                 = 'Buffalo BBQ';
        newLead.Company                         = 'Buffalo Wild Wings LLC';
        newLead.Secondary_Contact_First_Name__c = 'Secondary';
        newLead.Secondary_Contact_Last_Name__c  = 'Contact';
        newLead.Secondary_Contact_Email__c      = 'secondary.contact@test.com';

        insert newLead;

        test.startTest();
        //convert lead to an Account, Contact, Secondary Contact, and Venue
            Database.LeadConvert leadConvert = new database.LeadConvert();
            leadConvert.setLeadId(newLead.id);

            //set convertedStatus
            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
            leadConvert.setConvertedStatus(convertStatus.MasterLabel);
            leadConvert.setDoNotCreateOpportunity(false);


        //convert the lead and verify that is was successful
            Database.LeadConvertResult leadConvertResult = Database.convertLead(leadConvert);
        test.stopTest();

        Lead convLead = [Select id, ConvertedOpportunityId from Lead where id =: newLead.Id limit 1];

        Opportunity oppty = [Select id, Amount from Opportunity where id =: convLead.ConvertedOpportunityId limit 1];
        system.debug('Debug-jwr: oppty.Amount>' + oppty.Amount);

        System.assert(leadConvertResult.isSuccess());

        //query the Account object and validate that the the two Account records were created and they have the correct RecordTypeId
        List<Account> newAccountList = [SELECT id, recordTypeId, Name FROM Account WHERE Name = 'Buffalo Wild Wings LLC' OR Name = 'Buffalo BBQ'];
        system.assertEquals(2, newAccountList.size());

        for (Account eachAccount: newAccountLIst){
            if (eachAccount.Name == 'Buffalo BBQ'){
                system.assertEquals(physicalVenueRecordTypeId, eachAccount.RecordTypeId);
            }
            else if (eachAccount.Name == 'Buffalo Wild Wings LLC'){
                system.assertEquals(legalEntityRecordTypeId, eachAccount.RecordTypeId);
            }
        }

        //Query the Contact object for a Contact with a LastName of Contact, and a FirstName of Secondary and verify that the record was created
        List<Contact> newSecondaryContactList = [SELECT id, FirstName, LastName FROM Contact WHERE LastName = 'Contact' OR FirstName = 'Secondary'];
        system.assertEquals(1, newSecondaryContactList.size());

    }
    /**
    * @description As the system, when a Lead is converted, a secondary contact should be created if present on lead
    * record..
    */
    static testMethod void testLeadConversionCreateRelatedRecords() {
        init();

        //insert license class
        License_Class__c licenseClass = TestDataHelper.createLicenseClass('test_license_class', GlobalConstant.FORM_TYPE, true);

        Lead newLead = TestDataHelper.createGeneralLead(licenseClass.Id, false);
        newLead.Secondary_Contact_First_Name__c = 'Secondary';
        newLead.Secondary_Contact_Last_Name__c  = 'Contact';
        newLead.Secondary_Contact_Email__c      = 'secondary.contact@test.com';

        //insert new lead
        system.runAs(accountManager) {
            insert newLead;
        }

        //validate that owner id was properly set for invalid address
        newLead = [Select id, Name, Street, City, State, PostalCode, Valid_Address_Indicator__c, OwnerId, Status
                , Premise_Street__c, Premise_City__c, Premise_State__c, Premise_Zip_Code__c
                , IsConverted, ConvertedAccountId
        From Lead
        Where id = :newLead.Id Limit 1];

        //start test
        test.startTest();
        system.runAs(accountManager) {
            Database.LeadConvert lc = new database.LeadConvert();
            lc.setLeadId(newLead.id);
            lc.setDoNotCreateOpportunity(false);
            lc.setConvertedStatus('Converted');

            Database.LeadConvertResult lcr = Database.convertLead(lc);
        }
        test.stopTest();

        //get newly created lead and get the converted account id
        newLead = [Select id, Name, Street, City, State, PostalCode, Valid_Address_Indicator__c, OwnerId, Status
                , Premise_Street__c, Premise_City__c, Premise_State__c, Premise_Zip_Code__c
                , IsConverted, ConvertedAccountId
        From Lead
        Where id = :newLead.Id Limit 1];

        //get list of newly created contacts related to the lead that was converted
        list<Contact> newContactList = [Select id, Name from Contact where AccountId =: newLead.ConvertedAccountId];

        //verify that list size is 1
        system.assertEquals(2,newContactList.size());
    }

}