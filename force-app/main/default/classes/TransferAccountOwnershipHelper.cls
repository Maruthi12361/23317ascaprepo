/**
 * Created by Xede Consulting Group (Jason Burke) on 11/18/2019.
 */

/**
 * @description Provides helper functions for use by the TransferAccountOwnershipController
 * @author Xede Consulting Group
 * @date 2019-11-18
 */
public with sharing class TransferAccountOwnershipHelper {
    /**
     * @description The license type of the original owner's license policy
     */
    private String oldLicenseType;
    /**
     * @description The license type of the new owner's license policy
     */
    private String newLicenseType;
    /**
     * @description The list of new time periods and their transactions to insert for the new owner's license policy
     * when the old and new license policy types are both Per-Program
     */
    private List<TimePeriodInfo> timePeriodInfoList;
    /**
     * @description The list of TMLC fee batch jobs to run to create the time periods and transactions for the
     * new owner's license policy when old and new license policy types are NOT both Per-Program
     */
    private List<MonthlyTmlcFeeBatch> batchList;

    /**
     * @description Constructor that initializes the timePeriodInfoList if both the oldLicenseType and newLicenseType
     * are both Per-Program, or the batch List otherwise
     *
     * @param oldLicenseType The license type of the original owner's license policy
     * @param newLicenseType The license type of the new owner's license policy
     */
    public TransferAccountOwnershipHelper(String oldLicenseType, String newLicenseType) {
        this.oldLicenseType = oldLicenseType;
        this.newLicenseType = newLicenseType;

        // we're going to use one list or the other so we only initialize the one we are going to use
        if (this.oldLicenseType == GlobalConstant.PER_PROGRAM_TYPE && this.newLicenseType == GlobalConstant.PER_PROGRAM_TYPE) {
            timePeriodInfoList = new List<TimePeriodInfo>();
        }
        else {
            batchList = new List<MonthlyTmlcFeeBatch>();
        }
    }

    /**
     * @description Creates a new Transaction to add to the given timePeriod from
     * the original owner's license policy in order to credit the account
     *
     * @param timePeriod A time period from the original owner's license policy
     *
     * @return the credit transaction
     */
    public Transaction__c createCreditTransaction(License_Policy_Time_Period__c timePeriod) {
        if (oldLicenseType == GlobalConstant.BLANKET_TYPE || oldLicenseType == GlobalConstant.ALT_BLANKET_TYPE) {
            if (timePeriod.Amount_Due__c > 0) {
                // add new transaction with negative existing amount to zero-balance the time period
                return new Transaction__c(
                        License_Policy_Time_Period__c = timePeriod.Id,
                        Amount__c = -timePeriod.Amount_Due__c,
                        Transaction_Type__c = GlobalConstant.CREDIT_LITERAL
                );
            }
        }
        else if (oldLicenseType == GlobalConstant.PER_PROGRAM_TYPE && timePeriod.Transaction__r != null) {
            Decimal totalAmount = 0;
            for (Transaction__c tran : timePeriod.Transaction__r) {
                if (tran.Transaction_Type__c != GlobalConstant.PAYMENT_LITERAL && tran.Amount__c != null) {
                    totalAmount += tran.Amount__c;
                }
            }

            if (totalAmount > 0) {
                return new Transaction__c(
                        License_Policy_Time_Period__c = timePeriod.Id,
                        Amount__c = -totalAmount,
                        Transaction_Type__c = GlobalConstant.CREDIT_LITERAL
                );
            }
        }

        return null;
    }

    /**
     *
     *
     * @param timePeriodDate the start date of the new time period, must be the 1st of the month
     * @param newPolicyId
     * @param oldTimePeriod
     * @param isOffAir
     */
    public void addTimePeriod(Date timePeriodDate, License_Policy__c newPolicy, License_Policy_Time_Period__c oldTimePeriod, Boolean isOffAir) {
        Decimal oldAllocatedFee = null;
        Decimal oldAltBlanketFee = null;
        Decimal oldLnspaPercent = null;
        Decimal oldSupplementalFee = null;
        String oldAllocationType = null;
        if (oldTimePeriod != null) {
            oldAllocatedFee = oldTimePeriod.Allocated_Fee_Amount__c;
            oldAltBlanketFee = oldTimePeriod.Alternative_Blanket_Fee_Amount__c;
            oldLnspaPercent = oldTimePeriod.LNSPA_Percentage__c;
            oldSupplementalFee = oldTimePeriod.Supplemental_Fee__c;
            oldAllocationType = oldTimePeriod.Allocation_Type__c;
        }

        if (oldLicenseType == GlobalConstant.PER_PROGRAM_TYPE && newLicenseType == GlobalConstant.PER_PROGRAM_TYPE) {
            License_Policy_Time_Period__c newTimePeriod = new License_Policy_Time_Period__c(
                    License_Policy__c = newPolicy.Id,
                    Start_Date__c = timePeriodDate,
                    End_Date__c = timePeriodDate.addMonths(1).addDays(-1),
                    License_Type__c = newLicenseType,
                    Allocated_Fee_Amount__c = oldAllocatedFee != null ? oldAllocatedFee : newPolicy.Allocated_Fee_Amount__c,
                    Alternative_Blanket_Fee_Amount__c = oldAltBlanketFee != null ? oldAltBlanketFee : newPolicy.Alternative_Blanket_Fee_Amount__c,
                    LNSPA_Percentage__c = oldLnspaPercent != null ? oldLnspaPercent : newPolicy.LNSPA_Percentage__c,
                    Supplemental_Fee__c = oldSupplementalFee != null ? oldSupplementalFee : newPolicy.Supplemental_Fee__c,
                    Allocation_Type__c = oldAllocationType != null ? oldAllocationType : newPolicy.Allocated_Fee_Type__c,
                    Is_Off_Air__c = isOffAir,
                    Billing_Entity__c = newPolicy.Billing_Entity__c != null ? newPolicy.Billing_Entity__c : newPolicy.Licensee__c,
                    Financially_Responsible__c = newPolicy.Financially_Responsible__c != null ? newPolicy.Financially_Responsible__c : newPolicy.Licensee__c,
                    Reporting_Entity__c = newPolicy.Reporting_Entity__c != null ? newPolicy.Reporting_Entity__c : newPolicy.Licensee__c
            );

            List<Transaction__c> tranList = new List<Transaction__c>();
            System.debug('******* Processing Transactions *******');
            if (oldTimePeriod != null && oldTimePeriod.Transaction__r != null) {
                for (Transaction__c oldTran : oldTimePeriod.Transaction__r) {
                    System.debug(oldTran);
                    if (oldTran.Transaction_Type__c != GlobalConstant.PAYMENT_LITERAL && oldTran.Transaction_Type__c != GlobalConstant.CREDIT_LITERAL) {
                        tranList.add(oldTran.clone());
                    }
                }
            }

            timePeriodInfoList.add(new TimePeriodInfo(newTimePeriod, tranList));
        }
        else {
            batchList.add(new MonthlyTmlcFeeBatch(timePeriodDate.month(), timePeriodDate.year(), new Id[] { newPolicy.Id }, oldAllocatedFee, oldAltBlanketFee, oldLnspaPercent,
                    oldSupplementalFee, oldAllocationType, isOffAir));
        }
    }

    public void saveTimePeriods() {
        if (oldLicenseType == GlobalConstant.PER_PROGRAM_TYPE && newLicenseType == GlobalConstant.PER_PROGRAM_TYPE) {
            List<License_Policy_Time_Period__c> allTimePeriodList = new List<License_Policy_Time_Period__c>();
            for (TimePeriodInfo info : timePeriodInfoList) {
                allTimePeriodList.add(info.timePeriod);
            }
            insert allTimePeriodList;

            List<Transaction__c> allTranList = new List<Transaction__c>();
            for (TimePeriodInfo info : timePeriodInfoList) {
                for (Transaction__c tran : info.tranList) {
                    tran.License_Policy_Time_Period__c = info.timePeriod.Id;
                    allTranList.add(tran);
                }
            }
            insert allTranList;
        }
        else {
            for (MonthlyTmlcFeeBatch batch : batchList) {
                Database.executeBatch(batch);
            }
        }
    }

    public class TimePeriodInfo {
        public License_Policy_Time_Period__c timePeriod;
        public List<Transaction__c> tranList;

        public TimePeriodInfo(License_Policy_Time_Period__c timePeriod, List<Transaction__c> tranList) {
            this.timePeriod = timePeriod;
            this.tranList = tranList;
        }
    }
}