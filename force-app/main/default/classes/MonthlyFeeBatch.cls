/**
 * Created by Xede Consulting Group (Jason Burke) on 11/26/2019.
 *
 * @description Abstract class for batch jobs that run on a monthly basis and generate time periods and transactions with fees.
 */
public abstract class MonthlyFeeBatch implements Database.Batchable<SObject> {

    /**
     * @description today's date, the date this batch is being run
     */
    protected final Date today;
    /**
     * @description the name of the concrete subclass being run
     */
    protected final String subclassName;
    /**
     * @description The first day of the given month
     */
    protected final Datetime startMonthlyDate;
    /**
     * @description The last day of the given month
     */
    protected final Datetime endMonthlyDate;
    /**
     * @description the startMonthlyDate string for use in SOQL queries
     */
    protected final String startMonthlyDateStr;
    /**
     * @description the endMonthlyDate string for use in SOQL queries
     */
    protected final String endMonthlyDateStr;
    /**
     * @description The query used to find all the License Policies to process
     */
    protected final String query;

    protected final Integer maxNumTransactions;
    /**
     * @description Error logging object
     */
    protected final ErrorLogger logger = new ErrorLogger();

    /**
     * @description Creates an instance to process the given month and year
     *
     * @param subclassName The name of the concrete subclass that's running
     * @param month Month to process (1=Jan)
     * @param year Year to process
     */
    protected MonthlyFeeBatch(String subclassName, Integer month, Integer year, Integer maxNumTransactions) {
        today = Date.today();
        this.subclassName = subclassName;
        startMonthlyDate = Datetime.newInstance(year != null ? year : today.year(), month != null ? month : today.month(), 1);
        endMonthlyDate = startMonthlyDate.addMonths(1).addDays(-1);
        startMonthlyDateStr = startMonthlyDate.formatGMT('yyyy-MM-dd');
        endMonthlyDateStr = endMonthlyDate.formatGMT('yyyy-MM-dd');
        this.maxNumTransactions = maxNumTransactions;
    }

    /**
     * @description Returns a QueryLocator for the query built in the subclass' constructor
     *
     * @param bc the BatchableContext for this batch job
     *
     * @return the QueryLocator for the query built in the subclass' constructor
     */
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    /**
     * @description Creates/Updates the License Policy Time Period and Transaction records
     * and calculates the fees.
     *
     * @param bc the BatchableContext for this batch job
     * @param scope the subset of License Policies to process
     */
    public void execute(Database.BatchableContext bc, List<SObject> scope) {
        List<License_Policy__c> policyList = (List<License_Policy__c>) scope;

        // Map of License Policy Id to its batch-generated License Policy Time Period
        Map<Id, License_Policy_Time_Period__c> policyIdTimePeriodMap = new Map<Id, License_Policy_Time_Period__c>();
        // List of License Policy Time Period records to upsert (new and existing)
        List<License_Policy_Time_Period__c> timePeriodUpsertList = new List<License_Policy_Time_Period__c>();
        // List of License Policy Ids that have multiple batch-generated License Policy Time Periods for the month
        // (should only be 1 maximum)
        List<Id> errorPolicyIdList = new List<Id>();
        // List of License Policies that need their License Type set to their Future License Type
        List<License_Policy__c> policyUpdateList = new List<License_Policy__c>();

        for (License_Policy__c policy : policyList) {
            try {
                policyPreProcessing(policy);

                License_Policy_Time_Period__c timePeriod = null;

                if (policy.License_Policy_Time_Periods__r == null || policy.License_Policy_Time_Periods__r.isEmpty()) {
                    // create a new time period record
                    timePeriod = new License_Policy_Time_Period__c(
                            License_Policy__c = policy.Id,
                            Is_Batch_Created__c = true
                    );
                }
                else if (policy.License_Policy_Time_Periods__r.size() == 1
                        && policy.License_Policy_Time_Periods__r[0].Start_Date__c == getTimePeriodStartDate(policy)
                        && policy.License_Policy_Time_Periods__r[0].End_Date__c == getTimePeriodEndDate(policy)) {
                    // use the existing time period record
                    timePeriod = policy.License_Policy_Time_Periods__r[0];
                }
                else {  // error condition, should never be more than one time period and that time period must have correct start/end dates
                    errorPolicyIdList.add(policy.Id);
                }

                if (timePeriod != null) {
                    // update the time period's values from the policy if necessary
                    if (isTimePeriodUpdated(timePeriod, policy)) {
                        // time period values have changed so add it to the upsert list
                        timePeriodUpsertList.add(timePeriod);
                    }
                    // map this time period to the policy id
                    policyIdTimePeriodMap.put(policy.Id, timePeriod);
                }
            }
            catch (AscapException e) {
                logger.log(subclassName, 'execute', e.getMessage());
            }
        }

        if (!errorPolicyIdList.isEmpty()) {
            logger.log(
                    subclassName, 'execute',
                    'License Policies with the following Ids have invalid License Policy Time Periods for ' +
                            startMonthlyDate.format('MM-yyyy') + ' : ' + errorPolicyIdList
            );
        }

        performUpsert(timePeriodUpsertList);
        // now the time period records in the map have their IDs set (if they were just inserted)
        // because they are the same objects that are in the upsert list (whose objects' Ids were updated)

        // Map of time period Ids to its list of transactions (if any)
        Map<Id, List<Transaction__c>> timePeriodIdTransMap = new Map<Id, List<Transaction__c>>();
        for (Transaction__c tran : [
                SELECT License_Policy_Time_Period__c, Amount__c, Transaction_Description__c
                FROM Transaction__c
                WHERE License_Policy_Time_Period__c IN :policyIdTimePeriodMap.values()
                AND Is_Batch_Created__c = true
        ]) {
            if (!timePeriodIdTransMap.containsKey(tran.License_Policy_Time_Period__c)) {
                // initialize the transaction list
                timePeriodIdTransMap.put(tran.License_Policy_Time_Period__c, new List<Transaction__c>());
            }
            // add the transaction to the list
            timePeriodIdTransMap.get(tran.License_Policy_Time_Period__c).add(tran);
        }

        // now we have all the records we need and can finally compute the fees

        // list of Transaction records to upsert (new and existing)
        List<Transaction__c> tranUpsertList = new List<Transaction__c>();
        // list of Transaction records to delete (those in Off-Air time periods)
        List<Transaction__c> tranDeleteList = new List<Transaction__c>();
        // List of License Policy Time Period Ids that have multiple batch-generated transactions
        // (should only be a maximum of 1 batch-generated transaction)
        List<Id> errorTimePeriodIdList = new List<Id>();
        for (License_Policy__c policy : policyList) {
            try {
                // any License Policies that had errors above will not be in the map
                if (policyIdTimePeriodMap.containsKey(policy.Id)) {
                    // get the time period and transaction list to use
                    License_Policy_Time_Period__c timePeriod = policyIdTimePeriodMap.get(policy.Id);
                    List<Transaction__c> tranList = timePeriodIdTransMap.get(timePeriod.Id);

                    if (tranList == null || tranList.isEmpty()) {
                        // create new transactions
                        tranList = createTransactions(timePeriod, policy);
                    }
                    else if (tranList.size() <= maxNumTransactions) {
                        // do any processing of the existing transaction that is needed
                        // i.e. possibly adding to transaction delete list
                        tranList = processTransactions(tranList, timePeriod, policy, tranDeleteList);
                    }
                    else {  // error condition, should never be more than "maxNumTransactions" batch-generated transactions
                        errorTimePeriodIdList.add(timePeriod.Id);
                    }

                    if (tranList != null && !tranList.isEmpty()) {
                        // calculate the fee
                        Decimal[] feeList = calculateFeeAmounts(policy, tranList);
                        // if the fee has changed then add the transaction to the upsert list
                        for (Integer i = 0; i < tranList.size(); i++) {
                            if (tranList[i].Amount__c != feeList[i]) {
                                tranList[i].Amount__c = feeList[i];

                                if (tranList[i].Amount__c > 0) {
                                    tranUpsertList.add(tranList[i]);
                                }
                                else if (String.isNotBlank(tranList[i].Id)) {
                                    tranDeleteList.add(tranList[i]);
                                }
                            }
                        }
                    }
                }

                policyPostProcessing(policy, policyUpdateList);
            }
            catch (AscapException e) {
                logger.log(subclassName, 'execute', e.getMessage());
            }
        }

        if (!errorTimePeriodIdList.isEmpty()) {
            logger.log(
                    subclassName, 'execute',
                    'License Policy Time Periods with the following Ids have multiple batch-generated Transactions for ' +
                            startMonthlyDate.format('MM-yyyy') + ' : ' + errorTimePeriodIdList
            );
        }

        // do all of our DML
        performDelete(tranDeleteList);
        performUpsert(tranUpsertList);
        performUpdate(policyUpdateList);

        logger.save();
    }

    /**
     * @description noop
     *
     * @param bc the BatchableContext for this batch job
     */
    public virtual void finish(Database.BatchableContext bc) {}

    /**
     * @description if any time period fields differ from the policy's, copies those values to the time period
     *
     * @param timePeriod the Time Period to check
     * @param policy the License Policy to check against
     *
     * @return true if the time period has been updated from the policy, false otherwise
     */
    protected virtual Boolean isTimePeriodUpdated(License_Policy_Time_Period__c timePeriod, License_Policy__c policy) {
        Date startDate = getTimePeriodStartDate(policy);
        Date endDate = getTimePeriodEndDate(policy);

        // if any of the time period's fields differ from the policy's values, update all of the time period's fields
        if (timePeriod.Start_Date__c != startDate || timePeriod.End_Date__c != endDate) {
            timePeriod.Start_Date__c = startDate;
            timePeriod.End_Date__c = endDate;
            return true;
        }

        return false;
    }

    /**
     * @description Calculates the Time Period's start date
     *
     * @param policy the license policy to calculate the start date for
     *
     * @return the time period start date
     */
    protected virtual Date getTimePeriodStartDate(License_Policy__c policy) {
        return startMonthlyDate.dateGMT();
    }

    /**
     * @description Calculates the Time Period's end date
     *
     * @param policy the license policy to calculate the end date for
     *
     * @return the time period end date
     */
    protected virtual Date getTimePeriodEndDate(License_Policy__c policy) {
        return endMonthlyDate.dateGMT();
    }

    /**
     * @description Creates a new transaction with a calculated Transaction Date
     *
     * @param timePeriod the time period the transaction belongs to
     * @param policy the policy of the transaction
     *
     * @return the new transaction
     */
    protected virtual Transaction__c[] createTransactions(License_Policy_Time_Period__c timePeriod, License_Policy__c policy) {
        // return new Transaction record array
        return new Transaction__c[] {
                new Transaction__c(
                        License_Policy_Time_Period__c = timePeriod.Id,
                        Transaction_Type__c = GlobalConstant.BILLING_ITEM_LITERAL,
                        Transaction_Date__c = calculateTransactionDate(policy),
                        Is_Batch_Created__c = true
                )
        };
    }

    /**
     * @description Calculates the transaction date, defaults to the time period's start date
     *
     * @param policy the license policy to use
     *
     * @return the calculated transaction date
     */
    protected virtual Date calculateTransactionDate(License_Policy__c policy) {
        return getTimePeriodStartDate(policy);
    }

    /**
     * @description Does any needed processing of the transaction, defaults to noop
     *
     * @param tran the transaction to process
     * @param timePeriod the time period of the transaction
     * @param tranDeleteList the transaction deletion list
     *
     * @return the transaction after it has been processed
     */
    protected virtual Transaction__c[] processTransactions(Transaction__c[] tranList, License_Policy_Time_Period__c timePeriod, License_Policy__c policy,
            List<Transaction__c> tranDeleteList)
    {
        return tranList;
    }

    /**
     * @description Calculates the fee amount for the given license policy
     *
     * @param policy the policy to use
     *
     * @return the calculated fee
     */
    protected abstract Decimal[] calculateFeeAmounts(License_Policy__c policy, Transaction__c[] tranList);

    /**
     * @description Performs any pre-processing needed on the policy before the standard operations are performed
     *
     * @param policy the policy to process
     */
    protected virtual void policyPreProcessing(License_Policy__c policy) {}

    /**
     * @description Performs any post-processing needed on the policy after the standard operations are done
     *
     * @param policy the policy to process
     * @param policyUpdateList the list to add the policy to if it needs to be updated
     */
    protected virtual void policyPostProcessing(License_Policy__c policy, List<License_Policy__c> policyUpdateList) {}

    private void performUpsert(SObject[] sObjUpsertList) {
        Database.UpsertResult[] resultList = Database.upsert(sObjUpsertList, false);
        for (Database.UpsertResult result : resultList) {
            if (!result.isSuccess()) {
                for (Database.Error dbError : result.getErrors()) {
                    logger.log(subclassName, 'execute', dbError.getMessage());
                }
            }
        }
    }

    private void performUpdate(SObject[] sObjUpdateList) {
        Database.SaveResult [] resultList = Database.update(sObjUpdateList, false);
        for (Database.SaveResult  result : resultList) {
            if (!result.isSuccess()) {
                for (Database.Error dbError : result.getErrors()) {
                    logger.log(subclassName, 'execute', dbError.getMessage());
                }
            }
        }
    }

    private void performDelete(SObject[] sObjDeleteList) {
        Database.DeleteResult[] resultList = Database.delete(sObjDeleteList, false);
        for (Database.DeleteResult result : resultList) {
            if (!result.isSuccess()) {
                for (Database.Error dbError : result.getErrors()) {
                    logger.log(subclassName, 'execute', dbError.getMessage());
                }
            }
        }
    }
}