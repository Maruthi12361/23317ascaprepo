/**
 * Created by johnreedy on 2019-08-02.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-08-02   Xede Consulting Group 		Initial Creation                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This Apex Aura Controller supports the Primary Contact View Form.
* @author Xede Consulting Group
* @see PrimaryContactViewControllerTest
* @date 2019-08-02 
*/
public with sharing class PrimaryContactViewController {
    @AuraEnabled
    public static PrimaryContactInfo getPrimaryContact(String recordId) {

        //Get the primary contact for the given opportunity if exists
        OpportunityContactRole ocr = [
                SELECT Id, ContactId, Contact.Name
                     , Contact.MailingStreet
                     , Contact.MailingCity
                     , Contact.MailingState
                     , Contact.MailingPostalCode
                     , Contact.Phone
                     , Contact.Email
                     , IsPrimary
                     , Role
                  FROM OpportunityContactRole
                 WHERE OpportunityId =: recordId
                   AND IsPrimary = True LIMIT 1];

        //Create the PrimaryContactInfo record and return to the JavaScript controller
        PrimaryContactInfo response = new PrimaryContactInfo();
        response.ContactId = ocr.ContactId;
        response.ContactName = ocr.Contact.Name;
        response.MailingStreet = ocr.Contact.MailingStreet;
        response.MailingCity = ocr.Contact.MailingCity;
        response.MailingState = ocr.Contact.MailingState;
        response.MailingPostalCode = ocr.Contact.MailingPostalCode;
        response.Phone = ocr.Contact.Phone;
        response.Email = ocr.Contact.Email;
        response.IsPrimary = ocr.IsPrimary;
        response.PrimaryContactRole = ocr.Role;

        return response;

    }
    //Define the PrimaryContactInfo object
    Public class PrimaryContactInfo{
        @AuraEnabled
        public string ContactId {get; set;}
        @AuraEnabled
        public string ContactName {get; set;}
        @AuraEnabled
        public string MailingStreet {get; set;}
        @AuraEnabled
        public string MailingCity {get; set;}
        @AuraEnabled
        public string MailingState {get; set;}
        @AuraEnabled
        public string MailingPostalCode {get; set;}
        @AuraEnabled
        public string Phone {get; set;}
        @AuraEnabled
        public string Email {get; set;}
        @AuraEnabled
        public boolean IsPrimary {get; set;}
        @AuraEnabled
        public string PrimaryContactRole {get; set;}
    }
}