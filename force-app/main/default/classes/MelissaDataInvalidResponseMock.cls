/**
 * Created by johnreedy on 2019-01-31.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-01-31   Xede Consulting Group 		Initial Creation                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This test class returns a mock MelissaData Response for and address that was
* standardized but has an invalid street number.
* @author Xede Consulting Group
* @date 2019-01-31 
*/
@IsTest
public class MelissaDataInvalidResponseMock implements HttpCalloutMock {
    /**
     * @description this method accepts an address standardization request and returns an INVALID
     * MelissaDataResponse.
     * @param request
     * @return Http response
    */
    public static HTTPResponse respond(HTTPRequest request) {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{'
                + '"Records": ['
                + '{'
                + '"AddressExtras": " ",'
                + '"AddressKey": "48009000000",'
                + '"AddressLine1": "163 Bad Address",'
                + '"AddressLine2": " ",'
                + '"City": "Birmingham",'
                + '"CompanyName": " ",'
                + '"EmailAddress": " ",'
                + '"MelissaAddressKey": " ",'
                + '"MelissaAddressKeyBase": " ",'
                + '"NameFull": " ",'
                + '"PhoneNumber": " ",'
                + '"PostalCode": "48009",'
                + '"RecordExtras": " ",'
                + '"RecordID": "1",'
                + '"Reserved": " ",'
                + '"Results": "AE10",'
                + '"State": "MI"'
                + '}'
                + '],'
                + '"TotalRecords": "1",'
                + '"TransmissionReference": " ",'
                + '"TransmissionResults": " ",'
                + '"Version": "5.2.25"'
                + '}');
        response.setStatusCode(200);
        response.setStatus('OK');
        return response;
    }
}