/**
 * Created by johnreedy on 2019-01-30.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-01-30   Xede Consulting Group 		Initial Creation                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This test class returns a mock MelissaData Response for and address that was
* standardized and valid.
* @author Xede Consulting Group
* @date 2019-01-30 
*/
@IsTest
public class MelissaDataValidResponseMock implements HttpCalloutMock {
    /**
     * @description this method accepts an address standardization request and returns a VALID
     * MelissaDataResponse.
     * @param request
     * @return Http response
    */
    public static HTTPResponse respond(HTTPRequest request) {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{'
                + '"Records": ['
                + '{'
                + '"AddressExtras": " ",'
                + '"AddressKey": "48009752636",'
                + '"AddressLine1": "1636 Derby Rd",'
                + '"AddressLine2": " ",'
                + '"City": "Birmingham",'
                + '"CompanyName": " ",'
                + '"EmailAddress": " ",'
                + '"MelissaAddressKey": "1524214936",'
                + '"MelissaAddressKeyBase": " ",'
                + '"NameFull": " ",'
                + '"PhoneNumber": " ",'
                + '"PostalCode": "48009-7526",'
                + '"RecordExtras": " ",'
                + '"RecordID": "1",'
                + '"Reserved": " ",'
                + '"Results": "AC11,AS01",'
                + '"State": "MI"'
                + '}'
                + '],'
                + '"TotalRecords": "1",'
                + '"TransmissionReference": " ",'
                + '"TransmissionResults": " ",'
                + '"Version": "5.2.25"'
                + '}');
        response.setStatusCode(200);
        response.setStatus('OK');
        return response;
    }
}