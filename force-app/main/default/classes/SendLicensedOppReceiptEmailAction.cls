/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer              Description                                                  *
*   ----------  ---------------------  -------------------------------------------------------------*
*   2019-06-24  Xede Consulting Group  Initial Creation                                             *
*   2019-10-01  Xede Consulting Group  Qualified OrgWideEmail query to use custom label.            *
*                                                                                                   *
*   Code Coverage: 95.0%                                                                            *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This class contains an invocable method that is called from a process builder job.
* @author Xede Consulting Group
* @see SendLicensedOppReceiptEmailActionTest
* @date 2019-06-24 
*/
//noinspection ApexUnresolvableReference,ApexUnusedDeclaration
public without sharing class SendLicensedOppReceiptEmailAction {
    /**
     * @description This invocable method sends and email to a licensee with receipt to their License Documents when Opportunity stage = Licensed.
     * @param opportunityIdList
     * @param templateDevName
     */
    @InvocableMethod(Label='Send Opportunity Licensed Receipt Email' Description='Sends an email with receipt and licensing documents when Opportunity stage is Licensed.')

    public static void sendLicensedOpportunityReceiptEmail(List<Id> opportunityIdList){
        /*
        Setup: get email template id, organizational wide email address, and instantiate list of email messages
        */
        EmailTemplate emailTemplateId    = [SELECT Id, Name, DeveloperName FROM EmailTemplate WHERE DeveloperName =: Label.Opportunity_Licensed_Receipt_Email_Template LIMIT 1];
        OrgWideEmailAddress orgWideEmail = [
                SELECT Id, Address, DisplayName
                FROM OrgWideEmailAddress
                WHERE DisplayName =: label.Licensing_OrgWide_Email
                LIMIT 1];

        //Future to do: add where clause to above query as well as the SendSelfLicensingEmailAction class and any other "send email" classes we might add.
        List<Messaging.SingleEmailMessage> licensedOpportunityEmailList = new List<Messaging.SingleEmailMessage>();
        Map<Id, Id> primaryContactmap = new Map <Id, Id> ();
            for (OpportunityContactRole primaryContact: [
                    SELECT Id, ContactId, OpportunityId, IsPrimary, Contact.Email
                      FROM OpportunityContactRole
                     WHERE IsDeleted = false
                       AND OpportunityId = :opportunityIdList
                       AND Contact.Email != null
                       AND IsPrimary = true
                     LIMIT 1]){
                primaryContactMap.put(primaryContact.OpportunityId, primaryContact.ContactId);
            }
        /*
        For each opportunity passed in, instantiate an email message an add it to the list to be sent.
        */
        for (Id opportunityId: opportunityIdList){
            Messaging.SingleEmailMessage licensedOpportunityEmail = new Messaging.SingleEmailMessage();
            licensedOpportunityEmail.setOrgWideEmailAddressId(orgWideEmail.Id);
            licensedOpportunityEmail.setTemplateId(emailTemplateId.Id);
            licensedOpportunityEmail.setWhatId(opportunityId);
            licensedOpportunityEmail.setTargetObjectId(primaryContactMap.get(opportunityId));
            licensedOpportunityEmailList.add(licensedOpportunityEmail);
        }
        //Send emails to list of Opportunity passed-in
        try {
            Messaging.sendEmail(licensedOpportunityEmailList);
        }
        catch(Exception ex){
            System.debug('ERROR: An Error occurred while trying to send list of emails to Opportunities whose stage = Licensed.');
            System.debug('ERROR: Email List> ' + licensedOpportunityEmailList);
            System.debug('ERROR MESSAGE: ' + ex.getMessage());
        }
    }
}