/**
 * Created by johnreedy on 2019-09-04.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  -------------  ---------------------------------------------------------------------*
*   2019-09-04   Xede Consulting  Initial Creation                                                     *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This test class exercises the methods in the FeeCalculationFormHelper class.
* @author Xede Consulting Group
* @date 2019-09-04 
*/
@IsTest(seeAllData = False)
private class FeeCalculationFormHelperTest {
    static User userLevel1; // lowest level in hierarchy
    static User userLevel2;
    static User userLevel3;
    static User userLevel4;
    static UserRole salesRep; // lowest level in hierarchy
    static UserRole salesMgr;
    static UserRole salesDirector;
    static UserRole salesVP;
    static Account newAccount;
    static Opportunity newOppty;

    /**
    * @description This method initializes setup data.
    */
    static void init() {
        salesVP           = new UserRole(DeveloperName='SalesVP', Name='Sales VP');
        insert salesVP;

        salesDirector = new UserRole(DeveloperName='SalesRepresentative', ParentRoleId =salesVP.id, Name='Sales Director');
        insert salesDirector;
        salesMgr = new UserRole(DeveloperName='SalesManager', ParentRoleId =salesDirector.id, Name='Sales Manager');
        insert salesMgr;

        salesRep = new UserRole(DeveloperName='SalesRep', ParentRoleId =salesMgr.id, Name='Sales Representative');
        insert salesRep;

        userLevel1 = TestDataHelper.createUser('Level1', 'User', label.Licensing_Specialist_Profile_Name, false);
        userLevel1.UserRoleId = salesRep.id;
        insert userLevel1;

        userLevel2 = TestDataHelper.createUser('Level2', 'User', label.Licensing_Specialist_Profile_Name, false);
        userLevel2.UserRoleId = salesMgr.id;
        insert userLevel2;

        userLevel3 = TestDataHelper.createUser('Level3', 'User', label.Licensing_Specialist_Profile_Name, false);
        userLevel3.UserRoleId = salesDirector.id;
        insert userLevel3;

        userLevel4 = TestDataHelper.createUser('Level4', 'User', label.Licensing_Specialist_Profile_Name, false);
        userLevel4.UserRoleId = salesVP.id;
        insert userLevel4;

        system.runAs(userLevel1) {
            newAccount = TestDataHelper.createAccount('Ted\'s Tavern', 'Legal Entity', true);
            newOppty = TestDataHelper.createOpportunity('Ted\'s Tavern Opportunity', newAccount.Id, 'Form Licenses', 'Initial', true);
        }

    }

    /**
    * @description Given a role hierarchy (sales rep - vp), validate that the user, those above the user in the hierarchy,
    * and the ascap admins are returned in the authorized set of ids.
    */
    static testMethod void testBehavior(){
        init();

        Profile adminProfile = [SELECT id, Name FROM Profile WHERE Name =: label.ASCAP_Admin_Profile_Name limit 1];
        list<User> adminList = [SELECT Id, Name FROM User WHERE ProfileId =: adminProfile.Id];

        test.startTest();
            set<string> authorizedIdSet = FeeCalculationFormHelper.getAuthorizedUserList(userLevel1.Id, newOppty.Id);
        test.stopTest();

        list<User> totalUserList = [Select Id, Name, Profile.Name From User where id in: authorizedIdSet];

        system.assertEquals(4 + adminList.size(), authorizedIdSet.size());

    }
    /**
    * @description Given a role hierarchy (sales rep - vp), validate that a user (WITH NO ROLE), and the ascap admins
    * are returned in the authorized set of ids.
    */
    static testMethod void testBehavior2(){
        init();
        Profile adminProfile = [SELECT id, Name FROM Profile WHERE Name =: label.ASCAP_Admin_Profile_Name limit 1];
        list<User> adminList = [SELECT Id, Name FROM User WHERE ProfileId =: adminProfile.Id];

        User noRoleUser = TestDataHelper.createUser('NoRole', 'User', label.Licensing_Specialist_Profile_Name, true);
        User noRoleUser2 = TestDataHelper.createUser('NoRole', 'User2', label.Licensing_Specialist_Profile_Name, true);

        test.startTest();
            set<string> authorizedIdSet = FeeCalculationFormHelper.getAuthorizedUserList(noRoleUser.Id, newOppty.Id);
        test.stopTest();

        list<User> totalUserList = [Select Id, Name, Profile.Name From User where id in: authorizedIdSet];

        system.assertEquals(adminList.size(), authorizedIdSet.size());

    }
}