public class DynamicTransactionListController {
    @AuraEnabled
    public static License_Policy__c getLicensePolicy(String recordId) {
        String licPolicyQuery = QueryUtility.getObjectFieldsQuery(GlobalConstant.OBJ_LICENSE_POLICY) + ' From License_Policy__c where Id=:recordId  ' ;
        System.debug('\n licPolicyQuery==' + licPolicyQuery);
        List<License_Policy__c> licensePolicyList = (List<License_Policy__c>) Database.Query(licPolicyQuery);
        License_Policy__c licensePolicy = licensePolicyList != null && !licensePolicyList.isEmpty() ? licensePolicyList[0] : null;
        return licensePolicy;
    }
    @AuraEnabled
    public static License_Policy_Time_Period__c getLicensePolicyTimePeriod(String recordId) {
        String licPolicyTimePeriodQuery = QueryUtility.getObjectFieldsQuery(GlobalConstant.OBJ_LICENSE_POLICY_TIME_PERIOD) + ' From License_Policy_Time_Period__c where Id=:recordId  ' ;
        System.debug('\n licPolicyTimePeriodQuery==' + licPolicyTimePeriodQuery);
        List<License_Policy_Time_Period__c> licensePolicyTimePeriods = (List<License_Policy_Time_Period__c>) Database.Query(licPolicyTimePeriodQuery);
        License_Policy_Time_Period__c licensePolicy = licensePolicyTimePeriods != null && !licensePolicyTimePeriods.isEmpty() ? licensePolicyTimePeriods[0] : null;
        return licensePolicy;
    }
    @AuraEnabled
    public static Transaction__c getLicensePolicyTransaction(String recordId) {
        String licPolicyTransactionQuery = QueryUtility.getObjectFieldsQuery(GlobalConstant.OBJ_LICENSE_POLICY_TRANSACTION) + ' From Transaction__c where Id=:recordId  ' ;
        System.debug('\n licPolicyTransactionQuery==' + licPolicyTransactionQuery);
        List<Transaction__c> licensePolicyTransactions = (List<Transaction__c>) Database.Query(licPolicyTransactionQuery);
        Transaction__c trans = licensePolicyTransactions[0];
        return trans;
    }


    @AuraEnabled
    public static List<Transaction__c> getLicensePolicyTransactionList(String recordId) {
        License_Policy__c policy = getLicensePolicy(recordId);

        String licPolicyTimePeriodQuery = QueryUtility.getObjectFieldsQuery(GlobalConstant.OBJ_LICENSE_POLICY_TIME_PERIOD) + ' From License_Policy_Time_Period__c where License_Policy__c=:recordId  ' ;
        System.debug('\n licPolicyTimePeriodQuery==' + licPolicyTimePeriodQuery);
        Set<Id> licPolTimePeriodIds = new set<Id>();

        for(License_Policy_Time_Period__c lptp : (List<License_Policy_Time_Period__c>) Database.Query(licPolicyTimePeriodQuery)){
            licPolTimePeriodIds.add(lptp.Id);
        }

        String licPolicyTransactionQuery = QueryUtility.getObjectFieldsQuery(GlobalConstant.OBJ_LICENSE_POLICY_TRANSACTION) + ' From Transaction__c where License_Policy_Time_Period__c=:licPolTimePeriodIds order by CreatedDate desc ' ;
        System.debug('\n licPolicyTransactionQuery==' + licPolicyTransactionQuery);
        List<Transaction__c> licensePolicyTransactions = (List<Transaction__c>) Database.Query(licPolicyTransactionQuery);



        return null;
    }

}