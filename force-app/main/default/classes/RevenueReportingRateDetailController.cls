/**
 * Created by johnreedy on 2019-12-02.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-12-02   Xede Consulting Group 		Initial Creation                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description <description>
* @author Xede Consulting Group
* @see RevenueReportingRateDetailControllerTest
* @date 2019-12-02 
*/
public with sharing class RevenueReportingRateDetailController {
    public static Map<String,String> printableLabelsMap = new Map<String,String>();

    /**
     * @description This method creates the Rate Schedule Fee object and returns it to the RevenueReportingRateDetail for
     * rendering.
     * @param recordId
     * @param licenseClassId
     * @param feeResponse
     * @return RateScheduleFeeString
     */
    @AuraEnabled
    public static String getRateScheduleFees(String recordId, String licenseClassId, String feeResponse) {

        system.debug('Debug-jwr: RevenueReportingRateDetailController>');
        system.debug('Debug-jwr: licenseClassId>' + licenseClassId);
        system.debug('Debug-jwr: feeResponse>' + feeResponse);
        buildPrintableLabelsMap(licenseClassId);

        //Convert Fee Calculation Response to Map of Key Value Pairs
        Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(feeResponse);
        Map<String,Object> rateScheduleFeeAmountMap = (Map<String,Object>)m.get('fee_detail_dict');

        //string rateScheduleFeeAmountString='';
        List<ColumnMetadata> columnMetaDataList = new List<ColumnMetadata>();
        if (rateScheduleFeeAmountMap.size() > 0) {
            for (String key : rateScheduleFeeAmountMap.keySet()) {
                ColumnMetadata cm = new ColumnMetadata(key, String.valueOf(rateScheduleFeeAmountMap.get(key)));
                columnMetaDataList.add(cm);
            }
            //return currentLead.Fee_Calculation_Response__c;
            return JSON.serialize(columnMetaDataList);
        }

        return null;

    }

    /**
     * @description This method builds a map of response field names to printable names.
     * @param licenseClassId
     */
    private static void buildPrintableLabelsMap(Id licenseClassId){
        List<License_Class_Response_Info__c> responseFieldList = [Select Id,Response_Field_Name__c, Printable_Name__c FROM License_Class_Response_Info__c where License_Class__c =: licenseClassId];

        for (License_Class_Response_Info__c eachField: responseFieldList){
            printableLabelsMap.put(eachField.Response_Field_Name__c, eachField.Printable_Name__c);
        }
    }

    /**
    * @description This class represents the Rate Schedule Fee Amounts retruned by the DSl
    */
    public class ColumnMetadata {
        private String field_api_name;
        private String field_label;
        private String field_value;
        private String warning_message;
        private String hasMessage;

        public ColumnMetadata(String fieldAPIName, String fieldValue){

            //set the field_api_name to the printable label if found, otherwise remove all underscores.
            field_api_name  = fieldAPIName;
            if (printableLabelsMap.containsKey(field_api_name)){
                field_label = printableLabelsMap.get(field_api_name);
            }else {
                field_label = fieldAPIName.replaceAll('_', ' ');
            }
            //if field does not contains "_Count" then format as currency, otherwise format as number.
            if (!field_api_name.contains('_Count')) {
                field_value = fieldValue.rightPad(2, '0');
                String[] feeParts = field_value.split('\\.');
                if (fieldValue.contains('.')) {
                    feeParts[1] = feeParts[1].rightPad(2, '0');
                }
                else {
                    feeParts.add('00');
                }

                String currencyTemplate = '$ {0}.{1}';

                if (fieldAPIName != 'Warning_Message') {
                    String formattedCurrency = String.format(currencyTemplate, feeParts);
                    field_value = formattedCurrency.replaceAll(('(?<!,\\d)(?<=\\d)(?=(?:\\d\\d\\d)+\\b)'), ',');
                }
                else{
                    field_value = fieldValue;
                }
            }
            else {
                field_value = fieldValue.replaceAll(('(?<!,\\d)(?<=\\d)(?=(?:\\d\\d\\d)+\\b)'),',');
            }
        }
    }

}