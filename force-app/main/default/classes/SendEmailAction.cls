/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer   Description                                                             *
*   ----------  ----------- ------------------------------------------------------------------------*
*   2020-01-08   Xede       Initial Creation                                                        *
*                                                                                                   *
*****************************************************************************************************
*/

/**
 * @description Contains an InvocableMethod to send an email
 * @author Xede Consulting Group (Jason Burke)
 * @date 01-08-2020
 */
public with sharing class SendEmailAction {

    /**
     * @description Sends an email for the given targetObjectId and whatId
     *
     * @param inputList List of email input objects
     *
     * @return List of email output objects
     */
    @InvocableMethod(label='Send Email' description='Sends an email for the given targetObjectId and whatId')
    public static List<SendEmailOutput> sendEmail(List<SendEmailInput> inputList) {
        List<Messaging.Email> emailList = new List<Messaging.Email>();

        for (SendEmailInput input : inputList) {
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setTargetObjectId(input.targetObjectId);
            email.setWhatId(input.whatId);
            email.setToAddresses(new String[] { input.emailAddr });
            email.setSubject(input.emailSubject);
            email.setHtmlBody(input.emailBody);

            emailList.add(email);
        }

        List<SendEmailOutput> outputList = new List<SendEmailOutput>();
        Messaging.SendEmailResult[] resultList = Messaging.sendEmail(emailList);
        for (Messaging.SendEmailResult result : resultList) {
            outputList.add(new SendEmailOutput(result));
        }

        return outputList;
    }

    /**
     * @description Holds the data needed as inputs for the InvocableMethod
     */
    public class SendEmailInput {
        @InvocableVariable
        public Id targetObjectId;

        @InvocableVariable
        public Id whatId;

        @InvocableVariable
        public String emailAddr;

        @InvocableVariable
        public String emailSubject;

        @InvocableVariable
        public String emailBody;
    }

    /**
     * @description Class to hold the Messaging.SendEmailResult data, for use as the return class for the InvocableMethod
     */
    public class SendEmailOutput {
        @InvocableVariable
        public Boolean isSuccess;

        @InvocableVariable
        public SendEmailError[] errors;

        public SendEmailOutput(Messaging.SendEmailResult result) {
            isSuccess = result.isSuccess();
            errors = new List<SendEmailError>();
            for (Messaging.SendEmailError error : result.getErrors()) {
                errors.add(new SendEmailError(error));
            }
        }
    }

    /**
     * @description Class to hold the Messaging.SendEmailError data, for use in the return class for the InvocableMethod
     */
    public class SendEmailError {
        @InvocableVariable
        public String[] fields;

        @InvocableVariable
        public String message;

        @InvocableVariable
        public System.StatusCode statusCode;

        @InvocableVariable
        public String targetObjectId;

        public SendEmailError(Messaging.SendEmailError error) {
            fields = error.getFields();
            message = error.getMessage();
            statusCode = error.getStatusCode();
            targetObjectId = error.getTargetObjectId();
        }
    }
}