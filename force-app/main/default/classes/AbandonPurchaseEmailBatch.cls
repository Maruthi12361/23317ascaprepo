/**
 * Created by johnreedy on 2019-02-21.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer              Description                                                  *
*   ----------  ---------------------  -------------------------------------------------------------*
*   2019-02-21  Xede Consulting Group  Initial Creation                                             *
*   2019-10-01  Xede Consulting Group  Qualified OrgWideEmail query to use custom label.            *
*                                                                                                   *
*  Code Coverage: 93.3%                                                                             *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This batch class captures all Opportunities that were created by the Self-service
* Licensing app where the user abandoned the process before making a final payment.
* @author Xede Consulting Group
* @see AbandonPurchaseEmailBatchTest
* @date 2019-02-21 
*/
global class AbandonPurchaseEmailBatch implements Database.Batchable<sObject>, Database.Stateful {

    public string query;
    boolean emailListSuccess = false;
    list<Opportunity> sentEmailLeadList = new list<Opportunity>();
    integer recordsFound = 0;

    /**
    * @description This constructor accepts a custom query.
    * @param  queryString
    */
    global AbandonPurchaseEmailBatch(string queryString) {
        query = queryString;
        system.debug('INFO: Query String passed in> ' + query);
    }

    /**
    * @description This is the default constructor and constructs a query string to get all Self-service leads
    * abandoned after converting to an Account, Contact, and Opportunity in the last 30 minutes.
    */
    global AbandonPurchaseEmailBatch() {
        //get datetime - 30 minutes
        string thirty_minutes_ago = Datetime.now().addMinutes(-integer.valueOf(label.Lead_Abandon_Email_Time_Interval)).formatGMT('yyyy-MM-dd\'T\'HH:mm:ss');

        /*
        Get all Lead records where the lead source is 'Self-service' the Abandon Email Sent is false, the StageName is
        the Self-service StageName and the Created Date is more than 30 minutes ago.
        */
        query  = 'Select Id, StageName, CreatedDate, Abandon_Email_Sent_Date__c ' +
                '   From Opportunity ' +
                '  Where StageName = \'' + label.Opportunity_SelfService_StageName +  '\' ' +
                '    And CreatedDate <= ' + thirty_minutes_ago  + 'Z ' +
                '    And Abandon_Email_Sent_Date__c = Null';

        system.debug('INFO: query> ' + query);
    }

    /**
    * @description This method returns a Database.QueryLocator object that is passed to the execute method.
    * @params Batchable Context
    * @return Query Locator
    */
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    /**
    * @description This method executes for each set of records returned by the query.
    * @params Batchable Context
    * @params Scope of records returned by query
    */
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        recordsFound = scope.size();

        //Get Abandon Email Template
        EmailTemplate abandonEmailTemplateId = [
                SELECT id, Name, DeveloperName
                  FROM EmailTemplate
                 WHERE DeveloperName =: label.Lead_Abandon_Email_Template_DeveloperName
                 LIMIT 1];

        //Get Organization Wide Default Email Address
        OrgWideEmailAddress organizationWideEmail = [
                SELECT Id, Address, DisplayName
                  FROM OrgWideEmailAddress
                 WHERE DisplayName =: label.Licensing_OrgWide_Email
                 LIMIT 1];

        //Create set of Opportunity Ids to which the converted Leads will be related
        set<id> opportunityIdSet = new set<id>();
        for (sObject s : scope) {
            Opportunity each = (Opportunity) s;
            opportunityIdSet.add(each.Id);
        }

        //get list of related converted leads
        List<Lead> convertedLeadList = [
                SELECT Id, Email, ConvertedOpportunityId
                  FROM Lead
                 WHERE IsConverted = True
                   AND EmailBouncedDate = null
                   AND ConvertedOpportunityId in: opportunityIdSet];

        /*
        Iterate over list of converted leads returned in the convertedLeadList and send the contact an email.
        */
        List<Messaging.SingleEmailMessage> abandonEmailList = new List<Messaging.SingleEmailMessage>();
        for (Lead each: convertedLeadList){

            //Instantiate new email message and set values
            Messaging.SingleEmailMessage newAbandonEmail = new Messaging.SingleEmailMessage();
            newAbandonEmail.setOrgWideEmailAddressId(organizationWideEmail.Id);
            newAbandonEmail.setTargetObjectId(each.Id);
            newAbandonEmail.setTemplateID(abandonEmailTemplateId.Id);
            newAbandonEmail.setWhatID(each.ConvertedOpportunityId);
            abandonEmailList.add(newAbandonEmail);

            //update Opportunity's Abandon Email Sent Date field
            Opportunity updatedOpportunity = new Opportunity();
            updatedOpportunity.Id = each.ConvertedOpportunityId;
            updatedOpportunity.Abandon_Email_Sent_Date__c =  Datetime.now().format('yyyy-MM-dd\'T\'HH:mm:ss','America/New_York');
            sentEmailLeadList.add(updatedOpportunity);

        }

        //Send emails to list of Leads who abandoned self-service form
        try {
            Messaging.SendEmail(abandonEmailList);
            emailListSuccess = true;
        }
        catch(Exception ex){
            system.debug('ERROR: An Error occurred while trying to send list of emails.');
            system.debug('ERROR: Email List> ' + abandonEmailList);
            system.debug('ERROR MESSAGE: ' + ex.getMessage());
        }

    }

    /**
    * @description This method executes after all batches are processed.
    * @params Batchable Context
    */
    global void finish(Database.BatchableContext BC) {

        //If emails were sent without erorr, then update the Lead's Abandon Email Sent Date
        if (emailListSuccess == true){
            Database.SaveResult[] saveResultsList = Database.update(sentEmailLeadList);
            SaveResultsHelper saveResults = new SaveResultsHelper(saveResultsList);
        }
        else{
            if (recordsFound == 0){
                system.debug('INFO: No records were returned by the query.');

            }
            else{
                system.debug('ERROR: An error was encountered while sending the list of emails.');
            }
        }
    }
}