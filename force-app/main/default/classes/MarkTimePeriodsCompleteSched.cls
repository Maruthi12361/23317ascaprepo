/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-11-14   Xede Consulting Group 		Initial Creation                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This schedulable class calls the MarkTimePeriodsCompleteBatch class.
* @author Xede Consulting Group
* @date 2019-11-14 
*/
global class MarkTimePeriodsCompleteSched implements Schedulable {
    /**
    * @description This method executes the MarkTimePeriodsCompleteBatch process.
    * @param SC
    */
    global void execute(SchedulableContext SC) {
        MarkTimePeriodsCompleteBatch controller = new MarkTimePeriodsCompleteBatch();
        database.executeBatch(controller);
    }
}