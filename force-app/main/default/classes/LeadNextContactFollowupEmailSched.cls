/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-02-15   Xede Consulting Group 		Initial Creation                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This schedulable class calls the LeadNextContactFollowupEmailBatch process.
* @author Xede Consulting Group
* @date 2019-02-15
*/
global class LeadNextContactFollowupEmailSched implements Schedulable {
    global void execute(SchedulableContext ctx){
        LeadNextContactFollowupEmailBatch b = new LeadNextContactFollowupEmailBatch();
        database.executeBatch(b);
    }
}