/**
 * Created by Xede
 */

/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer   Description                                                             *
*   ----------  ----------- ------------------------------------------------------------------------*
*   2018-10-06  Xede  		Initial Creation                                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description Apex controller class for lightning component
* @author Xede Consulting Group
* @see LicensePolicyControllerTest for test class
* @date 2018-10-06
*/
public class LicensePolicyController_V2 {
    /**
    * @description Get the list of license class names
    * @author Xede Consulting Group
    * @param  NA  No input parameters
    * @return  List<String>     List of license class name
    * @date 2018-10-06
    */
    @AuraEnabled
    public static List<String> getLicenseClassList() {
        List<String> licenseClassNameList = new List<String>();

        for(License_Class__c licenseClass : [select Id,Name from License_Class__c]){
            licenseClassNameList.add(licenseClass.Name);
        }
        return licenseClassNameList;
    }
    /**
    * @description Get detail about a particular license policy
    * @author Xede Consulting Group
    * @param  String  License_Policy__c record id
    * @return  License_Policy__c     Record details for the id
    * @date 2018-10-06
    */
    @AuraEnabled
    public static License_Policy__c getLicensePolicy(String recordId) {
        String licPolicyQuery=QueryUtility.getObjectFieldsQuery(GlobalConstant.OBJ_LICENSE_POLICY) +' From License_Policy__c where Id=:recordId  ' ;
        System.debug('\n licPolicyQuery=='+licPolicyQuery);
        List<License_Policy__c> licensePolicyList = (List<License_Policy__c>)Database.Query(licPolicyQuery);
        License_Policy__c licensePolicy = licensePolicyList != null && !licensePolicyList.isEmpty() ? licensePolicyList[0] : null;
        return licensePolicy;
    }
    /**
    * @description Get detail about a particular license policy time period
    * @author Xede Consulting Group
    * @param  String  License_Policy_Time_Period__c record id
    * @return  License_Policy_Time_Period__c     Record details for the id
    * @date 2018-10-06
    */
    @AuraEnabled
    public static License_Policy_Time_Period__c getLicensePolicyTimePeriod(String recordId) {
        String licPolicyTimePeriodQuery=QueryUtility.getObjectFieldsQuery(GlobalConstant.OBJ_LICENSE_POLICY_TIME_PERIOD) + ' From License_Policy_Time_Period__c where Id=:recordId  ' ;
        System.debug('\n licPolicyTimePeriodQuery=='+licPolicyTimePeriodQuery);
        List<License_Policy_Time_Period__c> licensePolicyTimePeriodList = (List<License_Policy_Time_Period__c>)Database.Query(licPolicyTimePeriodQuery);
        License_Policy_Time_Period__c licensePolicyTimePeriod = licensePolicyTimePeriodList != null && !licensePolicyTimePeriodList.isEmpty() ? licensePolicyTimePeriodList[0] : null;

        return licensePolicyTimePeriod;
    }
    /**
    * @description Get the wrapper form for lightning component for a given License Policy Id
    * @author Xede Consulting Group
    * @param  String  License_Policy__c record id
    * @return  LicensePolicyForm  Wrapper form for the component
    * @date 2018-10-06
    */
    @AuraEnabled
    public static LicensePolicyForm getLicensePolicyForm(String recordId) {
        License_Policy__c licensePolicy = getLicensePolicy(recordId);
        String licenseClassName = licensePolicy.License_Class_Type__r.Name;

        LicenseClassConfig objPolicy;
        LicenseClassConfig_V2 objPolicyV2;

        boolean digital=false;
        if(licenseClassName=='Digital_1'){
            objPolicyV2 = GetLicenseClassParameters.GetLicenseClassConfigV2Info(licenseClassName);
            digital = true;
        }
        else{
            objPolicy = GetLicenseClassParameters.GetLicenseClassInfo(licenseClassName);
        }

        //LicenseClassConfig objPolicy = GetLicenseClassParameters.GetLicenseClassInfo(licenseClassName);

        LicensePolicyForm form = new LicensePolicyForm();
        form.licenseClassName = licenseClassName;

        List<LicenseClassConfig.Policy_parameter_list> paramList = objPolicy != null ? objPolicy.Policy_parameter_list : new List<LicenseClassConfig.Policy_parameter_list>();


        if(digital){
            LicenseClassConfig.Policy_parameter_list param;
            List<LicenseClassConfig_V2.Policy_parameter_list> paramV2List = objPolicyV2.Policy_parameter_list;
            for(LicenseClassConfig_V2.Policy_parameter_list param2: paramV2List){
                param = new LicenseClassConfig.Policy_parameter_list();
                param.container_name = param2.container_name;
                param.data_type = param2.data_type;
                param.name = param2.name;
                param.parameter_type = param2.parameter_type;
                paramList.add(param);

            }

        }

        form.Fields = getLicenseClassAttributes(licenseClassName, paramList);
        form.Record = licensePolicy;

        form =getLicensePolicyForm(licenseClassName, form,paramList);

        return form;
    }


    /**
     * @description Get the wrapper form for lightning component by passing the license class name
     * @author Xede Consulting Group
     * @param  String  License class name
     * @return  LicensePolicyForm  Wrapper form for the component
     * @date 2018-10-06
     */

    @AuraEnabled
    public static LicensePolicyForm getForm(String licenseClassName) {


        LicenseClassConfig objPolicy;
        LicenseClassConfig_V2 objPolicyV2;
        boolean digital=false;
        if(licenseClassName=='Digital_1'){
            objPolicyV2 = GetLicenseClassParameters.GetLicenseClassConfigV2Info(licenseClassName);
            digital = true;
        }
        else{
            objPolicy = GetLicenseClassParameters.GetLicenseClassInfo(licenseClassName);
        }


        LicensePolicyForm form = new LicensePolicyForm();
        form.licenseClassName = licenseClassName;

        List<LicenseClassConfig.Policy_parameter_list> paramList = objPolicy.Policy_parameter_list;

        if(digital){
            LicenseClassConfig.Policy_parameter_list param;
            List<LicenseClassConfig_V2.Policy_parameter_list> paramV2List = objPolicyV2.Policy_parameter_list;
            for(LicenseClassConfig_V2.Policy_parameter_list param2: paramV2List){
                param = new LicenseClassConfig.Policy_parameter_list();
                param.container_name = param2.container_name;
                param.data_type = param2.data_type;
                param.name = param2.name;
                param.parameter_type = param2.parameter_type;
                paramList.add(param);

            }

        }


        form =getLicensePolicyForm(licenseClassName, form,paramList);
        //form.Fields = getLicenseClassAttributes(licenseClassName, paramList);
        //form.Record = getRecord(recordId, objectName, form.Fields);
        return form;
    }

    /**
    * @description Get the wrapper form for lightning component by passing the license class name
    * @author Xede Consulting Group
    * @param  String  String format of the submitted form wrapper class
    * @param  String  License policy record id
    * @return  LicensePolicyForm  Wrapper form for the component
    * @date 2018-10-06
    */

    @AuraEnabled
    public static String sendForm(String submittedForm, String id) {
        System.debug('\n id==' + id);
        License_Policy__c licensePolicy = getLicensePolicy(id);
        //lc = getLicensePolicyTimePeriod(id);
        String licenseClassType = licensePolicy.License_Class_Type__r.Name;
        //System.debug('\n licenseClassType='+licenseClassType);

        String retVal = '{"License_Policy":{';
        retVal+=' "Id":"'+id+'",';
        retVal+=' "Name":"'+licensePolicy.Name+'",';
        retVal+=' "License_Type":"'+licenseClassType+'",';
        try {
            List<LicenseClassField> result = (List<LicenseClassField>) JSON.deserialize(submittedForm, List<LicenseClassField>.class);
            for (LicenseClassField myform : result) {
                System.debug('\n myform==' + myform);
                retVal += '"' + myform.name + '":' + myform.input_value + ',';
            }
            retVal = retVal.removeEnd(',');
            retVal += '},"Time_Period":{"type":"Annual","period":"2018-12-31"}';
        } catch (Exception ex) {
            System.debug('\n ex=' + ex.getMessage());
        }
        retVal += '}';
        System.debug('\n retVal' + retVal);

        License_Policy_Time_Period__c licPolicyTimePeriod = new License_Policy_Time_Period__c();
        licPolicyTimePeriod.License_Policy__c = licensePolicy.Id;
        licPolicyTimePeriod.Transaction_Date__c= System.today();
        try{
            licPolicyTimePeriod.Fee_Calculation_Request_String__c = retVal;
            LicenseClassFeeResponse objFeeResponse = GetLicenseClassParameters.GetLicenseClassFeeResult(retVal);
            licPolicyTimePeriod.Fee_Calculation_Reponse_String__c = JSON.serialize(objFeeResponse);

            licPolicyTimePeriod.License_Policy_Fee_Amount__c = objFeeResponse.lfaftp;
        }
        catch (Exception ex) {
            System.debug('\n ex=' + ex.getMessage());
            licPolicyTimePeriod.Fee_Calculation_Request_String__c=retVal;
            licPolicyTimePeriod.Fee_Calculation_Reponse_String__c = ex.getMessage();
            licPolicyTimePeriod.License_Policy_Fee_Amount__c =0;
        }
        System.debug('\n amount=='+licPolicyTimePeriod.License_Policy_Fee_Amount__c );
        insert licPolicyTimePeriod;


        return 'Fee Amount is $'+licPolicyTimePeriod.License_Policy_Fee_Amount__c;
    }

    /**
    * @description Get the license policy attributes from the web service call
    * @author Xede Consulting Group
    * @param  String  License class name
    * @param  List<LicenseClassConfig.Policy_parameter_list>  List of parameters retrieved for the license class from web service call
    * @return  List<LicenseClassField>   List of Wrapper for each field
    * @date 2018-10-06
    */

    private static List<LicenseClassField> getLicenseClassAttributes(String licClassName, List<LicenseClassConfig.Policy_parameter_list> paramList) {
        List<LicenseClassField> fields = new List<LicenseClassField>();
        for (LicenseClassConfig.Policy_parameter_list each : paramList) {

            system.debug('\n jwr-Debug: each> ' + each.name);
            boolean dbReq = each.parameter_type == 'payment' ? false : true;
            if (dbReq) {
                String fieldLabel = each.name.replaceAll('_', ' ');
                String fieldType = each.data_type;

                LicenseClassField licenseClassField = new LicenseClassField(each.name, fieldLabel, dbReq, fieldType, each.container_name);
                licenseClassField.licenseClassName = licClassName;
                if(fieldLabel.containsIgnoreCase('Month Count')){
                    licenseClassField.lowerBound =1;
                    licenseClassField.UpperBound =12;
                }
                fields.add(licenseClassField);
            }

        }

        return fields;
    }

    /**
* @description Populate the fields in different containsers. For now limiting to fields with container license_policy and premise_list
* @author Xede Consulting Group
* @param  String  License class name
* @param  LicensePolicyForm  Container for all the fields with different container field list
* @param  List<LicenseClassConfig.Policy_parameter_list>  List of parameters retrieved for the license class from web service call
* @return  List<LicenseClassField>   List of Wrapper for each field
* @date 2018-10-06
*/

    private static LicensePolicyForm getLicensePolicyForm(String licClassName, LicensePolicyForm form, List<LicenseClassConfig.Policy_parameter_list> paramList) {
        List<LicenseClassField> fields = new List<LicenseClassField>();
        List<LicenseClassField> premiseFields = new List<LicenseClassField>();
        for (LicenseClassConfig.Policy_parameter_list each : paramList) {

            system.debug('\n jwr-Debug: each> ' + each.name);
            boolean dbReq = each.parameter_type == 'payment' ? false : true;
            String containerName=each.container_name;
            if (dbReq) {
                String fieldLabel = each.name.replaceAll('_', ' ');
                String fieldType = each.data_type;

                LicenseClassField licenseClassField = new LicenseClassField(each.name, fieldLabel, dbReq, fieldType, each.container_name);
                licenseClassField.licenseClassName = licClassName;
                if(fieldLabel.containsIgnoreCase('Month Count')){
                    licenseClassField.lowerBound =1;
                    licenseClassField.UpperBound =12;
                }
                //Split the fields between different containers
                //Set fields for License_Policy container
                if(containerName.equalsIgnoreCase('License_Policy')){
                    fields.add(licenseClassField);

                }else if(containerName.equalsIgnoreCase('Premise')){
                    premiseFields.add(licenseClassField);
                }

            }

        }
        form.fields = fields;
        form.premiseFields = premiseFields;


        return form;
    }
}