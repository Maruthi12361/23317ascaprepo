/**
 * Created by johnreedy on 2019-01-24.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-01-24  Xede Consulting Group       Initial Creation                                        *
*   2019-01-31  Xede Consulting Group       Added Test.isRunningTest, so that mock responses can be *
*                                           returned to test class methods.                         *
*   2019-07-26  Xede Consulting Group       Removed Invalid Address Queue Assignment AO-5649        *
*   2019-10-07  Xede Consulting Group       Validates mailing and premise addresses independently   *
*   2019-10-16  Xede Consulting Group       Refactored logic into AddressHelper classes             *
*                                                                                                   *
*   Code Coverage: 100%                                                                             *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This class contains the callout methods to the MelissaData Address Standardization/Verification service.
* @author Xede Consulting Group
* @see MelissaDataWebServiceCalloutTest
* @date 2019-01-24 
*/
public with sharing class MelissaDataWebServiceCallout {
    /**
    * @description This method makes a future callout to MelissaData to standardize Lead Addresses.
    * @param entityAddrLeadIdSet - set of Lead Ids with modified entity (mailing) addresses
    * @param premiseAddrLeadIdSet - set of lead ids with modified premise addresses
    */
    @future (callout = true)
    public static void validateLeadAddress(Set<Id> entityAddrLeadIdSet, Set<Id> premiseAddrLeadIdSet) {
        //setting LeadHelper property to prevent on update, the method from being called again (from a future method)
        LeadHelper.FutureMethodCalled = true;

        validateAddress(new LeadAddressHelper(entityAddrLeadIdSet, premiseAddrLeadIdSet));
    }

    /**
     * @description calls the MelissaDataWebServiceCallout to validate Billing and Business (shipping) addresses
     *
     * @param billAddrAcctIdSet The ids of the accounts with changed Billing addresses
     * @param busAddrAcctIdSet The ids of the accounts with changed Business (shipping) addresses
     */
    @Future(Callout=true)
    public static void validateAccountAddress(Set<Id> billAddrAcctIdSet, Set<Id> busAddrAcctIdSet) {
        AccountHelper.FutureMethodCalled = true;

        validateAddress(new AccountAddressHelper(billAddrAcctIdSet, busAddrAcctIdSet));
    }

    /**
     * @description Loops through the SObjects in the addrHelper and performs a callout to MelissaData for each one that needs it
     *
     * @param addrHelper An AddressHelper concrete subclass instance
     */
    private static void validateAddress(AddressHelper addrHelper) {
        // query all the records that have changed addresses
        SObject[] sObjList = addrHelper.querySObjects();

        // loop through all the SObjects
        for (SObject sObj : sObjList) {
            // loop through all the address types
            String[] addrTypeList = addrHelper.getAddrTypeList();
            for (String addrType : addrTypeList) {
                // if this address needs MelissaData to validate it, perform a callout to its web service
                if (addrHelper.isCalloutNeeded(sObj, addrType)) {
                    MelissaDataResponse addrResp = performCallout(sObj.Id, addrHelper.getStreet(sObj, addrType), addrHelper.getCity(sObj, addrType), addrHelper.getState(sObj, addrType),
                                                                    addrHelper.getPostalCode(sObj, addrType), addrHelper.getCompany(sObj, addrType));

                    // update the SObject with the MelissaData response
                    // note: addrHelper stores the updated SObjects internally
                    addrHelper.updateSObject(sObj, addrType, addrResp);
                }
            }
        }

        //setting dml options, so that on update, the duplicate rules can be overridden
        Database.DMLOptions dmo = new Database.DMLOptions();
        dmo.DuplicateRuleHeader.allowSave = true;
        dmo.DuplicateRuleHeader.runAsCurrentUser = true;

        // get the updated SObjects from the addrHelper and save them
        Database.SaveResult[] saveResultList = Database.update(addrHelper.getUpdatedSObjects(), dmo);
        SaveResultsHelper saveResults = new SaveResultsHelper(saveResultList);
    }

    /**
     * @description calls the MelissaData web service to validate the given address
     *
     * @param rec the record id of the address' object
     * @param street
     * @param city
     * @param state
     * @param postalCode
     * @param company
     *
     * @return the web service response in a helper object
     */
    private static MelissaDataResponse performCallout(String rec, String street, String city, String state, String postalCode, String company) {
        if (String.isBlank(street)) {
            street = 'Not Provided';
        }
        if (String.isBlank(city)) {
            city = 'Not Provided';
        }
        if (String.isBlank(state)) {
            state = '';
        }
        if (String.isBlank(postalCode)) {
            postalCode = '';
        }
        if (String.isBlank(company)) {
            company = '';
        }

        String addrStr = '&rec' + rec + '&a1=' + street + '&city=' + city + '&state=' + state + '&postal=' + postalCode + '&company=' + company + '&format=JSON';
        // replace all blocks of whitespace with a url-encoded blank space
        addrStr = addrStr.replaceAll('\\s+', '%20');

        //Get MelissaData Properties from custom setting
        MelissaDataProperties__c mdp = MelissaDataProperties__c.getInstance();

        //Build http request and make callout
        HttpRequest req = new HttpRequest();
        req.setMethod(GlobalConstant.HTTP_GET_METHOD);
        req.setEndpoint(mdp.HTTP_End_Point__c + mdp.Security_Token__c + addrStr);

        //send request
        HttpResponse resp = new Http().send(req);

        // return the parsed response
        return MelissaDataResponse.parse(resp.getBody());
    }


}