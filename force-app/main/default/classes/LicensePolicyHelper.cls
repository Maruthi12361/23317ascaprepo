/**
 * Created by johnreedy on 2019-06-06.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-06-06   Xede Consulting Group      Initial Creation                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This helper class supports operations against the License Policy Object.
* @author Xede Consulting Group
* @see LicensePolicyHelperTest
* @date 2019-06-06 
*/
public with sharing class LicensePolicyHelper {

    public static Boolean doCreateRetroTimePeriods = true;

    private static ErrorLogger logger = new ErrorLogger();

    /**
     * description This method publishes an event indicating that a DrawLoop document has been created.
     * @param licensePolicyList
     * @param oldMap
     */
    public static void publishDocGenEvent(List<License_Policy__c> licensePolicyList, Map<Id,License_Policy__c> oldMap){

        List<License_Policy_DocGen_Event__e> publishEventsList = new List<License_Policy_DocGen_Event__e> ();

        for (License_Policy__c eachPolicy: licensePolicyList){
            License_Policy_DocGen_Event__e docGenEvent = new License_Policy_DocGen_Event__e();
            docGenEvent.License_Policy_Id__c = eachPolicy.Id;
            docGenEvent.DocGen_Completed_Date_Time__c = string.valueOf(eachPolicy.Self_Service_DDP_Finish_Date_Time__c);

            //if inserting a License Policy, publish the created event, else populated the new document created event.
            if (oldMap == null ){
                docGenEvent.DocGen_Message__c = 'Document Created';
                publishEventsList.add(docGenEvent);
            }
            else if (eachPolicy.Self_Service_DDP_Finish_Date_Time__c != oldMap.get(eachPolicy.Id).Self_Service_DDP_Finish_Date_Time__c){
                docGenEvent.DocGen_Message__c = 'New Document Creation Complete';
                publishEventsList.add(docGenEvent);
            }
        }
        if (publishEventsList.size() > 0){
            List<Database.SaveResult> results = EventBus.publish(publishEventsList);
        }
    }
    /**
     * dewscription This method updates teh document generated flag.
     * @param licensePolicyList
     * @param oldMap
     */
    public static void populateDDPFlags(List<License_Policy__c> licensePolicyList, Map<Id,License_Policy__c> oldMap){

        License_Policy__c oldLicensePolicy;
        for (License_Policy__c eachPolicy: licensePolicyList){
            oldLicensePolicy = oldMap.get(eachPolicy.Id);
            //if the ddp finished date is null OR the doc gen datetime is greater than the finish datetime, or the
            // generation datetime has changed, set the flog to false.
            if(eachPolicy.Self_Service_DDP_Finish_Date_Time__c == null ||
                    eachPolicy.Self_Service_DDP_Generation_Date_Time__c > eachPolicy.Self_Service_DDP_Finish_Date_Time__c ||
                    eachPolicy.Self_Service_DDP_Generation_Date_Time__c != oldMap.get(eachPolicy.Id).Self_Service_DDP_Generation_Date_Time__c ){
                eachPolicy.Document_Generated__c = false;
            }
            //if the ddp finished date is not null and the agreement link has changed, set the flag to true
            if(eachPolicy.Self_Service_License_Agreement_Link__c != null &&
                    eachPolicy.Self_Service_License_Agreement_Link__c != oldLicensePolicy.Self_Service_License_Agreement_Link__c ){
                eachPolicy.Document_Generated__c = true;
            }
        }

    }

    /**
     * @description Creates retroactive time periods and transactions for fixed fee license policies when their status
     * changes to "Active".
     *
     * @param newPolicyList the newly updated policies
     * @param oldPolicyMap the map of pre-update policy versions
     */
    public static void createRetroTimePeriods(License_Policy__c[] newPolicyList, Map<Id, License_Policy__c> oldPolicyMap) {
        if (!doCreateRetroTimePeriods) {
            return;
        }

        Id fixedFeeRecordTypeId = Schema.SObjectType.License_Policy__c.getRecordTypeInfosByDeveloperName().get(GlobalConstant.FIXED_TYPE).getRecordTypeId();

        // collect all of the fixed fee policies whose status just changed to "Active"
        List<License_Policy__c> activePolicyList = new List<License_Policy__c>();
        for (License_Policy__c newPolicy : newPolicyList) {
            License_Policy__c oldPolicy = oldPolicyMap != null ? oldPolicyMap.get(newPolicy.Id) : null;

            if (newPolicy.Status__c == GlobalConstant.ACTIVE_LITERAL &&
                    newPolicy.RecordTypeId == fixedFeeRecordTypeId &&
                    (oldPolicy == null || oldPolicy.Status__c != GlobalConstant.ACTIVE_LITERAL))
            {
                activePolicyList.add(newPolicy);
            }
        }

        if (activePolicyList.isEmpty()) {
            return;
        }

        // set the start/end dates to the 1st and last days of the current month
        Date today = Date.today();
        Datetime startMonthlyDate = Datetime.newInstance(today.toStartOfMonth(), Time.newInstance(0, 0, 0, 0));
        Datetime endMonthlyDate = Datetime.newInstance(today.addMonths(1).toStartOfMonth().addDays(-1), Time.newInstance(0, 0, 0, 0));

        // map of policy id to the next future due date
        Map<Id, Date> policyIdDueDateMap = new Map<Id, Date>();
        // map of batch date to run to a list of policy ids that need that month's time period generated
        Map<Date, List<Id>> batchDatePolicyIdMap = new Map<Date, List<Id>>();

        for (License_Policy__c policy : [
                SELECT Start_Date__c,
                    (SELECT Start_Date__c, End_Date__c
                    FROM License_Policy_Time_Periods__r
                    WHERE Start_Date__c <= :endMonthlyDate.dateGmt()
                    AND End_Date__c >= :startMonthlyDate.dateGmt()),
                    (SELECT Start_Date__c, End_Date__c, Billing_Frequency__c, Payment_Due_Date_Type__c, Offset_Day_of_Month__c
                    FROM Fixed_Fee_Definitions__r
                    ORDER BY Start_Date__c ASC)
                FROM License_Policy__c
                WHERE Id IN :activePolicyList
                AND Start_Date__c <= :startMonthlyDate.dateGmt()
        ]) {
            try {
                // calculate the next future due date
                Date dueDate = MonthlyFixedFeeHelper.calculateTransactionDate(policy, startMonthlyDate, endMonthlyDate);
                Integer i = 0;
                while (dueDate <= today) {  // for example, this could happen if the due date is on the 1st of the month and today is the 4th.
                    // calculate again using the next month's start/end dates until we get a future date
                    i++;
                    dueDate = MonthlyFixedFeeHelper.calculateTransactionDate(policy, startMonthlyDate.addMonths(i), endMonthlyDate.addDays(1).addMonths(i).addDays(-1));
                }

                policyIdDueDateMap.put(policy.Id, dueDate);

                // add this policy for every batch date from the policy start date to the current month start date
                Date batchDate = policy.Start_Date__c;
                do {
                    if (!batchDatePolicyIdMap.containsKey(batchDate)) {
                        batchDatePolicyIdMap.put(batchDate, new List<Id>());
                    }
                    batchDatePolicyIdMap.get(batchDate).add(policy.Id);
                    batchDate = batchDate.addMonths(1);
                } while (batchDate <= startMonthlyDate.dateGmt());
            }
            catch (Exception e) {
                logger.log(e);
            }
        }

        // fire off the batches
        List<Date> batchDateList = new List<Date>(batchDatePolicyIdMap.keySet());

        if (!batchDateList.isEmpty()) {
            batchDateList.sort();
            Database.executeBatch(new MonthlyFixedFeeBatch(batchDateList, batchDatePolicyIdMap, policyIdDueDateMap));
        }

        logger.save();
    }
}