/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer   Description                                                             *
*   ----------  ----------- ------------------------------------------------------------------------*
*   2019-01-22   FaithKindle  		Initial Creation                                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description DuplicateRecordSet test class - creates test Account, test DuplicateRecordSet, test DuplicateRecordItem.
* @author Xede Consulting Group
* @date 2018-01-22
*/
@isTest
private class duplicateRecordSetTest {
    static testMethod void myTest() {   
        // Query an existing DuplicateRule in the organization. 
        DuplicateRule dr = [SELECT Id, IsActive FROM DuplicateRule WHERE IsActive=true LIMIT 1];
        System.assert(dr != null);
        
        // Create a test DuplicateRecordSet based on the queried DuplicateRule.
        DuplicateRecordSet testDRS = new DuplicateRecordSet();
        testDRS.DuplicateRuleId = dr.Id;
        insert testDRS;
       
        // Create a test Account.
        Account testAccount = new Account();
        testAccount.Name = 'Acme Test';
        insert testAccount;    
        
        // Query a test DuplicateRecordSet.
        DuplicateRecordSet testDRS2 = [SELECT Id, Name FROM DuplicateRecordSet 
                                WHERE Name!= null LIMIT 1];
        System.assert(testDRS2 != null);
        
        // Query the test account that was inserted.
        Account testAccount2 = [SELECT Id, Name FROM Account 
                                WHERE Name='Acme Test' LIMIT 1];
        System.assert(testAccount2 != null);
        
        //Insert a test DuplicateRecordItem
        DuplicateRecordItem testDRI = new DuplicateRecordItem();
        testDRI.DuplicateRecordSetId = testDRS2.Id;
        testDRI.RecordId = testAccount2.Id;
        insert testDRI;
    } 
}