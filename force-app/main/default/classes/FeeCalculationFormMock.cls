/**
 * Created by johnreedy on 2019-11-07.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-11-07   Xede Consulting Group 		Initial Creation                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description <description>
* @author Xede Consulting Group
* @see FeeCalculationFormMockTest
* @date 2019-11-07 
*/
@isTest
public class FeeCalculationFormMock implements HttpCalloutMock {

    private HttpCalloutMock MelissaDataValidResponseMock = new MelissaDataValidResponseMock();
    private HttpCalloutMock FeeCalculationRequestMock = new FeeCalculationResponseMock();

    private integer counter=0;

    public  HTTPResponse respond(HTTPRequest request) {
        if (counter == 0){
            counter ++;
            return MelissaDataValidResponseMock.respond(request);

        }
        else{
            return MelissaDataValidResponseMock.respond(request);

            //return FeeCalculationResponseMock.respond(request);
        }
    }
}