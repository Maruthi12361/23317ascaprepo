/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer         Description                                                       *
*   ----------  ----------------- ------------------------------------------------------------------*
*   2018-11-21  Xede Consulting   Initial Creation                                                  *
*   2018-12-07  Xede Consulting   Added trim functionality to string evaluation.                    *
*   2019-02-07  Xede Consulting   Added createRelatedRecordsOnConversion method.                    *
*   2019-03-11  Xede Consulting   Set Bypass_Duplicate_Rules__c to true, when creating an           *
*                                 Account.                                                          *
*   2019-10-02  Xede Consulting   Added assignRegionByZipcode() method.                             *
*   2019-10-07  Xede Consulting   Validates mailing and premise addresses independently             *
*                                 Does not use MelissaData to validate address                      *
*                                       if it is marked "Valid Address"                             *
*   2020-01-13   Xede Consulting   Add functionality to support list of lead sources for Knockout    *
*                                 criteria.                                                       *
*                                                                                                   *
*   Code Coverage: 99%                                                                              *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This helper class manages operations against the Lead object.
* @author Xede Consulting Group
* @date 2018-11-21
*/

public with sharing class LeadHelper {
    public static boolean isFirstTime = True;
    public static boolean FutureMethodCalled = False;

    /**
    * @description For records that are bulk loaded, or if the user is and API User, this method validates lead field
    * values against knockout criteria.
    * @param  leadList
    */
    public static void applyKnockoutCriteria(List<Lead> leadList) {

        //Get running user properties
        Id runningUserId = UserInfo.getUserId();
        User runningUser = [SELECT Id, Name, API_User_Indicator__c FROM User WHERE id = :runningUserId LIMIT 1];

        //Get knockout criteria properties related to the Lead object - should only every be one.
        List<Knockout_Criteria__mdt> knockoutCriteriaList = [
                SELECT Label
                     , API_User_Indicator__c
                     , Disqualification_Reason_Type__c
                     , Evaluation_API_Names__c
                     , Keywords__c
                     , Object_Name__c
                     , Record_Collection_Count__c
                     , Status_Type__c
                     , Lead_Source_List__c
                 FROM Knockout_Criteria__mdt
                WHERE Object_Name__c = :GlobalConstant.OBJ_LEAD
        ];

        /*
        If the LeadList.size() is greater than the Record_Collection_Count__c; or if the knockoutCriteria.API_User_indicator
        is true, and the running user's API_User_Indicator__c is also true; or knockoutCriteria.API_User_indicator is false,
        the then apply the knockout criteria.
         */
        Map<String, Set<string>> fieldsBySourceMap   = new Map<String, Set<string>>();
        Map<String, Set<string>> keyWordsBySourceMap = new Map<String, Set<string>>();
        Map<String, Knockout_Criteria__mdt> knockoutCriteriaMap = new Map<String, Knockout_Criteria__mdt>();

        for (Knockout_Criteria__mdt each : knockoutCriteriaList) {
            String[] leadSourceList = each.Lead_Source_List__c.split(';');

            for (String eachSource : leadSourceList) {
                if (leadList.size() > each.Record_Collection_Count__c || (each.API_User_Indicator__c == true
                        && runningUser.API_User_Indicator__c) || each.API_User_Indicator__c == false) {

                    //convert fieldNames to lowercase and add tp fieldNameSet
                    String[] leadFieldsToEvaluate = each.Evaluation_API_Names__c.split(';');
                    Set<string> fieldNameSet = new Set<string>();
                    for (String fieldName : leadFieldsToEvaluate) {
                        fieldNameSet.add(fieldName.toLowerCase());
                    }

                    //convert keywordStrings to lower case and add to keywordSet
                    String[] keywordStrings = each.Keywords__c.split(';');
                    Set<string> keywordSet = new Set<string>();
                    for (String keyword : keywordStrings) {
                        keywordSet.add(keyword.toLowerCase().trim());
                    }
                    fieldsBySourceMap.put(each.Lead_Source_List__c, fieldNameSet);
                    keyWordsBySourceMap.put(each.Lead_Source_List__c, keywordSet);
                    knockoutCriteriaMap.put(each.Lead_Source_List__c, each);
                }
            }
        }
        
        /*
        Iterate over list of leads, and for fields to be evaluated, compare its value to the keyword collection,
        and if found, raise an error.
        */
        Map<String, Schema.SObjectField> leadFieldMap = Schema.SObjectType.Lead.fields.getMap();
        for (Lead each : leadList) {
            for (String fieldName : leadFieldMap.keySet()) {
                //if a map contains the LeadSource and the size of their sets is greater than zero
                if (fieldsBySourceMap.containsKey(each.LeadSource) && fieldsBySourceMap.get(each.LeadSource).size() > 0 && fieldsBySourceMap.get(each.LeadSource).contains(fieldName)
                        && keyWordsBySourceMap.containsKey(each.LeadSource) && keyWordsBySourceMap.get(each.LeadSource).size() > 0) {
                    for (String searchValue : keyWordsBySourceMap.get(each.LeadSource)) {
                        if (String.isNotBlank(String.valueOf(each.get(fieldName))) && String.valueOf(each.get(fieldName)).trim().toLowerCase().contains(searchValue.toLowerCase())) {
                            each.Status = knockoutCriteriaMap.get(each.LeadSource).Status_Type__c;
                            each.Disqualification_Reason_Type__c = knockoutCriteriaMap.get(each.LeadSource).Disqualification_Reason_Type__c;
                            each.Knockout_Keyword__c = searchValue;
                            System.debug(
                                    '\n--------------------------------------------------------------------' +
                                    '\n        Info: A record meeting the \'' + searchValue + '\' keyword search value was found while inserting the following record: ' +
                                    '\n   Lead Name: ' + each.LastName + ', ' + each.FirstName +
                                    '\n     Company: ' + each.Company +
                                    '\n Reason Code: ' + String.valueOf(knockoutCriteriaMap.get(each.LeadSource).Disqualification_Reason_Type__c +
                                    '\n--------------------------------------------------------------------'));
                        }
                    }
                }
            }
        }
    }

    /**
    * @description Given a list bulk loaded records(list size > 1), the method sets the bypass duplicate rules flag to
    * true, so that the records will not be rejected on insert.
    * @param  leadList
    */
    public static void setBypassDuplicateRulesFlag (list<Lead> leadList) {

        //get guest user profile id, as this is the profile that will be used to insert Self-service venues upon conversion
        id guestUserProfileId = [Select id from Profile where Name = 'Guest License User' Limit 1].id;

        //get the current running users profile id
        id runningUserProfileId = UserInfo.getProfileId();

        /*
       Iterate over list of leads and set the bypass duplicate rules flag is set to true.  This will allow duplicates
       to at least be saved to Salesforce where they can be further evaluated.
        */
        System.debug('Debug: variable>' + runningUserProfileId);
        if (leadList.size() > 1 || guestUserProfileId == runningUserProfileId) {
            for (Lead each : leadList) {
                each.Bypass_Duplicate_Rules__c = True;
                System.debug('Debug: In for loop>');
            }
        }
    }

    /**
    * @description This method accepts a list of Leads and if the bypass duplicate rules flag set to true, it updates it
    * to be false, so that it can be evaluated for duplicates.
    * @param  leadList
    */
    public static void removeBypassDuplicateRulesFlag (list<Lead> leadList) {

        list<Lead> updateLeadList = new list<Lead>();
        Set<Id> includedLeadIds = new Set<Id>();

        /*
        Iterate over list of leads and if the bypass duplicate rules flag is set to true, set it to false.
         */
        for (Lead each: leadList) {
            Lead bypassedLead = new Lead();
            bypassedLead.id = each.Id;
            if (each.Bypass_Duplicate_Rules__c == True){
                bypassedLead.Bypass_Duplicate_Rules__c = False;
                if(each.Id != null && each.Encrypted_Record_Id__c == null){
                    bypassedLead.Encrypted_Record_Id__c = ASCAPUtils.getCleanedEncryptedValue(each.Id);
                }
                updateLeadList.add(bypassedLead);
                includedLeadIds.add(each.Id);
            }else{
                if(each.Id != null && each.Encrypted_Record_Id__c == null &&  !includedLeadIds.contains(each.Id)){
                    bypassedLead.Encrypted_Record_Id__c = ASCAPUtils.getCleanedEncryptedValue(each.Id);
                    updateLeadList.add(bypassedLead);
                }
            }
        }

        Database.SaveResult[] saveResultList = Database.update(updateLeadList, false);
        SaveResultsHelper saveResults = new SaveResultsHelper(saveResultList);

    }
    /**
   * @description  New Media wants Company_Name__c populated with the value from Service_Name__c if the following is true
   * Status is being changed to converted &&
   * ISBLANK(Company_Name__c) &&
   * Lead.RecordType.DeveloperName == 'New_Media_Leads'
   * @param  leadList
   * @param  oldMap
   */
    public static void beforeConversionUpdates (list<Lead> leadList, map<id,Lead> oldMap) {
        Id newMediaRecTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('New Media').getRecordTypeId();
        for (Lead each: leadList) {
            Lead oldLead = oldMap.get(each.Id);
            if(each.Status == 'Converted') {
                if(each.RecordTypeId == newMediaRecTypeId && String.isBlank(each.Company_Name__c) &&  String.isNotBlank(each.Service_Name__c) ) {
                    each.Company_Name__c = each.Service_Name__c;
                }
            }
        }
    }
    /**
    * @description This method accepts a list of Leads and then builds a set of newly created lead ids and passes them
    * to the address standardization service.
    * @param  leadList
    * @param  oldMap
    */
    public static void validateAddress (list<Lead> leadList, map<id,Lead> oldMap) {
        new LeadAddressHelper().validateAddresses(leadList, oldMap);
    }
    /**
    * @description This method populates creates an encrypted record id
    * to the address standardization service.
    * @param  leadList
    * @param  oldMap
    */
    public static void assignEncryptedRecordId(list<Lead> leadList, map<id, Lead> oldMap) {

        for(Lead eachLead : leadList){
            if(eachLead.Id != null && eachLead.Encrypted_Record_Id__c == null){
                eachLead.Encrypted_Record_Id__c=ASCAPUtils.getCleanedEncryptedValue(eachLead.Id);
            }
        }

    }
    public static void checkForAddressChange(list<Lead> leadList, map<id,Lead> oldMap) {
        new LeadAddressHelper().checkForAddressChanges(leadList, oldMap);
    }

    /**
     * @description This method is called Before Insert and Before Update.  It assigns the Region to the Lead
     * based on the Premise Zip Code, using the mappings in the Zipcode Mapping custom object.
     * @param leadList
     * @param oldMap
    */
    public static void assignRegionByZipcode(List<Lead> newLeadList, Map<Id, Lead> oldLeadMap) {
        List<Lead> leadsToUpdateList = new List<Lead>();
        Set<string> zipcodeSet = new Set<string>();

        // collect all of the leads that have changed premise zipcodes and their zipcodes too
        for (Lead newLead : newLeadList) {
            String newZipcode5 = formatZipcode5(newLead.Premise_Zip_Code__c);

            Lead oldLead = oldLeadMap != null ? oldLeadMap.get(newLead.Id) : null;
            String oldZipcode5 = oldLead != null ? formatZipcode5(oldLead.Premise_Zip_Code__c) : '';

            if (newZipcode5 != oldZipcode5) {
                leadsToUpdateList.add(newLead);
                zipcodeSet.add(newZipcode5);
            }
        }

        // query for the region mappings of the collected zipcodes
        Map<String, String> zipcodeRegionMap = new Map<String, String>();
        for (Zipcode_Mapping__c zipcodeMapping : [
                SELECT Name, Region__c FROM Zipcode_Mapping__c WHERE Name IN :zipcodeSet
        ]) {
            zipcodeRegionMap.put(zipcodeMapping.Name, zipcodeMapping.Region__c);
        }

        // update the region on the collected leads
        for (Lead leadToUpdate : leadsToUpdateList) {
            leadToUpdate.Region__c = zipcodeRegionMap.get(formatZipcode5(leadToUpdate.Premise_Zip_Code__c));
        }
    }

    /**
     * @description normalizes a zipcode string to 5 digits by removing the +4 digits and/or left padding with zeroes
     * @param zipcode
     * @return the formatted zip code
     */
    private static String formatZipcode5(String zipcode) {
        if (zipcode == null) {
            return '';
        }

        if (zipcode.contains('-')) {
            zipcode = zipcode.substringBefore('-');
        }

        if (zipcode.length() > 5) {
            zipcode = zipcode.substring(0, 5);
        }

        return zipcode.leftPad(5, '0');
    }
}