/**
 * Created by johnreedy on 2019-01-21.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-01-21   Xede Consulting Group 		Initial Creation                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This helper class supports the Database.SaveResult class.  When called from a helper class, this
* class manages reading through the save results and writing to the debug log.
* @author Xede Consulting Group
* @see SaveResultHelperTest
* @date 2019-01-21 
*/
public with sharing class SaveResultsHelper {
    Database.SaveResult saveResult;
    List<Database.SaveResult> saveResultList;

    public SaveResultsHelper(Database.SaveResult saveResult) {
        this.saveResult = saveResult;
        printResultToDebug();
    }

    public SaveResultsHelper(List<Database.SaveResult> saveResultList) {
        this.saveResultList = saveResultList;
        printToDebug();
    }

    void printToDebug() {
        for (Database.SaveResult each : saveResultList) {
            if (each.isSuccess())
                continue;

            for (Database.Error error : each.getErrors()) {
                System.debug(LoggingLevel.ERROR, '<ERROR>An Exception has occurred - ' + error.getStatusCode() + ' - ' + error.getMessage() + ' - ' + error.getFields() + '</ERROR>');
            }
        }
    }

    void printResultToDebug() {
        if (!saveResult.isSuccess()) {
            for (Database.Error error : saveResult.getErrors())
                System.debug(LoggingLevel.ERROR, '<ERROR>An Exception has occurred - ' + error.getStatusCode() + ' - ' + error.getMessage() + ' - ' + error.getFields() + '</ERROR>');
        }
    }

    public List<String> returnErrorMessages() {
        List<String> errorMessages = new List<String>();

        for (Database.SaveResult each : saveResultList) {
            if (each.isSuccess())
                continue;

            for (Database.Error error : each.getErrors()) {
                errorMessages.add('<ERROR>An Exception has occurred - ' + error.getStatusCode() + ' - ' + error.getMessage() + ' - ' + error.getFields() + '</ERROR>');
            }
        }

        return errorMessages;
    }
}