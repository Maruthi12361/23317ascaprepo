@IsTest
public class LicenseClassConfigTest {

// This test method should give 100% coverage
static testMethod void testParse() {
String json = '{'+
'    \"name\": \"Music_in_Business_Per_Location_License_Agreement\",'+
'    \"desc\": \"Music_in_Business_Per_Location_License_Agreement\",'+
'    \"auditable\": true,'+
'    \"refundable\": true,'+
'    \"derived_value_dict\": {},'+
'    \"prorate_license_fee\": false,'+
'    \"proration_type\": null,'+
'    \"derived_variable_formula_list\": ['+
'        {'+
'            \"container_name\": \"License_Policy\",'+
'            \"data_type\": \"Number\",'+
'            \"formula_string\": \"if rs.Current_Yr_Estimate_Fee < 415.00:\\n        dv_value = 415.00\\nelse:\\n        dv_value = rs.Current_Yr_Estimate_Fee\\n\",'+
'            \"name\": \"Current_Yr_Derived_Est_Fee_Amount\",'+
'            \"run_after_rate_schedule_list\": ['+
'                \"Current_Yr_Estimate_Fee\"'+
'            ]'+
'        },'+
'        {'+
'            \"container_name\": \"License_Policy\",'+
'            \"data_type\": \"Number\",'+
'            \"formula_string\": \"if rs.Previous_Yr_Actual_Fee < 407.00 and lp.Event_Count >=3:\\n        dv_value = 407.00\\nelse:\\n        dv_value = rs.Previous_Yr_Actual_Fee\\n\",'+
'            \"name\": \"Previous_Yr_Derived_Fee_Amount\",'+
'            \"run_after_rate_schedule_list\": ['+
'                \"Previous_Yr_Actual_Fee\"'+
'            ]'+
'        }'+
'    ],'+
'    \"fee_adjustment_list\": [],'+
'    \"fee_adjustment_rule_items\": [],'+
'    \"fee_adjustment_formula_text\": \"formula_value = 0.00\",'+
'    \"fee_adjustment_parse_results\": [],'+
'    \"admin_fee_list\": [],'+
'    \"admin_fee_rule_items\": [],'+
'    \"admin_fee_formula_text\": null,'+
'    \"admin_fee_parse_results\": [],'+
'    \"per_container_rate_schedule_item_list\": ['+
'        {'+
'            \"container_name\": \"License_Policy\",'+
'            \"include_in_proration\": false,'+
'            \"included_in_CPI\": true,'+
'            \"name\": \"Current_Yr_Estimate_Fee\",'+
'            \"only_if_true\": \"True\",'+
'            \"only_if_true_boolean\": true,'+
'            \"rule_string\": \"ri_value = 0.0\\n\\nif ( (  True ) and ( True  ) ):\\n    ri_value = lcf.roundup(lp.Employee_Count * 2.066)\",'+
'            \"schedule_string\": \"\\n      &#124; True\\n----------------------------------------------------------------------------------------------------------------------\\n True &#124; lcf.roundup(lp.Employee_Count * 2.066)\\t\\t\\t\\t\\t\\t\\t\\n\"'+
'        },'+
'        {'+
'            \"container_name\": \"License_Policy\",'+
'            \"include_in_proration\": false,'+
'            \"included_in_CPI\": true,'+
'            \"name\": \"Previous_Yr_Actual_Fee\",'+
'            \"only_if_true\": \"True\",'+
'            \"only_if_true_boolean\": true,'+
'            \"rule_string\": \"ri_value = 0.0\\n\\nif ( (  True ) and ( True  ) ):\\n    ri_value = lcf.roundup ((lp.Employee_Event_Attendance_Count / lp.Event_Count) * 2.025)\",'+
'            \"schedule_string\": \"\\n      &#124; True\\n----------------------------------------------------------------------------------------------------------------------\\n True &#124; lcf.roundup ((lp.Employee_Event_Attendance_Count / lp.Event_Count) * 2.025)\\t\\t\\t\\t\\t\\n\\n\"'+
'        }'+
'    ],'+
'    \"per_container_rate_schedule_rule_items\": ['+
'        {'+
'            \"container_name\": \"License_Policy\",'+
'            \"include_in_proration\": false,'+
'            \"included_in_CPI\": true,'+
'            \"name\": \"Current_Yr_Estimate_Fee\",'+
'            \"only_if_true\": \"True\",'+
'            \"only_if_true_boolean\": true,'+
'            \"rule_string\": \"ri_value = 0.0\\n\\nif ( (  True ) and ( True  ) ):\\n    ri_value = lcf.roundup(lp.Employee_Count * 2.066)\",'+
'            \"schedule_string\": \"\\n      &#124; True\\n----------------------------------------------------------------------------------------------------------------------\\n True &#124; lcf.roundup(lp.Employee_Count * 2.066)\\t\\t\\t\\t\\t\\t\\t\\n\"'+
'        },'+
'        {'+
'            \"container_name\": \"License_Policy\",'+
'            \"include_in_proration\": false,'+
'            \"included_in_CPI\": true,'+
'            \"name\": \"Previous_Yr_Actual_Fee\",'+
'            \"only_if_true\": \"True\",'+
'            \"only_if_true_boolean\": true,'+
'            \"rule_string\": \"ri_value = 0.0\\n\\nif ( (  True ) and ( True  ) ):\\n    ri_value = lcf.roundup ((lp.Employee_Event_Attendance_Count / lp.Event_Count) * 2.025)\",'+
'            \"schedule_string\": \"\\n      &#124; True\\n----------------------------------------------------------------------------------------------------------------------\\n True &#124; lcf.roundup ((lp.Employee_Event_Attendance_Count / lp.Event_Count) * 2.025)\\t\\t\\t\\t\\t\\n\\n\"'+
'        }'+
'    ],'+
'    \"per_container_rate_schedule_rule_item_values\": [],'+
'    \"per_container_formula_text\": null,'+
'    \"container_rate_schedule_item_formula_text\": \"formula_value= lp.Current_Yr_Derived_Est_Fee_Amount + lp.Previous_Yr_Derived_Fee_Amount\",'+
'    \"per_container_rate_schedule_parse_results\": ['+
'        {'+
'            \"line_list\": ['+
'                \"&#124; True\",'+
'                \"----------------------------------------------------------------------------------------------------------------------\",'+
'                \"True &#124; lcf.roundup(lp.Employee_Count * 2.066)\"'+
'            ],'+
'            \"name\": \"Current_Yr_Estimate_Fee\",'+
'            \"schedule_string\": \"\\n      &#124; True\\n----------------------------------------------------------------------------------------------------------------------\\n True &#124; lcf.roundup(lp.Employee_Count * 2.066)                            \\n\",'+
'            \"value_matrix\": ['+
'                ['+
'                    \" lcf.roundup(lp.Employee_Count * 2.066)\"'+
'                ]'+
'            ],'+
'            \"x_axis_rules\": ['+
'                \"  True \"'+
'            ],'+
'            \"y_axis_rules\": ['+
'                \" True  \"'+
'            ]'+
'        },'+
'        {'+
'            \"line_list\": ['+
'                \"&#124; True\",'+
'                \"----------------------------------------------------------------------------------------------------------------------\",'+
'                \"True &#124; lcf.roundup ((lp.Employee_Event_Attendance_Count / lp.Event_Count) * 2.025)\"'+
'            ],'+
'            \"name\": \"Previous_Yr_Actual_Fee\",'+
'            \"schedule_string\": \"\\n      &#124; True\\n----------------------------------------------------------------------------------------------------------------------\\n True &#124; lcf.roundup ((lp.Employee_Event_Attendance_Count / lp.Event_Count) * 2.025)                    \\n\\n\",'+
'            \"value_matrix\": ['+
'                ['+
'                    \" lcf.roundup ((lp.Employee_Event_Attendance_Count / lp.Event_Count) * 2.025)\"'+
'                ]'+
'            ],'+
'            \"x_axis_rules\": ['+
'                \"  True \"'+
'            ],'+
'            \"y_axis_rules\": ['+
'                \" True  \"'+
'            ]'+
'        }'+
'    ],'+
'    \"policy_parameter_lp_child_dict\": {},'+
'    \"policy_parameter_list\": ['+
'        {'+
'            \"container_name\": \"License_Policy\",'+
'            \"data_type\": \"Integer\",'+
'            \"name\": \"Event_Count\",'+
'            \"parameter_type\": \"fee_calc\"'+
'        },'+
'        {'+
'            \"container_name\": \"License_Policy\",'+
'            \"data_type\": \"Integer\",'+
'            \"name\": \"Employee_Count\",'+
'            \"parameter_type\": \"fee_calc\"'+
'        },'+
'        {'+
'            \"container_name\": \"License_Policy\",'+
'            \"data_type\": \"Integer\",'+
'            \"name\": \"Employee_Event_Attendance_Count\",'+
'            \"parameter_type\": \"fee_calc\"'+
'        },'+
'        {'+
'            \"container_name\": \"License_Policy\",'+
'            \"data_type\": \"String\",'+
'            \"name\": \"Commencement_Date\",'+
'            \"parameter_type\": \"payment\"'+
'        }'+
'    ],'+
'    \"policy_parameter_constant_list\": [],'+
'    \"payment_schedule_list\": ['+
'        {'+
'            \"payment_frequency\": \"Annual\",'+
'            \"payment_schedule_list\": ['+
'                {'+
'                    \"payment_amount_rule\": null,'+
'                    \"payment_amount_type\": \"Actual\",'+
'                    \"payment_due_date_day_type\": \"lp.Commencement_Date\",'+
'                    \"payment_period_number\": \"1\"'+
'                },'+
'                {'+
'                    \"payment_amount_rule\": null,'+
'                    \"payment_amount_type\": \"Actual\",'+
'                    \"payment_due_date_day_type\": \"January-31\",'+
'                    \"payment_period_number\": \"2\"'+
'                }'+
'            ],'+
'            \"payment_schedule_name\": \"Annual Payment Terms\",'+
'            \"payment_schedule_rate_schedule_list\": ['+
'                {'+
'                    \"schedule_string\": \"\\n                         &#124;True\\n            -------------------------------------------------------------------------\\n            30 Days Late &#124; Outstanding Fee * 0.015 OR State maximum whichever is less\\n\",'+
'                    \"type\": \"late_payment\"'+
'                }'+
'            ]'+
'        }'+
'    ],'+
'    \"cpi_fee_adjustment_list\": ['+
'        {'+
'            \"adjustment_month\": \"October\",'+
'            \"context\": \"minimum_fee\",'+
'            \"enabled\": true,'+
'            \"index\": \"CPI-U\",'+
'            \"only_if_true\": \"True\",'+
'            \"rounding_type\": \"Minimum_Fee_to_Nearest_Dollar\"'+
'        },'+
'        {'+
'            \"adjustment_month\": \"October\",'+
'            \"context\": \"rate_schedule\",'+
'            \"enabled\": true,'+
'            \"index\": \"CPI-U\",'+
'            \"only_if_true\": \"True\",'+
'            \"rounding_type\": \"Per_Employee_Fee_to_Nearest_Half_Cent\"'+
'        }'+
'    ],'+
'    \"container_min_max_dict\": {'+
'        \"License_Policy\": {'+
'            \"container_name\": \"License_Policy\",'+
'            \"max_dv\": false,'+
'            \"max_fee_is_numeric\": true,'+
'            \"max_only_if_true\": \"True\",'+
'            \"maximum_annual_fee_amount\": null,'+
'            \"maximum_fee_amount\": null,'+
'            \"min_annual_dv\": false,'+
'            \"min_dv\": false,'+
'            \"min_fee_is_numeric\": true,'+
'            \"min_only_if_true\": \"True\",'+
'            \"minimum_annual_fee_amount\": 0.0,'+
'            \"minimum_fee_amount\": 0.0'+
'        }'+
'    },'+
'    \"required_instance_attributes\": ['+
'        \"lp.Event_Count\"'+
'    ],'+
'    \"json_schema_dict\": {'+
'        \"$schema\": \"http://json-schema.org/draft-04/schema#\",'+
'        \"type\": \"object\",'+
'        \"properties\": {'+
'            \"Id\": {'+
'                \"type\": \"string\"'+
'            },'+
'            \"Name\": {'+
'                \"type\": \"string\"'+
'            },'+
'            \"License_Type\": {'+
'                \"type\": \"string\"'+
'            },'+
'            \"Event_Count\": {'+
'                \"type\": \"integer\"'+
'            },'+
'            \"Employee_Count\": {'+
'                \"type\": \"integer\"'+
'            },'+
'            \"Employee_Event_Attendance_Count\": {'+
'                \"type\": \"integer\"'+
'            }'+
'        },'+
'        \"required\": ['+
'            \"Event_Count\",'+
'            \"Employee_Count\",'+
'            \"Employee_Event_Attendance_Count\"'+
'        ]'+
'    },'+
'    \"time_period_json_schema_dict\": {'+
'        \"$schema\": \"http://json-schema.org/draft-04/schema#\",'+
'        \"type\": \"object\",'+
'        \"properties\": {'+
'            \"type\": {'+
'                \"type\": \"string\"'+
'            },'+
'            \"period\": {'+
'                \"type\": \"string\"'+
'            }'+
'        }'+
'    },'+
'    \"validation_issues\": []'+
'}';
LicenseClassConfig r = LicenseClassConfig.parse(json);
System.assert(r != null);

json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';


json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
LicenseClassConfig.Payment_schedule_list objPayment_schedule_list = new LicenseClassConfig.Payment_schedule_list(System.JSON.createParser(json));
System.assert(objPayment_schedule_list != null);
System.assert(objPayment_schedule_list.payment_amount_rule == null);
System.assert(objPayment_schedule_list.payment_amount_type == null);
System.assert(objPayment_schedule_list.payment_due_date_day_type == null);
System.assert(objPayment_schedule_list.payment_period_number == null);

json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
LicenseClassConfig objLicenseClassConfig = new LicenseClassConfig(System.JSON.createParser(json));
System.assert(objLicenseClassConfig != null);
System.assert(objLicenseClassConfig.name == null);
System.assert(objLicenseClassConfig.desc_Z == null);
System.assert(objLicenseClassConfig.auditable == null);
System.assert(objLicenseClassConfig.refundable == null);
System.assert(objLicenseClassConfig.derived_value_dict == null);
System.assert(objLicenseClassConfig.prorate_license_fee == null);
System.assert(objLicenseClassConfig.proration_type == null);
System.assert(objLicenseClassConfig.derived_variable_formula_list == null);
System.assert(objLicenseClassConfig.fee_adjustment_list == null);
System.assert(objLicenseClassConfig.fee_adjustment_rule_items == null);
System.assert(objLicenseClassConfig.fee_adjustment_formula_text == null);
System.assert(objLicenseClassConfig.fee_adjustment_parse_results == null);
System.assert(objLicenseClassConfig.admin_fee_list == null);
System.assert(objLicenseClassConfig.admin_fee_rule_items == null);
System.assert(objLicenseClassConfig.admin_fee_formula_text == null);
System.assert(objLicenseClassConfig.admin_fee_parse_results == null);
System.assert(objLicenseClassConfig.per_container_rate_schedule_item_list == null);
System.assert(objLicenseClassConfig.per_container_rate_schedule_rule_items == null);
System.assert(objLicenseClassConfig.per_container_rate_schedule_rule_item_values == null);
System.assert(objLicenseClassConfig.per_container_formula_text == null);
System.assert(objLicenseClassConfig.container_rate_schedule_item_formula_text == null);
System.assert(objLicenseClassConfig.per_container_rate_schedule_parse_results == null);
System.assert(objLicenseClassConfig.policy_parameter_lp_child_dict == null);
System.assert(objLicenseClassConfig.policy_parameter_list == null);
System.assert(objLicenseClassConfig.policy_parameter_constant_list == null);
System.assert(objLicenseClassConfig.payment_schedule_list == null);

System.assert(objLicenseClassConfig.container_min_max_dict == null);
System.assert(objLicenseClassConfig.required_instance_attributes == null);
System.assert(objLicenseClassConfig.validation_issues == null);

json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
LicenseClassConfig.Properties_Z objProperties_Z = new LicenseClassConfig.Properties_Z(System.JSON.createParser(json));
System.assert(objProperties_Z != null);
System.assert(objProperties_Z.type_Z == null);
System.assert(objProperties_Z.period == null);

json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
LicenseClassConfig.Per_container_rate_schedule_parse_results objPer_container_rate_schedule_parse_results = new LicenseClassConfig.Per_container_rate_schedule_parse_results(System.JSON.createParser(json));
System.assert(objPer_container_rate_schedule_parse_results != null);
System.assert(objPer_container_rate_schedule_parse_results.line_list == null);
System.assert(objPer_container_rate_schedule_parse_results.name == null);
System.assert(objPer_container_rate_schedule_parse_results.schedule_string == null);
System.assert(objPer_container_rate_schedule_parse_results.value_matrix == null);
System.assert(objPer_container_rate_schedule_parse_results.x_axis_rules == null);
System.assert(objPer_container_rate_schedule_parse_results.y_axis_rules == null);

json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
LicenseClassConfig.Policy_parameter_list objPolicy_parameter_list = new LicenseClassConfig.Policy_parameter_list(System.JSON.createParser(json));
System.assert(objPolicy_parameter_list != null);
System.assert(objPolicy_parameter_list.container_name == null);
System.assert(objPolicy_parameter_list.data_type == null);
System.assert(objPolicy_parameter_list.name == null);
System.assert(objPolicy_parameter_list.parameter_type == null);

json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
LicenseClassConfig.Container_min_max_dict objContainer_min_max_dict = new LicenseClassConfig.Container_min_max_dict(System.JSON.createParser(json));
System.assert(objContainer_min_max_dict != null);
System.assert(objContainer_min_max_dict.License_Policy == null);

json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
LicenseClassConfig.Derived_value_dict objDerived_value_dict = new LicenseClassConfig.Derived_value_dict(System.JSON.createParser(json));
System.assert(objDerived_value_dict != null);

json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
LicenseClassConfig.Properties objProperties = new LicenseClassConfig.Properties(System.JSON.createParser(json));
System.assert(objProperties != null);
System.assert(objProperties.Id == null);
System.assert(objProperties.Name == null);
System.assert(objProperties.License_Type == null);



json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
LicenseClassConfig.Payment_schedule_list_Z objPayment_schedule_list_Z = new LicenseClassConfig.Payment_schedule_list_Z(System.JSON.createParser(json));
System.assert(objPayment_schedule_list_Z != null);
System.assert(objPayment_schedule_list_Z.payment_frequency == null);
System.assert(objPayment_schedule_list_Z.payment_schedule_list == null);
System.assert(objPayment_schedule_list_Z.payment_schedule_name == null);
System.assert(objPayment_schedule_list_Z.payment_schedule_rate_schedule_list == null);

json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
LicenseClassConfig.License_Policy objLicense_Policy = new LicenseClassConfig.License_Policy(System.JSON.createParser(json));
System.assert(objLicense_Policy != null);
System.assert(objLicense_Policy.container_name == null);
System.assert(objLicense_Policy.max_dv == null);
System.assert(objLicense_Policy.max_fee_is_numeric == null);
System.assert(objLicense_Policy.max_only_if_true == null);
System.assert(objLicense_Policy.maximum_annual_fee_amount == null);
System.assert(objLicense_Policy.maximum_fee_amount == null);

System.assert(objLicense_Policy.min_dv == null);
System.assert(objLicense_Policy.min_fee_is_numeric == null);
System.assert(objLicense_Policy.min_only_if_true == null);
System.assert(objLicense_Policy.minimum_annual_fee_amount == null);
System.assert(objLicense_Policy.minimum_fee_amount == null);



json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
LicenseClassConfig.Per_container_rate_schedule_item_list objPer_container_rate_schedule_item_list = new LicenseClassConfig.Per_container_rate_schedule_item_list(System.JSON.createParser(json));
System.assert(objPer_container_rate_schedule_item_list != null);
System.assert(objPer_container_rate_schedule_item_list.container_name == null);
System.assert(objPer_container_rate_schedule_item_list.include_in_proration == null);
System.assert(objPer_container_rate_schedule_item_list.included_in_CPI == null);
System.assert(objPer_container_rate_schedule_item_list.name == null);
System.assert(objPer_container_rate_schedule_item_list.only_if_true == null);
System.assert(objPer_container_rate_schedule_item_list.only_if_true_boolean == null);
System.assert(objPer_container_rate_schedule_item_list.rule_string == null);
System.assert(objPer_container_rate_schedule_item_list.schedule_string == null);



json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
LicenseClassConfig.Id objId = new LicenseClassConfig.Id(System.JSON.createParser(json));
System.assert(objId != null);
System.assert(objId.type_Z == null);

json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
LicenseClassConfig.Payment_schedule_rate_schedule_list objPayment_schedule_rate_schedule_list = new LicenseClassConfig.Payment_schedule_rate_schedule_list(System.JSON.createParser(json));
System.assert(objPayment_schedule_rate_schedule_list != null);
System.assert(objPayment_schedule_rate_schedule_list.schedule_string == null);
System.assert(objPayment_schedule_rate_schedule_list.type_Z == null);
}
}