/**
 * Created by johnreedy on 2019-02-15.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-02-15   Xede Consulting Group 		Initial Creation                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This test class exercises the methods in the ...
* @author Xede Consulting Group
* @date 2019-02-15 
*/
@IsTest (seeAllData = False)
private class LeadNextContactFollowupEmailTest {
    public static Lead newLead;

    /**
    * @description This method initializes setup data.
    */
    @testSetup
    public static void setup() {
        //instantiate mock response class
        Test.setMock(HttpCalloutMock.class, new MelissaDataValidResponseMock());

        //create license class
        License_Class__c licenseClass = TestDataHelper.createLicenseClass('Bar_and_Grill', GlobalConstant.FORM_TYPE, false);
        String uniqueKey = String.valueOf(System.currentTimeMillis());

        newLead = TestDataHelper.createLeadValidPremiseAddress('Smith', 'Joe', 'Smith Roadhouse', false);
        newLead.Region__c                     = 'Northeast';
        newLead.License_Class_Relationship__c = licenseClass.id;
        newLead.Fee_Calculation_Request__c    = label.JSON_Test_Request_String;
        newLead.Fee_Calculation_Response__c   = label.JSON_Test_Response_String;
        newLead.Email                         = 'joe.smith' + uniqueKey + '@test.com';
        insert newLead;
    }
    /**
    * @description This method queries the data created in the setup method.
    */
    public static void init() {
        newLead = [Select id, Next_Contact_Date__c From Lead Where LastName = 'Smith'];
    }

    /**
    * @description This test class method verifies that given a ...
    */
    static testMethod void testSendingFollowupEmail() {
        init();
        //instantiate mock response class
        Test.setMock(HttpCalloutMock.class, new MelissaDataValidResponseMock());

        //update next contact date to Today
        newLead.Next_Contact_Date__c = date.Today();

        //start test and update lead
        test.startTest();
            update newLead;
            LeadNextContactFollowupEmailBatch b = new LeadNextContactFollowupEmailBatch();
            Database.executeBatch(b);
        test.stopTest();

    }

    /**
    * @description This test class method verifies that given a ...
    */
    static testMethod void testSendingFollowupEmailAltQuery() {
        init();

        //instantiate mock response class
        Test.setMock(HttpCalloutMock.class, new MelissaDataValidResponseMock());

        //update next contact date to Today
        newLead.Next_Contact_Date__c = date.Today();

        //start test, update lead and provide custom query
        test.startTest();

        update newLead;
            string altQueryString =
                ' Select Id, FirstName, LastName, Email, Owner.Email, Owner.Name, Company_Name__c' +
                '      , Next_Step_Comments__c ' +
                '   From Lead' +
                '  Limit 1';
            LeadNextContactFollowupEmailBatch b = new LeadNextContactFollowupEmailBatch(altQueryString);
            Database.executeBatch(b);
        test.stopTest();

    }
}