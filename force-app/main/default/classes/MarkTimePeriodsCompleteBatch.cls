/**
 * Created by johnreedy on 2019-11-14.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer       Description                                                         *
*   ----------  --------------- --------------------------------------------------------------------*
*   2019-11-14  Xede Consulting Initial Creation                                                    *
*                                                                                                   *
*  Code Coverage: 100%                                                                              *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This batch process.
* @author Xede Consulting Group
* @see MarkTimePeriodsCompleteTest
* @date 2019-11-14 
*/
global class MarkTimePeriodsCompleteBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {

    String query;

    /**
    * @description Constructor that accepts a custom query
    * @param queryString
    */
    global MarkTimePeriodsCompleteBatch(string queryString) {
        query = queryString;
        system.debug('INFO: Query String passed in> ' + query);
    }
    /**
    * @description Default Constructor
    */
    global MarkTimePeriodsCompleteBatch() {
        /*
        Get all time periods whose Report Status is not 'Complete', License Type = 'Per-program, Stage is not 'Initial Report',
        and the Next Report Due Date in less than or equal to today.
        */
        query = 'Select Id, Next_Report_Due_Date__c, Most_Recent_Report_Status__c ' +
                '  From License_Policy_Time_Period__c ' +
                ' Where License_Policy__r.License_Type__c = \'' + GlobalConstant.PER_PROGRAM_TYPE_LITERAL + '\' '+
                '   And Most_Recent_Report_Status__c     != \'' + GlobalConstant.COMPLETE_STATUS_LITERAL + '\' ' +
                '   And Stage__c                         != \'' + GlobalConstant.INITIAL_REPORT_LITERAL + '\' ' +
                '   And Next_Report_Due_Date__c < Today';
    }
    /**
     * @description The start method gets the records returned by the query
     * @param BC
     * @return List<sObject>
     */
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    /**
    * @description The execute method processes the records returned by the query locator.
    * @param BC
    * @param scope
    */
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        ErrorLogger errLogger = new ErrorLogger();

        //For each record returned from the query, set the Most Recent Report Status to 'Complete'
        for (sObject s : scope) {
            License_Policy_Time_Period__c each = (License_Policy_Time_Period__c) s;
            each.Most_Recent_Report_Status__c  =  GlobalConstant.COMPLETE_STATUS_LITERAL;
        }

        //Update the records and log any errors to the Error_Log__c object.
        Database.SaveResult[] saveResultList = Database.update(scope, false);
        SaveResultsHelper saveResults = new SaveResultsHelper(saveResultList);
    }

    /**
    * @description The finish method executes after the execute method has completed.
    * @param BC
    */
    global void finish(Database.BatchableContext BC) {

    }
}