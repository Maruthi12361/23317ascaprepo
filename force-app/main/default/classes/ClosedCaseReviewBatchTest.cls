/**
 * Created by Xede Consulting Group (Jason Burke) on 12/12/2019.
 */

@isTest
private class ClosedCaseReviewBatchTest {

    @isTest
    static void testBehavior() {
        Schema.DescribeFieldResult caseTypeResult = Case.Type.getDescribe();
        Schema.PicklistEntry[] caseTypeList = caseTypeResult.getPicklistValues();
        List<Case> caseList = new List<Case>();
        for (Schema.PicklistEntry caseType : caseTypeList) {
            for (Integer i = 0; i < 10; i++) {
                caseList.add(new Case(
                        Type = caseType.getValue(),
                        Status = 'Closed'
                ));
            }
        }

        insert caseList;

        Date today = Date.today();
        Test.startTest();
        new ClosedCaseReviewScheduler().execute(null);
        Database.executeBatch(new ClosedCaseReviewBatch(today.month(), today.year(), 0.5));
        Test.stopTest();

        AggregateResult[] countList = [SELECT Type, COUNT(Id) FROM Case WHERE Review_Status__c = :Label.Needs_Review GROUP BY Type];
        System.assertEquals(caseTypeList.size(), countList.size());
        for (AggregateResult count : countList) {
            System.assertEquals(5, (Integer) count.get('expr0'));
        }

    }

}