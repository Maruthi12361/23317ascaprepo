/**
 * Created by Xede
 */

/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer   Description                                                             *
*   ----------  ----------- ------------------------------------------------------------------------*
*   2018-10-06  Xede  		Initial Creation                                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description Stores the common utility methods
* @author Xede Consulting Group
* @see NA for test class
* @date 2018-10-06
*/

public class AscapUtility {


/*
    public static Map<String,LicenseClass__mdt> getAllMatchingLegalEntity(){
        Map<String,LicenseClass__mdt> returnEntity = new Map<String,LicenseClass__mdt>();

        String mapKey;
        String retQuery=QueryUtility.getObjectFieldsQuery('LicenseClass__mdt') +' From LicenseClass__mdt ' ;
        System.debug('\n licPolicyQuery=='+retQuery);
        List<LicenseClass__mdt> mdtList = (List<LicenseClass__mdt>)Database.Query(retQuery);


        for (LicenseClass__mdt lc : mdtList) {

            returnEntity.put(lc.LicenseClassName__c,lc);

        }

        return returnEntity;
    }
    public static LicenseClass__mdt getGetLicClassMDTByName(String licenseClassName){

        String retQuery=QueryUtility.getObjectFieldsQuery('LicenseClass__mdt') +' From LicenseClass__mdt where LicenseClassName__c=:licenseClassName  limit 1' ;
        System.debug('\n licPolicyQuery=='+retQuery);
        List<LicenseClass__mdt> mdtList = (List<LicenseClass__mdt>)Database.Query(retQuery);



        //List<LicenseClass__mdt> licClasses = [SELECT MasterLabel,Label,Station_Code__c,Period_End_Yr_Month_Txt__c,Station_Code_Allowed__c,QualifiedApiName,LicenseClassName__c,Predominant_Programming_Type__c FROM LicenseClass__mdt where LicenseClassName__c=:licenseClassName limit 1];
        LicenseClass__mdt retInstance = mdtList != null && !mdtList.isEmpty() ? mdtList[0] : null;
        return retInstance;
    }
*/
    public static LicenseClassField__mdt getGetLicenseClassMDTByName(String licenseClassName){

        String retQuery=QueryUtility.getObjectFieldsQuery('LicenseClassField__mdt') +' From LicenseClassField__mdt where LicenseClassName__c=:licenseClassName  limit 1' ;
        System.debug('\n licPolicyQuery=='+retQuery);
        List<LicenseClassField__mdt> mdtList = (List<LicenseClassField__mdt>)Database.Query(retQuery);



        //List<LicenseClass__mdt> licClasses = [SELECT MasterLabel,Label,Station_Code__c,Period_End_Yr_Month_Txt__c,Station_Code_Allowed__c,QualifiedApiName,LicenseClassName__c,Predominant_Programming_Type__c FROM LicenseClass__mdt where LicenseClassName__c=:licenseClassName limit 1];
        LicenseClassField__mdt retInstance = mdtList != null && !mdtList.isEmpty() ? mdtList[0] : null;
        return retInstance;
    }

/*
    public static List<String> getLicClassFieldValues(String licenseClassName, String fieldName,String fieldNameAllowed){
        List<String> retValsFinal;
        //Set<String> tempSetClean;

        LicenseClass__mdt licClass = getGetLicClassMDTByName(licenseClassName);
        if(licClass != null){
            List<String> retVals = getPicklistValues('LicenseClass__mdt',fieldName);
            //return retVals;
            //Set<String> tempSet = new Set<String>();
            //tempSet.addAll(retVals);

            String allowedValues = (String) licClass.get(fieldNameAllowed);
            System.debug('\n allowedValues=='+allowedValues);
            System.debug('\n retVals=='+retVals);
            System.debug('\n fieldName=='+fieldName);
            System.debug('\n fieldNameAllowed=='+fieldNameAllowed);
            retValsFinal = allowedValues != null ? allowedValues.split(',') : null;

            if(retValsFinal == null  || retValsFinal.isEmpty()){
                retValsFinal = retVals;
            }
            //Set<String> tempSetAllowed = new Set<String>();
            //tempSetAllowed.addAll(allowedValuesList);



            System.debug('\n retValsFinal=='+retValsFinal);
        }
        System.debug('\n 11 retValsFinal=='+retValsFinal);
        return retValsFinal;
    }
*/
    public static List<String> getLicenseClassFieldValues(String licenseClassName, String fieldName){
        List<String> retValsFinal;

        LicenseClassField__mdt licClass = getGetLicenseClassMDTByName(licenseClassName);
        if(licClass != null){

            String fieldValue;
            
            if(licClass.Field1__c != null && licClass.Field1__c.equalsIgnoreCase(fieldName)){
                fieldValue = licClass.Field1Value__c;
            }else if(licClass.Field2__c != null && licClass.Field2__c.equalsIgnoreCase(fieldName)){
                fieldValue = licClass.Field2Value__c;
            }else if(licClass.Field3__c != null && licClass.Field3__c.equalsIgnoreCase(fieldName))
            {
                fieldValue = licClass.Field3Value__c;
            }else if(licClass.Field4__c != null && licClass.Field4__c.equalsIgnoreCase(fieldName))
            {
                fieldValue = licClass.Field4Value__c;
            }

            retValsFinal = fieldValue != null ? fieldValue.split(',') : null;


            System.debug('\n retValsFinal=='+retValsFinal);
        }
        System.debug('\n 11 retValsFinal=='+retValsFinal);
        return retValsFinal;
    }

    public static List<String> getLicenseClassPicklistValue(String possibleValues){
        List<String> returnList = new List<String>();
        List<String> pickListValueList = possibleValues != null ? possibleValues.split(',') : null;

        if(pickListValueList != null){

            //Remove leading and trailing whitespace
            for (String each: pickListValueList){
                returnList.add(each.trim().normalizeSpace());
            }
        }
        return returnList;
    }

    public static List<String> getPicklistValues(String ObjectApi_name,String Field_name) {
        System.debug('\n 11 Field_name=='+Field_name);
        System.debug('\n 11 ObjectApi_name=='+ObjectApi_name);

        List<String> lstPickvals = new List<String>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);//From the Object Api name retrieving the SObject
        Sobject Object_name = targetType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        if(field_map.get(Field_name) != null){
            List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject

            for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
                lstPickvals.add(a.getValue());//add the value  to our final list
            }
        }
        System.debug('\n 11 lstPickvals=='+lstPickvals);
        return lstPickvals;
    }
}