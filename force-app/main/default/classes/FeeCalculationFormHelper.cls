/**
 * Created by johnreedy on 2019-09-03.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-09-03   Xede Consulting Group 		Initial Creation                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This help class supports the FeeCalculationForm, FeeCalculationViewForm and FeeCalculationApp
* components.
* @author Xede Consulting Group
* @see FeeCalculationFormHelperTest
* @date 2019-09-03 
*/
public with sharing class FeeCalculationFormHelper {

    public static set<String> getAuthorizedUserList(id ownerId, id opptyId){
        //initialize sets and list
        set<string> authorizedUserIds = new set<string>();
        list<User> authorizedUserList = new list<User>();
        set<id> ParentRoleIdSet = new set<id>();

        //get owners privileges for the given record
        UserRecordAccess ownerRecordAccess = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId=: ownerId and RecordId=: opptyId limit 1];

        //get userId's role and parent's role
        User ownerRole = [SELECT id, UserRole.id, UserRole.ParentRoleId
                            FROM User
                           WHERE id =: ownerId
                           LIMIT 1 ];

        //if the usrs role, has a parent role, add user's role's parent role to set and navigate the hierarchy
        if (string.isNotEmpty(ownerRole.UserRole.ParentRoleId)) {
            ParentRoleIdSet.add(ownerRole.UserRole.ParentRoleId);

            //iterate the number of hierarchy levels deep to navigate up the role hierarchy and find all parent roles
            for (integer x = 1; x < integer.valueOf(label.Max_Role_Hierarchy_Levels); x++) {
                for (UserRole each : [SELECT id, Name, ParentRoleId
                                        FROM UserRole
                                       WHERE id IN:ParentRoleIdSet
                                         AND ParentRoleId != null]) {
                    ParentRoleIdSet.add(each.ParentRoleId);
                }
            }
        }

        //dynamically build the parent role include clause for the dynamic query
        string ParentRoleIds = '(';
        for (string parentRole: ParentRoleIdSet){
            ParentRoleIds = ParentRoleIds + '\'' + parentRole + '\',' ;
        }
        ParentRoleIds = ParentRoleIds.removeEnd(',');
        ParentRoleIds = ParentRoleIds + ')';

        //build the query string
        string authorizedUserQuery = '';
        //authorizedUserQuery = authorizedUserQuery + 'SELECT Id, Name FROM User WHERE id = \'' +  ownerId + '\'';
        //authorizedUserQuery = authorizedUserQuery + ' OR Profile.Name = \'' + label.ASCAP_Admin_Profile_Name + '\'';

        authorizedUserQuery = authorizedUserQuery + 'SELECT Id, Name FROM User WHERE Profile.Name = \'' + label.ASCAP_Admin_Profile_Name + '\'';
        if (ownerRecordAccess.HasEditAccess == true){
            authorizedUserQuery = authorizedUserQuery + ' OR Id = \'' +  ownerId + '\'';
        }
        if (ParentRoleIdSet.size() > 0){
            authorizedUserQuery = authorizedUserQuery + ' OR UserRole.id In ' +  ParentRoleIds;
        }

        //execute the query string
        authorizedUserList = database.query(authorizedUserQuery);

        //add the users returned by the query to the set of ids
        for (User each: authorizedUserList){
            authorizedUserIds.add(each.id);
        }

        return authorizedUserIds;
    }

}