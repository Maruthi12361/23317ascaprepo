/**
 * Created by Xede Consulting Group (Jason Burke) on 11/26/2019.
 *
 * @description Generates time periods and transactions for TMLC-type license policies
 */
public class MonthlyTmlcFeeBatch extends MonthlyFeeBatch implements Database.Batchable<SObject> {
    /**
     * @description the TMLC Allocated fee amount to use to override the policy's amount
     */
    private final Decimal allocatedFee;
    /**
     * @description the TMLC Alternative Blanket fee amount to use to override the policy's amount
     */
    private final Decimal alternateBlanketFee;

    private final Decimal supplementalFee;

    private final String allocationType;

    /**
     * @description the TMLC LNSPA percentage to use to override the policy's percentage
     */
    private final Decimal lnspaPercent;
    /**
     * @description whether or not the station is Off-Air during the month this batch job is running for
     */
    private final Boolean isOffAir;

    /**
     * @description Creates an instance to process the current month
     */
    public MonthlyTmlcFeeBatch() {
        this(null, null);
    }

    /**
     * @description Creates an instance to process the given month and year
     *
     * @param month Month to process (1=Jan)
     * @param year Year to process
     */
    public MonthlyTmlcFeeBatch(Integer month, Integer year) {
        this(month, year, null, null, null, null, null, null, null);
    }

    /**
     * @description Creates an instance to process the given month and year only for the given list of license policy Ids.
     * Uses the provided billing field values if they are set.
     *
     * @param month Month to process (1=Jan)
     * @param year Year to process
     * @param policyIdList list of license policy Ids to process
     * @param allocatedFee the allocated fee to use instead of the License Policy's value, ignored if null
     * @param alternateBlanketFee the alternative fee to use instead of the License Policy's value, ignored if null
     * @param lnspaPercent the LNSPA percentage to use instead of the License Policy's value, ignored if null
     * @param isOffAir whether or not the station is Off-Air for this month
     */
    public MonthlyTmlcFeeBatch(Integer month, Integer year, Id[] policyIdList, Decimal allocatedFee, Decimal alternateBlanketFee, Decimal lnspaPercent,
                                Decimal supplementalFee, String allocationType, Boolean isOffAir)
    {
        super('MonthlyTmlcFeeBatch', month, year, 2);

        String query =
                'SELECT License_Type__c, Future_License_Type__c, Future_License_Type_Date__c, Future_Reporting_Entity__c, License_Policy_Changes__c,' +
                        ' Allocated_Fee_Amount__c, LNSPA_Percentage__c, Alternative_Blanket_Fee_Amount__c, Supplemental_Fee__c, Allocated_Fee_Type__c,' +
                        ' Licensee__r.Name, Licensee__c, Billing_Entity__c, Financially_Responsible__c, Reporting_Entity__c, Venue__r.Is_Off_Air__c,' +
                        ' (SELECT Start_Date__c, End_Date__c, License_Type__c, Allocated_Fee_Amount__c, Allocation_Type__c,' +
                        ' LNSPA_Percentage__c, Alternative_Blanket_Fee_Amount__c, Supplemental_Fee__c, Is_Off_Air__c,' +
                        ' Billing_Entity__c, Financially_Responsible__c, Reporting_Entity__c' +
                        ' FROM License_Policy_Time_Periods__r' +
                        ' WHERE Start_Date__c <= ' + endMonthlyDateStr +
                        ' AND End_Date__c >= ' + startMonthlyDateStr + ')' +
                        ' FROM License_Policy__c' +
                        ' WHERE RecordType.DeveloperName = \'' + GlobalConstant.INDUSTRY_TYPE + '\'' +
                        ' AND Status__c = \'' + GlobalConstant.ACTIVE_LITERAL + '\'' +
                        ' AND License_Type__c IN (\'' + GlobalConstant.BLANKET_TYPE + '\', \'' + GlobalConstant.ALT_BLANKET_TYPE + '\', \'' + GlobalConstant.PER_PROGRAM_TYPE + '\')' +
                        ' AND Start_Date__c <= ' + endMonthlyDateStr +
                        ' AND (End_Date__c = null OR End_Date__c >= ' + startMonthlyDateStr + ')';

        if (policyIdList != null && !policyIdList.isEmpty()) {
            query += ' AND Id IN (\'' + String.join(policyIdList, '\', \'') + '\')';
        }

        this.query = query;
        this.allocatedFee = allocatedFee;
        this.alternateBlanketFee = alternateBlanketFee;
        this.lnspaPercent = lnspaPercent;
        this.supplementalFee = supplementalFee;
        this.allocationType = allocationType;
        this.isOffAir = isOffAir;
    }

    /**
     * @description see MonthlyFeeBatch class
     */
    protected override Boolean isTimePeriodUpdated(License_Policy_Time_Period__c timePeriod, License_Policy__c policy) {
        // run the superclass' version of the method first to check those values
        Boolean isUpdated = super.isTimePeriodUpdated(timePeriod, policy);

        // save the time period's original license type for use below
        String originalLicenseType = timePeriod.License_Type__c;

        // if any of the time period's fields differ from the policy's values, update all of the time period's fields
        if (timePeriod.License_Type__c != policy.License_Type__c || timePeriod.Allocated_Fee_Amount__c != getAllocatedFee(policy)
                || timePeriod.Alternative_Blanket_Fee_Amount__c != getAlternateBlanketFee(policy)
                || timePeriod.Supplemental_Fee__c != getSupplementalFee(policy) || timePeriod.Allocation_Type__c != getAllocationType(policy)
                || timePeriod.LNSPA_Percentage__c != getLnspaPercent(policy) || timePeriod.Is_Off_Air__c != getIsOffAir(policy)) {
            timePeriod.License_Type__c = policy.License_Type__c;
            timePeriod.Allocated_Fee_Amount__c = getAllocatedFee(policy);
            timePeriod.Alternative_Blanket_Fee_Amount__c = getAlternateBlanketFee(policy);
            timePeriod.Supplemental_Fee__c = getSupplementalFee(policy);
            timePeriod.Allocation_Type__c = getAllocationType(policy);
            timePeriod.LNSPA_Percentage__c = getLnspaPercent(policy);
            timePeriod.Is_Off_Air__c = getIsOffAir(policy);
            isUpdated = true;
        }

        if (timePeriod.License_Type__c == GlobalConstant.PER_PROGRAM_TYPE && originalLicenseType != GlobalConstant.PER_PROGRAM_TYPE) {
            // initialize these fields
            timePeriod.Stage__c = GlobalConstant.INITIAL_REPORT_LITERAL;
            timePeriod.Next_Report_Due_Date__c = timePeriod.End_Date__c.addDays(Integer.valueOf(Label.Per_Program_Next_Report_Offset));
        }
        else if (timePeriod.License_Type__c != GlobalConstant.PER_PROGRAM_TYPE && originalLicenseType == GlobalConstant.PER_PROGRAM_TYPE) {
            // these fields are invalid for the license type so blank them out
            timePeriod.Stage__c = null;
            timePeriod.Next_Report_Due_Date__c = null;
        }

        return isUpdated;
    }

    /**
     * @description see MonthlyFeeBatch class
     */
    protected override Transaction__c[] createTransactions(License_Policy_Time_Period__c timePeriod, License_Policy__c policy) {
        List<Transaction__c> tranList = new List<Transaction__c>();

        // all on-air time periods have supplemental fee transactions
        // per-program and off-air time periods have no regular fee transactions
        if (!timePeriod.Is_Off_Air__c) {
            tranList.add(createSupplementalTransaction(timePeriod, policy));

            if (timePeriod.License_Type__c != GlobalConstant.PER_PROGRAM_TYPE) {
                tranList.addAll(super.createTransactions(timePeriod, policy));
            }
        }

        return tranList;
    }

    /**
     * @description see MonthlyFeeBatch class
     */
    protected override Transaction__c[] processTransactions(Transaction__c[] tranList, License_Policy_Time_Period__c timePeriod, License_Policy__c policy,
            List<Transaction__c> tranDeleteList)
    {
        tranList = super.processTransactions(tranList, timePeriod, policy, tranDeleteList);

        List<Transaction__c> returnTranList = new List<Transaction__c>();

        if (timePeriod.Is_Off_Air__c) {
            tranDeleteList.addAll(tranList);
        }
        else {  // is On-Air
            if (timePeriod.License_Type__c == GlobalConstant.PER_PROGRAM_TYPE) {
                for (Transaction__c tran : tranList) {
                    if (tran.Transaction_Description__c == null || !tran.Transaction_Description__c.startsWith(GlobalConstant.SUPPLEMENTAL_FEE_LITERAL)) {
                        tranDeleteList.add(tran);
                    }
                    else {
                        returnTranList.add(tran);
                    }
                }
            }
            else {
                returnTranList.addAll(tranList);
            }

            if (returnTranList.isEmpty()) {
                returnTranList.add(createSupplementalTransaction(timePeriod, policy));
            }

            if (returnTranList.size() == 1 && timePeriod.License_Type__c != GlobalConstant.PER_PROGRAM_TYPE) {
                if (returnTranList[0].Transaction_Description__c == null || !returnTranList[0].Transaction_Description__c.startsWith(GlobalConstant.SUPPLEMENTAL_FEE_LITERAL)) {
                    returnTranList.add(createSupplementalTransaction(timePeriod, policy));
                }
                else {
                    returnTranList.addAll(super.createTransactions(timePeriod, policy));
                }
            }
        }

        return returnTranList;
    }

    /**
     * @description see MonthlyFeeBatch class
     */
    protected override Decimal[] calculateFeeAmounts(License_Policy__c policy, Transaction__c[] tranList) {
        List<Decimal> feeList = new List<Decimal>();

        for (Transaction__c tran : tranList) {
            if (tran.Transaction_Description__c != null && tran.Transaction_Description__c.startsWith(GlobalConstant.SUPPLEMENTAL_FEE_LITERAL)) {
                feeList.add(getSupplementalFee(policy));
            }
            else if (policy.License_Type__c == GlobalConstant.BLANKET_TYPE) {
                feeList.add(getAllocatedFee(policy));
            }
            else if (policy.License_Type__c == GlobalConstant.ALT_BLANKET_TYPE) {
                // LNSPA Percent is a whole percentage (i.e. 50.00) so we divide it by 100 to get its decimal value
                //fee = getAllocatedFee(policy) * ((0.85 * (getLnspaPercent(policy) / 100)) + 0.15);
                feeList.add(getAlternateBlanketFee(policy));
            }
        }

        return feeList;
    }

    /**
     * @description see MonthlyFeeBatch class
     */
    protected override void policyPreProcessing(License_Policy__c policy) {
        // check if the time period month we are processing is past the future license type date
        if (policy.Future_License_Type_Date__c != null && startMonthlyDate.date() >= policy.Future_License_Type_Date__c) {
            // copy the future license type to the current license type field
            // so its value is used by the rest of the code
            policy.License_Type__c = policy.Future_License_Type__c;
        }
    }

    /**
     * @description see MonthlyFeeBatch class
     */
    protected override void policyPostProcessing(License_Policy__c policy, List<License_Policy__c> policyUpdateList) {
        Boolean isPolicyUpdated = false;

        if (today.month() == 1) {
            policy.License_Policy_Changes__c = 0;
            isPolicyUpdated = true;
        }

        // check if the present date is past the future license type date
        if (policy.Future_License_Type_Date__c != null && today >= policy.Future_License_Type_Date__c) {
            // reset the future license type fields and add the policy to the update list
            // because now it is time to save these changes
            if (policy.Future_License_Type__c != null) {
                policy.License_Type__c = policy.Future_License_Type__c;
                policy.Future_License_Type__c = null;
            }
            policy.Reporting_Entity__c = policy.Future_Reporting_Entity__c;
            policy.Future_Reporting_Entity__c = null;
            policy.Future_License_Type_Date__c = null;
            policy.License_Policy_Changes__c++;

            isPolicyUpdated = true;
        }

        if (isPolicyUpdated) {
            policyUpdateList.add(policy);
        }
    }

    /**
     * @description gets the allocated fee to use for the given license policy
     *
     * @param policy the license policy to use
     *
     * @return the batch's allocated fee value if it's not null, the policy's value otherwise
     */
    private Decimal getAllocatedFee(License_Policy__c policy) {
        return allocatedFee != null ? allocatedFee : policy.Allocated_Fee_Amount__c;
    }

    /**
     * @description gets the alternative blanket fee to use for the given license policy
     *
     * @param policy the license policy to use
     *
     * @return the batch's alternative blanket fee value if it's not null, the policy's value otherwise
     */
    private Decimal getAlternateBlanketFee(License_Policy__c policy) {
        return alternateBlanketFee != null ? alternateBlanketFee : policy.Alternative_Blanket_Fee_Amount__c;
    }

    private Decimal getSupplementalFee(License_Policy__c policy) {
        return supplementalFee != null ? supplementalFee : policy.Supplemental_Fee__c;
    }

    private String getAllocationType(License_Policy__c policy) {
        return String.isNotBlank(allocationType) ? allocationType : policy.Allocated_Fee_Type__c;
    }

    /**
     * @description gets the LNSPA percentage to use for the given license policy
     *
     * @param policy the license policy to use
     *
     * @return the batch's LNSPA percentage value if it's not null, the policy's value otherwise
     */
    private Decimal getLnspaPercent(License_Policy__c policy) {
        return lnspaPercent != null ? lnspaPercent : policy.LNSPA_Percentage__c;
    }

    /**
     * @description gets the Is Off-Air value to use for the given license policy
     *
     * @param policy the license policy to use
     *
     * @return the batch's Is Off-Air value if it's not null, the policy's venue's value otherwise
     */
    private Boolean getIsOffAir(License_Policy__c policy) {
        if (isOffAir != null) {
            return isOffAir;
        }

        if (policy.Venue__r != null && policy.Venue__r.Is_Off_Air__c != null) {
            return policy.Venue__r.Is_Off_Air__c;
        }

        return false;
    }

    private Transaction__c createSupplementalTransaction(License_Policy_Time_Period__c timePeriod, License_Policy__c policy) {
        return new Transaction__c(
                License_Policy_Time_Period__c = timePeriod.Id,
                Transaction_Type__c = GlobalConstant.BILLING_ITEM_LITERAL,
                Transaction_Date__c = calculateTransactionDate(policy),
                Transaction_Description__c = GlobalConstant.SUPPLEMENTAL_FEE_LITERAL + ' for ' + policy.Licensee__r.Name,
                Is_Batch_Created__c = true
        );
    }
}