/**
 * Created by Xede Consulting Group (Jason Burke) on 10/16/2019.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-10-16  Xede Consulting Group       Initial Creation                                        *
*                                                                                                   *
*   Code Coverage: 97%                                                                              *
*                                                                                                   *
*****************************************************************************************************/
/**
 * @description Concrete subclass of AddressHelper for helping with Account addresses.
 * @see AddressHelper
 */
public with sharing class AccountAddressHelper extends AddressHelper {
    /**
     * @description Map to indicate if the Billing Address for a given Account Id has been updated by MelissaData yet.
     * Used by isCalloutNeeded() method.
     * @see AccountAddressHelper.isCalloutNeeded
     */
    private Map<Id, Boolean> acctIdBillingAddrUpdatedMap = new Map<Id, Boolean>();

    /**
     * @description Initializes the addrTypeList with all of the Account address types.
     * @see AddressHelper
     */
    public AccountAddressHelper() {
        addrTypeList.add(GlobalConstant.BUSINESS_ADDR_TYPE);
        addrTypeList.add(GlobalConstant.BILLING_ADDR_TYPE);
    }

    /**
     * @description Calls the no-arg constructor, then initializes the addrTypeSObjIdsMap
     * with the given Sets of Ids.
     * @see AddressHelper
     *
     * @param billAddrAcctIdSet Account Ids that have changed billing addresses
     * @param busAddrAcctIdSet Account Ids that have changed business addresses
     */
    public AccountAddressHelper(Set<Id> billAddrAcctIdSet, Set<Id> busAddrAcctIdSet) {
        this();
        addrTypeSObjIdsMap.put(GlobalConstant.BILLING_ADDR_TYPE, billAddrAcctIdSet);
        addrTypeSObjIdsMap.put(GlobalConstant.BUSINESS_ADDR_TYPE, busAddrAcctIdSet);
    }

    /**
     * @see AddressHelper.isAddrFound
     */
    public override Boolean isAddrFound(SObject sObj, String addrType) {
        if (sObj == null) {
            return false;
        }
        Account acct = (Account) sObj;

        // return true if any address component is not blank
        if (addrType == GlobalConstant.BILLING_ADDR_TYPE) {
            return String.isNotBlank(acct.BillingStreet) || String.isNotBlank(acct.BillingCity) || String.isNotBlank(acct.BillingState) || String.isNotBlank(acct.BillingPostalCode);
        }
        if (addrType == GlobalConstant.BUSINESS_ADDR_TYPE) {
            return String.isNotBlank(acct.ShippingStreet) || String.isNotBlank(acct.ShippingCity) || String.isNotBlank(acct.ShippingState) || String.isNotBlank(acct.ShippingPostalCode);
        }

        return false;
    }

    /**
     * @see AddressHelper.getStreet
     */
    public override String getStreet(SObject sObj, String addrType) {
        if (sObj == null) {
            return null;
        }
        Account acct = (Account) sObj;

        if (addrType == GlobalConstant.BILLING_ADDR_TYPE) {
            return acct.BillingStreet;
        }
        if (addrType == GlobalConstant.BUSINESS_ADDR_TYPE) {
            return acct.ShippingStreet;
        }

        return null;
    }

    /**
     * @see AddressHelper.getCity
     */
    public override String getCity(SObject sObj, String addrType) {
        if (sObj == null) {
            return null;
        }
        Account acct = (Account) sObj;

        if (addrType == GlobalConstant.BILLING_ADDR_TYPE) {
            return acct.BillingCity;
        }
        if (addrType == GlobalConstant.BUSINESS_ADDR_TYPE) {
            return acct.ShippingCity;
        }

        return null;
    }

    /**
     * @see AddressHelper.getState
     */
    public override String getState(SObject sObj, String addrType) {
        if (sObj == null) {
            return null;
        }
        Account acct = (Account) sObj;

        if (addrType == GlobalConstant.BILLING_ADDR_TYPE) {
            return acct.BillingState;
        }
        if (addrType == GlobalConstant.BUSINESS_ADDR_TYPE) {
            return acct.ShippingState;
        }

        return null;
    }

    /**
     * @see AddressHelper.getPostalCode
     */
    public override String getPostalCode(SObject sObj, String addrType) {
        if (sObj == null) {
            return null;
        }
        Account acct = (Account) sObj;

        if (addrType == GlobalConstant.BILLING_ADDR_TYPE) {
            return acct.BillingPostalCode;
        }
        if (addrType == GlobalConstant.BUSINESS_ADDR_TYPE) {
            return acct.ShippingPostalCode;
        }

        return null;
    }

    /**
     * @see AddressHelper.getCountry
     */
    public override String getCountry(SObject sObj, String addrType) {
        if (sObj == null) {
            return null;
        }
        Account acct = (Account) sObj;

        if (addrType == GlobalConstant.BILLING_ADDR_TYPE) {
            return acct.BillingCountry;
        }
        if (addrType == GlobalConstant.BUSINESS_ADDR_TYPE) {
            return acct.ShippingCountry;
        }

        return null;
    }

    /**
     * @description Checks the default implementation and if true, does an additional check to see if the address type
     * is Billing Address and if it has already been updated by MelissaData.  This can happen if the Business Address gets
     * updated by MelissaData and is copied to the Billing Address because it is the same as the Business Address.  In that
     * case, a separate callout for the Billing Address is not needed.
     *
     * @see AddressHelper.isCalloutNeeded
     */
    public override Boolean isCalloutNeeded(SObject sObj, String addrType) {
        if (!super.isCalloutNeeded(sObj, addrType)) {
            return false;
        }
        Account acct = (Account) sObj;

        if (addrType == GlobalConstant.BILLING_ADDR_TYPE && acctIdBillingAddrUpdatedMap.get(acct.Id) == true) {
            return false;
        }

        return true;
    }

    protected override SObject initialize(SObject sObj) {
        // clone a new object preserving the ID so we can use it to update
        // since we are in an after trigger, the original SObjects are read-only
        Account acct = (Account) sObj.clone(true);
        if (acct.Billing_Address_same_as_Business_Address__c == true) {
            acct.BillingStreet = acct.ShippingStreet;
            acct.BillingCity = acct.ShippingCity;
            acct.BillingState = acct.ShippingState;
            acct.BillingPostalCode = acct.ShippingPostalCode;
            acct.BillingCountry = acct.ShippingCountry;
        }

        return acct;
    }

    /**
     * @see AddressHelper.querySObjects
     */
    protected override SObject[] querySObjects(Set<Id> allSObjIdSet) {
        return [
                SELECT BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry,
                        ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry,
                        Billing_Address_same_as_Business_Address__c
                FROM Account
                WHERE Id IN :allSObjIdSet
        ];
    }

    /**
     * @see AddressHelper.updateSObject
     */
    protected override void updateSObject(SObject updateSObj, SObject originalSObj, String addrType, MelissaDataResponse addrResp) {
        Account updateAcct = (Account) updateSObj;
        Account originalAcct = (Account) originalSObj;

        // update the update SObject with info from the MelissaData response
        if (addrType == GlobalConstant.BILLING_ADDR_TYPE || (addrType == GlobalConstant.BUSINESS_ADDR_TYPE && originalAcct.Billing_Address_same_as_Business_Address__c == true)) {
            updateAcct.BillingStreet = addrResp.Records[0].AddressLine1;
            updateAcct.BillingCity = addrResp.Records[0].City;
            updateAcct.BillingState = addrResp.Records[0].State;
            updateAcct.BillingPostalCode = addrResp.Records[0].PostalCode;
            updateAcct.Melissa_Data_Processed_Indicator__c = true;
            updateAcct.Billing_Address_Last_Verified_Date__c = System.now();
            updateAcct.Standardized_Billing_Address_String__c = addrResp.Records[0].AddressLine1 + ' ' + addrResp.Records[0].City + ' ' + addrResp.Records[0].State + ' ' + addrResp.Records[0].PostalCode;

            // if this is the billing address, set the non-standardized field to the value entered by the user, and if
            // this is the Business address and the Same as Business indicator is set to true, then
            // set the non-standardized address to the value of the Business address entered by the user.
            if (addrType == GlobalConstant.BILLING_ADDR_TYPE) {
                updateAcct.Non_Standardized_Billing_Address_String__c = replaceCR(originalAcct.BillingStreet) + ' ' + originalAcct.BillingCity + ' ' + originalAcct.BillingState + ' ' + originalAcct.BillingPostalCode;
            }
            else {
                // must be the case that this is the business address type and the billing address is marked to be the same.
                // See the if statement above that got us into this block.
                updateAcct.Non_Standardized_Billing_Address_String__c = replaceCR(originalAcct.ShippingStreet) + ' ' + originalAcct.ShippingCity + ' ' + originalAcct.ShippingState + ' ' + originalAcct.ShippingPostalCode;
            }


            if (addrResp.Records[0].Results.contains(GlobalConstant.MELISSA_DATA_ERROR_PREFIX)) {   // an error has occurred
                updateAcct.Valid_Billing_Address_Indicator__c = false;
                updateAcct.Address_Status__c = Label.MelissaData_Invalid_Status;
            } else {
                updateAcct.Valid_Billing_Address_Indicator__c = true;
                updateAcct.Address_Status__c = Label.MelissaData_Valid_Status;
            }

            updateAcct.Billing_Address_Verification_Result_Code__c = getResultCodeString(addrResp.Records[0].Results);

            // mark the entity address as having been updated
            acctIdBillingAddrUpdatedMap.put(updateAcct.Id, true);
        }

        if (addrType == GlobalConstant.BUSINESS_ADDR_TYPE) {
            updateAcct.ShippingStreet = addrResp.Records[0].AddressLine1;
            updateAcct.ShippingCity = addrResp.Records[0].City;
            updateAcct.ShippingState = addrResp.Records[0].State;
            updateAcct.ShippingPostalCode = addrResp.Records[0].PostalCode;
            updateAcct.Melissa_Data_Bus_Processed_Indicator__c = true;
            updateAcct.Last_Verified_Business_Address_Date__c = System.now();
            updateAcct.Standardized_Business_Address_String__c = addrResp.Records[0].AddressLine1 + ' ' + addrResp.Records[0].City + ' ' + addrResp.Records[0].State + ' ' + addrResp.Records[0].PostalCode;
            updateAcct.NonStandardized_Business_Address_String__c = replaceCR(originalAcct.ShippingStreet) + ' ' + originalAcct.ShippingCity + ' ' + originalAcct.ShippingState + ' ' + originalAcct.ShippingPostalCode;

            if (addrResp.Records[0].Results.contains(GlobalConstant.MELISSA_DATA_ERROR_PREFIX)) {   // an error has occurred
                updateAcct.Valid_Business_Address_Indicator__c = false;
                updateAcct.Business_Address_Status__c = Label.MelissaData_Invalid_Status;
            } else {
                updateAcct.Valid_Business_Address_Indicator__c = true;
                updateAcct.Business_Address_Status__c = Label.MelissaData_Valid_Status;
            }

            updateAcct.Bus_Address_Verification_Result_Codes__c = getResultCodeString(addrResp.Records[0].Results);
        }
    }

    /**
     * @see AddressHelper.getAddrStatus
     */
    @TestVisible
    protected override String getAddrStatus(SObject sObj, String addrType) {
        if (sObj == null) {
            return null;
        }
        Account acct = (Account) sObj;

        if (addrType == GlobalConstant.BILLING_ADDR_TYPE) {
            return acct.Address_Status__c;
        }
        if (addrType == GlobalConstant.BUSINESS_ADDR_TYPE) {
            return acct.Business_Address_Status__c;
        }

        return null;
    }

    /**
     * @see AddressHelper.setAddrStatus
     */
    @TestVisible
    protected override void setAddrStatus(SObject sObj, String addrType, String status) {
        if (sObj == null) {
            return;
        }
        Account acct = (Account) sObj;

        if (addrType == GlobalConstant.BILLING_ADDR_TYPE) {
            acct.Address_Status__c = status;
        }
        if (addrType == GlobalConstant.BUSINESS_ADDR_TYPE) {
            acct.Business_Address_Status__c = status;
        }
    }

    /**
     * @see AddressHelper.buildAddrStr
     */
    @TestVisible
    protected override String buildAddrStr(SObject sObj, String addrType) {
        if (sObj == null) {
            return null;
        }
        Account acct = (Account) sObj;

        // build a string of all the address component fields
        if (addrType == GlobalConstant.BILLING_ADDR_TYPE) {
            return acct.BillingStreet + acct.BillingCity + acct.BillingState + acct.BillingPostalCode + acct.BillingCountry;
        }
        if (addrType == GlobalConstant.BUSINESS_ADDR_TYPE) {
            return acct.ShippingStreet + acct.ShippingCity + acct.ShippingState + acct.ShippingPostalCode + acct.ShippingCountry;
        }

        return null;
    }

    /**
     * @see AddressHelper.callAddrValidationService
     */
    protected override void callAddrValidationService() {
        MelissaDataWebServiceCallout.validateAccountAddress(addrTypeSObjIdsMap.get(GlobalConstant.BILLING_ADDR_TYPE), addrTypeSObjIdsMap.get(GlobalConstant.BUSINESS_ADDR_TYPE));
    }

    /**
     * @see AddressHelper.getAddrLastVerifiedDate
     */
    @TestVisible
    protected override Datetime getAddrLastVerifiedDate(SObject sObj, String addrType) {
        if (sObj == null) {
            return null;
        }
        Account acct = (Account) sObj;

        if (addrType == GlobalConstant.BILLING_ADDR_TYPE) {
            return acct.Billing_Address_Last_Verified_Date__c;
        }
        if (addrType == GlobalConstant.BUSINESS_ADDR_TYPE) {
            return acct.Last_Verified_Business_Address_Date__c;
        }

        return null;
    }

    /**
     * @see AddressHelper.getSObjectType
     */
    protected override String getSObjectType() {
        return GlobalConstant.OBJ_ACCOUNT;
    }

    /**
     * @see AddressHelper.getNewSObject
     */
    protected override SObject getNewSObject() {
        return new Account();
    }
}