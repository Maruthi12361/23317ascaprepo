/**
 * Created by Xede Consulting Group (Jason Burke) on 1/31/2020.
 */

public with sharing class MonthlyFixedFeeHelper {

    /**
     * @description calculates the transaction (due) date for the policy for a given month
     *
     * @param policy the license policy which should have child lists queried for with the following:
     *              (SELECT Start_Date__c, End_Date__c
     *               FROM License_Policy_Time_Periods__r
     *               WHERE Start_Date__c <= :endMonthlyDate.dateGmt()
     *               AND End_Date__c >= :startMonthlyDate.dateGmt()),
     *               (SELECT Start_Date__c, End_Date__c, Billing_Frequency__c, Payment_Due_Date_Type__c, Offset_Day_of_Month__c
     *               FROM Fixed_Fee_Definitions__r
     *               ORDER BY Start_Date__c ASC)
     * @param startMonthlyDate the start date (1st day) of the month
     * @param endMonthlyDate the end date (last day) of the month
     *
     * @return the transaction (due) date
     */
    public static Date calculateTransactionDate(License_Policy__c policy, Datetime startMonthlyDate, Datetime endMonthlyDate) {
        Fixed_Fee_Definition__c fixedFeeDef = getFixedFeeDefinition(policy, startMonthlyDate, endMonthlyDate);
        if (fixedFeeDef.Payment_Due_Date_Type__c == Label.Due_Date_Type_Date) {
            Integer dayOfMonth = Integer.valueOf(fixedFeeDef.Offset_Day_of_Month__c);
            return dayOfMonth > endMonthlyDate.dayGMT() ? endMonthlyDate.dateGMT() : Date.newInstance(endMonthlyDate.yearGMT(), endMonthlyDate.monthGMT(), dayOfMonth);
        }
        else if (fixedFeeDef.Payment_Due_Date_Type__c == Label.Due_Date_Type_Offset) {
            return getTimePeriodEndDate(policy, endMonthlyDate, fixedFeeDef).addDays(Integer.valueOf(fixedFeeDef.Offset_Day_of_Month__c));
        }

        return null;
    }

    /**
     * @description Gets the fixed fee definition for this month from the license policy.  Throws an AscapException if an error condition exists.
     *
     * @param policy the license policy to use
     * @param startMonthlyDate
     * @param endMonthlyDate
     *
     * @return the fixed fee definition
     */
    public static Fixed_Fee_Definition__c getFixedFeeDefinition(License_Policy__c policy, Datetime startMonthlyDate, Datetime endMonthlyDate) {
        if (policy.Fixed_Fee_Definitions__r == null || policy.Fixed_Fee_Definitions__r.isEmpty()) {
            throw new AscapException('License Policy with Id "' + policy.Id + '" has no Fixed Fee Definitions');
        }

        Date timePeriodStartDate = startMonthlyDate.dateGmt();
        Date timePeriodEndDate = endMonthlyDate.dateGmt();
        if (policy.License_Policy_Time_Periods__r != null && !policy.License_Policy_Time_Periods__r.isEmpty()) {
            if (policy.License_Policy_Time_Periods__r.size() > 1) {
                throw new AscapException('License Policy with Id "' + policy.Id + '" has multiple Time Periods for the same month: ' + startMonthlyDate.format('MM-yyyy'));
            }
            timePeriodStartDate = policy.License_Policy_Time_Periods__r[0].Start_Date__c;
            timePeriodEndDate = policy.License_Policy_Time_Periods__r[0].End_Date__c;
        }

        List<Fixed_Fee_Definition__c> fixedFeeDefList = new List<Fixed_Fee_Definition__c>();
        for (Fixed_Fee_Definition__c def : policy.Fixed_Fee_Definitions__r) {
//            System.debug('def.Start_Date__c: ' + def.Start_Date__c);
//            System.debug('timePeriodEndDate: ' + timePeriodEndDate);
//            System.debug('def.End_Date__c: ' + def.End_Date__c);
//            System.debug('timePeriodStartDate: ' + timePeriodStartDate);

            if (def.Start_Date__c <= timePeriodEndDate && def.End_Date__c >= timePeriodStartDate) {
                fixedFeeDefList.add(def);
            }
        }
        if (fixedFeeDefList.size() != 1) {
            throw new AscapException('License Policy with Id "' + policy.Id + '" does not have exactly one Fixed Fee Definition for month: ' + startMonthlyDate.format('MM-yyyy'));
        }

        Fixed_Fee_Definition__c fixedFeeDef = fixedFeeDefList[0];
        Set<String> billingFreqSet = new Set<String>();
        billingFreqSet.add(Label.Billing_Frequency_Monthly);
        billingFreqSet.add(Label.Billing_Frequency_Quarterly);
        billingFreqSet.add(Label.Billing_Frequency_Semi_Annually);
        billingFreqSet.add(Label.Billing_Frequency_Annually);
        if (!billingFreqSet.contains(fixedFeeDef.Billing_Frequency__c)) {
            throw new AscapException('Fixed Fee Definition with Id "' + fixedFeeDef.Id + '" has unknown Billing Frequency: ' + fixedFeeDef.Billing_Frequency__c);
        }

        return fixedFeeDef;
    }

    /**
     * @description calculates the time period start date for the policy, given a month's start date and a fixed fee definition that covers that date
     *
     * @param policy the fixed fee license policy
     * @param startMonthlyDate the first day of a month
     * @param fixedFeeDef the fixed fee definition
     *
     * @return the time period start date
     */
    public static Date getTimePeriodStartDate(License_Policy__c policy, Datetime startMonthlyDate, Fixed_Fee_Definition__c fixedFeeDef) {
        Integer numMonthsInPeriod = getNumMonthsInPeriod(fixedFeeDef);

        Integer offset = -Math.mod(startMonthlyDate.monthGMT() - policy.Start_Date__c.month(), numMonthsInPeriod);
        while (offset > 0) {
            offset -= numMonthsInPeriod;
        }

        Date calcStartDate = startMonthlyDate.addMonths(offset).dateGMT();
        return calcStartDate;
    }

    /**
     * @description calculates the time period end date for the policy, given a month's end date and a fixed fee definition that covers that date
     *
     * @param policy the fixed fee license policy
     * @param startMonthlyDate the last day of a month
     * @param fixedFeeDef the fixed fee definition
     *
     * @return the time period end date
     */public static Date getTimePeriodEndDate(License_Policy__c policy, Datetime endMonthlyDate, Fixed_Fee_Definition__c fixedFeeDef) {
        Integer numMonthsInPeriod = getNumMonthsInPeriod(fixedFeeDef);

        Integer offset = (numMonthsInPeriod - 1) - Math.mod(endMonthlyDate.monthGMT() - policy.Start_Date__c.month(), numMonthsInPeriod);
        while (offset >= numMonthsInPeriod) {
            offset -= numMonthsInPeriod;
        }

        Date calcEndDate = endMonthlyDate.addDays(1).addMonths(offset).addDays(-1).dateGMT();
        return calcEndDate;
    }

    /**
     * @description gets the number of months in a fixed fee definition's time period
     *
     * @param fixedFeeDef the fixed fee definition
     *
     * @return the number of months in a time period
     */
    public static Integer getNumMonthsInPeriod(Fixed_Fee_Definition__c fixedFeeDef) {
        if (fixedFeeDef.Billing_Frequency__c == Label.Billing_Frequency_Monthly) {
            return 1;
        }
        if (fixedFeeDef.Billing_Frequency__c == Label.Billing_Frequency_Quarterly) {
            return 3;
        }
        if (fixedFeeDef.Billing_Frequency__c == Label.Billing_Frequency_Semi_Annually) {
            return 6;
        }
        if (fixedFeeDef.Billing_Frequency__c == Label.Billing_Frequency_Annually) {
            return 12;
        }

        return 0;
    }
}