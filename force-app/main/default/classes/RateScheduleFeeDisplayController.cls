/**
 * Created by johnreedy on 2019-06-03.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-06-03   Xede Consulting Group 		Initial Creation                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This class populates the Rate Schedule Fee Amounts.
* @author Xede Consulting Group
* @see RateScheduleFeeDisplayControllerTest
* @date 2019-06-03 
*/
public with sharing class RateScheduleFeeDisplayController {
    public static Map<String,String> printableLabelsMap = new Map<String,String>();

    /**
     * @description This method creates the Rate Schedule Fee object and returns it to the FeeCalculationViewForm for
     * rendering.
     * @param recordId
     * @return RateScheduleFeeString
     */
    @AuraEnabled
    public static String getRateScheduleFees(String recordId) {
        String sObjectType = Id.valueOf(recordId).getSobjectType().getDescribe().getName();

        if (sObjectType == GlobalConstant.OBJ_LEAD){
            Lead currentLead = [SELECT Id, Fee_Calculation_Response__c, License_Class_Relationship__c FROM Lead WHERE Id =: recordId LIMIT 1];
            buildPrintableLabelsMap(currentLead.License_Class_Relationship__c);

            //Convert Fee Calculation Response to Map of Key Value Pairs
            Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(currentLead.Fee_Calculation_Response__c);
            Map<String,Object> rateScheduleFeeAmountMap = (Map<String,Object>)m.get('fee_detail_dict');

            //string rateScheduleFeeAmountString='';
            List<ColumnMetadata> columnMetaDataList = new List<ColumnMetadata>();
            for (String key: rateScheduleFeeAmountMap.keySet()){
                ColumnMetadata cm = new ColumnMetadata(key, String.valueOf(rateScheduleFeeAmountMap.get(key)));
                columnMetaDataList.add(cm);
            }

            //return currentLead.Fee_Calculation_Response__c;
            return JSON.serialize(columnMetaDataList);
        } else if (sObjectType == GlobalConstant.OBJ_OPPORTUNITY){
            Opportunity currentOpportunity = [SELECT Id, Fee_Calculation_Response__c, License_Class_Relationship__c FROM Opportunity WHERE Id =: recordId LIMIT 1];
            buildPrintableLabelsMap(currentOpportunity.License_Class_Relationship__c);

            //Convert Fee Calculation Response to Map of Key Value Pairs
            Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(currentOpportunity.Fee_Calculation_Response__c);
            Map<String,Object> rateScheduleFeeAmountMap = (Map<String,Object>)m.get('fee_detail_dict');

            //string rateScheduleFeeAmountString='';
            List<ColumnMetadata> columnMetaDataList = new List<ColumnMetadata>();
            for (String key: rateScheduleFeeAmountMap.keySet()){
                ColumnMetadata cm = new ColumnMetadata(key, String.valueOf(rateScheduleFeeAmountMap.get(key)));
                columnMetaDataList.add(cm);
            }

            //return currentLead.Fee_Calculation_Response__c;
            return JSON.serialize(columnMetaDataList);
        }
        else if (sObjectType == GlobalConstant.OBJ_LICENSE_POLICY){
            License_Policy__c currPolicy = [
                    SELECT Id, License_Class_Type__c
                      FROM License_Policy__c
                    WHERE Id =: recordId LIMIT 1];
            buildPrintableLabelsMap(currPolicy.License_Class_Type__c);

            License_Policy_Time_Period__c currTimePeriod = [
                    SELECT Id, Fee_Calculation_Response_String__c
                    FROM License_Policy_Time_Period__c
                    WHERE License_Policy__c =: recordId LIMIT 1];

            //Convert Fee Calculation Response to Map of Key Value Pairs
            Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(currTimePeriod.Fee_Calculation_Response_String__c);
            Map<String,Object> rateScheduleFeeAmountMap = (Map<String,Object>)m.get('fee_detail_dict');

            //string rateScheduleFeeAmountString='';
            List<ColumnMetadata> columnMetaDataList = new List<ColumnMetadata>();
            for (String key: rateScheduleFeeAmountMap.keySet()){
                ColumnMetadata cm = new ColumnMetadata(key, String.valueOf(rateScheduleFeeAmountMap.get(key)));
                columnMetaDataList.add(cm);
            }

            //return currentLead.Fee_Calculation_Response__c;
            return JSON.serialize(columnMetaDataList);
        }

        return null;
    }

    /**
     * @description This method builds a map of response field names to printable names.
     * @param licenseClassId
     */
    private static void buildPrintableLabelsMap(Id licenseClassId){
        List<License_Class_Response_Info__c> responseFieldList = [Select Id,Response_Field_Name__c, Printable_Name__c FROM License_Class_Response_Info__c where License_Class__c =: licenseClassId];

        for (License_Class_Response_Info__c eachField: responseFieldList){
            printableLabelsMap.put(eachField.Response_Field_Name__c, eachField.Printable_Name__c);
        }
    }

    /**
    * @description This class represents the Rate Schedule Fee Amounts retruned by the DSl
    */
    public class ColumnMetadata {
        private String field_api_name;
        private String field_label;
        private String field_value;

        public ColumnMetadata(String fieldAPIName, String fieldValue){

            //set the field_api_name to the printable label if found, otherwise remove all underscores.
            field_api_name  = fieldAPIName;
            if (printableLabelsMap.containsKey(field_api_name)){
                field_label = printableLabelsMap.get(field_api_name);
            }else {
                field_label = fieldAPIName.replaceAll('_', ' ');
            }
            //if field does not contains "_Count" then format as currency, otherwise format as number.
            if (!field_api_name.contains('_Count')) {
                field_value = fieldValue.rightPad(2, '0');
                String[] feeParts = field_value.split('\\.');
                if (fieldValue.contains('.')) {
                    feeParts[1] = feeParts[1].rightPad(2, '0');
                }
                else {
                    feeParts.add('00');
                }
                String currencyTemplate = '$ {0}.{1}';
                String formattedCurrency = String.format(currencyTemplate, feeParts);
                field_value = formattedCurrency.replaceAll(('(?<!,\\d)(?<=\\d)(?=(?:\\d\\d\\d)+\\b)'),',');
            }
            else {
                field_value = fieldValue.replaceAll(('(?<!,\\d)(?<=\\d)(?=(?:\\d\\d\\d)+\\b)'),',');
            }
        }
    }
}