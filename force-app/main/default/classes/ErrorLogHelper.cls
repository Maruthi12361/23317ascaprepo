/**
 * Created by johnreedy on 2019-02-13.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-02-13   Xede Consulting Group 		Initial Creation                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description <description>
* @author Xede Consulting Group
* @see ErrorLogHelperTest
* @date 2019-02-13 
*/
public with sharing class ErrorLogHelper {

    public static void logMessage(string className, string methodName, string userDefinedMessage, Exception systemException) {
        Error_Log__c errorLog = new Error_Log__c();
        errorLog.Class_Name__c = className;
        errorLog.Method_Name__c = methodName;
        if (string.isNotEmpty(userDefinedMessage)){
          errorLog.User_Define_Message__c = userDefinedMessage;
        }
        if (systemException != null){
            errorLog.System_Exception__c = systemException.getMessage();
        }
        //insert errorLog;

    }

}