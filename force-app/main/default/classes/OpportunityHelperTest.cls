/**
 * Created by johnreedy on 2019-08-23.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  ----------------------  ----------------------------------------------------------- *
*   2019-08-23  Xede Consulting Group 	Initial Creation                                            *
*   2019-09-26  Xede Consulting Group   Added new 'Core' test class method.                         *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description <description>
* @author Xede Consulting Group
* @see OpportunityHelperTestTest
* @date 2019-08-23 
*/
@IsTest (seeAllData = False)
public with sharing class OpportunityHelperTest {
    public static User LeadSpecialist1;
    public static User CentralSpecialist1;
    public static User CentralSpecialist2;
    public static User CentralSpecialist3;
    public static User CentralSpecialist4;
    public static User NortheastSpecialist1;
    public static Opportunity_Assignment_Matrix__c OAMX1;
    public static Opportunity_Assignment_Matrix__c OAMX2;
    public static Opportunity_Assignment_Matrix__c OAMX3;
    public static Opportunity_Assignment_Matrix__c OAMX4;
    public static License_Class__c bgtLC;
    public static License_Class__c airportLC;
    public static Lead newLead;

    /**
    * @description This method initializes setup data.
    */
    public static void init(){
        LeadSpecialist1 = TestDataHelper.createUser('Specialist', 'Lead', 'Lead Identification Specialist', true);
        CentralSpecialist1 = TestDataHelper.createUser('Specialist1', 'Suzy', 'Licensing Specialist', true);
        CentralSpecialist2 = TestDataHelper.createUser('Specialist2', 'Carl', 'Licensing Specialist', true);
        CentralSpecialist3 = TestDataHelper.createUser('Specialist3', 'Sam', 'Licensing Specialist', true);
        CentralSpecialist4 = TestDataHelper.createUser('Specialist4', 'Bill', 'Licensing Specialist', true);
        NortheastSpecialist1 = TestDataHelper.createUser('Specialist5', 'Nancy', 'Licensing Specialist', true);

        OAMX1 = TestDataHelper.createMatrixRecord(CentralSpecialist1.Id, 'Central', '$400', GlobalConstant.ACTIVE_LITERAL,'BGT;Winery', true);
        OAMX2 = TestDataHelper.createMatrixRecord(CentralSpecialist2.Id, 'Central', '$400', GlobalConstant.ACTIVE_LITERAL,'BGT;Winery',true);
        OAMX3 = TestDataHelper.createMatrixRecord(CentralSpecialist3.Id, 'Central', '$750', GlobalConstant.ACTIVE_LITERAL,'BGT;Winery',true);
        OAMX4 = TestDataHelper.createMatrixRecord(CentralSpecialist4.Id, 'Central', '$400', GlobalConstant.ACTIVE_LITERAL,'Winery',true);
        OAMX4 = TestDataHelper.createMatrixRecord(NortheastSpecialist1.Id, 'Northeast', 'No Limit', GlobalConstant.ACTIVE_LITERAL,'BGT',true);

        bgtLC = TestDataHelper.createLicenseClass('BGT', GlobalConstant.FORM_TYPE, false);
        bgtLC.License_Class_Short_Name__c = 'BGT';
        bgtLC.Active__c                   = true;
        insert bgtLC;

        airportLC = TestDataHelper.createLicenseClass('Airport', GlobalConstant.FORM_TYPE, false);
        airportLC.License_Class_Short_Name__c = 'Airport';
        airportLC.Active__c                   = true;
        insert airportLC;

        newLead = TestDataHelper.createGeneralLead(bgtLC.id, false);
        newLead.Region__c = 'Central';
        newLead.Last_Verified_Premise_Date__c = Date.today();
        newLead.Address_Last_Verified_Date__c = Date.today();
        newLead.Address_Status__c = 'Valid Address';
        newLead.Premise_Address_Status__c = 'Valid Address';
        newLead.Country = 'US';
        system.runAs(LeadSpecialist1) {
            insert newLead;
        }


    }
    /**
    * @description Given a converted Lead (owned by a Lead Identification Specialist), verify that the Opportunity is
    * assigned to the Licensing Specialist with the oldest Last Assigned Datetime.
    */
    static testMethod void testOpportunityAssignment() {
        init();

        //get list of Licensing Specialist for the Central Team
        list<Opportunity_Assignment_Matrix__c> oldestSpecialist = [
                SELECT id, License_Specialist__c, License_Specialist__r.Name, Assignment_Region__c, Last_Assigned_Datetime_long__c, Last_Assigned_Datetime__c
                  FROM Opportunity_Assignment_Matrix__c
                 WHERE Assignment_Region__c = 'Central'
                 ORDER BY Last_Assigned_Datetime_long__c ASC];

        //convert lead
        test.startTest();
            Database.LeadConvert leadConvert = new database.LeadConvert();
            leadConvert.setLeadId(newLead.id);

            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
            leadConvert.setConvertedStatus(convertStatus.MasterLabel);
            leadConvert.setDoNotCreateOpportunity(false);

            Database.LeadConvertResult leadConvertResult = Database.convertLead(leadConvert);
        test.stopTest();

        //get converted lead's Opportunity Id
        Lead convertedLead = [
                SELECT id, ConvertedOpportunityId
                  FROM Lead
                 WHERE id =: newLead.Id LIMIT 1];

        //get opportunity's owner id
        Opportunity convertedOpportunity = [
                SELECT id, OwnerId
                  FROM Opportunity WHERE id =: convertedLead.ConvertedOpportunityId LIMIT 1];

        //verify that the Opportunity's owner id is the License Specialist with the oldest Last Assigned Datetime
        system.assertEquals(oldestSpecialist[0].License_Specialist__c, convertedOpportunity.OwnerId);

    }
    /**
    * @description Given a converted Lead (owned by a Lead Identification Specialist), verify that the Opportunity is
    * assigned to the Northeast Licensing Specialist with the oldest Last Assigned Datetime that can work an opportunity
    * whose value is greater than #750.
    */
    static testMethod void testOpportunityAssignment750Plus() {
        init();
        newLead.Region__c = 'Northeast';
        newLead.Estimated_Value__c = 900;
        update newLead;

        //get list of Licensing Specialist for the Central Team
        list<Opportunity_Assignment_Matrix__c> oldestSpecialist = [
                SELECT id, License_Specialist__c, License_Specialist__r.Name, Assignment_Region__c, Last_Assigned_Datetime_long__c, Last_Assigned_Datetime__c
                  FROM Opportunity_Assignment_Matrix__c
                 WHERE Assignment_Region__c = 'Northeast'
                 ORDER BY Last_Assigned_Datetime_long__c ASC];

        //convert lead
        test.startTest();
        Database.LeadConvert leadConvert = new database.LeadConvert();
        leadConvert.setLeadId(newLead.id);

        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        leadConvert.setConvertedStatus(convertStatus.MasterLabel);
        leadConvert.setDoNotCreateOpportunity(false);

        Database.LeadConvertResult leadConvertResult = Database.convertLead(leadConvert);
        test.stopTest();

        //get converted lead's Opportunity Id
        Lead convertedLead = [
                SELECT id, ConvertedOpportunityId
                  FROM Lead
                 WHERE id =: newLead.Id LIMIT 1];

        //get opportunity's owner id
        Opportunity convertedOpportunity = [
                SELECT id, OwnerId
                  FROM Opportunity WHERE id =: convertedLead.ConvertedOpportunityId LIMIT 1];

        //verify that the Opportunity's owner id is the License Specialist with the oldest Last Assigned Datetime
        system.assertEquals(oldestSpecialist[0].License_Specialist__c, convertedOpportunity.OwnerId);

    }

    /**
    * @description Given a converted Lead (owned by a Lead Identification Specialist), verify that the Opportunity is
    * assigned to the Central Licensing Specialist with the oldest Last Assigned Datetime, where the opportunity value is
    * < 400 and the oppportunity is assigned to the 750 limit user.
    */
    static testMethod void testOpportunityAssignmentLessThan400() {
        init();
        newLead.Region__c = 'Central';
        newLead.Estimated_Value__c = 400;
        update newLead;

        //get list of Licensing Specialist for the Central Team
        list<Opportunity_Assignment_Matrix__c> oldestSpecialist = [
                SELECT id, License_Specialist__c, Authorized_Dollar_Limit__c, License_Specialist__r.Name, Assignment_Region__c
                        , Last_Assigned_Datetime_long__c, Last_Assigned_Datetime__c
                FROM Opportunity_Assignment_Matrix__c
                WHERE Assignment_Region__c = 'Central'
                ORDER BY Last_Assigned_Datetime_long__c ASC];

        for (Opportunity_Assignment_Matrix__c each: oldestSpecialist){
            if (each.Authorized_Dollar_Limit__c == '$750'){
                DateTime dt = DateTime.now().addDays(-1);
                Long currDateTimeLong = dt.getTime();
                each.Last_Assigned_Datetime_long__c = currDateTimeLong;
            }
        }
        update oldestSpecialist;

        //convert lead
        test.startTest();
        Database.LeadConvert leadConvert = new database.LeadConvert();
        leadConvert.setLeadId(newLead.id);

        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        leadConvert.setConvertedStatus(convertStatus.MasterLabel);
        leadConvert.setDoNotCreateOpportunity(false);

        Database.LeadConvertResult leadConvertResult = Database.convertLead(leadConvert);
        test.stopTest();

        //get converted lead's Opportunity Id
        Lead convertedLead = [
                SELECT id, ConvertedOpportunityId
                FROM Lead
                WHERE id =: newLead.Id LIMIT 1];

        //get opportunity's owner id
        Opportunity convertedOpportunity = [
                SELECT id, OwnerId
                FROM Opportunity WHERE id =: convertedLead.ConvertedOpportunityId LIMIT 1];

        //verify that the Opportunity's owner id is the License Specialist with the oldest Last Assigned Datetime
        system.assertEquals(CentralSpecialist3.Id, convertedOpportunity.OwnerId);

    }
    /**
    * @description Given a converted Lead (owned by a Lead Identification Specialist) and an Opportunity value of 500,
    * verify that the Opportunity is assigned to the Central Licensing Specialist that can handle up to $750, even though
    * there is a specialist whose last assignment date is older, but can only be assigned opportunities under 400.
    */
    static testMethod void testOpportunityAssignmentBetween400And750() {
        init();
        newLead.Region__c = 'Central';
        newLead.Estimated_Value__c = 500;
        update newLead;

        //get list of Licensing Specialist for the Central Team
        list<Opportunity_Assignment_Matrix__c> oldestSpecialist = [
                SELECT id, License_Specialist__c, Authorized_Dollar_Limit__c, License_Specialist__r.Name, Assignment_Region__c
                        , Last_Assigned_Datetime_long__c, Last_Assigned_Datetime__c
                FROM Opportunity_Assignment_Matrix__c
                WHERE Assignment_Region__c = 'Central'
                ORDER BY Last_Assigned_Datetime_long__c ASC];

        for (Opportunity_Assignment_Matrix__c each: oldestSpecialist){
            if (each.Authorized_Dollar_Limit__c == '$400'){
                DateTime dt = DateTime.now().addDays(-1);
                Long currDateTimeLong = dt.getTime();
                each.Last_Assigned_Datetime_long__c = currDateTimeLong;
            }
        }
        oldestSpecialist[0].Last_Assigned_Datetime_long__c = null;
        update oldestSpecialist;

        //convert lead
        test.startTest();
        Database.LeadConvert leadConvert = new database.LeadConvert();
        leadConvert.setLeadId(newLead.id);

        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        leadConvert.setConvertedStatus(convertStatus.MasterLabel);
        leadConvert.setDoNotCreateOpportunity(false);

        Database.LeadConvertResult leadConvertResult = Database.convertLead(leadConvert);
        test.stopTest();

        //get converted lead's Opportunity Id
        Lead convertedLead = [
                SELECT id, ConvertedOpportunityId
                FROM Lead
                WHERE id =: newLead.Id LIMIT 1];

        //get opportunity's owner id
        Opportunity convertedOpportunity = [
                SELECT id, OwnerId
                FROM Opportunity WHERE id =: convertedLead.ConvertedOpportunityId LIMIT 1];

        //verify that the Opportunity's owner id is the License Specialist with the oldest Last Assigned Datetime
        system.assertEquals(CentralSpecialist3.Id, convertedOpportunity.OwnerId);

    }
    /**
    * @description Given an Opportunity, that was not created from a Lead, verify that it is owned by the created by id.
    */
    static testMethod void testOpportunityAssignmentNotConvertedLead() {
        init();
        Account accountObj = TestDataHelper.createAccount('Test Account', 'Legal Entity', true);
        Opportunity opportunityObj = new Opportunity();

        //convert lead
        test.startTest();
            system.runAs(NortheastSpecialist1) {
                opportunityObj = TestDataHelper.createOpportunity('Test Opportunity', accountObj.id, 'Form Licenses', 'New', false);
                opportunityObj.Amount    = 400;
                opportunityObj.Region__c = 'Central';
                insert opportunityObj;
            }
        test.stopTest();

        //get opportunity's owner id
        Opportunity newOpportunity = [
                SELECT id, OwnerId
                FROM Opportunity WHERE id =: opportunityObj.Id LIMIT 1];

        //verify that the Opportunity's owner id is the License Specialist with the oldest Last Assigned Datetime
        system.assertEquals(NortheastSpecialist1.Id, newOpportunity.OwnerId);
    }
    /**
    * @description Given a converted Lead (owned by a Lead Identification Specialist), verify that the Opportunity is
    * assigned to the Core Licensing Specialist with the oldest Last Assigned Datetime (even though there are Central Team Members available)
    * , where the opportunity value is < 400.
    */
    static testMethod void testOpportunityAssignmentCore() {
        init();
        newLead.Region__c = 'Central';
        newLead.Estimated_Value__c = 400;
        update newLead;

        //get list of Licensing Specialist for the Central Team
        list<Opportunity_Assignment_Matrix__c> oldestSpecialist = [
                SELECT id, License_Specialist__c, Authorized_Dollar_Limit__c, License_Specialist__r.Name, Assignment_Region__c
                        , Last_Assigned_Datetime_long__c, Last_Assigned_Datetime__c
                  FROM Opportunity_Assignment_Matrix__c
                 WHERE Assignment_Region__c = 'Central'
                 ORDER BY Last_Assigned_Datetime_long__c ASC];

        //update the user with the $750 limit to the oldest date and set the region to 'Core'
        for (Opportunity_Assignment_Matrix__c each: oldestSpecialist){
            if (each.Authorized_Dollar_Limit__c == '$750'){
                DateTime dt = DateTime.now().addDays(-1);
                Long currDateTimeLong = dt.getTime();
                each.Last_Assigned_Datetime_long__c = currDateTimeLong;
                each.Assignment_Region__c = GlobalConstant.CORE_REGION_LITERAL;
            }
        }
        update oldestSpecialist;

        //convert lead
        test.startTest();
            Database.LeadConvert leadConvert = new database.LeadConvert();
            leadConvert.setLeadId(newLead.id);

            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=True LIMIT 1];
            leadConvert.setConvertedStatus(convertStatus.MasterLabel);
            leadConvert.setDoNotCreateOpportunity(false);

            Database.LeadConvertResult leadConvertResult = Database.convertLead(leadConvert);
        test.stopTest();

        //get converted lead's Opportunity Id
        Lead convertedLead = [
                SELECT id, ConvertedOpportunityId
                FROM Lead
                WHERE id =: newLead.Id LIMIT 1];

        //get opportunity's owner id
        Opportunity convertedOpportunity = [
                SELECT id, OwnerId
                FROM Opportunity WHERE id =: convertedLead.ConvertedOpportunityId LIMIT 1];

        //verify that the Opportunity's owner id is the License Specialist with the oldest Last Assigned Datetime
        system.assertEquals(CentralSpecialist3.Id, convertedOpportunity.OwnerId);

    }
    /**
    * @description Given a converted Lead (owned by a Lead Identification Specialist), where the ALL Licensing Specialists for the region
    * are not eligible to be assigned an Opportunity because either their Open Opportunity Count, or Average Number of Days Open exceeds
    * the limit, verify that the Opportunity is assigned to the Northeast Team Lead.
    */
    static testMethod void testOpportunityAssignmentTeamLead() {
        User thisUser = [Select id from User where id =: UserInfo.getUserId()];
        License_Class__c licenseClass = new License_Class__c();
        User NorthEastTeamLead = new User();
        User NortheastSpecialist = new User();
        User LeadIdentificationSpecialist = new User();

        system.runAs(thisUser){
            //get Northeast Team Lead Role Id (when tried to create, received duplicate DeveloperName error on Setup Object)
            UserRole northeastUserRole = [SELECT id FROM UserRole WHERE DeveloperName = 'Northeast_Team_lead'];

            //create Northeast Team Lead
            NorthEastTeamLead = TestDataHelper.createUser('TeamLead', 'Terry', 'Licensing Specialist', False);
            NorthEastTeamLead.UserRoleId = northeastUserRole.Id;
            insert NorthEastTeamLead;

            //create Northeast Specialist and Assign them to the Northeast Region in the Opportunity Assignment Matrix
            NortheastSpecialist = TestDataHelper.createUser('Specialist', 'Nancy', 'Licensing Specialist', true);

            LeadIdentificationSpecialist = TestDataHelper.createUser('LeadSpecialist', 'Larry', 'Lead Identification Specialist', True);

            //create License Class
            licenseClass = new License_Class__c();
            licenseClass = TestDataHelper.createLicenseClass('BGT', GlobalConstant.FORM_TYPE, false);
            licenseClass.Name = 'Bar and Grill';
            licenseClass.License_Class_Short_Name__c = 'BGT';
            licenseClass.Active__c                   = true;
            insert licenseClass;

            //create Opportunity Matrix record
            Opportunity_Assignment_Matrix__c OAMX = TestDataHelper.createMatrixRecord(NortheastSpecialist.Id, 'Northeast', '$400', GlobalConstant.ACTIVE_LITERAL,'BGT;Winery', true);

        }

        //create an account and 20 opportunities for the given Northeast Specialist
        system.RunAs(NortheastSpecialist) {
            //create account
            Account accountObj = TestDataHelper.createAccount('Northeast Account', 'Legal Entity', true);

            //create 20 opportunities for the given license specialist
            list<Opportunity> opportunityInsertList = new list<Opportunity>();
            for (integer inx = 0; inx <= integer.valueOf(label.Open_Opportunity_Count); inx++) {
                Opportunity newOpportunityObj = TestDataHelper.createOpportunity('Opportunity-' + inx, accountObj.Id, 'Form Licenses', 'Initial', false);
                opportunityInsertList.add(newOpportunityObj);
            }
            insert opportunityInsertList;
        }

        /*
        create and convert a lead with a lead identification specialist as the owner for a dollar amount where it would be
        assigned to the Northeast Specialist if they have capacity, in the Northeast Region
        */
        test.startTest();

            //Create a lead
            Lead leadObj = new Lead();
            leadObj = TestDataHelper.createGeneralLead(licenseClass.id, false);
            leadObj.Region__c = 'NorthEast';
            leadObj.Last_Verified_Premise_Date__c = Date.today();
            leadObj.Address_Last_Verified_Date__c = Date.today();
            leadObj.LastName = 'Last Name';
            leadObj.Estimated_Value__c = 300;
            leadObj.Address_Status__c = 'Valid Address';
            leadObj.Premise_Address_Status__c = 'Valid Address';
            leadObj.Country = 'US';

            //create a lead
            system.runAs(LeadIdentificationSpecialist) {
                insert leadObj;
            }

            //convert the lead
            Database.LeadConvert leadConvert = new database.LeadConvert();
            leadConvert.setLeadId(leadObj.id);

            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=True LIMIT 1];
            leadConvert.setConvertedStatus(convertStatus.MasterLabel);
            leadConvert.setDoNotCreateOpportunity(false);

            Database.LeadConvertResult leadConvertResult = Database.convertLead(leadConvert);
        test.stopTest();

        //get converted lead's Opportunity Id
        Lead convertedLead = [
                SELECT id, ConvertedOpportunityId
                FROM Lead
                WHERE id =: leadObj.Id LIMIT 1];

        //get opportunity's owner id
        Opportunity convertedOpportunity = [
                SELECT id, OwnerId
                FROM Opportunity WHERE id =: convertedLead.ConvertedOpportunityId LIMIT 1];

        //verify that the Opportunity's owner id is the License Specialist with the oldest Last Assigned Datetime
        system.assertEquals(NorthEastTeamLead.Id, convertedOpportunity.OwnerId);

    }
}