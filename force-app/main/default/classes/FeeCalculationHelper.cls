/**
 * Created by johnreedy on 2019-10-21.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  ------------------  ----------------------------------------------------------------*
*   2019-10-21  Xede Consulting	    Initial Creation                                                *
*   2020-01-07  Xede Consulting     Added reportingColumns to FeeCalculationObject class.           *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This helper class supports the Fee Calculation Controllers.
* @author Xede Consulting Group
* @see FeeCalculationHelperTest
* @date 2019-10-21 
*/
public with sharing class FeeCalculationHelper {
    /**
     * @description This method populates the FeeCalculationObject from the Lead object.
     * @param recordId
     * @return FeeCalculationObject
     */
    public static FeeCalculationObject getLeadFeeCalcObject (string recordId){
        FeeCalculationObject feeCalcObj = new FeeCalculationObject();
        Lead currLead = [
                SELECT Id, License_Class_Relationship__r.DSLLicenseTypeName__c, License_Class_Relationship__r.Coverage_Type__c
                     , License_Class_Relationship__r.Name, License_Commencement_Date__c, License_Coverage_End_Date__c
                     , License_Coverage_Start_Date__c, Fee_Calculation_Response__c
                  FROM Lead WHERE Id =: recordId
                 LIMIT 1];

        feeCalcObj.licenseClassName    = currLead.License_Class_Relationship__r.Name;
        feeCalcObj.licenseClassAPIName = currLead.License_Class_Relationship__r.DSLLicenseTypeName__c;
        feeCalcObj.coverageType        = currLead.License_Class_Relationship__r.Coverage_Type__c;
        feeCalcObj.commencementDate    = currLead.License_Coverage_Start_Date__c;

        //parse response string and get fee amount
        if (string.isNotBlank(currLead.Fee_Calculation_Response__c)) {
            LicenseClassFeeResponse responseString = LicenseClassFeeResponse.parse(currLead.Fee_Calculation_Response__c);
            feeCalcObj.licenseFeeAmount = responseString.lfaftp;
        }
        feeCalcObj.coverageStartDate = currLead.License_Coverage_Start_Date__c;
        feeCalcObj.coverageEndDate   = currLead.License_Coverage_End_Date__c;

        return feeCalcObj;
    }

    /**
     * @description This method populates the FeeCalculationObject from the Opportunity object.
     * @param recordId
     * @return FeeCalculationObject
     */
    public static FeeCalculationObject getOpptyFeeCalcObject (string recordId){
        FeeCalculationObject feeCalcObj = new FeeCalculationObject();
        Opportunity currOpportunity = [
                SELECT Id,License_Class_Relationship__r.DSLLicenseTypeName__c, OwnerId, License_Class_Relationship__r.Name
                     , License_Class_Relationship__r.Coverage_Type__c, License_Commencement_Date__c
                     , License_Coverage_End_Date__c, License_Coverage_Start_Date__c, Fee_Calculation_Response__c
                  FROM Opportunity
                 WHERE Id =: recordId
                 LIMIT 1];

        feeCalcObj.ownerId             = currOpportunity.OwnerId;
        feeCalcObj.licenseClassName    = currOpportunity.License_Class_Relationship__r.Name;
        feeCalcObj.licenseClassAPIName = currOpportunity.License_Class_Relationship__r.DSLLicenseTypeName__c;
        feeCalcObj.coverageType        = currOpportunity.License_Class_Relationship__r.Coverage_Type__c;
        feeCalcObj.commencementDate    = currOpportunity.License_Coverage_Start_Date__c;

        //parse response string and get fee amount
        if (string.isNotBlank(currOpportunity.Fee_Calculation_Response__c)) {
            LicenseClassFeeResponse responseString = LicenseClassFeeResponse.parse(currOpportunity.Fee_Calculation_Response__c);
            feeCalcObj.licenseFeeAmount = responseString.lfaftp;
        }
        feeCalcObj.coverageStartDate = currOpportunity.License_Coverage_Start_Date__c;
        feeCalcObj.coverageEndDate   = currOpportunity.License_Coverage_End_Date__c;

        return feeCalcObj;
    }
    /**
     * @description This method populates the FeeCalculationObject from the License Policy and related objects.
     * @param recordId
     * @return FeeCalculationObject
     */
    public static FeeCalculationObject getPolicyFeeCalcObject (string recordId){

        FeeCalculationObject feeCalcObj = new FeeCalculationObject();
        //get License Policy Information
        License_Policy__c currPolicy = [
                SELECT Id, License_Class_Type__c, License_Class_Type__r.Name, License_Class_Type__r.Coverage_Type__c
                     , License_Class_Type__r.DSLLicenseTypeName__c, Current_Transaction_Request__c, Current_Transaction_Response__c
                     , Start_Date__c, License_Class_Type__r.Fee_Reporting_Columns__c
                  FROM License_Policy__c
                 WHERE Id =: recordId
                 LIMIT 1];

        feeCalcObj.licenseClassId      = recordId;
        feeCalcObj.licenseClassName    = currPolicy.License_Class_Type__r.Name;
        feeCalcObj.licenseClassAPIName = currPolicy.License_Class_Type__r.DSLLicenseTypeName__c;
        feeCalcObj.coverageType        = currPolicy.License_Class_Type__r.Coverage_Type__c;
        feeCalcObj.commencementDate    = currPolicy.Start_Date__c;
        feeCalcObj.reportingColumns    = currPolicy.License_Class_Type__r.Fee_Reporting_Columns__c == null?12 : 12/integer.valueOf(currPolicy.License_Class_Type__r.Fee_Reporting_Columns__c);

                //get Time Period Information
        List<License_Policy_Time_Period__c> currTimePeriod =[
                SELECT Id, License_Policy_Fee_Amount__c, Fee_Calculation_Request_String__c, Fee_Calculation_Response_String__c
                     , Start_Date__c, End_Date__c
                  FROM License_Policy_Time_Period__c
                 WHERE License_Policy__c =: currPolicy.Id];

        //Should only every be one time period record (to avoid List has no rows: https://help.salesforce.com/articleView?id=000328824&type=1&mode=1)
        if (currTimePeriod.size()>0){
            feeCalcObj.inputJSONString   = currTimePeriod[0].Fee_Calculation_Request_String__c;
            //TODO: may need to change from which object fee amount is retrieved - currently it is on Time Period, but may be billing item
            feeCalcObj.licenseFeeAmount  = currTimePeriod[0].License_Policy_Fee_Amount__c !=null? currTimePeriod[0].License_Policy_Fee_Amount__c:0.00;
            feeCalcObj.coverageStartDate = currTimePeriod[0].Start_Date__c;
            feeCalcObj.coverageEndDate   = currTimePeriod[0].End_Date__c;


            if (string.isNotBlank(currTimePeriod[0].Fee_Calculation_Response_String__c)){
                if (currTimePeriod[0].Fee_Calculation_Response_String__c.contains('"fee_detail_dict": {}')) {
                    feeCalcObj.hasRateScheduleFees = false;

                } else {
                    feeCalcObj.hasRateScheduleFees = true;
                }
            }
        }

        return feeCalcObj;
    }

    /**
     * @description This class represents the Fee Calculation Object used to render the data on the Fee Calculation input and view forms.
     */
    public class FeeCalculationObject {
        public string licenseClassId;
        public string ownerId;
        public string licenseClassAPIName;
        public string licenseClassName;
        public string licenseClassType;
        public integer reportingColumns;
        public string inputJSONString;
        public decimal licenseFeeAmount;
        public string coverageType;
        public date coverageStartDate;
        public date coverageEndDate ;
        public date commencementDate ;
        public boolean hasRateScheduleFees ;

    }
}