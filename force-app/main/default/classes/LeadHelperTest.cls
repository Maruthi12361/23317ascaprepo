/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer               Description                                                 *
*   ----------  ---------------------   ------------------------------------------------------------*
*   2018-11-26  Xede Consulting Group   Initial Creation                                            *
*   2018-12-07  Xede Consulting Group   Added test class to validate string.trim() functionality is *
*                                       working.                                                    *
*   2019-04-04  Xede Consulting Group   Added test class method testOtherRelatedObjects.            *
*   2019-07-26  Xede Consulting Group   Removed Invalid Address Queue Assignment AO-5649            *
*****************************************************************************************************
*/
/**
* @description This test class exercises code that performs operations against the Lead object.
* @author Xede Consulting Group
* @date 2018-11-26
*/

@IsTest (seeAllData = False)
private class LeadHelperTest {
    public static Lead newLeadObj1;
    public static Lead newLeadObj2;
    public static User apiUser;
    public static User accountManager;
    public static User leadSpecialist;

    /**
    * @description This method initializes setup data.
    */
    @testSetup
    public static void setup(){
        User apiUser = TestDataHelper.createAdminUser('AdminUser', 'Andy', false); //NOPMD - reviewed
        apiUser.API_User_Indicator__c = True;
        insert apiUser;

        User accountManager = TestDataHelper.createUser('Manager', 'Account', label.Account_Manager_Profile_Name, true);
        User leadSpecialist = TestDataHelper.createUser('Specialist', 'Lead', label.Lead_Identification_Specialist_Profile_Name, true);

    }
    /**
    * @description This method queries the data created in the setup method.
    */
    public static void init() {

        apiUser = [SELECT Id, Name, API_User_Indicator__c FROM User WHERE LastName = 'AdminUser' LIMIT 1];
        accountManager = [SELECT Id, Name, API_User_Indicator__c FROM User WHERE LastName = 'Manager' LIMIT 1];
        leadSpecialist = [SELECT Id, Name, API_User_Indicator__c FROM User WHERE LastName = 'Specialist' LIMIT 1];
    }

    /**
    * @description This test method validates that given a list of Leads, where one has a keyword (Liquor) as part of the
    * last Name, and the user is not an API user, that the record is saved and the knockout related fields are set.
    */
    static testMethod void testInsertListOfLeads() {
        newLeadObj1 = TestDataHelper.createLead('Reedy', 'John', 'Xede Consulting Group', false);
        newLeadObj1.LeadSource ='Liquor List';

        newLeadObj2 = TestDataHelper.createLead('Liquor', 'Lexi', 'Xede Consulting Group', false);
        newLeadObj2.LeadSource ='Liquor List';

        List<Lead> leadList = new list<Lead>();
        leadList.add(newLeadObj1);
        leadList.add(newLeadObj2);

        insert leadList;

        //verify that Disqualification Reason is properly set.
        leadList = [SELECT Id, LastName, Disqualification_Reason_Type__c, Knockout_Keyword__c FROM Lead WHERE Id IN:leadList];
        for (Lead eachLead : leadList) {
            if (eachLead.LastName == 'Liquor') {
                system.assertEquals('knockout criteria', eachLead.Disqualification_Reason_Type__c.toLowerCase());
                system.assertEquals('liquor', eachLead.Knockout_Keyword__c.toLowerCase());

            } else {
                system.assertEquals(null, newLeadObj1.Disqualification_Reason_Type__c);
            }
        }
        System.debug('1.Number of Queries used in this apex code so far: ' + Limits.getQueries());
        System.debug('2.Number of rows queried in this apex code so far: ' + Limits.getDmlRows());
    }
    /**
    * @description This test method validates that given a Lead, with the keyword (Liquor) as part of the last Name,
    * and the user is an API user, that the record is saved and the knockout related fields are set.
    */
    static testMethod void testInsertLeadAsAPIUser() {
        init();

        //Verify that API User's Indicator is set to True
        system.AssertEquals(True, apiUser.API_User_Indicator__c);

        //Attempt to insert a Lead with a last name whose value contains key word search, and validate that the error is
        //thrown and caught.
        system.runAs (apiUser) {
            newLeadObj1 = TestDataHelper.createLead('Liquor', 'Lexi', 'Xede Consulting Group', false);
            newLeadObj1.LeadSource ='Liquor List';
            insert newLeadObj1;
        }
        Lead savedLeadObj = [SELECT Id, Disqualification_Reason_Type__c, Knockout_Keyword__c FROM Lead WHERE Id =: newLeadObj1.Id LIMIT 1];

        system.assertEquals('knockout criteria', savedLeadObj.Disqualification_Reason_Type__c.toLowerCase());
        system.assertEquals('liquor', savedLeadObj.Knockout_Keyword__c.toLowerCase());

        System.debug('2.Number of Queries used in this apex code so far: ' + Limits.getQueries());
        System.debug('2.Number of rows queried in this apex code so far: ' + Limits.getDmlRows());
    }
    /**
    * @description This test method validates that given a Lead, with the keyword (Liquor) as part of the last Name,
    * and the user is a NON-API user, that the 'Knock-out' criteria is NOT applied, the record is successfully created,
    * and the knockout related fields are set..
    */
    static testMethod void testInsertLeadAsNonAPIUser() {
        init();

        //Verify that API User's Indicator is set to False
        User thisUser = [SELECT Id, API_User_Indicator__c FROM User WHERE Id = :UserInfo.getUserId() ];
        system.AssertEquals(False, thisUser.API_User_Indicator__c);

        /*
        Attempt to insert a Lead with a last name whose value contains a key word, and validate that the record is saved.
        */
        system.runAs(thisUser) {
            newLeadObj1 = TestDataHelper.createLead('Liquor', 'Lexi', 'Xede Consulting Group', false);
            insert newLeadObj1;
        }

        list<Lead> liquorLeadList = [SELECT Id, Disqualification_Reason_Type__c, Knockout_Keyword__c FROM Lead WHERE LastName = 'Liquor'];

        system.assertEquals(1, liquorLeadList.size());
        for (Lead eachLead: liquorLeadList) {
            system.assertEquals(null, eachLead.Disqualification_Reason_Type__c);
            system.assertEquals(null, eachLead.Knockout_Keyword__c);
        }
        System.debug('3.Number of Queries used in this apex code so far: ' + Limits.getQueries());
        System.debug('3.Number of rows queried in this apex code so far: ' + Limits.getDmlRows());
    }

    /**
    * @description This test method validates that given a Lead, with the keyword (liquor *lower case*) as part of the last Name,
    * and the user is an API user, that the 'Knock-out' criteria is applied and the record is successfully created with
    * Disqualification and Keyword fields populated.
    */
    static testMethod void testInsertLeadAsAPIUserCaseInsensitive() {
        init();

        //Verify that API User's Indicator is set to True
        system.AssertEquals(True, apiUser.API_User_Indicator__c);

        /*
        Attempt to insert a Lead with a last name whose value contains a key word, and validate that the record is saved.
        */
        system.runAs (apiUser) {
            newLeadObj1 = TestDataHelper.createLead('liquor', 'Lexi', 'Xede Consulting Group', false);
            newLeadObj1.LeadSource ='Liquor List';
            insert newLeadObj1;
        }
        Lead savedLeadObj = [SELECT Id, Disqualification_Reason_Type__c, Knockout_Keyword__c FROM Lead WHERE Id =: newLeadObj1.Id LIMIT 1];

        system.assertEquals('knockout criteria', savedLeadObj.Disqualification_Reason_Type__c.toLowerCase());
        system.assertEquals('liquor', savedLeadObj.Knockout_Keyword__c.toLowerCase());

        System.debug('4.Number of Queries used in this apex code so far: ' + Limits.getQueries());
        System.debug('4.Number of rows queried in this apex code so far: ' + Limits.getDmlRows());

    }

    /**
    * @description This test method validates that given a Lead, with the keyword (' liquor ' *lower case with spaces*)
    * as part of the last Name,and the user is an API user, that the 'Knock-out' criteria is applied and the record is
    * successfully created with Disqualification and Keyword fields populated.
    */
    static testMethod void testInsertLeadAsAPIUserCaseInsensitiveSpaces() {
        init();

        //Verify that API User's Indicator is set to True
        system.AssertEquals(True, apiUser.API_User_Indicator__c);

        /*
        Attempt to insert a Lead with a last name whose value contains a key word, and validate that the record is saved.
        */
        test.startTest();
            system.runAs (apiUser) {
                newLeadObj1 = TestDataHelper.createLead(' liquor ', 'Lexi', 'Xede Consulting Group', false);
                newLeadObj1.LeadSource ='Liquor List';
                insert newLeadObj1;
            }
        test.stopTest();

        //validate that disqualification and knockout key word were properly set.
        Lead savedLeadObj = [SELECT Id, Disqualification_Reason_Type__c, Knockout_Keyword__c FROM Lead WHERE Id =: newLeadObj1.Id LIMIT 1];

        system.assertEquals('knockout criteria', savedLeadObj.Disqualification_Reason_Type__c.toLowerCase());
        system.assertEquals('liquor', savedLeadObj.Knockout_Keyword__c.toLowerCase());

        System.debug('5.Number of Queries used in this apex code so far: ' + Limits.getQueries());
        System.debug('5.Number of rows queried in this apex code so far: ' + Limits.getDmlRows());

    }
    /**
    * @description This test method validates that given a list of Leads, where at least one lead is a duplicate, that
    * the lead is successfully inserted and bypasses the Lead Duplication Rules.
    */
    static testMethod void testInsertLeadListWithDuplicates() {
        init();

        //instantiate mock response class
        Test.setMock(HttpCalloutMock.class, new MelissaDataValidResponseMock());

        //verify that lead object is currently empty
        list<Lead> currentLeadList = [SELECT id, Name FROM Lead];
        system.assertEquals(0, currentLeadList.size());

        //insert single lead
        Lead existingLeadObj = TestDataHelper.createLeadValidMailingAddress('Luther', 'Lex', 'Xede Consulting Group', true);
        //verify that lead was successfully inserted
        currentLeadList = [SELECT id, Name FROM Lead];
        system.assertEquals(1, currentLeadList.size());

        //create a list of leads adding at least one duplicate
        List<Lead> leadList = new list<Lead>();
        Lead dupLeadObj = TestDataHelper.createLeadValidMailingAddress('Luther', 'Lex', 'Xede Consulting Group', false);
        leadList.add(dupLeadObj);

        Lead otherLeadObj = TestDataHelper.createLead('Lane', 'Lois', 'Xede Consulting Group', false);
        otherLeadObj.Street = '55 Main Street';
        otherLeadObj.City = 'Anytown';
        otherLeadObj.State = 'NY';
        otherLeadObj.PostalCode = '10000';
        leadList.add(otherLeadObj);


        Test.startTest();
            system.runAs(accountManager) {
                insert leadList;
            }
        test.stopTest();

        //verify that lead list (woth duplicate) was successfully inserted
        currentLeadList = [SELECT id, Name, Bypass_Duplicate_Rules__c FROM Lead];
        system.assertEquals(3, currentLeadList.size());

        //verify that bypass duplicate rules flag has been set back to false.
        for (Lead each: leadList){
            system.assertEquals(false, each.Bypass_Duplicate_Rules__c);
        }

        //verify that two of the leads are named 'Luther'
        currentLeadList = [SELECT id, Name, Bypass_Duplicate_Rules__c FROM Lead WHERE LastName = 'Luther'];
        system.assertEquals(2, currentLeadList.size());

        System.debug('6.Number of Queries used in this apex code so far: ' + Limits.getQueries());
        System.debug('6.Number of rows queried in this apex code so far: ' + Limits.getDmlRows());

    }
    /**
    * @description As an Account Manager who has entered a Lead with an Valid Premise address, the Lead should be
    * assigned to the Account Manager, and the status should be properly set.
    */
    static testMethod void testValidMailingAddressAsAccountManager() {
        init();

        //instantiate mock response class
        Test.setMock(HttpCalloutMock.class, new MelissaDataValidResponseMock());

        Lead newLead = TestDataHelper.createLeadValidMailingAddress('Valid', 'Vicki', 'Buffalo Wild Wings', false);

        test.startTest();
            system.runAs(accountManager){
                insert newLead;
            }
        test.stopTest();

        //verify that ownerId, lead status, and valid address indicator were properly set
        newLead = [Select id, Name, Street, City, State, PostalCode, Valid_Address_Indicator__c, OwnerId, Status
                     From Lead
                    Where id =: newLead.Id Limit 1];

        //validate results
        system.assertEquals(accountManager.Id, newLead.OwnerId);
        system.assertEquals(GlobalConstant.LEAD_STATUS_OPEN, newLead.Status);
        system.assertEquals(True, newLead.Valid_Address_Indicator__c);

    }
    /**
    * @description As an Account Manager who has entered a Lead with an Valid Premise address, and has set the Mailing
    * Address Same as Premise indicator to True, the mailing address should be the same as the Premise address.
    */
    static testMethod void testValidMailingSameAsPremise() {
        init();

        //instantiate mock response class
        Test.setMock(HttpCalloutMock.class, new MelissaDataValidResponseMock());

        Lead newLead = TestDataHelper.createLeadValidMailingSameAsPremiseAddress('Valid', 'Vicki', 'Buffalo Wild Wings', false);

        test.startTest();
            system.runAs(accountManager){
                insert newLead;
            }
        test.stopTest();

        newLead = [Select id, Name, Street, City, State, PostalCode, Valid_Address_Indicator__c, OwnerId, Status
                        , Premise_Street__c, Premise_City__c, Premise_State__c, Premise_Zip_Code__c
                     From Lead
                    Where id =: newLead.Id Limit 1];

        //validate that premise and street address contain the same values.
        system.assertEquals(newLead.Premise_Street__c, newLead.Street);
        system.assertEquals(newLead.Premise_City__c, newLead.City);
        system.assertEquals(newLead.Premise_State__c, newLead.State);
        system.assertEquals(newLead.Premise_Zip_Code__c, newLead.PostalCode);

        System.debug('8.Number of Queries used in this apex code so far: ' + Limits.getQueries());
        System.debug('8.Number of rows queried in this apex code so far: ' + Limits.getDmlRows());

    }
    
    /**
    * @description As a Lead Specialist who has entered a Lead with an Invalid Premise address, the Lead should be assigned to the
    * Lead Specialist
    */
    static testMethod void testInvalidPremiseAddressAsLeadSpecialist() {
        init();

        //instantiate mock response class
        Test.setMock(HttpCalloutMock.class, new MelissaDataInvalidResponseMock());
        
        Lead newLead = TestDataHelper.createLeadInValidPremiseAddress('InValid', 'Vicki', 'Buffalo Wild Wings', false);

        //start test
        test.startTest();
            system.runAs(leadSpecialist){
                insert newLead;
            }
        test.stopTest();

        //validate that owner id was properly set for invalid address
        newLead = [Select id, Name, Street, City, State, PostalCode, Valid_Address_Indicator__c, OwnerId, Status
                        , Premise_Street__c, Premise_City__c, Premise_State__c, Premise_Zip_Code__c
                    From Lead
                   Where id =: newLead.Id Limit 1];

        system.assertEquals(leadSpecialist.Id, newLead.OwnerId);

    }
    static testMethod void testValidMailingSameAsValidPremise2() {
        init();

        //instantiate mock response class
        Test.setMock(HttpCalloutMock.class, new MelissaDataValidResponseMock());

        Lead newLead = TestDataHelper.createLeadValidMailingSameAsPremiseAddress('InValid', 'Vicki', 'Buffalo Wild Wings', false);
        newLead.Street      = '123 Valid Address';
        newLead.City        = 'Anytown';
        newLead.State       = 'NY';
        newLead.PostalCode  = '10000';

        //start test
        test.startTest();
            system.runAs(leadSpecialist){
                insert newLead;
            }
        test.stopTest();

        //validate that owner id was properly set for invalid address
        newLead = [Select id, Name, Street, City, State, PostalCode, Valid_Address_Indicator__c, OwnerId, Status
                        , Premise_Street__c, Premise_City__c, Premise_State__c, Premise_Zip_Code__c
                     From Lead
                    Where id =: newLead.Id Limit 1];

        //validate that premise and street address contain the same values.
        system.assertEquals(newLead.Premise_Street__c, newLead.Street);
        system.assertEquals(newLead.Premise_City__c, newLead.City);
        system.assertEquals(newLead.Premise_State__c, newLead.State);
        system.assertEquals(newLead.Premise_Zip_Code__c, newLead.PostalCode);

    }

    /**
    * @description As an Account Manager who has entered a Lead with an non-standardized Mailing address, update the lead
    * with a valid address.
    */
    static testMethod void testUpdateNonStandardPremiseToValid() {
        init();

        //instantiate mock response class
        Test.setMock(HttpCalloutMock.class, new MelissaDataValidResponseMock());

        Lead newLead = TestDataHelper.createLead('InValid', 'Vicki', 'Buffalo Wild Wings', false);
        newLead.Premise_Street__c = '123 Main St';


        //insert lead
        system.runAs(accountManager) {
            insert newLead;
        }

        //start test
        test.startTest();
            system.runAs(accountManager) {
                newLead.Premise_Street__c = '123 Valid Address';
                update newLead;
            }
        test.stopTest();

        //validate that owner id was properly set for invalid address
        newLead = [Select id, Name, Street, City, State, PostalCode, Valid_Address_Indicator__c, OwnerId, Status
                        , Premise_Street__c, Premise_City__c, Premise_State__c, Premise_Zip_Code__c
                     From Lead
                    Where id = :newLead.Id Limit 1];

        system.assertEquals(accountManager.Id, newLead.OwnerId);
    }
    /**
   * @description Copy Service Name into Company Name if it is blank before conversion.
   */
    static testMethod void testCopyCompanyNameIntoServiceNameBeforeConversion() {
        init();
        License_Class__c licenseClass = TestDataHelper.createLicenseClass('test_license_class', GlobalConstant.FORM_TYPE, true);
        Id newMediaRecTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('New Media').getRecordTypeId();
        Lead newLead = TestDataHelper.createNewMediaLead(licenseClass.Id, false);

        // Create a Lead without a value in Company_Name__c
        /*Lead newLead = new Lead(
                  LastName                  = 'Evil'
                , FirstName                 = 'Douglas'
                , Email                     = 'devil@ascap.com'
                , Company                   = 'Virtucon LLC'
                , Street                    = '123 Main St'
                , City                      = 'Troy'
                , State                     = 'MI'
                , PostalCode                = '48084'
                , Address_Status__c         = 'Valid Address'
                , Service_Name__c           = 'Virtucon.com'
                , Premise_Street__c         = '123 Main St'
                , Premise_City__c           = 'Troy'
                , Premise_State__c          = 'MI'
                , Premise_Zip_Code__c       = '48084'
                , Premise_Address_Status__c = 'Valid Address'
                , Estimated_Value__c        = 15.00
                , Content_Use_Type__c       = 'Interactive'
                , Similar_Web_Average_Monthly_Visit_Count__c = 10
                , recordtypeid              = newMediaRecTypeId
        );*/
        system.runAs(accountManager) {
            insert newLead;
        }

        //start test
        test.startTest();
        system.runAs(accountManager) {
            Database.LeadConvert lc = new database.LeadConvert();
            lc.setLeadId(newLead.id);
            lc.setDoNotCreateOpportunity(false);
            lc.setConvertedStatus('Converted');

            Database.LeadConvertResult lcr = Database.convertLead(lc);

        }
        test.stopTest();

        //validate that the Company_Name__c has the value that was in the Service_Name__c
        newLead = [Select id, Name, Service_Name__c, Company_Name__c
        From Lead
        Where id = :newLead.Id Limit 1];

        system.assertEquals(newLead.Company_Name__c, newLead.Service_Name__c);
    }

    /**
    * @description Convert a lead with a venue that has the same name and address as the legal entity, bypassing the duplicate
    * rules.
    */
    static testMethod void testDuplicateVenueAddress() {
        init();
        //TODO:  Need to add license class and respect validation rule needed for conversion
        //Test.setMock(HttpCalloutMock.class, new MelissaDataValidResponseMock());

        //create test license class
        License_Class__c licenseClass = TestDataHelper.createLicenseClass('test_license_class', GlobalConstant.FORM_TYPE, true);

        Lead newLead = TestDataHelper.createGeneralLead(licenseClass.Id, false);
        newLead.Company_Name__c = 'Buffalo Wild Wings';
        newLead.Company         = 'Buffalo Wild Wings';

        insert newLead;

        test.startTest();
            //convert lead to an Account, Contact, Secondary Contact, and Venue
            Database.LeadConvert leadConvert = new database.LeadConvert();
            leadConvert.setLeadId(newLead.id);

            //set convertedStatus
            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
            leadConvert.setConvertedStatus(convertStatus.MasterLabel);

            //convert the lead and verify that is was successful
            Database.LeadConvertResult leadConvertResult = Database.convertLead(leadConvert);
        test.stopTest();

        System.assert(leadConvertResult.isSuccess());

        //query the Account object and validate that the the two Account records were created
        List<Account> newAccountList = [SELECT id, recordTypeId, Name FROM Account WHERE Name = 'Buffalo Wild Wings'];
        system.assertEquals(2, newAccountList.size());

    }

    static testMethod void testAssignRegionByZipCode() {
        Zipcode_Mapping__c[] zipMapList = new Zipcode_Mapping__c[] {
                new Zipcode_Mapping__c(Name = '72821', Region__c = 'Central'),
                new Zipcode_Mapping__c(Name = '07030', Region__c = 'Northeast'),
                new Zipcode_Mapping__c(Name = '32306', Region__c = 'Southeast'),
                new Zipcode_Mapping__c(Name = '85625', Region__c = 'West')
        };
        insert zipMapList;

        Lead lead0 = TestDataHelper.createLead('LastName0', 'FirstName0', 'Company0', false);
        lead0.Premise_Zip_Code__c = '72821';
        Lead lead1 = TestDataHelper.createLead('LastName1', 'FirstName1', 'Company1', false);
        lead1.Premise_Zip_Code__c = '7030';
        Lead lead2 = TestDataHelper.createLead('LastName2', 'FirstName2', 'Company2', false);
        lead2.Premise_Zip_Code__c = '32306-1234';

        Lead[] leadList = new Lead[] { lead0, lead1, lead2 };

        Test.startTest();
        insert leadList;

        leadList = [SELECT Premise_Zip_Code__c, Region__c FROM Lead ORDER BY LastName ASC];
        System.assertEquals(3, leadList.size());
        System.assertEquals('Central', leadList[0].Region__c);
        System.assertEquals('Northeast', leadList[1].Region__c);
        System.assertEquals('Southeast', leadList[2].Region__c);

        leadList[0].Premise_Zip_Code__c = '85625';
        leadList[1].Premise_Zip_Code__c = '85625-1234';
        leadList[2].Premise_Zip_Code__c = '856251234';
        update leadList;

        leadList = [SELECT Premise_Zip_Code__c, Region__c FROM Lead ORDER BY LastName ASC];
        System.assertEquals(3, leadList.size());
        System.assertEquals('West', leadList[0].Region__c);
        System.assertEquals('West', leadList[1].Region__c);
        System.assertEquals('West', leadList[2].Region__c);
        Test.stopTest();
    }

    @isTest
    static void testLeadAddressHelperNulls() {
        LeadAddressHelper helper = new LeadAddressHelper();
        Test.startTest();
        System.assert(!helper.isAddrFound(null, null));
        System.assert(!helper.isAddrFound(new Lead(), null));
        System.assertEquals(null, helper.getStreet(null, null));
        System.assertEquals(null, helper.getStreet(new Lead(), null));
        System.assertEquals(null, helper.getCity(null, null));
        System.assertEquals(null, helper.getCity(new Lead(), null));
        System.assertEquals(null, helper.getState(null, null));
        System.assertEquals(null, helper.getState(new Lead(), null));
        System.assertEquals(null, helper.getPostalCode(null, null));
        System.assertEquals(null, helper.getPostalCode(new Lead(), null));
        System.assertEquals(null, helper.getCompany(null, null));
        System.assertEquals(null, helper.getCompany(new Lead(), null));
        System.assertEquals(null, helper.getAddrStatus(null, null));
        System.assertEquals(null, helper.getAddrStatus(new Lead(), null));
        System.assertEquals(null, helper.buildAddrStr(new Lead(), null));
        System.assertEquals(null, helper.getAddrLastVerifiedDate(null, null));
        System.assertEquals(null, helper.getAddrLastVerifiedDate(new Lead(), null));
        Test.stopTest();
    }
}