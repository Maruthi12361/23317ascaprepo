/**
 * Created by johnreedy on 2020-01-02.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2020-01-02   Xede Consulting Group 		Initial Creation                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This Controller class supports the FeeReportingForm AuraDefinitionBundle.
* @author Xede Consulting Group
* @see FeeReportingFormControllerTest
* @date 2020-01-02 
*/
public with sharing class FeeReportingFormController {

    //todo remove debug statements from GetLicenseClassParameters class
    /**
     * @description This auraEnabled method builds the wrapper class used to dynamically build the lightning component page.
     * @param recordId
     * @return json string (FieldMetadataWrapper)
     */
    @AuraEnabled
    public static String getLicenseClassConfiguration(Id recordId) {
        ErrorLogger logger = new ErrorLogger();

        License_Policy__c policy = [
            SELECT Id
                 , Name
                 , End_Date__c
                 , Fee_Report_Frequency__c
              FROM License_Policy__c
             WHERE Id = :recordId LIMIT 1];

        //populate feeCalcObject
        FeeCalculationHelper.FeeCalculationObject feeCalcObj = new FeeCalculationHelper.FeeCalculationObject();
        feeCalcObj = FeeCalculationHelper.getPolicyFeeCalcObject(recordId);

        //get the licensee class parameters from the DSL
        LicenseClassConfig objPolicy = GetLicenseClassParameters.GetLicenseClassInfo(feeCalcObj.licenseClassAPIName);
        List<LicenseClassConfig.Policy_parameter_list> paramList = objPolicy != null ? objPolicy.policy_parameter_list : new List<LicenseClassConfig.Policy_parameter_list>();

        //need to query for field info
        List<License_Class_Field_Info__c> fieldInfoList = [
           SELECT Id
                , Name
                , License_Class__c
                , DSL_Parameter_Name__c
                , Help_Text__c
                , Internal_Field_Label__c
                , Self_Service_Label__c
                , Order__c
                , Value__c
                , Values_Not_Allowed__c
                , Minimum_Value__c
                , Maximum_Value__c
                , Minimum_Value_Message__c
                , Maximum_Value_Message__c
                , Default_Display_Enabled__c
                , Dependent_Child_Field__c
             FROM License_Class_Field_Info__c
            WHERE License_Class__r.DSLLicenseTypeName__c = :feeCalcObj.licenseClassAPIName];

        //build map of DSL Parameter to License Class Field Info metadata
        Map<String, License_Class_Field_Info__c> licenseClassFieldMap = new Map<String, License_Class_Field_Info__c>();
        for (License_Class_Field_Info__c eachField: fieldInfoList) {
            licenseClassFieldMap.put(eachField.DSL_Parameter_Name__c, eachField);
        }

        //build FieldMetadataWrapper
        FieldMetadataWrapper cmw = new FieldMetadataWrapper();
        cmw.timePeriod = new License_Policy_Time_Period__c();

        cmw.licenseClassId           = feeCalcObj.licenseClassId;
        cmw.licenseClassName         = feeCalcObj.licenseClassName;
        cmw.LicensePolicyName        = policy.Name;
        cmw.LicenseClassConfig       = feeCalcObj.licenseClassAPIName;
        cmw.policyEndDate            = policy.End_Date__c;
        cmw.reportingColumns         = feeCalcObj.reportingColumns;

        //derive default coverage dates based on reporting frequency
        List<Date> coverageDates = new List<Date>(deriveCoverageDates(policy.Fee_Report_Frequency__c));
        cmw.timePeriod.Start_Date__c = coverageDates[0];
        cmw.timePeriod.End_Date__c   = coverageDates[1];

        cmw.hasRateScheduleFees      = false;


        for (LicenseClassConfig.Policy_parameter_list each: paramList){

            //only fields with a parameter type of fee_calc are displayed on the page
            if (each.parameter_type == GlobalConstant.FEE_CALC_PARAMETER_TYPE){
                string picklistValues;
                picklistValues       = licenseClassFieldMap.get(each.name).Value__c;

                string notAcceptedValues;
                notAcceptedValues    = licenseClassFieldMap.get(each.name).Values_Not_Allowed__c ;

                String fieldDisplayConditionValue;
                fieldDisplayConditionValue = licenseClassFieldMap.get(each.name).Dependent_Child_Field__c ;

                FieldMetadata cm = new FieldMetadata();
                cm.field_label        = licenseClassFieldMap.get(each.name).Internal_Field_Label__c == null?'missing label': licenseClassFieldMap.get(each.name).Internal_Field_Label__c;
                cm.field_api_name     = licenseClassFieldMap.get(each.name).DSL_Parameter_Name__c;
                cm.field_help_text    = licenseClassFieldMap.get(each.name).Help_Text__c;
                cm.field_type         = each.data_type.toUpperCase();
                cm.field_display      = licenseClassFieldMap.get(each.name).Default_Display_Enabled__c ;
                cm.field_order        = licenseClassFieldMap.get(each.name).Order__c;
                cm.field_minValue     = Integer.valueOf(licenseClassFieldMap.get(each.name).Minimum_Value__c);
                cm.field_maxValue     = Integer.valueOf(licenseClassFieldMap.get(each.name).Maximum_Value__c);
                cm.field_minValueMsg  = licenseClassFieldMap.get(each.name).Minimum_Value_Message__c ;
                cm.field_maxValueMsg  = licenseClassFieldMap.get(each.name).Maximum_Value_Message__c ;
                cm.field_display      = licenseClassFieldMap.get(each.name).Default_Display_Enabled__c;
                cm.dependent_fieldNames = new list<string>();

                //build list of picklist values and those that are not acceptable as a value to save, such as '--'
                if(each.data_type.containsIgnoreCase(GlobalConstant.STRING_DATA_TYPE)){
                    cm.possible_values      = picklistValues != null? ASCAPUtils.getLicenseClassPicklistValue(picklistValues):null;
                    cm.notAccepted_values   = notAcceptedValues != null? ASCAPUtils.getLicenseClassPicklistValue(notAcceptedValues):null;

                    if(cm.possible_values != null && cm.possible_values.size() > 0){
                        cm.field_type=GlobalConstant.PICKLIST_DATA_TYPE;
                    }
                    cm.selectedValue = cm.possible_values[1];
                }
                //populate the dependent field names field with any field dependencies
                cm.dependent_fieldNames = fieldDisplayConditionValue != null? AscapUtils.getLicenseClassPicklistValue(fieldDisplayConditionValue):null;

                //initialize page values
                cm.booleanValue = false;
                cm.decimalValue = 0.0;

                cmw.field_level_metadata.add(cm);
            }
        }

        //sort the field level data base on the sort 'Order' field
        cmw.field_level_metadata.sort();

        //sendEmail('cmw', json.serialize(cmw));

        return JSON.serialize(cmw, true);
    }

    /**
    * @description This AuraEnabled method returns the fee to the form for display.
    * @param recordId
    * @return ReturnWrapper
    */
    @AuraEnabled
    public static String sendForm(String recordId, String LicensePolicyName, String licenseClassId, String LicenseClassConfig, String licensePolicy, String policyTimePeriod, Date policyEndDate) {

        System.debug('RevenueReportingFormController.sendForm');
        System.debug('recordId: ' + recordId);
        System.debug('licensePolicy: ' + licensePolicy);
        System.debug('LicensePolicyName: ' + LicensePolicyName);
        System.debug('LicenseClassId:' + licenseClassId);
        System.debug('LicenseClassConfig: ' + LicenseClassConfig);
        System.debug('policyTimePeriod:' + policyTimePeriod);
        System.debug('policyEndDate:' + policyEndDate);

        ReturnWrapper returnWrapper;

        //validate reporting start date is first of month
        Map<String, Object> timePeriodMap = (Map<String, Object>) JSON.deserializeUntyped(policyTimePeriod);
        //date reportingStartDate = Date.valueOf(String.valueOf(timePeriodMap.get('Start_Date__c')));
        //date reportingEndDate = Date.valueOf(String.valueOf(timePeriodMap.get('End_Date__c')));

        integer startDateDayOfMonth = Date.valueOf(String.valueOf(timePeriodMap.get('Start_Date__c'))).day();
        if (startDateDayOfMonth != 1){
            throw new AscapException('The Reporting period Start Date must be the first of the month.');
        }

        //TODO: add additional validations

        //build list of field metadata from the fields submitted (displayed) on the form
        List<FieldMetadata> licensePolicySubmitted = null;
        if (licensePolicy != null && !String.isEmpty(licensePolicy)) {
            licensePolicySubmitted = (List<FieldMetadata>) JSON.deserialize(licensePolicy, List<FieldMetadata>.class);
        }

        //sendEmail('policy submittted', licensePolicy);
        //construct DSL request
        String feeRequestJSONString = buildDSLRequest(licensePolicySubmitted, recordId, LicensePolicyName, LicenseClassConfig, timePeriodMap);

        //sendEmail('json', feeRequestJSONString);
        LicenseClassFeeResponse objFeeResponse = new LicenseClassFeeResponse();
        Decimal feeAmount = 0.0;

        //get license class fee amount and set fee amount
        try{
            objFeeResponse = GetLicenseClassParameters.GetLicenseClassFeeResult(feeRequestJSONString);
            HttpResponse response = objFeeResponse.response;
            if(response != null && response.getStatusCode() != 200){
                returnWrapper = new ReturnWrapper(GlobalConstant.ERROR_LITERAL,'Error processing request. Status Code='+objFeeResponse.response.getStatusCode());
                return JSON.serialize(returnWrapper, true);
            }else{
                feeAmount = objFeeResponse.lfaftp_prorated != null && objFeeResponse.lfaftp_prorated > 0 ? objFeeResponse.lfaftp_prorated : objFeeResponse.lfaftp;
            }
        }
        catch (Exception ex) {
            returnWrapper = new ReturnWrapper('ERROR',ex.getMessage());
            return ('ERROR: An error occurred while trying to retrieve the fee from the DSL in the sendForm method of the FeeCalculationFormController.\n' + ex.getMessage());
        }

        //instantiate new returnWrapper and populate fields
        returnWrapper = new ReturnWrapper(GlobalConstant.SUCCESS_LITERAL,'Fee Amount is $' + feeAmount);
        returnWrapper.feeAmount           = String.valueOf(feeAmount);
        returnWrapper.feeRequest          = feeRequestJSONString;  //this is being returned so that they can be stored on the transaction record to see the request that calculated the fee
        returnWrapper.feeResponse         = objFeeResponse.response.getBody(); //this is being returned so that they can be stored on the transaction record to see the request that calculated the fee
        returnWrapper.hasRateScheduleFees = True;

        //serialize wrapper class and return
        String returnValue = JSON.serialize(returnWrapper, true);
        //sendEmail('jsonrequest', returnValue);
        return returnValue;
    }

    /**
    * @description This AuraEnabled method creates the time period and transaction records.
    * @param recordId
    * @return jsonString
    */
    @AuraEnabled
    public static String saveReport(String recordId, String feeAmount, String feeRequest, String feeResponse, String policyTimePeriod) {
        ErrorLogger logger = new ErrorLogger();
        list<Transaction_Codes__mdt> transactionCodeList = [
                SELECT Id, Label, Transaction_Code__c, Transaction_Description__c
                From Transaction_Codes__mdt];

        Map<String,Transaction_Codes__mdt> transactionCodeMap = new Map<String,Transaction_Codes__mdt>();
        for (Transaction_Codes__mdt each: transactionCodeList){
            transactionCodeMap.put(each.Label,each);
        }

        System.debug('RevenueReportingFormController.saveReport');
        System.debug('recordId: ' + recordId);
        System.debug('feeAmount: ' + feeAmount);
        System.debug('feeRequest: ' + feeRequest);
        System.debug('feeResponse: ' + feeResponse);
        System.debug('policyTimePeriod: ' + policyTimePeriod);

        Map<String, Object> timePeriodMap = (Map<String, Object>) JSON.deserializeUntyped(policyTimePeriod);

        //convert time period dates passed in from string to date
        Date startDate = Date.valueOf(String.valueOf(timePeriodMap.get('Start_Date__c')));
        Date endDate = Date.valueOf(String.valueOf(timePeriodMap.get('End_Date__c')));

        //get the responsible parties from the license policy so that they can be stored on the time period for historical purposes
        License_Policy__c licensePolicy =[
                SELECT Id, Financially_Responsible__c, Billing_Entity__c, Reporting_Entity__c
                FROM License_Policy__c
                WHERE Id =: recordId
                LIMIT 1];

        //find any existing time periods for the same reporting period if they exist
        List<License_Policy_Time_Period__c> existingTimePeriod =[
                SELECT Id, License_Policy_Fee_Amount__c, Start_Date__c, End_Date__c
                        , Fee_Calculation_Request_String__c, Fee_Calculation_Response_String__c, Total_Amount__c
                        , Transaction_Count__c
                FROM License_Policy_Time_Period__c
                WHERE License_Policy__r.Id =: recordId
                AND Start_Date__c = : startDate
                AND End_Date__c =: endDate];

        //if the time period already exists, craete a new transaction that is the delta between the new and previous value
        Savepoint sp = Database.setSavepoint();
        if (existingTimePeriod.size() > 0){
            if (existingTimePeriod[0].Transaction_Count__c >= integer.valueOf(Label.Maximum_Number_of_VMVPD_Revisions)){
                throw new AscapException('Limit of 2 revisions per reporting period has been met. \n You can not submit and additional report for this reporting period.');
            }
            License_Policy_Time_Period__c timePeriod = new License_Policy_Time_Period__c();
            timePeriod.Id                                 = existingTimePeriod[0].Id;
            timePeriod.Fee_Calculation_Request_String__c  = feeRequest;
            timePeriod.Fee_Calculation_Response_String__c = feeResponse;
            timePeriod.Billing_Entity__c                  = licensePolicy.Billing_Entity__c;
            timePeriod.Financially_Responsible__c         = licensePolicy.Financially_Responsible__c;
            timePeriod.Reporting_Entity__c                = licensePolicy.Reporting_Entity__c;
            try {
                update timePeriod;
            }
            catch(Exception ex){
                logger.log(ex).save();
                throw new AscapException('An error occurred while attempting to submit your report, please contact Customer Service, or your ASCAPOne Administrator for further assistance.');
            }

            Transaction__c trans = new Transaction__c();
            trans.License_Policy_Time_Period__c      = existingTimePeriod[0].Id;
            system.debug('Debug-jwr: feeAmount>' + feeAmount);
            trans.Amount__c                          = Decimal.valueOf(feeAmount.replaceAll(',', '')) - existingTimePeriod[0].Total_Amount__c;
            if (trans.Amount__c > 0){
                trans.Transaction_Type__c            = GlobalConstant.BILLING_ITEM_LITERAL;
            }
            else{
                trans.Transaction_Type__c            = GlobalConstant.CREDIT_LITERAL;
            }
            trans.Transaction_Date__c                = Date.Today();
            trans.Transaction_Code__c                = transactionCodeMap.get('License Fee Adjustment').Transaction_Code__c;
            trans.Transaction_Description__c         = transactionCodeMap.get('License Fee Adjustment').Transaction_Description__c;
            trans.Fee_Calculation_Response_String__c = feeResponse;
            trans.Fee_Calculation_Request_String__c  = feeRequest;

            try {
                insert trans;
            }
            catch(Exception ex){
                Database.rollback(sp);
                logger.log(ex).save();
                throw new AscapException('An error occurred while attempting to sumbit your report, please contact Customer Service, or your ASCAPOne Administrator for further assistance.');
            }
        }
        else{
            //create new time period record
            License_Policy_Time_Period__c timePeriod = new License_Policy_Time_Period__c();
            timePeriod.Start_Date__c                      = startDate;
            timePeriod.End_Date__c                        = endDate;
            timePeriod.License_Policy__c                  = recordId;
            timePeriod.Fee_Calculation_Request_String__c  = feeRequest;
            timePeriod.Fee_Calculation_Response_String__c = feeResponse;
            timePeriod.Billing_Entity__c                  = licensePolicy.Billing_Entity__c;
            timePeriod.Financially_Responsible__c         = licensePolicy.Financially_Responsible__c;
            timePeriod.Reporting_Entity__c                = licensePolicy.Reporting_Entity__c;
            try {
                insert timePeriod;
            }
            catch(Exception ex){
                logger.log(ex).save();
                throw new AscapException('An error occurred while attempting to submit your report, please contact Customer Service, or your ASCAPOne Administrator for further assistance.');
            }

            //create new transaction record
            Transaction__c trans = new Transaction__c();
            trans.License_Policy_Time_Period__c      = timePeriod.Id;
            trans.Transaction_Date__c                = Date.Today();
            trans.Transaction_Type__c                = GlobalConstant.BILLING_ITEM_LITERAL;
            trans.Transaction_Code__c                = transactionCodeMap.get('License Fee').Transaction_Code__c;
            trans.Transaction_Description__c         = transactionCodeMap.get('License Fee').Transaction_Description__c;
            trans.Fee_Calculation_Response_String__c = feeResponse;
            trans.Fee_Calculation_Request_String__c  = feeRequest;
            trans.Amount__c                          = decimal.valueOf(feeAmount.replaceAll(',',''));

            try {
                insert trans;
            }
            catch(Exception ex){
                Database.rollback(sp);
                logger.log(ex).save();
                throw new AscapException('An error occurred while attempting to submit your report, please contact Customer Service, or your ASCAPOne Administrator for further assistance.');
            }
        }
        return '{"msg": "Records saved"}';

    }
    private static String buildDSLRequest(List<FieldMetadata> licensePolicySubmitted, String recordId, String policyName, String licenseClass, Map<String, Object> timePeriodMap){

        //build request string header
        String feeRequestString =
            '{"License_Policy":{'
                + ' "Id":"' + recordId + '",'
                + ' "Name":"' + policyName + '",'
                + ' "License_Type":"' + licenseClass + '",';

        //iterate over list of fields in policy
        for(FieldMetadata eachField: licensePolicySubmitted){
            if (eachField.field_type == GlobalConstant.PICKLIST_DATA_TYPE){
                feeRequestString += '"' + eachField.field_api_name + '":"' + eachField.selectedValue + '",';
            }
            if (eachField.field_type == GlobalConstant.NUMBER_DATA_TYPE){
                feeRequestString += '"' + eachField.field_api_name + '":' + eachField.decimalValue + ',';
            }
            if (eachField.field_type == GlobalConstant.BOOLEAN_DATA_TYPE){
                feeRequestString += '"' + eachField.field_api_name + '":' + eachField.booleanValue + ',';
            }
        }

        feeRequestString = feeRequestString.subStringBeforeLast(',') + '},';

        //add the time period property to the request
        feeRequestString +=
            '"Time_Period":{'
                +  '"type": "Annual",'
                +  '"period": "' + timePeriodMap.get('Start_Date__c') + '"}}';

        return feeRequestString;
    }

    /**
     * @description This private method is used to derive the start and end reporting dates based on the value in the
     * reporting frequency field.
     * @param reportingFrequency
     * @return list of dates (coverageDates)
     */
    private static List<Date> deriveCoverageDates(String reportingFrequency){
        List<Date> coverageDates = new List<Date>();

        //if reporting frequency is null, default to monthly
        if (reportingFrequency == null){
            reportingFrequency = 'monthly';
        }

        //if the reportingFrequency is either 'annual' or yearly, set the reportingFrequency to 'yearly'
        if (reportingFrequency.toLowerCase() == 'annual' || reportingFrequency.toLowerCase() == 'yearly'){
            reportingFrequency = 'yearly';
        }

        //generate default reporting dates based on reporting frequency value
        switch on reportingFrequency.toLowerCase(){
            when 'monthly' {
                coverageDates.add(System.Today().addMonths(-1).toStartOfMonth());
                coverageDates.add(System.Today().toStartOfMonth().addDays(-1));
            }
            when 'quarterly' {
                Set<String> firstQuarterMonthSet = new Set<String>{'1','2','3'};
                Set<String> secondQuarterMonthSet = new Set<String>{'4','5','6'};
                Set<String> thirdQuarterMonthSet = new Set<String>{'7','8','9'};
                Set<String> fourthQuarterMonthSet = new Set<String>{'10','11','12'};
                if (firstQuarterMonthSet.contains(string.valueOf(System.Today().month()))){
                    coverageDates.add(Date.newInstance(System.Today().year() - 1, 10, 1));
                    coverageDates.add(Date.newInstance(System.Today().year() - 1, 12, 31));
                }
                else if (secondQuarterMonthSet.contains(string.valueOf(System.Today().month()))){
                    coverageDates.add(Date.newInstance(System.Today().year(), 1, 1));
                    coverageDates.add(Date.newInstance(System.Today().year(), 3, 31));
                }
                else if (thirdQuarterMonthSet.contains(string.valueOf(System.Today().month()))){
                    coverageDates.add(Date.newInstance(System.Today().year(), 4, 1));
                    coverageDates.add(Date.newInstance(System.Today().year(), 6, 30));
                }
                else if (fourthQuarterMonthSet.contains(string.valueOf(System.Today().month()))){
                    coverageDates.add(Date.newInstance(System.Today().year(), 7, 1));
                    coverageDates.add(Date.newInstance(System.Today().year(), 9, 31));
                }
                else{
                    //add info to log indicating quarter not found
                    coverageDates.add(System.Today().addMonths(-1).toStartOfMonth());
                    coverageDates.add(System.Today().toStartOfMonth().addDays(-1));
                }

            }
            when 'semi-annual'{
                if (System.Today().month() > 6){
                    coverageDates.add(Date.newInstance(System.Today().year(), 1, 1));
                    coverageDates.add(Date.newInstance(System.Today().year(), 6, 30));
                }
                else{
                    coverageDates.add(Date.newInstance(System.Today().year() -1, 7, 1));
                    coverageDates.add(Date.newInstance(System.Today().year() -1 , 12, 31));
                }
            }
            when 'yearly' {
                coverageDates.add(Date.newInstance(System.Today().year() -1, 1, 1));
                coverageDates.add(Date.newInstance(System.Today().year() -1 , 12, 31));
            }
            when else{
                coverageDates.add(System.Today().addMonths(-1).toStartOfMonth());
                coverageDates.add(System.Today().toStartOfMonth().addDays(-1));
            }
        }
        return coverageDates;
    }

    /**
     * @description This wrapper class contains the metadata required to populate the lightning component page and
     * calculate the fee.
     */
    private class FieldMetadataWrapper {
        private String licenseClassId;
        private String licenseClassName;
        private String LicenseClassConfig;
        private String LicensePolicyName;
        private Date policyEndDate;
        private Integer reportingColumns;
        private List<FieldMetadata> field_level_metadata;
        private License_Policy_Time_Period__c timePeriod { get; set; }
        public Boolean hasRateScheduleFees { get;set; }

        public FieldMetadataWrapper(){
            //initialize time period and list
            timePeriod = new License_Policy_Time_Period__c();
            field_level_metadata = new list<FieldMetadata>();
        }
    }

    /**
     * @description This class describes the fields required in order to define a field to be rendered on the page
     */
    public class FieldMetadata implements Comparable {
        private String field_label;
        private String field_api_name;
        private String field_help_text;
        private String field_type;
        private Decimal field_order;
        private Integer field_minValue;
        private Integer field_maxValue;
        private String field_minValueMsg;
        private String field_maxValueMsg;
        private Boolean field_display;
        private Decimal max_discount;
        public Decimal decimalValue { get; set; }
        public Boolean booleanValue { get; set; }
        public String selectedValue { get; set; }
        public List<String> possible_values{ get;set; }
        public List<String> notAccepted_values{ get;set; }
        public List<String> dependent_fieldNames{ get;set; }


        public Integer compareTo(Object objToCompare) {
            FieldMetadata that = (FieldMetadata)objToCompare;
            if (this.field_order == that.field_order) return 0;
            if (this.field_order == null) return 1;
            if (that.field_order == null) return -1;
            if (this.field_order > that.field_order) return 1;
            return -1;
        }
    }

    /**
     * @description This wrapper class contains the fields returned to the revenue reporting form once the fee has been calculated.
    */
    public class ReturnWrapper {
        private String message {get;set;}
        private String code {get;set;}
        private String feeAmount {get;set;}
        private Boolean hasRateScheduleFees {get;set;}
        private String feeRequest {get;set;}
        private String feeResponse {get;set;}

        public ReturnWrapper(String errorCode, String  errorMessage){
            message = errorMessage;
            code = errorCode;
        }
    }

    /**
     * @description This method is used to send values that can't be fully displayed in the debug log to an email
     * address for debugging purposes and can be removed later - if desired.
     * @param subject
     * @param emailBody
     */
    public static void sendEmail(String subject, String emailBody){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'jreedy@ascap.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject(subject);
        mail.setPlainTextBody(emailBody);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}