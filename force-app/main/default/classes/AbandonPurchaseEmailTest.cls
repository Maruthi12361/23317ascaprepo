/**
 * Created by johnreedy on 2019-04-16.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  -------------  ---------------------------------------------------------------------*
*   2019-04-16   Xede Consulting  Initial Creation                                                  *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This test class execises the methhods in the ...
* @author Xede Consulting Group
* @date 2019-04-16 
*/
@IsTest
private class AbandonPurchaseEmailTest {
    static Lead newLead;

    /**
    * @description This method initializes setup data.
    */
    static void init(){
        if (newLead == null) {

            //insert license class
            License_Class__c licenseClass = TestDataHelper.createLicenseClass('test_license_class', GlobalConstant.FORM_TYPE, true);

            //insert new lead with fields required for conversion
            newLead = TestDataHelper.createLeadValidPremiseAddress('Email', 'Abandon', 'Abandon Email LLC', false);
            newLead.Company_Name__c = 'Test Company';
            newLead.Company = 'Legal Business Entity';
            newLead.FirstName = 'Primary';
            newLead.LastName = 'Contact';
            newLead.Email = 'primary.contact@test.com';
            newLead.Secondary_Contact_First_Name__c = 'Secondary';
            newLead.Secondary_Contact_Last_Name__c = 'Contact';
            newLead.Secondary_Contact_Email__c = 'secondary.contact@test.com';
            newLead.Region__c = 'Northeast';
            newLead.License_Class_Relationship__c = licenseClass.Id;
            newLead.Fee_Calculation_Request__c = label.JSON_Test_Request_String;
            newLead.Fee_Calculation_Response__c = label.JSON_Test_Response_String;
            newLead.Estimated_Value__c = 0;
            newLead.Valid_Address_Indicator__c = True;
            newLead.Address_Status__c = 'MelissaData - Validated';
            newLead.LeadSource = label.Self_Serve_LeadSource_Literal;
            insert newLead;

            Database.LeadConvert lc = new database.LeadConvert();
            lc.setLeadId(newLead.id);

            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
            lc.setConvertedStatus(convertStatus.MasterLabel);

            Database.LeadConvertResult lcr = Database.convertLead(lc);
            System.assert(lcr.isSuccess());
        }
    }

    /**
    * @description This test class method tests default controller.
    */
    static testMethod void testDefaultConstructor(){
        //initialize test data
        init();
        string thirty_minutes_ago = Datetime.now().addMinutes(-integer.valueOf(label.Lead_Abandon_Email_Time_Interval)+10).format();

        //instantiate mock response class
        Test.setMock(HttpCalloutMock.class, new MelissaDataValidResponseMock());

        //verify that newLead was created and Abandon Email Date is null
        Opportunity newOpportunity = [Select Id, Name, Abandon_Email_Sent_Date__c From Opportunity limit 1];
        Test.setCreatedDate(newOpportunity.Id, Datetime.parse(thirty_minutes_ago));

        system.assert(true,newOpportunity.Abandon_Email_Sent_Date__c == null);

        newOpportunity.StageName = label.Opportunity_SelfService_StageName;
        update newOpportunity;

        //run test using default constructor (no parameters)
        test.startTest();
            string query = 'Select Id, StageName, CreatedDate, Abandon_Email_Sent_Date__c  From Opportunity where CreatedDate = Today';
            AbandonPurchaseEmailBatch controller = new AbandonPurchaseEmailBatch(query);
            Database.executeBatch(controller);
        test.stopTest();

        //validate that Abandon Email Sent Date is no longer null
        newOpportunity = [Select Id, Name, Abandon_Email_Sent_Date__c, CreatedDate From Opportunity limit 1];
        system.debug('Debug: newOpportunity>' + newOpportunity);
        system.assert(true,newLead.Abandon_Email_Sent_Date__c != null);
    }
    /**
    * @description This test class method tests the schedulable class.
    */
    static testMethod void testSchedulableClass(){

        test.startTest();
            Id jobId = System.schedule('Abandon Purchase Email', '0 0 0 * * ? *', new AbandonPurchaseEmailSched());
        test.stopTest();

        System.assert(jobId != null);

    }
}