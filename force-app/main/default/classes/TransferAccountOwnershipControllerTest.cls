/**
 * Created by Xede Consulting Group (Jason Burke) on 10/25/2019.
 */

@IsTest
private class TransferAccountOwnershipControllerTest {
    @testSetup
    static void setup() {
        TestDataHelper.createAscapGlobalProperties(false, 0);
        TestDataHelper.createLicenseClass(GlobalConstant.TMLC_LICENSE_CLASS_NAME, GlobalConstant.INDUSTRY_TYPE, true);
    }

    @isTest
    static void testGetLicenseClasses() {
        Test.startTest();
        License_Class__c[] classes = TransferAccountOwnershipController.getLicenseClasses();
        Test.stopTest();
        System.assertEquals(1, classes.size());
    }

    @isTest
    static void testGetTmlcLicensePolicy() {
        License_Class__c tmlcLicenseClass = [SELECT Id FROM License_Class__c WHERE License_Policy_Record_Type__c = :GlobalConstant.INDUSTRY_TYPE];

        Account venueAccount = TestDataHelper.createAccount('Station Account', 'Station', true);
        Date startDate = Date.today().toStartOfMonth();
        Date endDate = startDate.addMonths(1).addDays(-1);
        License_Policy__c licensePolicy = TestDataHelper.createLicensePolicy(tmlcLicenseClass.Id, startDate, endDate, false);
        licensePolicy.Venue__c = venueAccount.Id;
        licensePolicy.Licensing_Area__c = GlobalConstant.BROADCAST_TELEVISION_LITERAL;
        licensePolicy.License_Type__c = GlobalConstant.BLANKET_TYPE;
        licensePolicy.Allocated_Fee_Amount__c = 1000;
        licensePolicy.LNSPA_Percentage__c = 20;
        insert licensePolicy;

        Test.startTest();
        License_Policy__c loadedLicensePolicy = TransferAccountOwnershipController.getTmlcLicensePolicy(venueAccount.Id);
        Test.stopTest();

        System.assertNotEquals(null, licensePolicy);
        System.assertEquals(venueAccount.Name, loadedLicensePolicy.Venue__r.Name);
        System.assertEquals(licensePolicy.License_Type__c, loadedLicensePolicy.License_Type__c);
        System.assertEquals(licensePolicy.Allocated_Fee_Amount__c, loadedLicensePolicy.Allocated_Fee_Amount__c);
        System.assertEquals(licensePolicy.LNSPA_Percentage__c, loadedLicensePolicy.LNSPA_Percentage__c);
    }

    @isTest
    static void testTransferOwnership() {
        Date todayDate = Date.today();

        Account legalEntityAccount1 = TestDataHelper.createAccount('Legal Entity 1', 'Legal Entity', true);
        Account legalEntityAccount2 = TestDataHelper.createAccount('Legal Entity 2', 'Legal Entity', true);
        Account venueAccount = TestDataHelper.createAccount('Station Account', 'Station', false);
        venueAccount.ParentId = legalEntityAccount1.Id;
        insert venueAccount;

        License_Class__c tmlcLicenseClass = [SELECT Id FROM License_Class__c WHERE License_Policy_Record_Type__c = :GlobalConstant.INDUSTRY_TYPE];

        Date policy1StartDate = todayDate.toStartOfMonth().addYears(-1);
        License_Policy__c policy1 = TestDataHelper.createLicensePolicy(tmlcLicenseClass.Id, policy1StartDate, null, false);
        policy1.Licensee__c = legalEntityAccount1.Id;
        policy1.Venue__c = venueAccount.Id;
        policy1.Licensing_Area__c = GlobalConstant.BROADCAST_TELEVISION_LITERAL;
        policy1.License_Type__c = GlobalConstant.BLANKET_TYPE;
        policy1.Allocated_Fee_Amount__c = 1000.0;
        policy1.Alternative_Blanket_Fee_Amount__c = 575.0;
        policy1.LNSPA_Percentage__c = 50.0;
        insert policy1;

        Date timePeriod1StartDate = todayDate.toStartOfMonth();
        Date timePeriod1EndDate = timePeriod1StartDate.addMonths(1).addDays(-1);
        License_Policy_Time_Period__c timePeriod1 = TestDataHelper.createLicensePolicyTimePeriod(policy1.Id, timePeriod1StartDate, timePeriod1EndDate, true);

        Transaction__c tran1 = TestDataHelper.createTransaction(timePeriod1.Id, 1000, true);

        Date effectiveDate = todayDate.addMonths(-1).toStartOfMonth();
        Date[] offAirDates = new Date[] { todayDate.toStartOfMonth() };
        Test.startTest();
        TransferAccountOwnershipController.transferOwnership(venueAccount.Id, legalEntityAccount2.Id, effectiveDate, offAirDates, GlobalConstant.ALT_BLANKET_TYPE, policy1.Id);
        Test.stopTest();

        // validate the old records
        venueAccount = [
                SELECT ParentId
                FROM Account
                WHERE Id = :venueAccount.Id
        ];
        System.assertEquals(legalEntityAccount2.Id, venueAccount.ParentId);

        policy1 = [
                SELECT License_Class_Type__c, Start_Date__c, End_Date__c, Licensee__c, Venue__c, License_Type__c,
                        Allocated_Fee_Amount__c, Alternative_Blanket_Fee_Amount__c, LNSPA_Percentage__c
                FROM License_Policy__c
                WHERE Id = :policy1.Id
        ];
        System.assertEquals(tmlcLicenseClass.Id, policy1.License_Class_Type__c);
        System.assertEquals(policy1StartDate, policy1.Start_Date__c);
        System.assertEquals(effectiveDate.addDays(-1), policy1.End_Date__c);
        System.assertEquals(legalEntityAccount1.Id, policy1.Licensee__c);
        System.assertEquals(venueAccount.Id, policy1.Venue__c);
        System.assertEquals(GlobalConstant.BLANKET_TYPE, policy1.License_Type__c);
        System.assertEquals(1000, policy1.Allocated_Fee_Amount__c);
        System.assertEquals(575, policy1.Alternative_Blanket_Fee_Amount__c);
        System.assertEquals(50, policy1.LNSPA_Percentage__c);

        timePeriod1 = [
                SELECT License_Policy__c, Start_Date__c, End_Date__c, Amount_Due__c
                FROM License_Policy_Time_Period__c
                WHERE Id = :timePeriod1.Id
        ];
        System.assertEquals(policy1.Id, timePeriod1.License_Policy__c);
        System.assertEquals(timePeriod1StartDate, timePeriod1.Start_Date__c);
        System.assertEquals(timePeriod1EndDate, timePeriod1.End_Date__c);
        System.assertEquals(0, timePeriod1.Amount_Due__c);

        tran1 = [
                SELECT License_Policy_Time_Period__c, Amount__c
                FROM Transaction__c
                WHERE Id = :tran1.Id
        ];
        System.assertEquals(timePeriod1.Id, tran1.License_Policy_Time_Period__c);
        System.assertEquals(1000, tran1.Amount__c);

        // validate the new records
        License_Policy__c[] newPolicyList = [
                SELECT Start_Date__c, License_Type__c, Allocated_Fee_Amount__c, Alternative_Blanket_Fee_Amount__c, LNSPA_Percentage__c,
                        Billing_Entity__c, Financially_Responsible__c, Reporting_Entity__c
                FROM License_Policy__c
                WHERE Licensee__c = :legalEntityAccount2.Id
                AND Venue__c = :venueAccount.Id
        ];
        System.assertEquals(1, newPolicyList.size());
        License_Policy__c policy2 = newPolicyList[0];
        System.assertEquals(effectiveDate, policy2.Start_Date__c);
        System.assertEquals(GlobalConstant.ALT_BLANKET_TYPE, policy2.License_Type__c);
        System.assertEquals(1000, policy2.Allocated_Fee_Amount__c);
        System.assertEquals(575, policy1.Alternative_Blanket_Fee_Amount__c);
        System.assertEquals(50, policy2.LNSPA_Percentage__c);
        System.assertEquals(null, policy2.Billing_Entity__c);
        System.assertEquals(null, policy2.Financially_Responsible__c);
        System.assertEquals(null, policy2.Reporting_Entity__c);

        License_Policy_Time_Period__c[] newTimePeriodList = [
                SELECT Start_Date__c, End_Date__c, Amount_Due__c
                FROM License_Policy_Time_Period__c
                WHERE License_Policy__c = :policy2.Id
                ORDER BY Start_Date__c
        ];
        System.assertEquals(2, newTimePeriodList.size());
        System.assertEquals(todayDate.addMonths(-1).toStartOfMonth(), newTimePeriodList[0].Start_Date__c);
        System.assertEquals(todayDate.toStartOfMonth().addDays(-1), newTimePeriodList[0].End_Date__c);
        System.assertEquals(575, newTimePeriodList[0].Amount_Due__c);

        System.assertEquals(todayDate.toStartOfMonth(), newTimePeriodList[1].Start_Date__c);
        System.assertEquals(todayDate.addMonths(1).toStartOfMonth().addDays(-1), newTimePeriodList[1].End_Date__c);
        System.assertEquals(0, newTimePeriodList[1].Amount_Due__c);

        Transaction__c[] newTranList = [
                SELECT Amount__c
                FROM Transaction__c
                WHERE License_Policy_Time_Period__c IN :newTimePeriodList
        ];
        System.assertEquals(1, newTranList.size());
        System.assertEquals(575, newTranList[0].Amount__c);
    }

    @isTest
    static void testPerProgramTransfer() {
        Date todayDate = Date.today();

        Account legalEntityAccount1 = TestDataHelper.createAccount('Legal Entity 1', 'Legal Entity', true);
        Account legalEntityAccount2 = TestDataHelper.createAccount('Legal Entity 2', 'Legal Entity', true);
        Account reporter = TestDataHelper.createAccount('Reporting Entity', 'Representative Body', false);
        reporter.Account_Classification__c = 'Vendor';
        insert reporter;
        Account venueAccount = TestDataHelper.createAccount('Station Account', 'Station', false);
        venueAccount.ParentId = legalEntityAccount1.Id;
        insert venueAccount;

        License_Class__c tmlcLicenseClass = [SELECT Id FROM License_Class__c WHERE License_Policy_Record_Type__c = :GlobalConstant.INDUSTRY_TYPE];

        Date policy1StartDate = todayDate.toStartOfMonth().addYears(-1);
        License_Policy__c policy1 = TestDataHelper.createLicensePolicy(tmlcLicenseClass.Id, policy1StartDate, null, false);
        policy1.Licensee__c = legalEntityAccount1.Id;
        policy1.Reporting_Entity__c = reporter.Id;
        policy1.Venue__c = venueAccount.Id;
        policy1.Licensing_Area__c = GlobalConstant.BROADCAST_TELEVISION_LITERAL;
        policy1.License_Type__c = GlobalConstant.PER_PROGRAM_TYPE;
        policy1.Allocated_Fee_Amount__c = 1000.0;
        policy1.Alternative_Blanket_Fee_Amount__c = 575.0;
        policy1.LNSPA_Percentage__c = 50.0;
        insert policy1;

        Date timePeriod1StartDate = todayDate.addMonths(-1).toStartOfMonth();
        Date timePeriod1EndDate = timePeriod1StartDate.addMonths(1).addDays(-1);
        License_Policy_Time_Period__c timePeriod1 = TestDataHelper.createLicensePolicyTimePeriod(policy1.Id, timePeriod1StartDate, timePeriod1EndDate,true);

        Transaction__c tranInitial = TestDataHelper.createTransaction(timePeriod1.Id, 1000, GlobalConstant.INITIAL_QUOTE_LITERAL, true);
        Transaction__c tranFinal = TestDataHelper.createTransaction(timePeriod1.Id, 500, GlobalConstant.FINAL_LITERAL, true);
        Transaction__c tranPayment = TestDataHelper.createTransaction(timePeriod1.Id, -1500, GlobalConstant.PAYMENT_LITERAL, true);

        Date effectiveDate = todayDate.addMonths(-1).toStartOfMonth();
        Date[] offAirDates = new Date[] { todayDate.toStartOfMonth() };
        Test.startTest();
        TransferAccountOwnershipController.transferOwnership(venueAccount.Id, legalEntityAccount2.Id, effectiveDate, offAirDates, GlobalConstant.PER_PROGRAM_TYPE, policy1.Id);
        Test.stopTest();

        // validate the old records
        venueAccount = [
                SELECT ParentId
                FROM Account
                WHERE Id = :venueAccount.Id
        ];
        System.assertEquals(legalEntityAccount2.Id, venueAccount.ParentId);

        policy1 = [
                SELECT License_Class_Type__c, Start_Date__c, End_Date__c, Licensee__c, Venue__c, License_Type__c,
                        Allocated_Fee_Amount__c, Alternative_Blanket_Fee_Amount__c, LNSPA_Percentage__c
                FROM License_Policy__c
                WHERE Id = :policy1.Id
        ];
        System.assertEquals(tmlcLicenseClass.Id, policy1.License_Class_Type__c);
        System.assertEquals(policy1StartDate, policy1.Start_Date__c);
        System.assertEquals(effectiveDate.addDays(-1), policy1.End_Date__c);
        System.assertEquals(legalEntityAccount1.Id, policy1.Licensee__c);
        System.assertEquals(venueAccount.Id, policy1.Venue__c);
        System.assertEquals(GlobalConstant.PER_PROGRAM_TYPE, policy1.License_Type__c);
        System.assertEquals(1000, policy1.Allocated_Fee_Amount__c);
        System.assertEquals(575, policy1.Alternative_Blanket_Fee_Amount__c);
        System.assertEquals(50, policy1.LNSPA_Percentage__c);

        timePeriod1 = [
                SELECT License_Policy__c, Start_Date__c, End_Date__c, Amount_Due__c
                FROM License_Policy_Time_Period__c
                WHERE Id = :timePeriod1.Id
        ];
        System.assertEquals(policy1.Id, timePeriod1.License_Policy__c);
        System.assertEquals(timePeriod1StartDate, timePeriod1.Start_Date__c);
        System.assertEquals(timePeriod1EndDate, timePeriod1.End_Date__c);
        System.assertEquals(-1500, timePeriod1.Amount_Due__c);

        tranInitial = [
                SELECT License_Policy_Time_Period__c, Amount__c, Transaction_Type__c
                FROM Transaction__c
                WHERE Id = :tranInitial.Id
        ];
        System.assertEquals(timePeriod1.Id, tranInitial.License_Policy_Time_Period__c);
        System.assertEquals(GlobalConstant.INITIAL_QUOTE_LITERAL, tranInitial.Transaction_Type__c);
        System.assertEquals(1000, tranInitial.Amount__c);

        tranFinal = [
                SELECT License_Policy_Time_Period__c, Amount__c, Transaction_Type__c
                FROM Transaction__c
                WHERE Id = :tranFinal.Id
        ];
        System.assertEquals(timePeriod1.Id, tranFinal.License_Policy_Time_Period__c);
        System.assertEquals(GlobalConstant.FINAL_LITERAL, tranFinal.Transaction_Type__c);
        System.assertEquals(500, tranFinal.Amount__c);

        tranPayment = [
                SELECT License_Policy_Time_Period__c, Amount__c, Transaction_Type__c
                FROM Transaction__c
                WHERE Id = :tranPayment.Id
        ];
        System.assertEquals(timePeriod1.Id, tranPayment.License_Policy_Time_Period__c);
        System.assertEquals(GlobalConstant.PAYMENT_LITERAL, tranPayment.Transaction_Type__c);
        System.assertEquals(-1500, tranPayment.Amount__c);

        // validate the new records
        License_Policy__c[] newPolicyList = [
                SELECT Start_Date__c, License_Type__c, Allocated_Fee_Amount__c, Alternative_Blanket_Fee_Amount__c, LNSPA_Percentage__c,
                        Billing_Entity__c, Financially_Responsible__c, Reporting_Entity__c
                FROM License_Policy__c
                WHERE Licensee__c = :legalEntityAccount2.Id
                AND Venue__c = :venueAccount.Id
        ];
        System.assertEquals(1, newPolicyList.size());
        License_Policy__c policy2 = newPolicyList[0];
        System.assertEquals(effectiveDate, policy2.Start_Date__c);
        System.assertEquals(GlobalConstant.PER_PROGRAM_TYPE, policy2.License_Type__c);
        System.assertEquals(1000, policy2.Allocated_Fee_Amount__c);
        System.assertEquals(575, policy1.Alternative_Blanket_Fee_Amount__c);
        System.assertEquals(50, policy2.LNSPA_Percentage__c);
        System.assertEquals(null, policy2.Billing_Entity__c);
        System.assertEquals(null, policy2.Financially_Responsible__c);
        System.assertEquals(null, policy2.Reporting_Entity__c);

        License_Policy_Time_Period__c[] newTimePeriodList = [
                SELECT Start_Date__c, End_Date__c, Amount_Due__c
                FROM License_Policy_Time_Period__c
                WHERE License_Policy__c = :policy2.Id
                ORDER BY Start_Date__c
        ];
        System.assertEquals(2, newTimePeriodList.size());
        System.assertEquals(todayDate.addMonths(-1).toStartOfMonth(), newTimePeriodList[0].Start_Date__c);
        System.assertEquals(todayDate.toStartOfMonth().addDays(-1), newTimePeriodList[0].End_Date__c);
        System.assertEquals(1500, newTimePeriodList[0].Amount_Due__c);

        System.assertEquals(todayDate.toStartOfMonth(), newTimePeriodList[1].Start_Date__c);
        System.assertEquals(todayDate.addMonths(1).toStartOfMonth().addDays(-1), newTimePeriodList[1].End_Date__c);
        System.assertEquals(0, newTimePeriodList[1].Amount_Due__c);

        Transaction__c[] newTranList = [
                SELECT Amount__c, Transaction_Type__c
                FROM Transaction__c
                WHERE License_Policy_Time_Period__c = :newTimePeriodList[0].Id
                ORDER BY Amount__c ASC
        ];
        System.assertEquals(2, newTranList.size());
        System.debug(newTranList);
        System.assertEquals(GlobalConstant.FINAL_LITERAL, newTranList[0].Transaction_Type__c);
        System.assertEquals(500, newTranList[0].Amount__c);
        System.assertEquals(GlobalConstant.INITIAL_QUOTE_LITERAL, newTranList[1].Transaction_Type__c);
        System.assertEquals(1000, newTranList[1].Amount__c);
    }
}