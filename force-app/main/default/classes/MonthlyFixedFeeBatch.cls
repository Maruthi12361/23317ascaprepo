/**
 * Created by Xede Consulting Group (Jason Burke) on 11/25/2019.
 *
 * @description Generates time periods and transactions for Fixed Fee-type license policies
 */
public class MonthlyFixedFeeBatch extends MonthlyFeeBatch implements Database.Batchable<SObject> {
    private final Map<Id, Date> policyIdDueDateMap;

    private final Date[] batchDateList;

    private final Map<Date, List<Id>> batchDatePolicyIdMap;

    /**
     * @description Creates an instance to process the current month
     */
    public MonthlyFixedFeeBatch() {
        this(null, null);
    }

    /**
     * @description Creates an instance to process the given month and year
     *
     * @param month Month to process (1=Jan)
     * @param year Year to process
     */
    public MonthlyFixedFeeBatch(Integer month, Integer year) {
        this(month, year, null);
    }

    /**
     * @description Creates an instance to process the given month and year only for the given list of license policy Ids.
     *
     * @param month Month to process (1=Jan)
     * @param year Year to process
     * @param policyIdList list of license policy Ids to process
     */
    public MonthlyFixedFeeBatch(Integer month, Integer year, Id[] policyIdList) {
        super('MonthlyFixedFeeBatch', month, year, 1);

        this.query = buildQuery(policyIdList);
        }

    public MonthlyFixedFeeBatch(Date[] batchDateList, Map<Date, List<Id>> batchDatePolicyIdMap, Map<Id, Date> policyIdDueDateMap) {
        super('MonthlyFixedFeeBatch', batchDateList[0].month(), batchDateList[0].year(), 1);

        this.batchDateList = batchDateList;
        this.batchDatePolicyIdMap = batchDatePolicyIdMap;
        this.policyIdDueDateMap = policyIdDueDateMap;

        Date batchDate = this.batchDateList.remove(0);
        Id[] policyIdList = this.batchDatePolicyIdMap.remove(batchDate);

        this.query = buildQuery(policyIdList);
    }

    public override void finish(Database.BatchableContext bc) {
        if (batchDateList != null && !batchDateList.isEmpty()) {
            Database.executeBatch(new MonthlyFixedFeeBatch(batchDateList, batchDatePolicyIdMap, policyIdDueDateMap));
        }
    }

    /**
     * @description see MonthlyFeeBatch class
     */
    protected override Date getTimePeriodStartDate(License_Policy__c policy) {
        Fixed_Fee_Definition__c fixedFeeDef = MonthlyFixedFeeHelper.getFixedFeeDefinition(policy, startMonthlyDate, endMonthlyDate);
        if (fixedFeeDef.Billing_Frequency__c == Label.Billing_Frequency_Monthly) {
            return super.getTimePeriodStartDate(policy);
        }

        return MonthlyFixedFeeHelper.getTimePeriodStartDate(policy, startMonthlyDate, fixedFeeDef);
    }

    /**
     * @description see MonthlyFeeBatch class
     */
    protected override Date getTimePeriodEndDate(License_Policy__c policy) {
        Fixed_Fee_Definition__c fixedFeeDef = MonthlyFixedFeeHelper.getFixedFeeDefinition(policy, startMonthlyDate, endMonthlyDate);
        if (fixedFeeDef.Billing_Frequency__c == Label.Billing_Frequency_Monthly) {
            return super.getTimePeriodEndDate(policy);
        }

        return MonthlyFixedFeeHelper.getTimePeriodEndDate(policy, endMonthlyDate, fixedFeeDef);
    }

    /**
     * @description see MonthlyFeeBatch class
     */
    protected override Date calculateTransactionDate(License_Policy__c policy) {
        if (policyIdDueDateMap != null && policyIdDueDateMap.containsKey(policy.Id)) {
            return policyIdDueDateMap.get(policy.Id);
        }

        Date transDate = MonthlyFixedFeeHelper.calculateTransactionDate(policy, startMonthlyDate, endMonthlyDate);
        return transDate != null ? transDate : super.calculateTransactionDate(policy);
    }

    /**
     * @description see MonthlyFeeBatch class
     */
    protected override Decimal[] calculateFeeAmounts(License_Policy__c policy, Transaction__c[] tranList) {
        Fixed_Fee_Definition__c fixedFeeDef = MonthlyFixedFeeHelper.getFixedFeeDefinition(policy, startMonthlyDate, endMonthlyDate);
        Integer numMonths = fixedFeeDef.Start_Date__c.monthsBetween(fixedFeeDef.End_Date__c) + 1;
        if (fixedFeeDef.Billing_Frequency__c == Label.Billing_Frequency_Monthly) {
            return new Decimal[] { fixedFeeDef.Total_Amount__c / numMonths };
        }

        Decimal numMonthsInPeriod = Decimal.valueOf(MonthlyFixedFeeHelper.getNumMonthsInPeriod(fixedFeeDef));

        Integer numPeriods = Math.ceil(numMonths / numMonthsInPeriod).intValue();

        return new Decimal[] { fixedFeeDef.Total_Amount__c / numPeriods };
    }

    private String buildQuery(Id[] policyIdList) {
        String query =
                'SELECT License_Type__c, Start_Date__c,' +
                        ' Licensee__c, Billing_Entity__c, Financially_Responsible__c, Reporting_Entity__c,' +
                        ' (SELECT Start_Date__c, End_Date__c, License_Type__c,' +
                        ' Billing_Entity__c, Financially_Responsible__c, Reporting_Entity__c' +
                        ' FROM License_Policy_Time_Periods__r' +
                        ' WHERE Start_Date__c <= ' + endMonthlyDateStr +
                        ' AND End_Date__c >= ' + startMonthlyDateStr + '),' +
                        ' (SELECT ' + ASCAPUtils.getAllFieldNamesSelectString('Fixed_Fee_Definition__c') +
                        ' FROM Fixed_Fee_Definitions__r' +
                        ' ORDER BY Start_Date__c ASC)' +
                        ' FROM License_Policy__c' +
                        ' WHERE RecordType.DeveloperName = \'' + GlobalConstant.FIXED_TYPE + '\'' +
                        ' AND Status__c = \'' + GlobalConstant.ACTIVE_LITERAL + '\'' +
                        ' AND Start_Date__c <= ' + endMonthlyDateStr +
                        ' AND (End_Date__c = null OR End_Date__c >= ' + startMonthlyDateStr + ')';

        if (policyIdList != null && !policyIdList.isEmpty()) {
            query += ' AND Id IN (\'' + String.join(policyIdList, '\', \'') + '\')';
        }
        System.debug(query);

        return query;
    }
}