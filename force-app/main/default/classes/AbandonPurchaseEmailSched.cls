/**
 * Created by johnreedy on 2019-04-16.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-04-16   Xede Consulting Group 		Initial Creation                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This schedulable class executes the AbandonPurchaseEmailSched process.
* @author Xede Consulting Group
* @see AbandonPurchaseEmailSchedTest
* @date 2019-04-16 
*/
global class AbandonPurchaseEmailSched implements Schedulable {
    global void execute(SchedulableContext SC) {
        AbandonPurchaseEmailBatch controller = new AbandonPurchaseEmailBatch();
        database.executebatch(controller);
    }
}