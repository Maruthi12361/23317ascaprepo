public class LaunchChatPageController {

    public String recordId {get; set;}
    public String leadId {get; set;}
    public String contactId {get; set;}
    public String accountId {get; set;}
    public String premiseAccountId {get; set;}
    public Boolean enableChat{get; set;}

    public LaunchChatPageController(){
        leadId = apexpages.currentpage().getparameters().get('leadId');
        contactId = apexpages.currentpage().getparameters().get('contactId');
        accountId = apexpages.currentpage().getparameters().get('accountId');
        premiseAccountId = apexpages.currentpage().getparameters().get('premiseAccountId');
        recordId = apexpages.currentpage().getparameters().get('recordId');
        enableChat = false;
    }

    public PageReference onPageLoad(){
        enableChat = true;
        return null;
    }

}