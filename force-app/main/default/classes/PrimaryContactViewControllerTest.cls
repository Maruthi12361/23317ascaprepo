/**
 * Created by johnreedy on 2019-08-07.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-08-07   Xede Consulting Group 		Initial Creation                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This test class exercises the PrimaryContactViewController.
* @author Xede Consulting Group
* @see PrimaryContactViewControllerTest
* @date 2019-08-07 
*/
@IsTest
private class PrimaryContactViewControllerTest {

    static Account newAccount;
    static Contact newContact;
    static Opportunity newOppty;

    /**
    * @description This method initializes setup data.
    */
    static void init(){
        newAccount = TestDataHelper.createAccount('DragonClaw Enterprises', label.RecordTypeAccountLegalEntity, true);
        newContact = TestDataHelper.createContact(newAccount.Id, 'Luther', 'Lex',  true);
        newOppty   = TestDataHelper.createOpportunity('Test Oppty', newAccount.Id, label.RecordTypeOpptyFormLicense, 'Prospect', true);
    }

    /**
    * @description For a given Opportunity with a Primary Contact, verify that the controller returns the PrimaryContactInfo.
    */
    static testMethod void testController(){
        //initialize test data
        init();

        test.startTest();
            OpportunityContactRole ocr = new OpportunityContactRole();
            ocr.OpportunityId   = newOppty.Id;
            ocr.ContactId       = newContact.Id;
            ocr.IsPrimary       = True;
            insert ocr;
        test.stopTest();

        PrimaryContactViewController.PrimaryContactInfo pci = PrimaryContactViewController.getPrimaryContact(newOppty.Id);
        system.assertEquals('Lex Luther', pci.ContactName);
    }
}