public class LeadConversionHelper {

    public static void createLicensePolicyTransaction(list<Opportunity> opportunityList, map<id,Opportunity> oldMap){

        /*
        Because this process needs to create a child, grandchild and great grandchild, this process cannot be bulkified.
        As a result, if the opportunityList size is greater than 1, exist the process.
        */
        if (opportunityList.size() > 1){
            return;
        }

        list<License_Policy__c> relatedPolicy = [Select id,License_Class_Type__c,License_Class_Type__r.Coverage_Type__c  From License_Policy__c where Opportunity__c in: opportunityList];
        list<License_Policy_Time_Period__c> relatedTimePeriod = [Select id, License_Policy__c,License_Policy__r.License_Class_Type__c,License_Policy__r.License_Class_Type__r.Coverage_Type__c From License_Policy_Time_Period__c where License_Policy__c in: relatedPolicy];
        list<Transaction__c> relatedTransaction = [Select id From Transaction__c where License_Policy_Time_Period__c In: relatedTimePeriod];

        /*
        Iterate over opportunity list of opportunities and create or update license policy, time period, and transaction.
        */
        Set<Id> licenseClassIds = new Set<Id>();
        Map<Id,License_Class__c> licenseClassMap = new Map<Id,License_Class__c>();
        for (Opportunity eachOppty: opportunityList){
            licenseClassIds.add(eachOppty.License_Class_Relationship__c);
        }
        if(!licenseClassIds.isEmpty()){
            licenseClassMap = new Map<Id,License_Class__c>([select Id,Coverage_Type__c,Name,License_Policy_Record_Type__c from License_Class__c where Id=:licenseClassIds]);
        }
        License_Class__c associatedLicenseClass;
        for (Opportunity eachOppty: opportunityList){
            if (oldMap == null || (string.isNotBlank(eachOppty.Fee_Calculation_Request__c) &&
                    string.isNotBlank(eachOppty.License_Class_Relationship__c) && (eachOppty.Amount != oldMap.get(eachOppty.Id).Amount))) {
                associatedLicenseClass = licenseClassMap.get(eachOppty.License_Class_Relationship__c);

                License_Policy__c newLicPolicy = new License_Policy__c();
                //Insert/update License Policy
                if (relatedPolicy.size() == 1){
                    newLicPolicy.id = relatedPolicy[0].Id;
                }
                else {
                    newLicPolicy.RecordTypeId = Schema.SObjectType.License_Policy__c.getRecordTypeInfosByDeveloperName().get(associatedLicenseClass.License_Policy_Record_Type__c).getRecordTypeId();
                }
                newLicPolicy.Licensee__c            = eachOppty.AccountId;
                newLicPolicy.License_Class_Type__c  = eachOppty.License_Class_Relationship__c;
                newLicPolicy.Opportunity__c         = eachOppty.Id;
                newLicPolicy.Status__c              = 'Draft';
                newLicPolicy.Start_Date__c          = date.Today();

                upsert newLicPolicy;

                String licenseClassCoverage = associatedLicenseClass != null ? associatedLicenseClass.Coverage_Type__c : null;



                //Insert/update Time Period
                integer YearNumber = Date.Today().year();
                License_Policy_Time_Period__c newLicPolicyTP = new License_Policy_Time_Period__c();
                //if Time Period exists, set id, else set license policy id for insert (master-detail relationship)
                if (relatedTimePeriod.size() == 1){
                    newLicPolicyTP.id = relatedTimePeriod[0].Id;
                }
                else{
                    newLicPolicyTP.License_Policy__c   = newLicPolicy.Id;
                }
                newLicPolicyTP.Start_Date__c       = date.newInstance(YearNumber, 1,1);
                newLicPolicyTP.End_Date__c         = date.newInstance(YearNumber, 12,31);
                //if Coverage Type is Contract Year, override default with today's date and a year from now
                if (licenseClassCoverage != null && licenseClassCoverage == 'Contract Year') {
                    newLicPolicyTP.Start_Date__c = System.today();
                    newLicPolicyTP.End_Date__c   = System.today().addDays(-1).addYears(1);
                }
                newLicPolicyTP.Billing_Entity__c  = eachOppty.AccountId;
                newLicPolicyTP.Transaction_Date__c = date.Today();

                upsert newLicPolicyTP;

                //Insert/update Transaction, else set time period id for insert (master-detail relationship)
                Transaction__c billingItem = new Transaction__c();
                //if transaction exists, set id
                if (relatedTransaction.size() == 1){
                    billingItem.id = relatedTransaction[0].Id;
                }
                else{
                    billingItem.License_Policy_Time_Period__c     = newLicPolicyTP.Id;
                }
                billingItem.Fee_Amount__c                     = eachOppty.Amount;
                billingItem.Fee_Calculation_Request_String__c = eachOppty.Fee_Calculation_Request__c;
                billingItem.Fee_Calculation_Response_String__c = eachOppty.Fee_Calculation_Response__c;
                billingItem.Transaction_Date__c               = date.Today();
                billingItem.Transaction_Type__c               = 'Initial Quote';

                upsert billingItem;
            }
        }
    }
    /**
    * @description This method accepts a list of Leads and if the isConverted flag is changed from false to true, then
    * iterate over the LeadConversionDestination metadata type, and create additional Account related records.
    * @param  leadList
    * @param  oldMap
    */
    public static void createRelatedRecordsOnConversion (list<Lead> leadList, map<id,Lead> oldMap) {
        ErrorLogger logger = new ErrorLogger();
        string classMethodString = 'LeadConversionHelper-createRelatedRecordsOnConversion';
        string message;

        //build list of leads that have been converted
        list<Lead> convertedLeadList = new list<Lead>();
        for (Lead eachLead: leadList){
            if (eachLead.isConverted == true && oldMap.get(eachLead.id).isConverted == false && eachLead.LeadSource != label.Self_Service_Lead_Source){
                convertedLeadList.add(eachLead);
            }
        }

        //if NO leads were converted, exit the method
        if (convertedLeadList.size() == 0){
            return;
        }

        //build map of Lead fields
        map<String, Schema.SObjectField> leadFieldMap = Schema.SObjectType.Lead.fields.getMap();

        //instantiate sObject to be populated (maybe multiple objects in Destination_Object__r.Object_API_Name__c e.g. Contact, Account)
        map<String, sObject> targetObjectMap = new map<string,sObject>();

        /*
        Iterated over list of records in the LeadConversionDestination metadata type and build a map of target objects,
        and source field to target field mapping.  Also, instantiate the related object so that the record can be created.
        example: {'Contact: {Secondary_Contact_First_Name: FirstName}
        */
        map<string, map<string,string>> sourceToTargetFieldMap = new map<string, map<string, string>>();
        for (Field_Mapping__mdt eachFieldMap: [select Source_Field__c, Destination_Field__c, Destination_Object__r.Object_API_Name__c, Destination_Object__r.Active_Indicator__c
        from Field_Mapping__mdt
        Where Destination_Object__r.Active_Indicator__c = true]){

            //if the sourceToTargetFieldMap is empty, or id does not contain the object name, add the object name to the map,
            if (sourceToTargetFieldMap.size() == 0 || sourceToTargetFieldMap.containsKey(eachFieldMap.Destination_Object__r.Object_API_Name__c) == false){
                sourceToTargetFieldMap.put(eachFieldMap.Destination_Object__r.Object_API_Name__c, new map<string, string>());

                //get object type
                Schema.SObjectType convertObjectType = Schema.getGlobalDescribe().get(eachFieldMap.Destination_Object__r.Object_API_Name__c);

                //add object to map by string name
                targetObjectMap.put(eachFieldMap.Destination_Object__r.Object_API_Name__c, convertObjectType.newSObject());
            }
            //for the object, add the source to target field mapping
            sourceToTargetFieldMap.get(eachFieldMap.Destination_Object__r.Object_API_Name__c).put(eachFieldMap.Source_Field__c, eachFieldMap.Destination_Field__c);

        }

        /*
        Iterated over list of records in the LeadConversionDestination metadata type and build a map of target objects,
        and source record type to target record type mapping.
        */
        map<string, map<string,string>> sourceToTargetRecordTypeMap = new map<string, map<string, string>>();
        for (Record_Type_Mapping__mdt eachRecordTypeMap: [Select Originating_Record_Type_Name__c, Destination_Record_Type_Name__c
                , Lead_Conversion_Destination__r.Object_API_Name__c, Lead_Conversion_Destination__r.Active_Indicator__c
        From Record_Type_Mapping__mdt
        Where Lead_Conversion_Destination__r.Active_Indicator__c = true]){

            //system.debug('Debug: eachRecordTypeMap>' + eachRecordTypeMap);

            Schema.SObjectType objType = Schema.getGlobalDescribe().get(eachRecordTypeMap.Lead_Conversion_Destination__r.Object_API_Name__c);
            Schema.DescribeSobjectResult objTypeDesc = objType.getDescribe();
            map<string, Schema.RecordTypeInfo> recTypeMap = objTypeDesc.getRecordTypeInfosByName();
            id sourceRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get(eachRecordTypeMap.Originating_Record_Type_Name__c).getRecordTypeId();
            id targetRecordTypeId = recTypeMap.get(eachRecordTypeMap.Destination_Record_Type_Name__c).getRecordTypeId();


            //if the sourceToTargetFieldMap is empty, or id does not contain the object name, add the object name to the map,
            if (sourceToTargetRecordTypeMap.size() == 0 || sourceToTargetRecordTypeMap.containsKey(eachRecordTypeMap.Lead_Conversion_Destination__r.Object_API_Name__c) == false){
                sourceToTargetRecordTypeMap.put(eachRecordTypeMap.Lead_Conversion_Destination__r.Object_API_Name__c, new map<string, string>());
            }

            //for the object, add the source to target field mapping
            sourceToTargetRecordTypeMap.get(eachRecordTypeMap.Lead_Conversion_Destination__r.Object_API_Name__c).put(sourceRecordTypeId, targetRecordTypeId);
        }

        /*
        Iterate over list of converted leads and for each lead field in in the sourceToTargetField map, get its datatype
        and set the contact field name to the value in the converted lead field.
        */
        List<sObject> secondaryContactList = new List<sObject>();
        for (Lead convertedLead: convertedLeadList){
            //iterate over each object in the targetObjectMap.keySet()
            for (string targetObjectName: targetObjectMap.keySet()) {
                if (targetObjectName == GlobalConstant.OBJ_ACCOUNT){
                    targetObjectMap.get(targetObjectName).put(GlobalConstant.ACCOUNT_PARENT_ID_FIELD, convertedLead.ConvertedAccountId);
                    targetObjectMap.get(targetObjectName).put(GlobalConstant.ACCOUNT_DUPLICATE_BYPASS_FIELD, True);
                    targetObjectMap.get(targetObjectName).put('RecordTypeId', sourceToTargetRecordTypeMap.get(GlobalConstant.OBJ_ACCOUNT).get(convertedLead.RecordTypeId));
                    targetObjectMap.get(targetObjectName).put(GlobalConstant.ACCOUNT_PARENT_ID_FIELD, convertedLead.ConvertedAccountId);
                }
                else {
                    targetObjectMap.get(targetObjectName).put(GlobalConstant.ACCOUNT_ID_FIELD, convertedLead.ConvertedAccountId);
                }
                //iterate over the list of lead field name keySet for the given object, and if sourceToTargetFieldMap contains the field, set the value for the given record.
                for (string fieldName: sourceToTargetFieldMap.get(targetObjectName).keySet()) {
                    SObjectField sObjField = leadFieldMap.get(fieldName);  //can these two lines be combined?  leadFieldMap.get(fieldName).getDescribe();

                    //try to find the corresponding field
                    Schema.DescribeFieldResult describeField;
                    try {
                        describeField = sObjField.getDescribe();
                    }
                    catch (Exception ex){
                        message = 'An error occurred while trying to perform a lookup against field ' + fieldName;
                        logger.log(classMethodString,message);
                        logger.log(ex);
                        continue;

                    }
                    //try to set the value of the field for the given object
                    try {
                        if (string.valueOf(describeField.getType()) == GlobalConstant.CURRENCY_DATA_TYPE || string.valueOf(describeField.getType()) == GlobalConstant.DOUBLE_DATA_TYPE || string.valueOf(describeField.getType()) == GlobalConstant.PERCENT_DATA_TYPE) {
                            targetObjectMap.get(targetObjectName).put(sourceToTargetFieldMap.get(string.valueOf(targetObjectName)).get(fieldName), (decimal) convertedLead.get(fieldName));
                        } else if (string.valueOf(describeField.getType()) == GlobalConstant.DATE_DATA_TYPE) {
                            targetObjectMap.get(targetObjectName).put(sourceToTargetFieldMap.get(string.valueOf(targetObjectName)).get(fieldName), (date) convertedLead.get(fieldName));
                        } else if (string.valueOf(describeField.getType()) == GlobalConstant.DATETIME_DATA_TYPE) {
                            targetObjectMap.get(targetObjectName).put(sourceToTargetFieldMap.get(string.valueOf(targetObjectName)).get(fieldName), (datetime) convertedLead.get(fieldName));
                        } else if (string.valueOf(describeField.getType()) == GlobalConstant.BOOLEAN_DATA_TYPE) {
                            targetObjectMap.get(targetObjectName).put(sourceToTargetFieldMap.get(string.valueOf(targetObjectName)).get(fieldName), (boolean) convertedLead.get(fieldName));
                        } else if (string.valueOf(describeField.getType()) == GlobalConstant.INTEGER_DATA_TYPE) {
                            targetObjectMap.get(targetObjectName).put(sourceToTargetFieldMap.get(string.valueOf(targetObjectName)).get(fieldName), integer.valueOf(convertedLead.get(fieldName)));
                        } else {
                            targetObjectMap.get(targetObjectName).put(sourceToTargetFieldMap.get(string.valueOf(targetObjectName)).get(fieldName), string.valueOf(convertedLead.get(fieldName)));
                        }
                    }catch (Exception ex){
                        system.debug('ERROR: An error occurred while trying to set the value of the ' + sourceToTargetFieldMap.get(string.valueOf(targetObjectName)).get(fieldName) + ' on the ' + targetObjectName + ' object.');
                    }
                }
            }
        }

        //for each object in the targetObjectMap, insert the record.
        for (string eachObjectName: targetObjectMap.keySet()) {
            Database.SaveResult saveResultRecord = Database.insert(targetObjectMap.get(eachObjectName), false);
            SaveResultsHelper saveResults = new SaveResultsHelper(saveResultRecord);
        }

        logger.save();
    }
}