/**
 * Created by johnreedy on 2019-01-31.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-01-31   Xede Consulting Group 		Initial Creation                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This test class returns a mock MelissaData Response for and address that was not able
* to be standardized.
* @author Xede Consulting Group
* @date 2019-01-31 
*/
@IsTest
public class MelissaDataNonStandardizedResponseMock implements HttpCalloutMock {
    /**
     * @description this method accepts an address standardization request and returns a
     * Non-Standardized MelissaDataResponse.
     * @param request
     * @return Http response
    */
    public static HTTPResponse respond(HTTPRequest request) {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{'
                + '"Records": ['
                + '{'
                + '"AddressExtras": " ",'
                + '"AddressKey": "48009000000",'
                + '"AddressLine1": "123 Main",'
                + '"AddressLine2": " ",'
                + '"City": "Anytown",'
                + '"CompanyName": " ",'
                + '"EmailAddress": " ",'
                + '"MelissaAddressKey": " ",'
                + '"MelissaAddressKeyBase": " ",'
                + '"NameFull": " ",'
                + '"PhoneNumber": " ",'
                + '"PostalCode": "10000",'
                + '"RecordExtras": " ",'
                + '"RecordID": "1",'
                + '"Reserved": " ",'
                + '"Results": " ",'
                + '"State": "NY"'
                + '}'
                + '],'
                + '"TotalRecords": "1",'
                + '"TransmissionReference": " ",'
                + '"TransmissionResults": " ",'
                + '"Version": "5.2.25"'
                + '}');
        response.setStatusCode(200);
        response.setStatus('OK');
        return response;
    }
}