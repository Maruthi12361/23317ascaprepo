/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer           Description                                                     *
*   ----------  -----------------   ----------------------------------------------------------------*
*   2019-06-01  Xede Consulting   	Initial Creation                                                *
*   2019-07-02  Xede Consulting     Added functionality to add venue to license policy.             *
*   2019-07-22  Xede Consulting     Updated condition under which related records are created.      *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This trigger manages the distribution of work and the order of operations for events
* against the Opportunity object.
* @author Xede Consulting Group
* @date
*/
public class OpportunityFeeCalcHelper {
    public static Boolean isFirstTime = True;

    /**
    * @description This method creates a policy, time period, and transaction for a given opportunity.
    * @param opportunityList
    */
    public static void createLicensePolicyTransaction(List<Opportunity> opportunityList, Map<Id,Opportunity> oldMap){
        string runningUserName = UserInfo.getName();
        if (runningUserName == label.SelfService_Site_User_Name || UserInfo.getUserType() == GlobalConstant.GUEST_USER ){
            return;
        }
        /*
        Because this process needs to create a child, grandchild and great grandchild, this process cannot be bulkified.
        As a result, if the opportunityList size is greater than 1, exist the process.
        */
        if (opportunityList.size() > 1){
            return;
        }
        Id accountId = opportunityList[0].AccountId;

        List<Lead> leads = [select Id from Lead where IsConverted=true AND (ConvertedOpportunityId=:opportunityList[0].Id OR ConvertedAccountId=:accountId) limit 1];
        Lead lead = leads != null && !leads.isEmpty() ? leads[0] : null;
        Id leadId=lead != null ? lead.Id : null;

        //get Accounts(Venues) related to the Legal Business Entity on the Opportunity that were created today.
        List<Account> venueList = [SELECT Id, ParentId
                                     FROM Account
                                    WHERE ParentId =: opportunityList[0].AccountId
                                      AND CreatedDate = TODAY
                                    ORDER BY CreatedDate DESC LIMIT 1];



        List<License_Policy__c> relatedPolicy = [SELECT Id,License_Class_Type__c,License_Class_Type__r.Coverage_Type__c   FROM License_Policy__c WHERE Opportunity__c IN: opportunityList OR (Licensee__c != null AND Licensee__c=:accountId) or (SelfServiceLead__c != null AND SelfServiceLead__c	=:leadId	)];
        List<License_Policy_Time_Period__c> relatedTimePeriod = [SELECT Id, License_Policy__c,License_Policy__r.License_Class_Type__c,License_Policy__r.License_Class_Type__r.Coverage_Type__c  FROM License_Policy_Time_Period__c WHERE License_Policy__c IN: relatedPolicy];
        List<Transaction__c> relatedTransaction = [SELECT Id FROM Transaction__c WHERE License_Policy_Time_Period__c IN: relatedTimePeriod];

        Set<Id> opportuntyIds = new Set<Id>();
        /*
        Iterate over opportunity list of opportunities and create or update license policy, time period, and transaction.
        */
        Set<Id> licenseClassIds = new Set<Id>();
        Map<Id,License_Class__c> licenseClassMap = new Map<Id,License_Class__c>();
        for (Opportunity eachOppty: opportunityList){
            licenseClassIds.add(eachOppty.License_Class_Relationship__c);
        }
        if(!licenseClassIds.isEmpty()){
            licenseClassMap = new Map<Id,License_Class__c>([select Id,Coverage_Type__c,Name, License_Policy_Record_Type__c from License_Class__c where Id=:licenseClassIds]);
        }
        License_Class__c associatedLicenseClass;
        for (Opportunity eachOppty: opportunityList){
            if(eachOppty.Id != null && eachOppty.Encrypted_Record_Id__c == null){
                opportuntyIds.add(eachOppty.Id);
            }
            //if the opportunity is new, or the Fee_Calculation_Request__c field has changed, then create/update related records.
            if (oldMap == null || eachOppty.Amount != oldMap.get(eachOppty.Id).Amount
                               || eachOppty.Fee_Calculation_Request__c != oldMap.get(eachOppty.Id).Fee_Calculation_Request__c){
                associatedLicenseClass = licenseClassMap.get(eachOppty.License_Class_Relationship__c);

                License_Policy__c newLicPolicy = new License_Policy__c();
                //Insert/update License Policy
                if (relatedPolicy.size() == 1){
                    newLicPolicy.Id = relatedPolicy[0].Id;
                }
                else if (string.isBlank(eachOppty.License_Class_Relationship__c)){
                    continue;
                }
                else {
                    newLicPolicy.RecordTypeId = Schema.SObjectType.License_Policy__c.getRecordTypeInfosByDeveloperName().get(associatedLicenseClass.License_Policy_Record_Type__c).getRecordTypeId();
                }
                newLicPolicy.Licensee__c            = eachOppty.AccountId;
                newLicPolicy.License_Class_Type__c  = eachOppty.License_Class_Relationship__c;
                newLicPolicy.Opportunity__c         = eachOppty.Id;
                newLicPolicy.Status__c              = 'Draft';
                newLicPolicy.Start_Date__c          = eachOppty.License_Coverage_Start_Date__c;
                newLicPolicy.End_Date__c            = eachOppty.License_Coverage_End_Date__c;

                //if Licensee record is found, then set the venue
                if (venueList.size() >0){
                    newLicPolicy.Venue__c = venueList[0].Id;
                }

                try {
                    upsert newLicPolicy;

                }catch(Exception ex){
                    system.debug('ERROR: An error occurred while trying to UPSERT the License Policy>' + ex.getMessage());
                }
                String licenseClassCoverage = associatedLicenseClass != null ? associatedLicenseClass.Coverage_Type__c : null;

                //Insert/update Time Period
                Integer YearNumber = Date.today().year();
                License_Policy_Time_Period__c newTimePeriod = new License_Policy_Time_Period__c();

                //if Time Period exists, set id, else set license policy id for insert (master-detail relationship)
                if (relatedTimePeriod.size() == 1){
                    newTimePeriod.Id = relatedTimePeriod[0].Id;
                }
                else if (string.isBlank(newLicPolicy.Id)){
                    continue;
                }
                else{
                    newTimePeriod.License_Policy__c   = newLicPolicy.Id;
                }
                newTimePeriod.Start_Date__c       = Date.newInstance(YearNumber, 1,1);
                newTimePeriod.End_Date__c         = Date.newInstance(YearNumber, 12,31);
                newTimePeriod.Billing_Entity__c  = eachOppty.AccountId;
                newTimePeriod.Transaction_Date__c = Date.today();
                newTimePeriod.Fee_Calculation_Request_String__c = eachOppty.Fee_Calculation_Request__c;
                newTimePeriod.Fee_Calculation_Response_String__c = eachOppty.Fee_Calculation_Response__c;
                //if Coverage Type is Contract Year, override default with today's date and a year from now
                if (licenseClassCoverage != null && licenseClassCoverage == 'Contract Year') {
                    newTimePeriod.Start_Date__c = eachOppty.License_Coverage_Start_Date__c;
                    newTimePeriod.End_Date__c   = eachOppty.License_Coverage_End_Date__c;
                }
                try{
                    upsert newTimePeriod;
                }catch(Exception ex){
                    system.debug('ERROR: An error occurred while trying to UPSERT the License Policy Time Period>' + ex.getMessage());
                }

                //Insert/update Transaction, else set time period id for insert (master-detail relationship)
                Transaction__c billingItem = new Transaction__c();
                //if transaction exists, set id

                if (relatedTransaction.size() == 1){
                    billingItem.Id = relatedTransaction[0].Id;
                }
                else if (string.isBlank(newTimePeriod.Id)){
                    continue;
                }
                else{
                    billingItem.License_Policy_Time_Period__c     = newTimePeriod.Id;
                }
                billingItem.Fee_Amount__c                     = eachOppty.Amount;
                billingItem.Fee_Calculation_Request_String__c = eachOppty.Fee_Calculation_Request__c;
                billingItem.Fee_Calculation_Response_String__c = eachOppty.Fee_Calculation_Response__c;
                billingItem.Transaction_Date__c               = Date.today();
                billingItem.Transaction_Type__c               = 'Initial Quote';
                try {
                    upsert billingItem;

                }catch(Exception ex){
                    system.debug('ERROR: An error occurred while trying to UPSERT the Transaction>' + ex.getMessage());
                }
            }
        }
        if(!opportuntyIds.isEmpty() && !System.isBatch() && !System.isFuture()){
            assignEncryptedRecordId(opportuntyIds);
        }
    }
    /**
    * @description This method populates an encrypted record id
    * to the address standardization service.
    * @param  opportunityIds
    */
    @future
    public static void assignEncryptedRecordId(Set<Id> opportunityIds){
        List<Opportunity> updateOpptyList = new List<Opportunity>();
        if(!opportunityIds.isEmpty()){
            for(Id recordId: opportunityIds){
                Opportunity eachOpty = new Opportunity();
                eachOpty.Id = recordId;
                eachOpty.Encrypted_Record_Id__c=ASCAPUtils.getCleanedEncryptedValue(recordId);
                updateOpptyList.add(eachOpty);
            }

            //update opportunity
            Database.SaveResult[] saveResultList = Database.update(updateOpptyList, false);
            SaveResultsHelper saveResults = new SaveResultsHelper(saveResultList);
        }

    }
    /**
    * @description This method removes the value in the Fee_Calculation_Request__c if the License Class Relationship changes.
    * @param opportunityList
    */
    public static void deleteRequestOnLicenseChange(List<Opportunity> opptyList, Map<Id,Opportunity> oldMap){
        List<Opportunity> updateOpptyList = new List<Opportunity>();

        //iterate over opportunity list and if the license class relationship changes, update the Fee_Calculation_Request__c to an empty string.
        for (Opportunity each: opptyList){
            if (each.License_Class_Relationship__c != oldMap.get(each.Id).License_Class_Relationship__c){
                Opportunity currRecord = new Opportunity();
                currRecord.Id = each.Id;
                currRecord.Fee_Calculation_Request__c = '';
                currRecord.Fee_Calculation_Response__c = '';
                currRecord.Amount = Null;
                updateOpptyList.add(currRecord);
            }
        }

        //update opportunity
        Database.SaveResult[] saveResultList = Database.update(updateOpptyList, false);
        SaveResultsHelper saveResults = new SaveResultsHelper(saveResultList);
    }
    /**
    * @description This method populates creates an encrypted record id
    * to the address standardization service.
    * @param  leadList
    * @param  oldMap
    */
    public static void assignEncryptedRecordId(list<Opportunity> opportunityList, map<id, Opportunity> oldMap) {

        for(Opportunity eachOpty: opportunityList){
            if(eachOpty.Id != null && eachOpty.Encrypted_Record_Id__c == null){
                eachOpty.Encrypted_Record_Id__c=ASCAPUtils.getCleanedEncryptedValue(eachOpty.Id);
            }
        }

    }

}