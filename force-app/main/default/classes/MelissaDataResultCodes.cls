/**
 * Created by johnreedy on 2019-01-24.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-01-24   Xede Consulting Group 		Initial Creation                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description <description>
* @author Xede Consulting Group
* @see MelissaDataResultCodesTest
* @date 2019-01-24 
*/
public class MelissaDataResultCodes {

    public Map<String, String> description { get; set; }

    public MelissaDataResultCodes(){
        description = new Map<string, string>{
                //AS - Address Status
                'AS01' => 'The address is valid and deliverable according to official postal agencies.',
                'AS02' => 'The street address was verified but the suite/apartment number is missing or invalid.',
                'AS03' => 'This US address is not serviced by the USPS but does exist and may receive mail through third party carriers like UPS',
                'AS09' => 'The address is in a non-supported country.',
                'AS10' => 'The address is a Commercial Mail Receiving Agency (CMRA) like a Mailboxes Etc. These addresses include a Private Mail Box (PMB or #) number',
                'AS11'	=>	'A Post Office Box address with a street style address instead of the normal PO Box # address format.',
                'AS12'	=>	'The record moved to a new address.',
                'AS13'	=> 'The address has been converted by LACSLink® from a rural-style address to a city-style address.',
                'AS14' => 'A suite was appended by SuiteLink™ using the address and company name',
                'AS15'	=> 'An apartment number was appended by AddressPlus using the address and last name.',
                'AS16'	=> 'The address has been unoccupied for more than 90 days.',
                'AS17'	=> 'The address does not currently receive mail but will likely in the near future.',
                'AS20'	=> 'This address can only receive mail delivered through the USPS (i.e. PO Box or a military address).',
                'AS23'	=> 'Extraneous information not used in verifying the address was found. This includes unnecessary sub premises and other unrecognized data. Additional information will either be parsed into the Suite or Parsed Garbage/AddressExtras output.',
                'AS24'	=> 'Address identified by the USPS where carriers cannot physically access a building or door for mail delivery.',

                //AC - Address Change
                'AC01' => 'The postal code was changed or added.',
                'AC02' => 'The administrative area (state, province) was added or changed.',
                'AC03' => 'The locality (city, municipality) name was added or changed.',
                'AC04' => 'The address was found to be an alternate record and changed to the base (preferred) version.',
                'AC05'	=> 'An alias is a common abbreviation for a long street name, such as “MLK Blvd” for “Martin Luther King Blvd.” This change code indicates that the full street name (preferred) has been substituted for the alias.',
                'AC06'	=> 'Address1 was swapped with Address2 because Address1 could not be verified and Address2 could be verified.',
                'AC07'	=> 'Address1 was swapped with Company because only Company had a valid address.',
                'AC08'	=> 'A non-empty plus4 was changed.',
                'AC09'	=> 'The dependent locality (urbanization) was changed.',
                'AC10' => 'The thoroughfare (street) name was added or changed due to a spelling correction.',
                'AC11' => 'The thoroughfare (street) leading or trailing type was added or changed, such as from "St" to "Rd."',
                'AC12'	=> 'The thoroughfare (street) pre-directional or post-directional was added or changed, such as from "N" to "NW."',
                'AC13'	=> 'The sub premise (suite) type was added or changed, such as from “STE” to “APT.”',
                'AC14'	=> 'The sub premise (suite) unit number was added or changed.',
                'AC20'	=> 'The house number was changed.',

                //AE - Address Error
                'AE01' => 'The address could not be verified at least up to the postal code level.r',
                'AE02' => 'Could not match the input street to a unique street name. Either no matches or too many matches found.',
                'AE03' => 'The combination of directionals (N, E, SW, etc) and the suffix (AVE, ST, BLVD) is not correct and produced multiple possible matches',
                'AE04' => 'A physical plot exists but is not a deliverable addresses. One example might be a railroad track or river running alongside this street, as they would prevent construction of homes in that location.',
                'AE05' => 'The address was matched to multiple records.',
                'AE06' => 'This address currently cannot be verified but was identified by the Early Warning System (EWS) as belonging to a upcoming area and will likely be included in a future update.',
                'AE07' => 'Minimum requirements for the address to be verified is not met. Address must have at least one address line and also the postal code or the locality/administrative area.',
                'AE08'	=> 'The thoroughfare (street address) was found but the sub premise (suite) was not valid.',
                'AE09'	=> 'The thoroughfare (street address) was found but the sub premise (suite) was missing.',
                'AE10' => 'The premise (house or building) number for the address is not valid.',
                'AE11'	=> 'The premise (house or building) number for the address is missing.',
                'AE12'	=> 'The PO (Post Office Box), RR (Rural Route), or HC (Highway Contract) Box number is invalid.',
                'AE13'	=> 'The PO (Post Office Box), RR (Rural Route), or HC (Highway Contract) Box number is missing.',
                'AE14'	=> 'The address is a Commercial Mail Receiving Agency (CMRA) and the Private Mail Box (PMB or #) number is missing.',
                'AE17'	=> 'A sub premise (suite) number was entered but the address does not have secondaries. (Deprecated - See AS23)',
                'AE21'	=> 'The input MAK was not found. This can be caused by an improperly formatted MAK (a proper MAK is 10 numerical digits long) or by requesting a MAK number that has not yet been assigned to a location.'
        };
    }
}