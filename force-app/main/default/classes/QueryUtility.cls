/**
 * Created by Xede
 */

/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer   Description                                                             *
*   ----------  ----------- ------------------------------------------------------------------------*
*   2018-10-06  Xede  		Initial Creation                                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description Stores the common utility methods related to SOQL
* @author Xede Consulting Group
* @see QueryUtililtyTest for test class
* @date 2018-10-06
*/
public class QueryUtility {
    /**
* This method generates the SOQL with all the fields on the object.
For lookup and master-details fields you need to include them specifically
*
* @param  objectName  The name of the object for which we need to generate SOQL with all the fields
* @return  strQuery    SOQL with Select statement with all the fields
**/
    public static String getObjectFieldsQuery(String objectName){

        String strQuery = ' Select ';
        Map<String, Schema.SObjectType> gdMap = Schema.getGlobalDescribe();
        Schema.Describesobjectresult dsr = gdMap.get(objectName).getDescribe();
        Map<String, Schema.SObjectField> fieldMap = (Map<String, Schema.SObjectField>) dsr.fields.getMap();

        for(String strField : fieldMap.keySet()){
            strQuery += strField +',';
        }

        strQuery = strQuery.substring(0, strQuery.length() - 1);

        if(objectName == GlobalConstant.OBJ_LICENSE_POLICY){
            strQuery +=',License_Class_Type__r.Name';
            strQuery +=',License_Class_Type__r.DSLLicenseTypeName__c';
            strQuery +=',License_Class_Type__r.Premise_Required__c';
            strQuery +=',License_Class_Type__r.Self_Service_Enabled__c';
            strQuery +=',License_Class_Type__r.Fee_Calculation_Help_Text__c';
            strQuery +=',License_Class_Type__r.Coverage_Type__c';
            strQuery +=',Licensee__r.Name';
            strQuery +=',Opportunity__r.Name';
        }

        if(objectName == GlobalConstant.OBJ_LICENSE_POLICY_TIME_PERIOD){
            strQuery +=',License_Policy__r.Name';
            strQuery +=',License_Policy__r.License_Class_Type__c';
            strQuery +=',License_Policy__r.License_Class_Type__r.Name';
            strQuery +=',Financially_Responsible__r.Name';

        }
        if(objectName == GlobalConstant.OBJ_LICENSE_POLICY_TRANSACTION){
            strQuery +=',License_Policy_Time_Period__r.Name';
            strQuery +=',License_Policy_Time_Period__r.License_Policy__r.License_Class_Type__c';
            strQuery +=',License_Policy_Time_Period__r.License_Policy__r.License_Class_Type__r.Name';
            strQuery +=',License_Policy_Time_Period__r.License_Policy__r.License_Class_Type__r.Fee_Calculation_Help_Text__c';

        }
        return (objectName == null || objectName =='') ? null : strQuery;
    }

}