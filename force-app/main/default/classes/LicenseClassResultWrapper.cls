public class LicenseClassResultWrapper {


/*******************************************/
/* Wrapper class to display license class      */
/*****************************************/

    public class LicenseClassResultRecordWrapper {

        @AuraEnabled public String id{get;set;}
        @AuraEnabled public boolean isSelected{get;set;}
        @AuraEnabled public String isPremiseRequired{get;set;}
        @AuraEnabled public String licenseClassId{get;set;}
        @AuraEnabled public String licenseClassName{get;set;}
        @AuraEnabled public String dslLicenseClassName{get;set;}
        @AuraEnabled public String displayName{get;set;}
        @AuraEnabled public String description{get;set;}

        public LicenseClassResultRecordWrapper() {
            isSelected = false;
            isPremiseRequired = 'N';
            licenseClassId = '';
            licenseClassName = '';
            description = '';
            displayName='';
            id = '';
            dslLicenseClassName='';

        }
    }
}