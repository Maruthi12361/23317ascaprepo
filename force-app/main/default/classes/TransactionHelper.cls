/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer         Description                                                       *
*   ----------  ----------------  ------------------------------------------------------------------*
*   2019-04-30  Xede Consulting   Initial Creation                                                  *
*   2019-07-09  Xede Consulting   Added condition to check for empty fee_detail_dict returned from  *
*                                 DSL.                                                              *
*   2019-10-03  Xede Consulting   Added section header to Input Parameter field output.             *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This helper class supports operations against the Transaction object.
* @author Xede Consulting Group
* @see TransactionHelperTest
* @date 2019-04-30 
*/
public with sharing class TransactionHelper {
    //Currently, DrawLoop cannot handle html tables, but if it supports them in the future, the useTableFormat can be set to true in the Trigger
    public static boolean useTableFormat = False;
    private static map<string,map<string,string>> responsePrintableLabelMap = new map<string,map<string,string>>();
    private static map<string, map<string, map<string, string>>> licenseParameterPrintableLabelMap = new map<string, map<string, map<string, string>>>();

    //private static map<string,map<string,string>> parameterPrintableLabelMap = new map<string,map<string,string>>();

    /**
    * @description This method populates two rich text field Rate_Schedule_Fee_Amounts__c used/merged by DrawLoop to populate the
    * License Agreement document.
    * @param  transactionList - list of transactions created
    * @param  oldMap - oldMap
    */
    public static void createRateScheduleFeeDetail(list<Transaction__c> transactionList, map<id,Transaction__c> oldMap){
        //verify that there are records to process (Fee_Calculation_Response_String__c not null)
        list<Transaction__c> processList = new list<Transaction__c>();
        for (Transaction__c each: transactionList){
            if (string.isNotBlank(each.Fee_Calculation_Response_String__c)){
                processList.add(each);
            }
        }

        if (processList.size() == 0){
            return;
        }

        //get list of printable names from License_Class_Response_Info__c by License Class and Response field
        list<License_Class_Response_Info__c> licenseClassResponseList = [
               SELECT Id
                    , License_Class__r.DSLLicenseTypeName__c
                    , Response_Field_Name__c
                    , Printable_Name__c
                 FROM License_Class_Response_Info__c];

        //get list of printable names from License_Class_Response_Info__c by License Class and Response field
        list<License_Class_Field_Info__c> licenseClassParameterList = [
               SELECT Id
                    , License_Class__r.DSLLicenseTypeName__c
                    , DSL_Parameter_Name__c
                    , Printable_Label__c
                 FROM License_Class_Field_Info__c];


        //build map of the license class to Rate Schedule printable name map<licenseClassName,map<rateSchedule,printableName>>
        for (License_Class_Response_Info__c eachResponseInfo: licenseClassResponseList){
            if (!responsePrintableLabelMap.containsKey(eachResponseInfo.License_Class__r.DSLLicenseTypeName__c)){
                responsePrintableLabelMap.put(eachResponseInfo.License_Class__r.DSLLicenseTypeName__c, new map<string,string>());
            }
            responsePrintableLabelMap.get(eachResponseInfo.License_Class__r.DSLLicenseTypeName__c).put(eachResponseInfo.Response_Field_Name__c, eachResponseInfo.Printable_Name__c);
        }

        /*
        Iterate over list of transactions passed in to trigger, and if the request has changed, recreate the printable parameter field and printable rate shedule fee field.
        */
        for (Transaction__c eachTransaction: processList){
            if (oldMap == null || eachTransaction.Fee_Calculation_Request_String__c != null && eachTransaction.Fee_Calculation_Request_String__c != oldMap.get(eachTransaction.id).Fee_Calculation_Request_String__c){

                //get the license class name from the json request
                string licenseClassDSLName = getLicenseClassName(eachTransaction.Fee_Calculation_Request_String__c);

                //build map of Rate Schedule key value pairs (labels and values)
                string feeDetailLabel = label.Fee_Detail_Dictionary_String;

                map<string,string>  rateScheduleMap = new map<string,string>(parseJSONField(eachTransaction.Fee_Calculation_Response_String__c, feeDetailLabel));
                //eachTransaction.Rate_Schedule_Fee_Amounts__c = '';
                eachTransaction.Rate_Schedule_Fee_Amounts__c = convertResponseToHTML(rateScheduleMap, licenseClassDSLName);
            }
        }
    }
    /**
    * @description This method populates two rich text Input_Parameter_Values__c field used/merged by DrawLoop to populate the
    * License Agreement document.
    * @param  transactionList - list of transactions created
    * @param  oldMap - oldMap
    */
    public static void createMusicUseFeeDetail(list<Transaction__c> transactionList, map<id,Transaction__c> oldMap) {
        //verify that there are records to process (Fee_Calculation_Request_String__c not null)
        list<Transaction__c> processList = new list<Transaction__c>();
        for (Transaction__c each: transactionList){
            if (string.isNotBlank(each.Fee_Calculation_Request_String__c)){
                processList.add(each);
            }
        }

        if (processList.size() == 0){
            return;
        }

        set<string> licenseClassNameSet = new set<string>();

        //build set of those license classes passed to method as part of list of Transactions
        for (Transaction__c each: processList){
            licenseClassNameSet.add(getLicenseClassName(each.Fee_Calculation_Request_String__c));
        }
        licenseParameterPrintableLabelMap = new map<string, map<string, map<string, string>>>();
        //get list of printable names from License_Class_Response_Info__c by License Class and Response field
        list<License_Class_Field_Info__c> licenseClassParameterList = [
                SELECT Id
                    , License_Class__r.DSLLicenseTypeName__c
                    , DSL_Parameter_Name__c
                    , Printable_Label__c
                    , Section_Header__c
                    , Order__c
                 FROM License_Class_Field_Info__c
                WHERE License_Class__r.DSLLicenseTypeName__c in: licenseClassNameSet
                ORDER BY Section_Header__c, Order__c ASC];

        /*
        Iterate over the license class info object and build a map of field api names and printable values for each
        section header and license class map<licenseClass,map<sectionHeader,map<fieldAPIName,printableName>>>
         */
        for (License_Class_Field_Info__c eachFieldInfo : licenseClassParameterList) {
            string sectionHeader = eachFieldInfo.Section_Header__c != null ? eachFieldInfo.Section_Header__c : label.Section_Header_Default_Value;

            //if map does not contain license class, add license class, section header, and field api name and printable label (all levels of the map)
            if (!licenseParameterPrintableLabelMap.containsKey(eachFieldInfo.License_Class__r.DSLLicenseTypeName__c)) {
                licenseParameterPrintableLabelMap.put(eachFieldInfo.License_Class__r.DSLLicenseTypeName__c, new map<string, map<string, string>>());
                licenseParameterPrintableLabelMap.get(eachFieldInfo.License_Class__r.DSLLicenseTypeName__c).put(sectionHeader, new map<string, string>());
                licenseParameterPrintableLabelMap.get(eachFieldInfo.License_Class__r.DSLLicenseTypeName__c).get(sectionHeader).put(eachFieldInfo.DSL_Parameter_Name__c, eachFieldInfo.Printable_Label__c);
            } else {
                //if license class name in map, check for section header and add section header and field api name and printable label
                if (!licenseParameterPrintableLabelMap.get(eachFieldInfo.License_Class__r.DSLLicenseTypeName__c).containsKey(sectionHeader)) {
                    licenseParameterPrintableLabelMap.get(eachFieldInfo.License_Class__r.DSLLicenseTypeName__c).put(sectionHeader, new map<string, string>());
                    licenseParameterPrintableLabelMap.get(eachFieldInfo.License_Class__r.DSLLicenseTypeName__c).get(sectionHeader).put(eachFieldInfo.DSL_Parameter_Name__c, eachFieldInfo.Printable_Label__c);
                }
                // if license class name and section header are in the map, check for the field api name, and if missing add it and printable label
                else {
                    if (!licenseParameterPrintableLabelMap.get(eachFieldInfo.License_Class__r.DSLLicenseTypeName__c).get(sectionHeader).containsKey(eachFieldInfo.DSL_Parameter_Name__c)) {
                        licenseParameterPrintableLabelMap.get(eachFieldInfo.License_Class__r.DSLLicenseTypeName__c).get(sectionHeader).put(eachFieldInfo.DSL_Parameter_Name__c, eachFieldInfo.Printable_Label__c);
                    }
                }
            }
        }

        /*
        Iterate over list of transactions passed in to method, and if the request has changed, recreate the printable
        parameter field.
        */
        for (Transaction__c eachTransaction: processList){
            if (oldMap == null || eachTransaction.Fee_Calculation_Request_String__c != null && eachTransaction.Fee_Calculation_Request_String__c != oldMap.get(eachTransaction.id).Fee_Calculation_Request_String__c){

                //get the license class name from the json request
                string licenseClassDSLName = getLicenseClassName(eachTransaction.Fee_Calculation_Request_String__c);

                //build map of Input Parameter key value pairs (labels and values)
                string licensePolicyLabel = label.License_Policy_Dictionary_String;
                string inputField = '';

                map<string, string> inputParameterMap = new map<string, string>(parseJSONField(eachTransaction.Fee_Calculation_Request_String__c, licensePolicyLabel));

                //iterate over each section for the  license
                for (string eachSection: licenseParameterPrintableLabelMap.get(licenseClassDSLName).keySet()) {
                    string sectionHeaderString = eachSection.contains('-')?eachSection.substringAfterLast('-'):eachSection;
                    inputField += '<b><br>' + sectionHeaderString + '</b><br>';

                    map<string,string> sectionParametersMap = new map<string,string>();

                    for (string eachInputParam: inputParameterMap.keySet()){
                        if (licenseParameterPrintableLabelMap.get(licenseClassDSLName).get(eachSection).containsKey(eachInputParam.replaceAll('"',''))){
                            sectionParametersMap.put(eachInputParam.replaceAll('"',''),inputParameterMap.get(eachInputParam));
                        }
                    }
                    inputField += convertParametersToHTML(sectionParametersMap, licenseClassDSLName, eachSection);
                    eachTransaction.Input_Parameter_Values__c = inputField;
                }
            }
        }
    }

    /**
    * @description This method parses a json string from the end of the startingLabel to the last curly brace '}" and
    * adds the values to the a map which is returned to the calling method.
    * @param  jsonString - json string of key value pairs
    * @param  startingLabel - the starting label after which the text should be returned
    * @return keyValuePairMap - map of key value pairs
    */
    private static map<string, string> parseJSONField(string jsonString, string startingLabel){
        //get all content between the startingLabel and the following curly brace '}'
        string[] keyValuePairString = string.valueOf(jsonString).substringBetween(startingLabel,'}').split(',');

        //build map of DSL field api names and values
        map<string,string> keyValuePairMap = new map<string,string>();

        //for each key value pair in the keyValuePairString string, add them to the rateScheduleMap
        for (String eachString: keyValuePairString){
            string[] keyValuePairs = eachString.split(':');
            if (keyValuePairs.size() > 1) {
                keyValuePairMap.put(keyValuePairs[0].replaceAll('\\s+', ''), keyValuePairs[1].replaceAll('"', ''));
            }
        }
        return keyValuePairMap;
    }

    /**
    * @description This method converts the map of key value pairs into the html/rich text format and returns it to the
    * calling method.
    * @param  mapToConvert - map of key value pairs to format
    * @param  conversionType - the conversionType (Parameter or Rate Schedule)
    * @return htmlBody
    */
    private static string convertResponseToHTML(map<string,string> mapToConvert, string licenseClassType){

        string htmlBody ='';
        /*
        Iterate over response fields and remove fields to exclude from output, format the values, and build the html string
        to be displayed in the rich text field.
        */
        for (string eachField: mapToConvert.keySet()) {
            string[] excludedStringArray = label.License_Policy_Excluded_Field_Strings.split(';');

            //if a field in the request is one of the fields to exclude ("Id", "Name", "License_Policy", then skip it, only output parameters.
            if (excludedStringArray.contains(eachField.replaceAll('"', ''))) {
                continue;
            }

            string valueField = '';
            //if the value is true or false, convert to Yes and No
            if (mapToConvert.get(eachField).toLowerCase() == GlobalConstant.LOWERCASE_TRUE_STRING || mapToConvert.get(eachField).toLowerCase() == GlobalConstant.LOWERCASE_FALSE_STRING) {
                valueField = convertBoolToPrint(mapToConvert.get(eachField));
            }
            else {
                //if the field is a rate schedule field, and DOES NOT contain  '_count' in the field api name, then format it as a currency
                if (!eachField.toLowerCase().contains(GlobalConstant.UNDERSCORE_COUNT_LITERAL)) {


                    string fee_amount = mapToConvert.get(eachField).rightPad(2, '0');
                    string[] feeParts = fee_amount.split('\\.');
                    if (mapToConvert.get(eachField).contains('.')) {
                        feeParts[1] = feeParts[1].rightPad(2, '0');
                    }
                    else {
                        feeParts.add('00');
                    }

                    String currencyTemplate = '${0}.{1}';
                    String formattedCurrency = String.format(currencyTemplate, feeParts);
                    valueField = formattedCurrency.replaceAll(('(?<!,\\d)(?<=\\d)(?=(?:\\d\\d\\d)+\\b)'),',');
                }
                else {
                    valueField = mapToConvert.get(eachField).replaceAll(('(?<!,\\d)(?<=\\d)(?=(?:\\d\\d\\d)+\\b)'),',');
                }
            }
            string parameterAPIName = eachField.replace('"', '');

            /*
            If useTableFormat is True, then you table markup, otherwise use simple html tags.  Rich text field accepts
            both, however, DrawLoop can't and trips table formats from field creating one long string.
            */
            if (useTableFormat) {
                //If printable label exists in LicenseClassResponseInfo object, use its label, otherwise remove underscores from DSL API Name
                if (responsePrintableLabelMap.get(licenseClassType).containsKey(parameterAPIName)) {
                    htmlBody += '<td style="vertical-align: top; width: 300px;">' + responsePrintableLabelMap.get(licenseClassType).get(parameterAPIName) + '</td> ';
                }
                else {
                    htmlBody += '<td style="vertical-align: top; width: 300px;">' + parameterAPIName.replace('_', ' ') + '</td>';
                }
                htmlBody += '<td style="vertical-align: top; width: 100px; text-align: right">' + valueField + '</td></tr>';
            }
            else {
                //If printable label exists in LicenseClassResponseInfo object, use its label, otherwise remove underscores from DSL API Name
                if (responsePrintableLabelMap.get(licenseClassType).containsKey(parameterAPIName)) {
                    htmlBody += responsePrintableLabelMap.get(licenseClassType).get(parameterAPIName) + ': ';
                } else {
                    htmlBody += parameterAPIName.replace('_', ' ') + ': ';
                }
                htmlBody += valueField + '<br>';
            }
        }

        return htmlBody;
    }
    /**
    * @description This method converts the map of key value pairs into the html/rich text format and returns it to the
    * calling method.
    * @param  mapToConvert - map of key value pairs to format
    * @param  conversionType - the conversionType (Parameter or Rate Schedule)
    * @return htmlBody
    */
    private static string convertParametersToHTML(map<string,string> mapToConvert, string licenseClassType, string sectionHeader){

        string htmlBody ='';

        for (string eachField: mapToConvert.keySet()) {
            string[] excludedStringArray = label.License_Policy_Excluded_Field_Strings.split(';');

            //if a field in the request is one of the fields to exclude ("Id", "Name", "License_Policy", then skip it, only output parameters.
            if (excludedStringArray.contains(eachField.replaceAll('"', ''))) {
                continue;
            }

            string valueField = '';
            //if the value is true or false, convert to Yes and No
            if (mapToConvert.get(eachField).toLowerCase() == GlobalConstant.LOWERCASE_TRUE_STRING || mapToConvert.get(eachField).toLowerCase() == GlobalConstant.LOWERCASE_FALSE_STRING) {
                valueField = convertBoolToPrint(mapToConvert.get(eachField));
            }
            else {
                valueField = mapToConvert.get(eachField).replaceAll(('(?<!,\\d)(?<=\\d)(?=(?:\\d\\d\\d)+\\b)'),',');
            }
            string parameterAPIName = eachField.replace('"', '');

            /*
            if useTableFormat is True, then you table markup, otherwise use simple html tags.  Rich text field accepts
            both, however, DrawLoop can't and trips table formats from field creating one long string.
            */
            if (useTableFormat) {
                //If printable label exists in LicenseClassFieldInfo object, use its label, otherwise remove underscores from DSL API Name
                if (licenseParameterPrintableLabelMap.get(licenseClassType).get(sectionHeader).containsKey(parameterAPIName)) {
                    htmlBody += '<td style="vertical-align: top; width: 300px;">' + licenseParameterPrintableLabelMap.get(licenseClassType).get(sectionHeader).get(parameterAPIName) + '</td>';
                }
                else {
                    htmlBody += '<td style="vertical-align: top; width: 300px;">' + parameterAPIName.replace('_', ' ') + '</td>';
                }
                htmlBody += '<td style="vertical-align: top; width: 100px; text-align: center">' + valueField + '</td></tr>';

            }
            else {
                //If printable label exists in LicenseClassFieldInfo object, use its label, otherwise remove underscores from DSL API Name
                if (licenseParameterPrintableLabelMap.size() > 0  && licenseParameterPrintableLabelMap.get(licenseClassType).get(sectionHeader).containsKey(parameterAPIName)) {
                    htmlBody += licenseParameterPrintableLabelMap.get(licenseClassType).get(sectionHeader).get(parameterAPIName) + ': ';
                }
                else {
                    htmlBody += parameterAPIName.replace('_', ' ') + ': ';
                }
                htmlBody += valueField + '<br>';
            }
        }
        return htmlBody;
    }

    /**
    * @description This method converts boolean values to another value for printing ('Yes', 'No')
    * @param  mapToConvert - map of key value pairs to format
    * @param  conversionType - the conversionType (Parameter or Rate Schedule)
    * @return printableValue
    */
    private static string convertBoolToPrint(string booleanValue){
        string printableValue = '';
        if (booleanValue.toLowerCase() == GlobalConstant.LOWERCASE_TRUE_STRING){
            printableValue = GlobalConstant.YES_STRING_LITERAL;
        }
        else{
            printableValue = GlobalConstant.NO_STRING_LITERAL;
        }
        return printableValue;
    }

    /**
    * @description This method gets the license class name from the request string.
    * @param  jsonString - json string (DSL Request)
    * @return LicenseClassName
    */
    private static string getLicenseClassName(string jsonString){
        string LicenseClassName = '';
        // Parse JSON response to get all the totalPrice field values.
        JSONParser parser = JSON.createParser(jsonString);
        while (parser.nextToken() != null) {

            //if the field name is 'License_Type' then get the value
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == GlobalConstant.LICENSE_TYPE_LITERAL)) {
                parser.nextToken();
                LicenseClassName = parser.getText();
            }
        }
        return LicenseClassName;
    }
}