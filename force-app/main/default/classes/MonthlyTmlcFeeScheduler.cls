/**
 * Created by Xede Consulting Group (Jason Burke) on 11/19/2019.
 */

/**
 * @description Schedules the MonthlyTmlcFeeBatch job
 */
public with sharing class MonthlyTmlcFeeScheduler implements Schedulable {

    /**
     * @description Starts the MonthlyTmlcFeeBatch job for the next month
     *
     * @param sc
     */
    public void execute(SchedulableContext sc) {
        Date batchDate = Date.today().addMonths(1);
        Database.executeBatch(new MonthlyTmlcFeeBatch(batchDate.month(), batchDate.year()));
    }
}