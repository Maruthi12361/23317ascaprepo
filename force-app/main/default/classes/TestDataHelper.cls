/**
 * Created by johnreedy on 2018-11-27.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer   Description                                                             *
*   ----------  ----------- ------------------------------------------------------------------------*
*   2018-11-27   Xede       Initial Creation                                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This helper class contains methods used to create test data from test classes.
* @author Xede Consulting Group
* @date 2018-11-27
*/
@isTest
public with sharing class TestDataHelper {
    public static string dslRequest = '{"License_Policy":{ "Id":"00Q1g00000", "Name":"My Test LLC", "License_Type":"test_license_class","Test_Count":10},"Time_Period":{"type":"Annual","period":"2020-7-8"}}';
    public static string dslResponse = '{"lfaftp": 710.0, "lfaftp_prorated": 0.0, "fee_detail_dict": {}, "other_related_info_dict": {"lfaftp_not_subject_to_cpi_adjustment": 0.0, "admin_fee_list": [], "fees_subject_to_proration": 0.0, "prorated_fees": 0.0, "minimum_annual_fee_amount": 0.0, "maximum_annual_fee_amount": null}}';

    public static void createAscapGlobalProperties(Boolean isLoggingOn, Integer retentionDays) {
        insert new ASCAP_Global_Properties__c(
                Error_Logging__c = isLoggingOn,
                Error_Log_Retention_Days__c = retentionDays
        );
    }

    /**
    * @description This helper class method creates a test Account.
    * @param Name Name of the Account
    * @param accountRecordType RecordType Name
    * @param withInsert When true, the record is created within the method itself, else the instantiated Account is passed
    * back to the calling method for insert.
    * @return Account Returns an Account record
    */
    public static Account createAccount(string accountName, string accountRecordType, boolean withInsert){
        Test.setMock(HttpCalloutMock.class, new MelissaDataValidResponseMock());

        Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(accountRecordType).getRecordTypeId();

        Account accountObj = new Account();
        accountObj.Name         = accountName;
        accountObj.RecordTypeId = accountRecordTypeId;

        if (withInsert){
            insert accountObj;
        }

        return accountObj;
    }

    public static Account createAccountWithAddresses(String accountName, String accountRecordType, Boolean setBillingAddress, Boolean setShippingAddress, Boolean withInsert) {
        Account acct = createAccount(accountName, accountRecordType, false);
        if (setBillingAddress) {
            acct.BillingStreet = '4454 Woodward Ave';
            acct.BillingCity = 'Detroit';
            acct.BillingState = 'MI';
            acct.BillingPostalCode = '48201';
            acct.BillingCountry = 'US';
        }

        if (setShippingAddress) {
            acct.ShippingStreet = '2648 W Grand Blvd';
            acct.ShippingCity = 'Detroit';
            acct.ShippingState = 'MI';
            acct.ShippingPostalCode = '48208';
            acct.ShippingCountry = 'US';
        }

        if (withInsert) {
            insert acct;
        }

        return acct;
    }

    /**
    * @description This helper class method creates a test Opportunity.
    * @param accountId Id of the Account to which the opportunity is related
    * @param opportunityRecordType RecordType Name
    * @param withInsert When true, the record is created within the method itself, else the instantiated Opportunity is passed
    * back to the calling method for insert.
    * @return Opportunity Returns an Opportunity record
    */
    public static Opportunity createOpportunity(string opptyName, string accountId, string opportunityRecordType, string stageName, boolean withInsert){
        Id opportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(opportunityRecordType).getRecordTypeId();

        Opportunity opportunityObj = new Opportunity();
        opportunityObj.Name         = opptyName;
        opportunityObj.AccountId    = accountId;
        opportunityObj.RecordTypeId = opportunityRecordTypeId;
        opportunityObj.StageName    = stageName;
        opportunityObj.CloseDate    = system.Today().addDays(60);

        if (withInsert){
            insert opportunityObj;
        }

        return opportunityObj;
    }
    /**
    * @description This helper class method creates a test Contact.
    * @param accountId Id of the Account to which the opportunity is related
    * @param lastName Contact's Last Name
    * @param firstName Contact's first Name
    * @param withInsert When true, the record is created within the method itself, else the instantiated Contact is passed
    * back to the calling method for insert.
    * @return Contact Returns an Contact record
    */
    public static Contact createContact(string accountId, string lastName, string firstName, boolean withInsert){
        String uniqueKey = String.valueOf(System.currentTimeMillis());

        Contact contactObj = new Contact();
        contactObj.AccountId         = accountId;
        contactObj.LastName          = lastName;
        contactObj.FirstName         = firstName;
        contactObj.MailingStreet     = '42 Fairhaven Commons Way';
        contactObj.MailingCity       = 'Fairhaven';
        contactObj.MailingState      = 'MA';
        contactObj.MailingPostalCode = '02719';
        contactObj.Email             = firstName + '.' + lastName + uniqueKey + '@ascap.com';

        if (withInsert){
            insert contactObj;
        }

        return contactObj;
    }
    /**
    * @description This helper class method creates a test Lead.
    * @param lastName Last name of Lead
    * @param firstName First name of Lead
    * @param companyName Name of company related to lead
    * @param withInsert When true, the record is created within the method itself, else the instantiated lead is passed
    * back to the calling method for insert.
    * @return Lead Returns a Lead record
    */
    public static Lead createLead(string lastName, string firstName, string companyName, boolean withInsert){
        Test.setMock(HttpCalloutMock.class, new MelissaDataValidResponseMock());
        //instantiate a new lead
        Lead leadObj = new Lead();

        leadObj.FirstName   = firstName;
        leadObj.LastName    = lastName;
        leadObj.Company     = companyName;
        leadObj.Company_Name__c  = companyName;

        if (withInsert){
            insert leadObj;
        }

        return leadObj;
    }

    /**
    * @description This helper class method creates a test Lead with a General record type.
    * @param licenseClassId License Class Id related to the lead
    * @param withInsert When true, the record is created within the method itself, else the instantiated lead is passed
    * back to the calling method for insert.
    * @return Lead Returns a Lead record
    */
    public static Lead createGeneralLead(string licenseClassId, boolean withInsert){
        Id generalRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get(label.General_Record_Type_Name).getRecordTypeId();
        Test.setMock(HttpCalloutMock.class, new MelissaDataValidResponseMock());

        //instantiate a new lead
        Lead leadObj = new Lead(
              LastName                      = 'Larry'
            , FirstName                     = 'Lead'
            , Email                         = 'llead@ascap.com'
            , License_Class_Relationship__c = licenseClassId
            , Company                       = 'The 703 LLC'
            , Street                        = '123 Main St'
            , City                          = 'Troy'
            , State                         = 'MI'
            , PostalCode                    = '48084'
            , Country                       = 'US'
            , Address_Last_Verified_Date__c = Datetime.now()
            , Address_Status__c             = 'Valid Address'
            , Valid_Address_Indicator__c    = True
            , Company_Name__c               = 'The 703 Bar and Grill'
            , Premise_Street__c             = '123 Main St'
            , Premise_City__c               = 'Troy'
            , Premise_State__c              = 'MI'
            , Premise_Zip_Code__c           = '48084'
            , Premise_Country__c            = 'United States'
            , Last_Verified_Premise_Date__c = Datetime.now()
            , Premise_Address_Status__c     = 'Valid Address'
            , Valid_Premise_Indicator__c    = True
            , recordtypeid                  = generalRecordTypeId
            , Fee_Calculation_Request__c    = dslRequest
            , Fee_Calculation_Response__c   = dslResponse
            , Estimated_Value__c            = 15.00
        );

        if (withInsert){
            insert leadObj;
        }

        return leadObj;
    }
    /**
    * @description This helper class method creates a test Lead with a NewMedia record type.
    * @param licenseClassId License Class Id related to the lead
    * @param withInsert When true, the record is created within the method itself, else the instantiated lead is passed
    * back to the calling method for insert.
    * @return Lead Returns a Lead record
    */
    public static Lead createNewMediaLead(string licenseClassId, boolean withInsert){
        Id newMediaRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get(label.New_Media_Lead_Record_Type).getRecordTypeId();
        Test.setMock(HttpCalloutMock.class, new MelissaDataValidResponseMock());

        //instantiate a new lead
        Lead leadObj = new Lead(
                  LastName                      = 'Larry'
                , FirstName                     = 'Lead'
                , Email                         = 'llead@ascap.com'
                , License_Class_Relationship__c = licenseClassId
                , Company                       = 'The 703 LLC'
                , Service_Name__c               = 'Virtucon.com'
                , Street                        = '123 Main St'
                , City                          = 'Troy'
                , State                         = 'MI'
                , PostalCode                    = '48084'
                , Country                       = 'US'
                , Address_Last_Verified_Date__c = Datetime.now()
                , Address_Status__c             = 'Valid Address'
                , Valid_Address_Indicator__c    = True
                , Premise_Street__c             = '123 Main St'
                , Premise_City__c               = 'Troy'
                , Premise_State__c              = 'MI'
                , Premise_Zip_Code__c           = '48084'
                , Premise_Country__c            = 'United States'
                , Last_Verified_Premise_Date__c = Datetime.now()
                , Premise_Address_Status__c     = 'Valid Address'
                , Valid_Premise_Indicator__c    = True
                , Content_Use_Type__c           = 'Interactive'
                , recordtypeid                  = newMediaRecordTypeId
                , Similar_Web_Average_Monthly_Visit_Count__c = 10
                , Fee_Calculation_Request__c    = dslRequest
                , Fee_Calculation_Response__c   = dslResponse
                , Estimated_Value__c            = 15.00
        );

        if (withInsert){
            insert leadObj;
        }

        return leadObj;
    }
    /**
    * @description This helper class method creates a test Lead with a Valid Mailing address
    * @param lastName Last name of Lead
    * @param firstName First name of Lead
    * @param companyName Name of company related to lead
    * @param withInsert When true, the record is created within the method itself, else the instantiated lead is passed
    * back to the calling method for insert.
    * @return Lead Returns a Lead record
    */
    public static Lead createLeadValidMailingAddress(string lastName, string firstName, string companyName, boolean withInsert){
        Test.setMock(HttpCalloutMock.class, new MelissaDataValidResponseMock());
        //instantiate a new lead
        Lead leadObj = new Lead();

        leadObj.FirstName        = firstName;
        leadObj.LastName         = lastName;
        leadObj.Company          = companyName;
        leadObj.Company_Name__c  = companyName;
        leadObj.Street           = '123 Valid Address';
        leadObj.City             = 'Anytown';
        leadObj.State            = 'NY';
        leadObj.PostalCode       = '10000';

        if (withInsert){
            insert leadObj;
        }

        return leadObj;
    }
    /**
    * @description This helper class method creates a test Lead with a InValid Mailing address
    * @param lastName Last name of Lead
    * @param firstName First name of Lead
    * @param companyName Name of company related to lead
    * @param withInsert When true, the record is created within the method itself, else the instantiated lead is passed
    * back to the calling method for insert.
    * @return Lead Returns a Lead record
    */
    public static Lead createLeadInValidMailingAddress(string lastName, string firstName, string companyName, boolean withInsert){
        Test.setMock(HttpCalloutMock.class, new MelissaDataInvalidResponseMock());
        //instantiate a new lead
        Lead leadObj = new Lead();

        leadObj.FirstName       = firstName;
        leadObj.LastName        = lastName;
        leadObj.Company         = companyName;
        leadObj.Company_Name__c = companyName;
        leadObj.Street          = '456 Bad Address';
        leadObj.City            = 'Anytown';
        leadObj.State           = 'NY';
        leadObj.PostalCode      = '10000';

        if (withInsert){
            insert leadObj;
        }

        return leadObj;
    }
    /**
    * @description This helper class method creates a test Lead with a Valid Premise address
    * @param lastName Last name of Lead
    * @param firstName First name of Lead
    * @param companyName Name of company related to lead
    * @param withInsert When true, the record is created within the method itself, else the instantiated lead is passed
    * back to the calling method for insert.
    * @return Lead Returns a Lead record
    */
    public static Lead createLeadValidPremiseAddress(string lastName, string firstName, string companyName, boolean withInsert){
        Test.setMock(HttpCalloutMock.class, new MelissaDataValidResponseMock());

        //instantiate a new lead
        Lead leadObj = new Lead();

        leadObj.FirstName           = firstName;
        leadObj.LastName            = lastName;
        leadObj.Company             = companyName;
        leadObj.Company_Name__c     = companyName;
        leadObj.Premise_Street__c   = '123 Valid Address';
        leadObj.Premise_City__c     = 'Anytown';
        leadObj.Premise_State__c    = 'NY';
        leadObj.Premise_Zip_Code__c = '10000';

        if (withInsert){
            insert leadObj;
        }

        return leadObj;
    }
    /**
    * @description This helper class method creates a test Lead with a InValid Premise address
    * @param lastName Last name of Lead
    * @param firstName First name of Lead
    * @param companyName Name of company related to lead
    * @param withInsert When true, the record is created within the method itself, else the instantiated lead is passed
    * back to the calling method for insert.
    * @return Lead Returns a Lead record
    */
    public static Lead createLeadInValidPremiseAddress(string lastName, string firstName, string companyName, boolean withInsert){
        Test.setMock(HttpCalloutMock.class, new MelissaDataInvalidResponseMock());

        //instantiate a new lead
        Lead leadObj = new Lead();

        leadObj.FirstName           = firstName;
        leadObj.LastName            = lastName;
        leadObj.Company             = companyName;
        leadObj.Company_Name__c     = companyName;
        leadObj.Premise_Street__c   = '456 Bad Address';
        leadObj.Premise_City__c     = 'Anytown';
        leadObj.Premise_State__c    = 'NY';
        leadObj.Premise_Zip_Code__c = '10000';

        if (withInsert){
            insert leadObj;
        }

        return leadObj;
    }
    /**
    * @description This helper class method creates a test Lead with a Valid Premise address and Mailing Same as Premise
    * is true
    * @param lastName Last name of Lead
    * @param firstName First name of Lead
    * @param companyName Name of company related to lead
    * @param withInsert When true, the record is created within the method itself, else the instantiated lead is passed
    * back to the calling method for insert.
    * @return Lead Returns a Lead record
    */
    public static Lead createLeadValidMailingSameAsPremiseAddress(string lastName, string firstName, string companyName, boolean withInsert){
        Test.setMock(HttpCalloutMock.class, new MelissaDataInvalidResponseMock());

        //instantiate a new lead
        Lead leadObj = new Lead();

        leadObj.FirstName           = firstName;
        leadObj.LastName            = lastName;
        leadObj.Company             = companyName;
        leadObj.Company_Name__c     = companyName;
        leadObj.Premise_Street__c   = '123 Valid Address';
        leadObj.Premise_City__c     = 'Anytown';
        leadObj.Premise_State__c    = 'NY';
        leadObj.Premise_Zip_Code__c = '10000';
        leadObj.Mailing_same_as_premise_indicator__c = true;

        if (withInsert){
            insert leadObj;
        }

        return leadObj;
    }
    /**
    * @description This helper class method creates a test API User.
    * @param lastName Last name of User
    * @param firstName First name of User
    * @param withInsert When true, the record is created within the method itself, else the instantiated lead is passed
    * back to the calling method for insert.
    * @return Lead Returns a User record
    */
    public static User createAdminUser(string lastName, string firstName, boolean withInsert){
        Profile adminProfile = new Profile();
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() ];

        System.runAs (thisUser) {
            adminProfile = [SELECT Id, name FROM Profile WHERE Name =: label.ASCAP_Admin_Profile_Name LIMIT 1];
        }
        String uniqueKey = String.valueOf(System.currentTimeMillis());

        User adminUser = new User();

        adminUser.profileId         = adminProfile.Id;
        adminUser.username          = firstName + '.' + lastName + '-' + uniqueKey + '@ascap.com';
        adminUser.email             = firstName + '.' + lastName + '-' + uniqueKey + '@ascap.com';
        adminUser.emailencodingkey  = 'UTF-8';
        adminUser.localesidkey      = 'en_US';
        adminUser.languagelocalekey = 'en_US';
        adminUser.timezonesidkey    = 'America/New_York';
        adminUser.alias             = 'myAlias';
        adminUser.lastname          = lastName;
        adminUser.firstname         = firstName;

        if (withInsert){
            insert adminUser;
        }

        return adminUser;
    }
    /**
    * @description This helper class method creates a test API User.
    * @param lastName Last name of User
    * @param firstName First name of User
    * @param profileName Profile Name
    * @param withInsert When true, the record is created within the method itself, else the instantiated lead is passed
    * back to the calling method for insert.
    * @return Lead Returns a User record
    */
    public static User createUser(string lastName, string firstName, string profileName,  boolean withInsert){
        Profile userProfile = new Profile();
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() ];

        System.runAs (thisUser) {
            userProfile = [SELECT Id, name FROM Profile WHERE Name =: profileName LIMIT 1];
        }
        String uniqueKey = String.valueOf(System.currentTimeMillis());

        User adminUser = new User();

        adminUser.profileId         = userProfile.Id;
        adminUser.username          = firstName + '.' + lastName + '-' + uniqueKey + '@ascap.com';
        adminUser.email             = firstName + '.' + lastName + '-' + uniqueKey + '@ascap.com';
        adminUser.emailencodingkey  = 'UTF-8';
        adminUser.localesidkey      = 'en_US';
        adminUser.languagelocalekey = 'en_US';
        adminUser.timezonesidkey    = 'America/New_York';
        adminUser.alias             = 'myAlias';
        adminUser.lastname          = lastName;
        adminUser.firstname         = firstName;

        if (withInsert){
            insert adminUser;
        }

        return adminUser;
    }
    /**
    * @description This helper class method creates a test License Class
    * @param licenseClassName Name of the License Class
    * @param withInsert When true, the record is created within the method itself, else the instantiated License class is passed
    * back to the calling method for insert.
    * @return Returns a License Class record
    */
    public static License_Class__c createLicenseClass(string licenseClassName, String licensePolicyRecordType, boolean withInsert){
        //instantiate a new lead
        License_Class__c licenseClassObj = new License_Class__c();

        licenseClassObj.Name                  = licenseClassName;
        licenseClassObj.Active__c             = true;
        licenseClassObj.DSLLicenseTypeName__c = licenseClassName;
        licenseClassObj.License_Policy_Record_Type__c = licensePolicyRecordType;

        if (withInsert){
            insert licenseClassObj;
        }

        return licenseClassObj;
    }

    public static License_Policy__c createLicensePolicy(Id licenseClassId, Date startDate, Date endDate, Boolean withInsert) {
        License_Class__c licenseClass = [SELECT License_Policy_Record_Type__c FROM License_Class__c WHERE Id = :licenseClassId];
        
        License_Policy__c policy = new License_Policy__c(
                License_Class_Type__c = licenseClassId,
                Start_Date__c = startDate,
                End_Date__c = endDate,
                Status__c = GlobalConstant.ACTIVE_LITERAL,
                RecordTypeId = Schema.SObjectType.License_Policy__c.getRecordTypeInfosByDeveloperName().get(licenseClass.License_Policy_Record_Type__c).getRecordTypeId()
        );

        if (withInsert) {
            insert policy;
        }

        return policy;
    }

    public static License_Policy_Time_Period__c createLicensePolicyTimePeriod(Id licensePolicyId, Date startDate, Date endDate, Boolean withInsert) {
        return createLicensePolicyTimePeriod(licensePolicyId, startDate, endDate, false, withInsert);
    }

    public static License_Policy_Time_Period__c createLicensePolicyTimePeriod(Id licensePolicyId, Date startDate, Date endDate, Boolean isOffAir, Boolean withInsert) {
        License_Policy__c licensePolicy = [
                SELECT License_Type__c, Allocated_Fee_Amount__c, LNSPA_Percentage__c,
                        Licensee__c, Billing_Entity__c, Financially_Responsible__c, Reporting_Entity__c
                FROM License_Policy__c WHERE Id = :licensePolicyId
        ];

        License_Policy_Time_Period__c timePeriod = new License_Policy_Time_Period__c(
                License_Policy__c = licensePolicyId,
                Start_Date__c = startDate,
                End_Date__c = endDate,
                License_Type__c = licensePolicy.License_Type__c,
                Allocated_Fee_Amount__c = licensePolicy.Allocated_Fee_Amount__c,
                LNSPA_Percentage__c = licensePolicy.LNSPA_Percentage__c,
                Is_Off_Air__c = isOffAir,
                Billing_Entity__c = licensePolicy.Billing_Entity__c != null ? licensePolicy.Billing_Entity__c : licensePolicy.Licensee__c,
                Financially_Responsible__c = licensePolicy.Financially_Responsible__c != null ? licensePolicy.Financially_Responsible__c : licensePolicy.Licensee__c,
                Reporting_Entity__c = licensePolicy.Reporting_Entity__c
        );

        if (withInsert) {
            insert timePeriod;
        }

        return timePeriod;
    }

    public static Transaction__c createTransaction(Id licensePolicyTimePeriodId, Decimal amount, Boolean withInsert) {
        return createTransaction(licensePolicyTimePeriodId, amount, null, withInsert);
    }

    public static Transaction__c createTransaction(Id licensePolicyTimePeriodId, Decimal amount, String type, Boolean withInsert) {
        Transaction__c trans = new Transaction__c(
                License_Policy_Time_Period__c = licensePolicyTimePeriodId,
                Amount__c = amount,
                Transaction_Type__c = type
        );

        if (withInsert) {
            insert trans;
        }

        return trans;
    }

    /**
    * @description This helper class method creates a Opportunity Matrix record.
    * @param userId User Id
    * @param teamName Team/Region User is assigned to
    * @param thresholdRange The value range the user is authorized to work
    * @param currentStatus The current status of the user
    * @param certified The license classes user is certified to work
    * @param withInsert When true, the record is created within the method itself, else the instantiated lead is passed
    * back to the calling method for insert.
    * @return Lead Returns an Opportunity_Assignment_Matrix__c record
    */
    public static Opportunity_Assignment_Matrix__c createMatrixRecord(string userId, string regionName, string authorizedDollarLimit, string currentStatus,  string certified, boolean withInsert){
        DateTime dt = DateTime.now();
        Long currDateTimeLong = dt.getTime();

        Opportunity_Assignment_Matrix__c oam = new Opportunity_Assignment_Matrix__c();
        oam.License_Specialist__c            = userId;
        oam.Assignment_Region__c             = regionName;
        oam.Authorized_Dollar_Limit__c       = authorizedDollarLimit;
        oam.Certified__c                     = certified;
        oam.Status__c                        = currentStatus;
        oam.Active_Opportunity_Count__c      = 10;
        oam.Active_Opportunity_Count__c      = 10;
        oam.Last_Assigned_Datetime_long__c   = currDateTimeLong;
        oam.Last_Assigned_Datetime__c        = dt;
        oam.Average_Open_Opportunity_Days__c = 20;

        if (withInsert){
            insert oam;
        }
        return oam;
    }
    /**
    * @description This helper class method creates a single collection result.
    * @return saveResult
    */
    public static Database.SaveResult collectResult() {
        Database.SaveResult saveResult =
                (Database.SaveResult)JSON.deserialize(

                        '{'
                                +       '"success":false,'
                                +       '"errors":['
                                +           '{'
                                +               '"message":"You cannot do this...",'
                                +               '"statusCode":"FIELD_CUSTOM_VALIDATION_EXCEPTION"'
                                +           '}'
                                +       ']'
                                +   '}',
                        Database.SaveResult.class
                );

        return saveResult;
    }
    /**
    * @description This helper class method creates a collection results.
    * @return saveResultList
    */
    public static List<Database.SaveResult> collectResults() {
        List<Database.SaveResult> saveResultList = new List<Database.SaveResult>();

        Database.SaveResult successResult = (Database.SaveResult)JSON.deserialize(

                '{'
                        +       '"success":true,'
                        +       '"id":"0013000000abcde"'
                        +   '}',
                Database.SaveResult.class
        );

        Database.SaveResult failureResult = (Database.SaveResult)JSON.deserialize(

                '{'
                        +       '"success":false,'
                        +       '"errors":['
                        +           '{'
                        +               '"message":"You cannot do this...",'
                        +               '"statusCode":"FIELD_CUSTOM_VALIDATION_EXCEPTION"'
                        +           '}'
                        +       ']'
                        +   '}',
                Database.SaveResult.class
        );

        saveResultList.add(successResult);
        saveResultList.add(failureResult);

        return saveResultList;
    }

}