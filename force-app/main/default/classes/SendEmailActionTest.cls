/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer   Description                                                             *
*   ----------  ----------- ------------------------------------------------------------------------*
*   2020-01-08   Xede       Initial Creation                                                        *
*                                                                                                   *
*****************************************************************************************************
*/

/**
 * @description Test class for SendEmailAction class
 * @author Xede Consulting Group (Jason Burke)
 * @date 01-08-2020
 */
@IsTest
private class SendEmailActionTest {

    @isTest
    static void testBehavior() {
        Account acct = TestDataHelper.createAccount('Test Account', 'Legal Entity', true);
        Contact cont = TestDataHelper.createContact(acct.Id, 'Lastname', 'Firstname', false);
        cont.Email = 'test@zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz.com';
        insert cont;
        Case cs = new Case();
        insert cs;

        SendEmailAction.SendEmailInput input = new SendEmailAction.SendEmailInput();
        input.targetObjectId = cont.Id;
        input.whatId = cs.Id;
        input.emailAddr = cont.Email;
        input.emailSubject = 'Test Subject';
        input.emailBody = '<html><body>Test Body</body></html>';

        Test.startTest();
        SendEmailAction.SendEmailOutput[] outputList = SendEmailAction.sendEmail(new SendEmailAction.SendEmailInput[] { input });
        Test.stopTest();

        System.assertEquals(1, outputList.size());
        System.assert(outputList[0].isSuccess);
    }
}