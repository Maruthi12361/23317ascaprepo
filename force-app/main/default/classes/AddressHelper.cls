/**
 * Created by Xede Consulting Group (Jason Burke) on 10/15/2019.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-10-15  Xede Consulting Group       Initial Creation                                        *
*                                                                                                   *
*   Code Coverage: 96%                                                                              *
*                                                                                                   *
*****************************************************************************************************/
/**
 * @description Parent abstract class for helping with MelissaData address validation.  Create a concrete
 * subclass for each SObject (e.g. Account, Lead) whose addresses you need to validate.
 * @author Xede Consulting Group
 * @date 2019-10-15
 */
public abstract with sharing class AddressHelper {
    /**
     * @description Contains strings that refer to each type of address on the SObject (e.g. 'Billing Address').
     * Used to loop over all addresses on the SObject and indicate to subclasses which address is currently being processed.
     * THE ADDRESS TYPES WILL BE PROCESSED IN THE ORDER THEY ARE LISTED, SO ENSURE THAT DEPENDENT ADDRESS TYPES ARE LISTED
     * _AFTER_ THE TYPES THEY ARE DEPENDENT ON!
     */
    protected List<String> addrTypeList = new List<String>();
    /**
     * @description Map of the SObject's address types to Sets of the SObject Ids that have updated those address types.
     */
    protected Map<String, Set<Id>> addrTypeSObjIdsMap = new Map<String, Set<Id>>();
    /**
     * @description Map of the SObject Id to the updated SObject itself.
     * The updated SObject's address fields are updated with the address info from MelissaData.
     */
    protected Map<Id, SObject> updatedSObjMap = new Map<Id, SObject>();

    /**
     * @description Getter for the address type list
     *
     * @return A copy of addrTypeList
     */
    public List<String> getAddrTypeList() {
        return new List<String>(addrTypeList);
    }

    /**
     * @description Getter for the updated SObjects
     *
     * @return A list of all the updated SObjects in updatedSObjMap
     */
    public List<SObject> getUpdatedSObjects() {
        return updatedSObjMap.values();
    }

    /**
     * @description Called by triggers.  Collects the SObjects that have changed an address
     * and have not marked that address as a "Valid Address" (i.e. to override MelissaData).
     * Calls the MelissaData validation service if it finds any matching SObjects.
     *
     * @param newSObjList from Trigger.new
     * @param oldSObjMap from Trigger.oldMap (update) or null (insert)
     */
    public void validateAddresses(List<SObject> newSObjList, Map<Id, SObject> oldSObjMap) {
        addrTypeSObjIdsMap.clear(); // in case this isn't the first use of this helper

        Pattern usaPattern = Pattern.compile(GlobalConstant.USA_REGEX);

        Map<Id, SObject> sObjectUpdateMap = new Map<Id, SObject>();

        // loop through all the changed SObjects
        for (SObject newSObj : newSObjList) {
            newSObj = initialize(newSObj);

            // get the old SObject from the map if it exists
            SObject oldSObj = oldSObjMap != null ? oldSObjMap.get(newSObj.Id) : null;

            // loop through all the address types
            for (String addrType : addrTypeList) {
                // only proceed if the address is in the USA and the status is not "Valid Address", indicating an override of MelissaData
                String addrCountry = getCountry(newSObj, addrType);
                // assume address is in USA if addrCountry is blank
                if ((String.isNotBlank(addrCountry) && !usaPattern.matcher(addrCountry).matches())) {
                    // address is not in the USA
                    if (getAddrStatus(newSObj, addrType) != Label.Valid_Address_Status) {
                        setAddrStatus(newSObj, addrType, Label.MelissaData_Invalid_Status);
                    }
                    sObjectUpdateMap.put(newSObj.Id, newSObj);
                }
                else if (getAddrStatus(newSObj, addrType) != Label.Valid_Address_Status) {
                    // build strings of the new and old addresses
                    String newAddr = buildAddrStr(newSObj, addrType);
                    String oldAddr = buildAddrStr(oldSObj, addrType);

                    // if the address has changed add the SObject Id to the address type map
                    if (newAddr != oldAddr) {
                        if (!addrTypeSObjIdsMap.containsKey(addrType)) {
                            // initialize the Set of Ids for this address type
                            addrTypeSObjIdsMap.put(addrType, new Set<Id>());
                        }
                        addrTypeSObjIdsMap.get(addrType).add(newSObj.Id);
                    }
                }
            }
        }

        // call the MelissaData address validation service if there are addresses that have changed
        if (!addrTypeSObjIdsMap.isEmpty()) {
            callAddrValidationService();
        }

        if (!sObjectUpdateMap.isEmpty()) {
            update sObjectUpdateMap.values();
        }
    }

    /**
     * @description Called by triggers.  Collects the SObjects whose addresses have changed and been updated by MelissaData.
     * Publishes a platform event for each address on each matching SObject, so the UI can pop a toast message notifying the user of this.
     *
     * @param newSObjList Trigger.new
     * @param oldSObjMap Trigger.oldMap
     */
    public void checkForAddressChanges(List<SObject> newSObjList, Map<Id, SObject> oldSObjMap) {
        // Map of SObject Ids to the address types that have been updated by MelissaData
        Map<Id, List<String>> sObjIdAddrTypesMap = new Map<Id, List<String>>();

        // loop through all the changed SObjects
        for (SObject newSObj : newSObjList) {
            // get the old SObject from the map if it exists
            SObject oldSObj = oldSObjMap != null ? oldSObjMap.get(newSObj.Id) : null;

            // loop through all the address types
            for (String addrType : addrTypeList) {
                String addrStatus = getAddrStatus(newSObj, addrType);
                if (addrStatus != null && addrStatus.contains('Melissa')) { // updated by MelissaData
                    // if the address has been verified by MelissaData this change
                    // then add the address type to the SObject Id map
                    if (getAddrLastVerifiedDate(newSObj, addrType) != getAddrLastVerifiedDate(oldSObj, addrType)) {
                        if (!sObjIdAddrTypesMap.containsKey(newSObj.Id)) {
                            // initialize the List of address types for this SObject Ids
                            sObjIdAddrTypesMap.put(newSObj.Id, new List<String>());
                        }
                        sObjIdAddrTypesMap.get(newSObj.Id).add(addrType);
                    }
                }
            }
        }

        // if there are addresses that have changed and been verified, publish an event for each address
        if (!sObjIdAddrTypesMap.isEmpty()) {
            List<Address_Standardization_Events__e> addressEventList = new List<Address_Standardization_Events__e>();

            // loop through the SObject Ids
            for (Id sObjId : sObjIdAddrTypesMap.keySet()) {
                List<String> addrTypeList = sObjIdAddrTypesMap.get(sObjId);
                // loop through the changed and verified address types
                for (String addrType : addrTypeList) {
                    // create a new event for this SObject and address type
                    Address_Standardization_Events__e addressChangeEvent = new Address_Standardization_Events__e();
                    addressChangeEvent.Object_Id__c = sObjId;
                    addressChangeEvent.Object_Type__c = getSObjectType();
                    addressChangeEvent.Address_Type__c = addrType;
                    addressEventList.add(addressChangeEvent);
                }
            }

            // publish the events
            List<Database.SaveResult> saveResults = EventBus.publish(addressEventList);
        }

    }

    /**
     * @description Queries for all of the address fields on all of the SObjects that have changed addresses.
     *
     * @return The queried SObjects
     */
    public SObject[] querySObjects() {
        // collect all of the SObject Ids with changed addresses
        Set<Id> allSObjIdSet = new Set<Id>();
        for (Set<Id> sObjIdSet : addrTypeSObjIdsMap.values()) {
            if (sObjIdSet != null) {
                allSObjIdSet.addAll(sObjIdSet);
            }
        }

        // call the subclass method to do the actual query
        return querySObjects(allSObjIdSet);
    }

    /**
     * @description Updates the update SObject's address with the info from MelissaData
     *
     * @param originalSObj The original SObject before being updated
     * @param addrType The address type
     * @param addrResp The MelissaData info
     */
    public void updateSObject(SObject originalSObj, String addrType, MelissaDataResponse addrResp) {
        if (originalSObj == null || addrResp == null) {
            return;
        }

        // get the update SObject from the map, or create it if needed
        SObject updateSObj = updatedSObjMap.get(originalSObj.Id);
        if (updateSObj == null) {
            // initialize a new update SObject for this original SObject
            updateSObj = getNewSObject();
            updateSObj.Id = originalSObj.Id;
            updatedSObjMap.put(updateSObj.Id, updateSObj);
        }

        // call the subclass method to do the actual updating
        updateSObject(updateSObj, originalSObj, addrType, addrResp);
    }

    /**
     * @description Determines if a callout is needed to update this address type on this SObject.
     * The default implementation checks that the address type has been changed on this SObject and is not blank.
     * Can be overridden by subclasses.
     *
     * @param sObj The SObject
     * @param addrType The address type
     *
     * @return true if a callout is needed, false otherwise.
     */
    public virtual Boolean isCalloutNeeded(SObject sObj, String addrType) {
        if (sObj == null) {
            return false;
        }

        Set<Id> sObjIdSet = addrTypeSObjIdsMap.get(addrType);
        return sObjIdSet != null && sObjIdSet.contains(sObj.Id) && isAddrFound(sObj, addrType);
    }

    /**
     * @description Getter for the company component of the address.  Default implementation returns null.
     * Can be overridden in subclasses.
     *
     * @param sObj The SObject
     * @param addrType The address type
     *
     * @return The company
     */
    public virtual String getCompany(SObject sObj, String addrType) {
        return null;
    }

    /**
     * @description Getter for the street component of the address.
     *
     * @param sObj The SObject
     * @param addrType The address type
     *
     * @return The street
     */
    public abstract String getStreet(SObject sObj, String addrType);

    /**
     * @description Getter for the city component of the address.
     *
     * @param sObj The SObject
     * @param addrType The address type
     *
     * @return The city
     */
    public abstract String getCity(SObject sObj, String addrType);

    /**
     * @description Getter for the state component of the address.
     *
     * @param sObj The SObject
     * @param addrType The address type
     *
     * @return The state
     */
    public abstract String getState(SObject sObj, String addrType);

    /**
     * @description Getter for the postal code component of the address.
     *
     * @param sObj The SObject
     * @param addrType The address type
     *
     * @return The postal code
     */
    public abstract String getPostalCode(SObject sObj, String addrType);

    /**
     * @description Getter for the country component of the address.
     *
     * @param sObj the SObject
     * @param addrType the address type
     *
     * @return the country
     */
    public abstract String getCountry(SObject sObj, String addrType);

    protected virtual SObject initialize(SObject sObj) {
        return sObj;
    }

    /**
    * @description This method takes a string of return codes, parses them, and returns a full list of error codes
    * and their description, highlighting in bold the ones that are errors.
    * @param  returnCodes The comma-delimited return codes
    * @return The return codes and their descriptions
    */
    protected String getResultCodeString(string returnCodes){
        string returnString = '';

        //Build list of return codes
        string[] returnCodeList = returnCodes.split(',');

        //Instantiate map of MelissaData return codes and descriptions
        MelissaDataResultCodes melissaDataResultCodesMap = new MelissaDataResultCodes();

        //Iterate over list of return codes and add each one to return string, and make Errors bold.
        for (string eachCode: returnCodeList){
            if (melissaDataResultCodesMap.description.containsKey(eachCode)) {
                if (eachCode.contains(GlobalConstant.MELISSA_DATA_ERROR_PREFIX)) {
                    returnString = returnString + '<b>' + eachCode + ': ' + melissaDataResultCodesMap.description.get(eachCode) + '</b><br>';
                } else {
                    returnString = returnString + eachCode + ': ' + melissaDataResultCodesMap.description.get(eachCode) + '<br>';
                }
            }
            else {
                returnString = returnString + eachCode + ': need to add description to map<br>';
            }
        }
        return returnString;
    }

    /**
     * @description Replaces all carriage return characters in a string with spaces
     *
     * @param str The string to process
     *
     * @return The processed string
     */
    protected String replaceCR(String str) {
        return str != null ? str.replaceAll('\r', ' ') : '';
    }

    /**
     * @description Queries for all the address fields on all the SObjects in the given Set of Ids
     *
     * @param allSObjIdSet The Set of Ids to query on
     *
     * @return The queried SObjects
     */
    protected abstract SObject[] querySObjects(Set<Id> allSObjIdSet);

    /**
     * @description Checks if the given address type is blank
     *
     * @param sObj The SObject
     * @param addrType The address type
     *
     * @return false if the address is blank, true otherwise
     */
    protected abstract Boolean isAddrFound(SObject sObj, String addrType);

    /**
     * @description Getter for the status field value of the given address type
     *
     * @param sObj The SObject
     * @param addrType The address type
     *
     * @return The address status
     */
    protected abstract String getAddrStatus(SObject sObj, String addrType);

    /**
     * @description Setter for the status field of the given address type
     *
     * @param sObj The SObject
     * @param addrType The address type
     * @param status The address status
     */
    protected abstract void setAddrStatus(SObject sObj, String addrType, String status);

    /**
     * @description Concatenates all the address fields into a single string
     *
     * @param sObj The SObject
     * @param addrType The address type
     *
     * @return The address string
     */
    protected abstract String buildAddrStr(SObject sObj, String addrType);

    /**
     * @description Calls the MelissaData address validation service
     */
    protected abstract void callAddrValidationService();

    /**
     * @description Getter for the addresses last MelissaData verification date
     *
     * @param sObj The SObject
     * @param addrType The address type
     *
     * @return The last MelissaData verification Datetime
     */
    protected abstract Datetime getAddrLastVerifiedDate(SObject sObj, String addrType);

    /**
     * @description Getter for the name of the SObject the current class is for (e.g. "Account", "Lead")
     *
     * @return The SObject type name
     */
    protected abstract String getSObjectType();

    /**
     * @description Getter for a new SObject instance of the type the current class is for (e.g. Account, Lead)
     *
     * @return The new SObject
     */
    protected abstract SObject getNewSObject();

    /**
     * @description Updates the address fields on the updateSObj from the MelissaData address info.
     *
     * @param updateSObj The updated SObject
     * @param originalSObject The original SObject before MelissaData updates
     * @param addrType The address type
     * @param addrResp The MelissaData info
     */
    protected abstract void updateSObject(SObject updateSObj, SObject originalSObject, String addrType, MelissaDataResponse addrResp);
}