/**
 * Created by Xede Consulting Group (Jason Burke) on 11/13/2019.
 */

public with sharing class AvailableRecordTypeSelectController {

    @AuraEnabled
    public static MyRecordTypeInfo[] getAvailableRecordTypes(String sObjectType, String excludedRecordTypes) {
        Schema.DescribeSobjectResult[] results = Schema.describeSObjects(new String[] { sObjectType });
        System.assert(results != null && !results.isEmpty(), 'SObject type "' + sObjectType + '" not found!');

        String[] excludedRecordTypesList = excludedRecordTypes != null ? excludedRecordTypes.split('\\s*,\\s*') : new String[0];

        List<MyRecordTypeInfo> myRtInfos = new List<MyRecordTypeInfo>();
        List<Schema.RecordTypeInfo> rtInfos = results[0].getRecordTypeInfos();
        for (Schema.RecordTypeInfo rtInfo : rtInfos) {
            if (rtInfo.isAvailable() && !excludedRecordTypesList.contains(rtInfo.getDeveloperName())) {
                myRtInfos.add(new MyRecordTypeInfo(rtInfo.getRecordTypeId(), rtInfo.getName()));
            }
        }

        myRtInfos.sort();
        return myRtInfos;
    }

    public class MyRecordTypeInfo implements Comparable {
        @AuraEnabled
        public Id id;
        @AuraEnabled
        public String name;

        public MyRecordTypeInfo(Id id, String name) {
            this.id = id;
            this.name = name;
        }

        public Integer compareTo(Object obj) {
            MyRecordTypeInfo that = (MyRecordTypeInfo) obj;
            return this.name.compareTo(that.name);
        }
    }
}