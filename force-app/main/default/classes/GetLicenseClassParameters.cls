/**
 * Created by Xede
 */

/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer   Description                                                             *
*   ----------  ----------- ------------------------------------------------------------------------*
*   2018-10-06  Xede  		Initial Creation                                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description Makes the web service call and get the license class related metadata information
* @author Xede Consulting Group
* @see GetLicenseClassParametersTest for test class
* @date 2018-10-06
*/
public class GetLicenseClassParameters {

    public static LicenseClassConfig GetLicenseClassInfo(string license_class_name) {
        boolean debugMode = Label.GetLicenseClassParameters_Debug.toLowerCase() == 'true'? true : false;

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(Label.License_Class_Metadata_Endpoint+license_class_name);
        request.setMethod('GET');
        request.setHeader('Content-type', 'application/json');
        request.setHeader('Authorization', 'Bearer ' + label.License_Class_Fee_Calc_Bearer_Token);

        System.debug('\n license class='+Label.License_Class_Metadata_Endpoint+license_class_name);
        HttpResponse response = new HttpResponse();
        if(Test.isRunningTest()){
            response = LicenseClassTestDataHelper.getTestLicenseClassInfo();
        }else{
            response = http.send(request);
        }

        string jsonResponseString = response.getBody();
        //if debugMode custom setting is set to true, then output the metadata returned by the DSL
        if (debugMode) {
            System.debug('\n jsonResponseString==' + jsonResponseString);
        }

        //parse the metadata returned from the DSL for a given license class
        LicenseClassConfig objPolicy = new LicenseClassConfig(System.JSON.createParser(jsonResponseString));

        if (debugMode){
            for (LicenseClassConfig.Policy_parameter_list each: objPolicy.Policy_parameter_list){
                system.debug('\n jwr-Debug: Parameter> ' + each.container_name+'-'+each.data_type+'-'+each.name+'-'+each.parameter_type);
            }
        }

        return objPolicy;

    }
    public static LicenseClassConfig_V2 GetLicenseClassConfigV2Info(string license_class_name) {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(Label.License_Class_Metadata_Endpoint+license_class_name);
        request.setMethod('GET');
        request.setHeader('Content-type', 'application/json');
        request.setHeader('Authorization', 'Bearer ' + label.License_Class_Fee_Calc_Bearer_Token);

        HttpResponse response = new HttpResponse();
        if(Test.isRunningTest()){
            response = LicenseClassTestDataHelper.getTestLicenseClass2Info();
        }else{
            response = http.send(request);
        }

        string jsonResponseString = response.getBody();
        System.debug('\n jsonResponseString=='+jsonResponseString);
        LicenseClassConfig_V2 objPolicy = new LicenseClassConfig_V2(System.JSON.createParser(jsonResponseString));

        for (LicenseClassConfig_V2.Policy_parameter_list each: objPolicy.Policy_parameter_list){
            system.debug('\n jwr-Debug: each> ' + each.name);
        }
        return objPolicy;

    }


    public static LicenseClassFeeResponse GetLicenseClassFeeResult (string inputJSONString) {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(Label.License_Class_FeeResult_Endpoint);
        request.setMethod('GET');
        request.setHeader('Content-type', 'application/json');
        request.setHeader('Authorization', 'Bearer ' + label.License_Class_Fee_Calc_Bearer_Token);

        request.setBody(inputJSONString);
        HttpResponse response = new HttpResponse();
        LicenseClassFeeResponse feeResponseResult = new LicenseClassFeeResponse();

        try{
            if(Test.isRunningTest()) {
                response = LicenseClassTestDataHelper.getTestLicenseClassFeeResult();
            }
            else{
                response = http.send(request);
            }
        }catch(Exception ex){
            feeResponseResult.response = response;
            return feeResponseResult;
        }
        if(response != null && response.getStatusCode() != 200){
            feeResponseResult.response = response;
            return feeResponseResult;
        }

        string jsonResponseString = response.getBody();

        system.debug('\n jsonResponseString==' + jsonResponseString);

        feeResponseResult = LicenseClassFeeResponse.parse(jsonResponseString);
        feeResponseResult.response = response;

        system.debug('\n feeResponseResult==' + feeResponseResult);


        return feeResponseResult;

    }
}
