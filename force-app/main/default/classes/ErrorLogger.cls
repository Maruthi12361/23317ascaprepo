/**
 * Created by johnreedy on 2019-02-13.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-02-13   Xede Consulting Group      Initial Creation                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This public class supports user and system defined error logging.
* @author Xede Consulting Group
* @see ErrorLogHelperTest
* @date 2019-02-13 
*/
public without sharing class ErrorLogger {
    //get ascap global properties settings
    private static ASCAP_Global_Properties__c globalProperties = ASCAP_Global_Properties__c.getOrgDefaults();

    private Profile runningUserProfile;
    private string className;
    private string methodName;
    private List<Error_Log__c> logList = new List<Error_Log__c>();

    /**
    * @description Empty constructor.
    */
    public ErrorLogger() {
        deleteOldRecords();
    }

    /**
    * @description This writes message from an Exception object to the Error Log object.
    * @param exceptionObject
    */
    public ErrorLogger log(Exception exceptionObject){
        //if error logging is false, exit
        if (globalProperties.Error_Logging__c == false){
            return this;
        }

        className  = 'Unknown';
        methodName = 'Unknown';

        /*
        If the exceptionObject is not null, iterate over each line in exception stacktrace and find the class name
        and the method name.
        */
        if (exceptionObject != null) {
            String[] lines = exceptionObject.getStackTraceString().split('\n');
            for (String line : lines) {
                //if the source is AnonymousBlock continue to next record
                if (line.startsWith('AnonymousBlock:')) {
                    className = 'AnonymousBlock';
                    break;
                }
                //if the line starts with 'Class', get the class and method name
                if (line.startsWith('Class.')) {
                    className = line.substringAfter('.').substringBeforeLast('.');
                    methodName = line.substringBefore(':').substringAfterLast('.');
                    break;
                }
            }
        }

        // log the exception message
        logMessage(GlobalConstant.SYSTEM_EXCEPTION_LITERAL, exceptionObject.getMessage());

        return this;
    }

    /**
    * @description This writes a user defined message to the Error Log object.
    * @param classMethodName
    * @param message
    */
    public ErrorLogger log(string classMethodName, string message){
        //if error logging is false, exit
        if (globalProperties.Error_Logging__c == false){
            return this;
        }

        className  = 'Unknown';
        methodName = 'Unknown';

        //classMethodName is a hyphen delimited string, parse and set class and method names
        if (String.isNotBlank(classMethodName)) {
            String[] classMethodNameList = classMethodName.split('-');
            className = classMethodNameList[0];
            if (classMethodNameList.size() > 1) {
                methodName = classMethodNameList[1];
            }
        }

        // log the user-defined message
        logMessage(GlobalConstant.USER_DEFINED_LITERAL, message);

        return this;
    }

    public ErrorLogger log(String className, String methodName, String message) {
        //if error logging is false, exit
        if (globalProperties.Error_Logging__c == false){
            return this;
        }

        this.className = String.isNotBlank(className) ? className : 'Unknown';
        this.methodName = String.isNotBlank(methodName) ? methodName : 'Unknown';

        // log the user-defined message
        logMessage(GlobalConstant.USER_DEFINED_LITERAL, message);

        return this;
    }

    public ErrorLogger save() {
        if (!logList.isEmpty()) {
            //insert error log records
            try{
                insert logList;
                if (globalProperties.Send_Email__c){
                    sendErrorLogEmail();
                }
                logList.clear();
            }
            catch(Exception ex){
                system.debug('Debug:ErrorLogger.logMessage Error writing messages to error log object:');
                for (Error_Log__c log : logList) {
                    System.debug(log);
                }
            }
        }

        return this;
    }

    /**
    * @description This private method writes an exception or user defined message to the error log object.
    * @param messageType
    * @param message
    */
    private void logMessage(string messageType, string message) {
        //instantiate new error log object and set fields
        Error_Log__c errorLog = new Error_Log__c();
        errorLog.Class_Name__c     = className;
        errorLog.Method_Name__c    = methodName;
        errorLog.Error_Datetime__c = System.Now();
        errorLog.User_Id__c        = UserInfo.getUserId();
        errorLog.Running_User__c   = UserInfo.getUserId();
        errorLog.User_Name__c      = UserInfo.getName();
        errorLog.User_Profile__c   = getRunningUserProfile().Name;
        errorLog.Message_Type__c   = messageType;
        errorLog.Message__c        = message;

        logList.add(errorLog);
    }

    /**
    * @description This private method sends an email to a select list of users when an error is logged.
    * @param errorLog
    */
    private void sendErrorLogEmail(){
        //instantiate new mail message
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        // Strings to hold the email addresses to which you are sending the email.
        String[] toAddresses = globalProperties.Error_Log_Email_List__c.split(';');
        mail.setToAddresses(toAddresses);

        // Specify the address used when the recipients reply to the email.
        mail.setReplyTo('ascaponesupport@ascap.com');

        // Specify the name used as the display name.
        mail.setSenderDisplayName('ascapone Dev Support');

        // Specify the subject line for your email address.
        mail.setSubject('New Errors Logged');

        String htmlBody = '<p>Errors were encountered within Salesforce.  Please see below for details.</p>';
        for (Error_Log__c errorLog : logList) {
            htmlBody += '<br/>-------------------------------------------';
            htmlBody += '<br/>Error Log Id: ' + errorLog.Id ;
            htmlBody += '<br/>  Class Name: ' + errorLog.Class_Name__c;
            htmlBody += '<br/> Method Name: ' + errorLog.Method_Name__c;
            htmlBody += '<br/>   User Name: ' + errorLog.User_Name__c;
            htmlBody += '<br/>User Profile: ' + errorLog.User_Profile__c;
            htmlBody += '<br/>Message Type: ' + errorLog.Message_Type__c;
            htmlBody += '<br/>     Message: ' + errorLog.Message__c;
        }

        mail.setHtmlBody(htmlBody);

        // Send the email you have created.
        try {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
        }
        catch (Exception ex){
            system.debug('Debug-ErrorLogger:sendErrorLogEmail Error: An error occurred while attempting to send error log' +
                    'email.\n' + ex.getMessage());
        }
    }

    private Profile getRunningUserProfile() {
        if (runningUserProfile == null) {
            runningUserProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()];
        }

        return runningUserProfile;
    }

    /**
    * @description This private method deletes records from the Error Log table that have exceeded the retention period.
    */
    private void deleteOldRecords(){
        integer daysAgo = globalProperties.Error_Log_Retention_Days__c != null ? globalProperties.Error_Log_Retention_Days__c.intValue() : 0;
        List<Error_Log__c> errorLogList = [
                SELECT id
                  FROM Error_Log__c
                 WHERE Error_Datetime__c <=: System.Now().addDays( - daysAgo)];

        if (!errorLogList.isEmpty()) {
            try{
                delete errorLogList;
            }
            catch(Exception ex){
                system.debug('Debug-ErrorLogger:deleteOldRecords Error: Unable to delete old Error Log Records more than ' +
                        globalProperties.Error_Log_Retention_Days__c + ' days old.\n' + ex.getMessage());
            }
        }
    }
}