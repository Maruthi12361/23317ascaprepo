/**
 * Created by johnreedy on 2019-10-11.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                Description                                                *
*   ----------  -----------------------  ---------------------------------------------------------- *
*   2019-10-11   Xede Consulting Group 	 Initial Creation                                           *
*                                                                                                   *
*   Code Coverage: 100.0%
*****************************************************************************************************
*/
/**
* @description This helper class supports operations against the AccountContactRelation object.
* @author Xede Consulting Group
* @see AccountContactRoleHelperTest
* @date 2019-10-11 
*/
public with sharing class AccountContactRelationHelper {
    /**
    * @description This method checks for an existing Key Contact and displays an error if a user tries to enter
    * a duplicate key contact.
    * @author Xede Consulting Group
    * @see AccountContactRoleHelperTest
    * @date 2019-10-11
    */
    public static void checkForDuplicateKeyContact(List<AccountContactRelation> contactRelationshipList){

        //populate list of Active key contacts in list passed in to method
        List<AccountContactRelation> keyContactIdList = new List<AccountContactRelation>();
        set<id> accountIdSet = new set<id>();

        for (AccountContactRelation each: contactRelationshipList){
            if (each.Roles != null && each.Roles.contains(GlobalConstant.KEY_CONTACT_LITERAL) && each.IsActive == True){
                accountIdSet.add(each.AccountId);
                keyContactIdList.add(each);
            }
        }

        //if no key contacts exist in list, then exit method
        if (keyContactIdList.size() == 0){
            return;
        }

        //create map of existing Active key contacts, for the list of Account Contact Relationships passed in to method.
        List<AccountContactRelation> existingKeyContactRoleList = [
                SELECT Id, AccountId, ContactId, Roles
                  FROM AccountContactRelation
                 WHERE AccountId in: accountIdSet
                   AND Roles Includes (:GlobalConstant.KEY_CONTACT_LITERAL)
                   AND IsActive = True
        ];

        map<string,string> accountKeyContactMap = new map<string,string>();
        for (AccountContactRelation each: existingKeyContactRoleList){
            accountKeyContactMap.put(each.AccountId, each.ContactId);
        }

        /*
        Iterate over list of key contacts and see if another key contact already exists, and if so, display the key
        contact error.
        */
        for (AccountContactRelation each: keyContactIdList){
            if (accountKeyContactMap.containsKey(each.AccountId)){
                if (each.ContactId != accountKeyContactMap.get(each.AccountId)) {
                    each.addError(label.errMessage_Key_Contact);
                }
            }
            else{
                //if not key contacts exist for account, add this key contact
                accountKeyContactMap.put(each.AccountId, each.ContactId);
            }
        }
    }
}