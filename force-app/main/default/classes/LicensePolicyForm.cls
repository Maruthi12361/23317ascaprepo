/**
 * Created by Xede
 */

/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer   Description                                                             *
*   ----------  ----------- ------------------------------------------------------------------------*
*   2018-10-06  Xede  		Initial Creation                                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description Wrapper class of the fields returned by web service call
* @author Xede Consulting Group
* @see LicensePolicyFormTest for test class
* @date 2018-10-06
*/
public class LicensePolicyForm {
    @AuraEnabled
    public List<LicenseClassField> fields { get; set; }

    @AuraEnabled
    public List<LicenseClassField> premiseFields { get; set; }

    @AuraEnabled
    public SObject record { get; set; }

    @AuraEnabled
    public License_Policy_Time_Period__c licensePolicyTimePeriod{ get; set; }

    @AuraEnabled
    public Transaction__c transactionRecord{get;set;}

    @AuraEnabled
    public Boolean prorate_license_fee { get; set; }

    @AuraEnabled
    public String proration_type { get; set; }

    @AuraEnabled
    public String licenseClassName { get; set; }

    @AuraEnabled
    public String licenseClassId { get; set; }

    public LicensePolicyForm() {
        fields = new List<LicenseClassField>();
        premiseFields = new List<LicenseClassField>();
    }
}