public class LicenseClassConfig_V3 {
    public String name;
    //public String desc;
    public boolean auditable;
    public boolean refundable;
    public cls_derived_value_dict derived_value_dict;
    public boolean prorate_license_fee;
    public cls_proration_type proration_type;
    public cls_derived_variable_formula_list[] derived_variable_formula_list;
    public cls_fee_adjustment_list[] fee_adjustment_list;
    public cls_fee_adjustment_rule_items[] fee_adjustment_rule_items;
    public String fee_adjustment_formula_text;	//formula_value = 0.00
    public cls_fee_adjustment_parse_results[] fee_adjustment_parse_results;
    public cls_admin_fee_list[] admin_fee_list;
    public cls_admin_fee_rule_items[] admin_fee_rule_items;
    public cls_admin_fee_formula_text admin_fee_formula_text;
    public cls_admin_fee_parse_results[] admin_fee_parse_results;
    public cls_per_container_rate_schedule_item_list[] per_container_rate_schedule_item_list;
    public cls_per_container_rate_schedule_rule_items[] per_container_rate_schedule_rule_items;
    public cls_per_container_rate_schedule_rule_item_values[] per_container_rate_schedule_rule_item_values;
    public cls_per_container_formula_text per_container_formula_text;
    public String container_rate_schedule_item_formula_text;	//formula_value= rs.Container_Competition_Total
    public cls_per_container_rate_schedule_parse_results[] per_container_rate_schedule_parse_results;
    public cls_policy_parameter_lp_child_dict policy_parameter_lp_child_dict;
    public cls_policy_parameter_list[] policy_parameter_list;
    public cls_policy_parameter_constant_list[] policy_parameter_constant_list;
    public cls_payment_schedule_list[] payment_schedule_list;
    public cls_cpi_fee_adjustment_list[] cpi_fee_adjustment_list;
    public cls_container_min_max_dict container_min_max_dict;
    public cls_required_instance_attributes[] required_instance_attributes;

    public cls_validation_issues[] validation_issues;
    class cls_derived_value_dict {
    }
    class cls_proration_type {
    }
    class cls_derived_variable_formula_list {
    }
    class cls_fee_adjustment_list {
    }
    class cls_fee_adjustment_rule_items {
    }
    class cls_fee_adjustment_parse_results {
    }
    class cls_admin_fee_list {
    }
    class cls_admin_fee_rule_items {
    }
    class cls_admin_fee_formula_text {
    }
    class cls_admin_fee_parse_results {
    }
    class cls_per_container_rate_schedule_item_list {
        public String container_name;	//Competition
        public boolean include_in_proration;
        public boolean included_in_CPI;
        public String name;	//Annual_Fee
        public String only_if_true;	//True
        public boolean only_if_true_boolean;
        public String rule_string;	//
        public String schedule_string;	//
    }
    class cls_per_container_rate_schedule_rule_items {
        public String container_name;	//Competition
        public boolean include_in_proration;
        public boolean included_in_CPI;
        public String name;	//Annual_Fee
        public String only_if_true;	//True
        public boolean only_if_true_boolean;
        public String rule_string;	//
        public String schedule_string;	//
    }
    class cls_per_container_rate_schedule_rule_item_values {
    }
    class cls_per_container_formula_text {
    }
    class cls_per_container_rate_schedule_parse_results {
        public cls_line_list[] line_list;
        public String name;	//Annual_Fee
        public String schedule_string;	//
        public cls_value_matrix[] value_matrix;

    }
    class cls_line_list {
    }
    class cls_value_matrix {
    }

    class cls_policy_parameter_lp_child_dict {
    }
    class cls_policy_parameter_list {
        public String container_name;	//Competition
        public String data_type;	//Integer
        public String name;	//Attendees_Num
        public String parameter_type;	//fee_calc
    }
    class cls_policy_parameter_constant_list {
    }
    class cls_payment_schedule_list {
        public cls_payment_schedule_list[] payment_schedule_list;
        public cls_payment_amount_rule payment_amount_rule;
        public String payment_amount_type;	//Actual
        public String payment_due_date_day_type;	//
        public String payment_period_number;	//1
        public String payment_schedule_name;	//Annual Payment Terms
        public cls_payment_schedule_rate_schedule_list[] payment_schedule_rate_schedule_list;
    }
    class cls_payment_amount_rule {
    }
    class cls_payment_schedule_rate_schedule_list {
    }
    class cls_cpi_fee_adjustment_list {
        public String adjustment_month;	//October
        public String context;	//rate_schedule
        public boolean enabled;
        public String index;	//CPI-U
        public String only_if_true;	//True
        public String rounding_type;	//Nearest_Dollar
    }
    class cls_container_min_max_dict {
        public cls_License_Policy License_Policy;
    }
    class cls_License_Policy {
        public String container_name;	//License_Policy
        public boolean max_dv;
        public boolean max_fee_is_numeric;
        public String max_only_if_true;	//True
        public cls_maximum_annual_fee_amount maximum_annual_fee_amount;
        public cls_maximum_fee_amount maximum_fee_amount;
        public boolean min_annual_dv;
        public boolean min_dv;
        public boolean min_fee_is_numeric;
        public String min_only_if_true;	//True
        public Integer minimum_annual_fee_amount;	//0
        public Integer minimum_fee_amount;	//0
    }
    class cls_maximum_annual_fee_amount {
    }
    class cls_maximum_fee_amount {
    }
    class cls_required_instance_attributes {

    }

    class cls_properties {
        public cls_type type;
        public cls_period period;
    }
    class cls_Id {
        public String type;	//string
    }
    class cls_Name {
        public String type;	//string
    }
    class cls_License_Type {
        public String type;	//string
    }
    class cls_Competition_List {
        public String type;	//array
        public cls_items[] items;
    }
    class cls_items {
        public String type;	//object
        public cls_properties properties;

    }
    class cls_Name_and_Address {
        public String type;	//string
    }


    class cls_type {
        public String type;	//string
    }
    class cls_period {
        public String type;	//string
    }
    class cls_validation_issues {
    }
    public static LicenseClassConfig_V3 parse(String json){
        return (LicenseClassConfig_V3) System.JSON.deserialize(json, LicenseClassConfig_V3.class);
    }


}