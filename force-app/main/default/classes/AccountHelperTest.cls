/**
 * Created by Xede Consulting Group (Jason Burke) on 10/14/2019.
 */

@IsTest
private class AccountHelperTest {
    public static User apiUser;
    public static User accountManager;
    public static User leadSpecialist;

    /**
    * @description This method initializes setup data.
    */
    @testSetup
    public static void setup(){
        User apiUser = TestDataHelper.createAdminUser('AdminUser', 'Andy', false); //NOPMD - reviewed
        apiUser.API_User_Indicator__c = True;
        insert apiUser;

        User accountManager = TestDataHelper.createUser('Manager', 'Account', label.Account_Manager_Profile_Name, true);
        User leadSpecialist = TestDataHelper.createUser('Specialist', 'Lead', label.Lead_Identification_Specialist_Profile_Name, true);

    }
    /**
    * @description This method queries the data created in the setup method.
    */
    public static void init() {

        apiUser = [SELECT Id, Name, API_User_Indicator__c FROM User WHERE LastName = 'AdminUser' LIMIT 1];
        accountManager = [SELECT Id, Name, API_User_Indicator__c FROM User WHERE LastName = 'Manager' LIMIT 1];
        leadSpecialist = [SELECT Id, Name, API_User_Indicator__c FROM User WHERE LastName = 'Specialist' LIMIT 1];
    }

    @isTest
    static void testAddressValidationInsert() {
        init();

        //instantiate mock response class
        Test.setMock(HttpCalloutMock.class, new MelissaDataValidResponseMock());

        Account acct = TestDataHelper.createAccountWithAddresses('Test Account', 'Station', true, true, false);

        test.startTest();
        system.runAs(accountManager) {
            insert acct;
        }
        test.stopTest();

        acct = [
                SELECT Melissa_Data_Processed_Indicator__c, Valid_Billing_Address_Indicator__c, Address_Status__c,
                        Melissa_Data_Bus_Processed_Indicator__c, Valid_Business_Address_Indicator__c, Business_Address_Status__c
                FROM Account WHERE Id = :acct.Id
        ];
        System.assert(acct.Melissa_Data_Processed_Indicator__c);
        System.assert(acct.Valid_Billing_Address_Indicator__c);
        System.assertEquals(label.MelissaData_Valid_Status, acct.Address_Status__c);
        System.assert(acct.Melissa_Data_Bus_Processed_Indicator__c);
        System.assert(acct.Valid_Business_Address_Indicator__c);
        System.assertEquals(label.MelissaData_Valid_Status, acct.Business_Address_Status__c);
    }

    @IsTest
    static void testAddressValidationUpdate() {
        init();

        //instantiate mock response class
        Test.setMock(HttpCalloutMock.class, new MelissaDataValidResponseMock());

        Account acct = TestDataHelper.createAccountWithAddresses('Test Account', 'Station', true, true, false);
        acct.Address_Status__c = Label.Valid_Address_Status;
        acct.Business_Address_Status__c = Label.Valid_Address_Status;
        insert acct;

        test.startTest();
        system.runAs(accountManager) {
            acct.BillingStreet = '2000 Brush St';
            acct.BillingPostalCode = '48226';
            acct.Address_Status__c = label.MelissaData_Invalid_Status;
            acct.ShippingStreet = '2100 Wooodward Ave';
            acct.ShippingPostalCode = '48201';
            acct.Business_Address_Status__c = label.MelissaData_Invalid_Status;
            update acct;
        }
        Test.stopTest();

        acct = [
                SELECT Melissa_Data_Processed_Indicator__c, Valid_Billing_Address_Indicator__c, Address_Status__c,
                        Melissa_Data_Bus_Processed_Indicator__c, Valid_Business_Address_Indicator__c, Business_Address_Status__c
                FROM Account WHERE Id = :acct.Id
        ];
        System.assert(acct.Melissa_Data_Processed_Indicator__c);
        System.assert(acct.Valid_Billing_Address_Indicator__c);
        System.assertEquals(label.MelissaData_Valid_Status, acct.Address_Status__c);
        System.assert(acct.Melissa_Data_Bus_Processed_Indicator__c);
        System.assert(acct.Valid_Business_Address_Indicator__c);
        System.assertEquals(label.MelissaData_Valid_Status, acct.Business_Address_Status__c);
    }

    @isTest
    static void testNonUsaAddresses() {
        Account acct = TestDataHelper.createAccountWithAddresses('Test Account', 'Station', false, false, false);
        acct.ShippingStreet = '123 Conehead Lane';
        acct.ShippingCity = 'Paris';
        acct.ShippingState = 'ZZZ';
        acct.ShippingPostalCode = 'abc123';
        acct.ShippingCountry = 'France';
        acct.Billing_Address_same_as_Business_Address__c = true;

        test.startTest();
        insert acct;
        test.stopTest();

        acct = [
                SELECT ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry,
                        BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry,
                        Business_Address_Status__c, Address_Status__c
                FROM Account WHERE Id = :acct.Id
        ];

        System.assertEquals(acct.ShippingStreet, acct.BillingStreet);
        System.assertEquals(acct.ShippingCity, acct.BillingCity);
        System.assertEquals(acct.ShippingState, acct.BillingState);
        System.assertEquals(acct.ShippingPostalCode, acct.BillingPostalCode);
        System.assertEquals(acct.ShippingCountry, acct.BillingCountry);
        System.assertEquals(Label.MelissaData_Invalid_Status, acct.Business_Address_Status__c);
        System.assertEquals(Label.MelissaData_Invalid_Status, acct.Address_Status__c);

    }

    @isTest
    static void testAccountAddressHelperNulls() {
        AccountAddressHelper helper = new AccountAddressHelper();
        Test.startTest();
        System.assert(!helper.isAddrFound(null, null));
        System.assert(!helper.isAddrFound(new Account(), null));
        System.assertEquals(null, helper.getStreet(null, null));
        System.assertEquals(null, helper.getStreet(new Account(), null));
        System.assertEquals(null, helper.getCity(null, null));
        System.assertEquals(null, helper.getCity(new Account(), null));
        System.assertEquals(null, helper.getState(null, null));
        System.assertEquals(null, helper.getState(new Account(), null));
        System.assertEquals(null, helper.getPostalCode(null, null));
        System.assertEquals(null, helper.getPostalCode(new Account(), null));
        System.assertEquals(null, helper.getAddrStatus(null, null));
        System.assertEquals(null, helper.getAddrStatus(new Account(), null));
        System.assertEquals(null, helper.buildAddrStr(new Account(), null));
        System.assertEquals(null, helper.getAddrLastVerifiedDate(null, null));
        System.assertEquals(null, helper.getAddrLastVerifiedDate(new Account(), null));
        Test.stopTest();
    }

    @isTest
    static void testInheritVipDesignation() {
        init();
        TestDataHelper.createAscapGlobalProperties(false, 0);

        DisableTrigger__c disableAccountTrigger = new DisableTrigger__c(Name = 'AccountTrigger', Disabled__c = true);
        insert disableAccountTrigger;

        Account parentAcct = TestDataHelper.createAccount('Parent Account', 'Legal Entity', false);
        parentAcct.Bypass_Duplicate_Rules__c = true;
        insert parentAcct;
        System.debug('!!! INSERTED PARENT ACCOUNT !!!');

        List<Account> childAcctList = new List<Account>();
        for (Integer i = 0; i < 20; i++) {
            Account childAcct = TestDataHelper.createAccount('Child Account ' + i, 'Broadcast Television', false);
            childAcct.ParentId = parentAcct.Id;
            childAcct.Bypass_Duplicate_Rules__c = true;
            childAcctList.add(childAcct);
        }
        insert childAcctList;
        System.debug('!!!!!! INSERTED CHILD ACCOUNTS : ' + childAcctList.size() + ' !!!!!!');

        List<Account> grandAcctList = new List<Account>();
        Integer i = 0;
        for (Account childAcct : childAcctList) {
            for (Integer j = 0; j < 20; j++) {
                Account grandAcct = TestDataHelper.createAccount('Grandchild Account ' + i + ' ' + j, 'Broadcast Television', false);
                grandAcct.ParentId = childAcct.Id;
                grandAcct.Bypass_Duplicate_Rules__c = true;
                grandAcctList.add(grandAcct);
            }
            i++;
        }
        insert grandAcctList;
        System.debug('!!!!!!!!! INSERTED GRANDCHILD ACCOUNTS : ' + grandAcctList.size() + ' !!!!!!!!!');

        disableAccountTrigger.Disabled__c = false;
        update disableAccountTrigger;

        Test.startTest();
        parentAcct.VIP__c = true;
        parentAcct.Serviced_By__c = accountManager.Id;
        update parentAcct;
        Test.stopTest();

        for (Account acct : [SELECT Name, VIP__c, Serviced_By__c FROM Account]) {
//            System.debug(acct);
            System.assert(acct.VIP__c);
            System.assertEquals(accountManager.Id, acct.Serviced_By__c);
        }
    }
}