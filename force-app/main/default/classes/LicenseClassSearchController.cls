public with sharing class LicenseClassSearchController {
    @AuraEnabled
    public static List < License_Class__c > findLicenseClass(String searchKeyWord) {
        String searchKey = searchKeyWord + '%';
        List < License_Class__c > returnList = new List < License_Class__c > ();
        List < License_Class__c > licenseClassList = [select id, Name from License_Class__c where Name LIKE: searchKey LIMIT 500];

        for (License_Class__c licenseClass: licenseClassList) {
            returnList.add(licenseClass);
        }
        return returnList;
    }
}