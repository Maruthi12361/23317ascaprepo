/**
 * Created by Xede Consulting Group (Jason Burke) on 11/13/2019.
 */

@IsTest
private class AvailableRecordTypeSelectControllerTest {
    @isTest
    static void testBehavior() {
        Test.startTest();
        List<AvailableRecordTypeSelectController.MyRecordTypeInfo> myRecordTypeInfos =
                AvailableRecordTypeSelectController.getAvailableRecordTypes('Account', 'Master');
        Test.stopTest();

        System.assert(myRecordTypeInfos.size() > 0);
    }
}