/**
 * Created by Xede
 */

/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer   Description                                                             *
*   ----------  ----------- ------------------------------------------------------------------------*
*   2018-10-06  Xede  		Initial Creation                                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description Makes the web service call and get the data
* @author Xede Consulting Group
* @see LicenseClassFeeResponseTest for test class
* @date 2018-10-06
*/
public class LicenseClassFeeResponse {
    public HttpResponse response {get;set;}

    public class Admin_fee_list {
    }

    public Double lfaftp;
    public Double lfaftp_not_subject_to_cpi_adjustment;
    public List<Admin_fee_list> admin_fee_list;
    public Double fees_subject_to_proration;
    public Double prorated_fees;
    public Double lfaftp_prorated;
    public Double minimum_annual_fee_amount;
    public Object maximum_annual_fee_amount;

    public Fee_detail_dict fee_detail_dict;
    public Other_related_info_dict other_related_info_dict;

    public class Other_related_info_dict {
        public Double lfaftp_not_subject_to_cpi_adjustment;
        public List<Admin_fee_list> admin_fee_list;
        public Double fees_subject_to_proration;
        public Double prorated_fees;
        public Double minimum_annual_fee_amount;
        public Object maximum_annual_fee_amount;
    }

    public class Fee_detail_dict {

    }
    public static LicenseClassFeeResponse parse(String json) {
        return (LicenseClassFeeResponse) System.JSON.deserialize(json, LicenseClassFeeResponse.class);
    }
}