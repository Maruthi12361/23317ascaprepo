/**
 * Created by johnreedy on 2019-10-01.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer          Description                                                      *
*   ----------  -----------------  -----------------------------------------------------------------*
*   2019-10-01  Xede Consulting    Initial Creation                                                 *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This test class exercises the SendSelfLicensingEmailAction class.
* @author Xede Consulting Group
* @date 2019-10-01
*/
@IsTest
private class SendSelfLicensingEmailActionTest {
    /**
    * @description Given a new Lead, pass the id to the SendSelfLicensingEmailAction.sendSelfLicensingEmail
    * method.
    */
    static testMethod void testEmailAction(){
        List<Id> idList = new list<id>();

        //create lead test record
        Lead leadObj = TestDataHelper.createLeadValidPremiseAddress('Lastname', 'Firstname', 'Test BBQ', true);

        //add test record id to list
        idList.add(leadObj.Id);

        //run test
        test.StartTest();
            SendSelfLicensingEmailAction.sendSelfLicensingEmail(idList);
        test.StopTest();
    }
}