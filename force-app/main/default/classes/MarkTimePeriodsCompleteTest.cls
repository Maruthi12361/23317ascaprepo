/**
 * Created by johnreedy on 2019-11-14.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  -------------  ---------------------------------------------------------------------*
*   2019-11-14   Xede Consulting  Initial Creation                                                     *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This test class execises the methhods in the ...
* @author Xede Consulting Group
* @date 2019-11-14 
*/
@IsTest
private class MarkTimePeriodsCompleteTest {
    static Account licenseeObj;
    static Account stationObj;
    static License_Class__c licenseClassObj;
    static License_Policy__c perProgramPolicyObj;
    static License_Policy_Time_Period__c timePeriodObj;

    /**
    * @description This method initializes setup data.
    */
    static void init(){
        licenseeObj = TestDataHelper.createAccount('Broadcasting Corp', 'Legal Entity', true);
        stationObj  = TestDataHelper.createAccount('WWWW TV Detroit', 'Broadcast Television', False);
        licenseClassObj = TestDataHelper.createLicenseClass('Teest LC', GlobalConstant.INDUSTRY_TYPE, true);
        perProgramPolicyObj = TestDataHelper.createLicensePolicy(licenseClassObj.Id, Date.Today().toStartOfMonth(), Date.Today().addMonths(1).addDays(-1).toStartOfMonth(),false);
        perProgramPolicyObj.Licensing_Area__c = 'Broadcast Television';
        perProgramPolicyObj.License_Type__c = GlobalConstant.PER_PROGRAM_TYPE_LITERAL;
        insert perProgramPolicyObj;

        timePeriodObj = TestDataHelper.createLicensePolicyTimePeriod(perProgramPolicyObj.Id, Date.Today().toStartOfMonth(), Date.Today().addMonths(1).addDays(-1).toStartOfMonth(),false);
        timePeriodObj.Stage__c = 'Final Adjustment';
        timePeriodObj.Most_Recent_Report_Status__c = 'Review In Progress';
        timePeriodObj.Next_Report_Due_Date__c = Date.Today().addDays(-1);
        insert timePeriodObj;
    }

    /**
    * @description Given a Time Period record that meets the criteria to mark it complete, mark it complete.
    */
    static testMethod void testStandardConstructor(){
        init();

        License_Policy_Time_Period__c currTimePeriod = [SELECT Id, Most_Recent_Report_Status__c FROM License_Policy_Time_Period__c WHERE Id = :timePeriodObj.Id LIMIT 1];
        System.assertEquals('Review In Progress', currTimePeriod.Most_Recent_Report_Status__c);

        Test.startTest();
            MarkTimePeriodsCompleteBatch controller = new MarkTimePeriodsCompleteBatch();
            database.executeBatch(controller);
        Test.stopTest();

        currTimePeriod = [SELECT Id, Most_Recent_Report_Status__c FROM License_Policy_Time_Period__c WHERE Id = :timePeriodObj.Id LIMIT 1];
        System.assertEquals(GlobalConstant.COMPLETE_STATUS_LITERAL, currTimePeriod.Most_Recent_Report_Status__c);

    }
    /**
    * @description Given a Time Period record that meets the criteria of the custom query, mark it complete.
    */
    static testMethod void testCustomConstructor(){
        init();
        License_Policy_Time_Period__c currTimePeriod = [SELECT Id, Most_Recent_Report_Status__c FROM License_Policy_Time_Period__c WHERE Id = :timePeriodObj.Id LIMIT 1];
        System.assertEquals('Review In Progress', currTimePeriod.Most_Recent_Report_Status__c);

        string query = 'Select Id, Next_Report_Due_Date__c   ' +
                '         From License_Policy_Time_Period__c  ' +
                '        Where Most_Recent_Report_Status__c != \'Complete\'' +
                '          And Next_Report_Due_Date__c <= Today   ' +
                '          And License_Policy__r.License_Type__c = \'Per-Program\'';

        Test.startTest();
            MarkTimePeriodsCompleteBatch controller = new MarkTimePeriodsCompleteBatch(query);
            database.executeBatch(controller);
        Test.stopTest();

        currTimePeriod = [SELECT Id, Most_Recent_Report_Status__c FROM License_Policy_Time_Period__c WHERE Id = :timePeriodObj.Id LIMIT 1];
        System.assertEquals(GlobalConstant.COMPLETE_STATUS_LITERAL, currTimePeriod.Most_Recent_Report_Status__c);

    }
    /**
    * @description Given a Time Period record with a Stage of 'Initial Report' - but whose Next Report
    * Date is today, skip the record and leave its Most Recent Report Status as is.
    */
    static testMethod void testInitialReportStage(){
        init();

        timePeriodObj.Stage__c = GlobalConstant.INITIAL_REPORT_LITERAL;
        update timePeriodObj;

        License_Policy_Time_Period__c currTimePeriod = [SELECT Id, Most_Recent_Report_Status__c FROM License_Policy_Time_Period__c WHERE Id = :timePeriodObj.Id LIMIT 1];
        System.assertEquals('Review In Progress', currTimePeriod.Most_Recent_Report_Status__c);

        Test.startTest();
            MarkTimePeriodsCompleteBatch controller = new MarkTimePeriodsCompleteBatch();
            database.executeBatch(controller);
        Test.stopTest();

        currTimePeriod = [SELECT Id, Most_Recent_Report_Status__c FROM License_Policy_Time_Period__c WHERE Id = :timePeriodObj.Id LIMIT 1];
        System.assertEquals('Review In Progress', currTimePeriod.Most_Recent_Report_Status__c);


    }
}