/**
 * Created by johnreedy on 2019-10-14.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                Description                                                *
*   ----------  -----------------------  ---------------------------------------------------------- *
*   2019-10-14   Xede Consulting Group 	 Initial Creation                                           *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This test class exercises the AccountContactRelationHelper class.
* @author Xede Consulting Group
* @see AccountContactRelationshipHelperTestTest
* @date 2019-10-14 
*/
@IsTest
public with sharing class AccountContactRelationHelperTest {

    static Account accountObj;
    static Contact keyContactObj;

    static void init(){
        accountObj    = TestDataHelper.createAccount('Account Name', 'Legal Entity', true);
        keyContactObj = TestDataHelper.createContact(accountObj.Id, 'Contact', 'Key', true);
    }
    /**
    * @description Given a new contact, edit the relationship and set to 'Key Contact'
    */
    private static testMethod void firstKeyContact() {
        init();

        //get current acr record for accountObj and keyContactObj
        AccountContactRelation acr = [
                SELECT id, AccountId, ContactId, Roles
                  FROM AccountContactRelation
                 WHERE AccountId =: accountObj.Id
                   AND ContactId =: keyContactObj.Id
                 LIMIT 1];

        //update acr record with 'Key Contact' role
        test.startTest();
            acr.Roles = GlobalConstant.KEY_CONTACT_LITERAL;
            update acr;
        test.stopTest();

        //verify that Roles was set to 'Key Contact'
        acr = [
                SELECT id, AccountId, ContactId, Roles
                  FROM AccountContactRelation
                 WHERE id = :acr.Id
                 LIMIT 1];

        system.assertEquals(true, acr.Roles.contains(GlobalConstant.KEY_CONTACT_LITERAL));

    }
    /**
    * @description Given an existing 'Key Contact', create a new contact, and attempt to edit the relationship and set
    * to 'Key Contact'.  Exception should be thrown indicating duplicate 'Key Contact'
    */
    private static testMethod void duplicateKeyContact() {
        init();

        //get current acr record for accountObj and keyContactObj
        AccountContactRelation acr = [
                SELECT id, AccountId, ContactId, Roles
                  FROM AccountContactRelation
                 WHERE AccountId =: accountObj.Id
                   AND ContactId =: keyContactObj.Id
                 LIMIT 1];

        //update acr as 'Key Contact'
        acr.Roles = GlobalConstant.KEY_CONTACT_LITERAL;
        update acr;

        //create new Contact record
        Contact duplicateKeyContact = TestDataHelper.createContact(accountObj.Id, 'Contact', 'Duplicate', true);

        //get acr for new Contact
        AccountContactRelation acrDup = [
                SELECT id, AccountId, ContactId, Roles
                  FROM AccountContactRelation
                 WHERE AccountId =: accountObj.Id
                   AND ContactId =: duplicateKeyContact.Id
                 LIMIT 1];

        //start test
        test.startTest();
            //update new Contact acr with 'Key Contact' role and validate that error message is thrown
            acrDup.Roles = GlobalConstant.KEY_CONTACT_LITERAL;
                try {
                    update acrDup;
                }
                catch (Exception ex){
                    system.assertEquals(true, ex.getMessage().contains(label.errMessage_Key_Contact));
                }
        test.stopTest();

    }
    /**
    * @description Given an existing INACTIVE 'Key Contact', create a new contact, and attempt to edit the relationship and set
    * to 'Key Contact'.  Since original key contact is Inactive, duplicated Key Contact should be successfully updated.
    */
    private static testMethod void inactiveKeyContact() {
        init();

        //get current acr record for accountObj and keyContactObj
        AccountContactRelation acr = [
                SELECT id, AccountId, ContactId, Roles
                  FROM AccountContactRelation
                 WHERE AccountId =: accountObj.Id
                   AND ContactId =: keyContactObj.Id
                 LIMIT 1];

        //update acr as 'Key Contact'
        acr.Roles    = GlobalConstant.KEY_CONTACT_LITERAL;
        acr.IsActive = False;
        update acr;

        //create new Contact record
        Contact duplicateKeyContact = TestDataHelper.createContact(accountObj.Id, 'Contact', 'Duplicate', true);

        //get acr for new Contact
        AccountContactRelation acrDup = [
                SELECT id, AccountId, ContactId, Roles
                  FROM AccountContactRelation
                 WHERE AccountId =: accountObj.Id
                   AND ContactId =: keyContactObj.Id
                 LIMIT 1];

        //start test
        test.startTest();

            //update new Contact acr with 'Key Contact' role and validate that it is successfully updates
            acrDup.Roles = GlobalConstant.KEY_CONTACT_LITERAL;
            update acrDup;

        test.stopTest();

        //verify that Roles was set to 'Key Contact'
        acrDup = [
                SELECT id, AccountId, ContactId, Roles
                  FROM AccountContactRelation
                 WHERE id = :acrDup.id
                 LIMIT 1];

        system.assertEquals(true, acrDup.Roles.contains(GlobalConstant.KEY_CONTACT_LITERAL));


    }
}