/**
 * Created by Xede Consulting Group (Jason Burke) on 12/11/2019.
 */

@IsTest
private class MarkOffAirMonthsControllerTest {
    @isTest
    static void testBehavior() {
        Account licensee = TestDataHelper.createAccount('Test Account', 'Broadcast Television', true);
        License_Class__c licenseClass = TestDataHelper.createLicenseClass('Test Class', GlobalConstant.INDUSTRY_TYPE, true);
        Date startDate = Date.newInstance(Date.today().year(), 1, 1);
        Date endDate = Date.newInstance(startDate.year(), 12, 31);
        License_Policy__c policy = TestDataHelper.createLicensePolicy(licenseClass.Id, startDate, endDate, true);

        License_Policy_Time_Period__c timePeriod = TestDataHelper.createLicensePolicyTimePeriod(policy.Id, startDate, endDate, true);
        Transaction__c tran = new Transaction__c(
                License_Policy_Time_Period__c = timePeriod.Id,
                Amount__c = 1200,
                Transaction_Type__c = GlobalConstant.BILLING_ITEM_LITERAL,
                Transaction_Date__c = startDate.addDays(15)
        );
        insert tran;

        Date[] offAirDates = new Date[] {
                Date.newInstance(startDate.year(), 1, 1),
                Date.newInstance(startDate.year(), 6, 1),
                Date.newInstance(startDate.year(), 12, 1)
        };

        Test.startTest();
        MarkOffAirMonthsController.markOffAirMonths(timePeriod.Id, offAirDates);
        Test.stopTest();

        System.assertEquals(5, [SELECT COUNT() FROM License_Policy_Time_Period__c]);
        Transaction__c creditTran = [SELECT Amount__c FROM Transaction__c WHERE Transaction_Type__c = :GlobalConstant.CREDIT_LITERAL];
        System.assertEquals(-tran.Amount__c / 4, Integer.valueOf(creditTran.Amount__c));
    }
}