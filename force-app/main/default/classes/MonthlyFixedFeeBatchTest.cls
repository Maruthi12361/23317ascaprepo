/**
 * Created by Xede Consulting Group (Jason Burke) on 11/27/2019.
 */

@IsTest
private class MonthlyFixedFeeBatchTest {
    @testSetup
    static void setup() {
        TestDataHelper.createAscapGlobalProperties(false, 0);
        TestDataHelper.createLicenseClass('Fixed Fee', GlobalConstant.FIXED_TYPE, true);
    }

    @isTest
    static void testHappyPath() {
        LicensePolicyHelper.doCreateRetroTimePeriods = false;

        License_Class__c fixedFeeLicenseClass = [SELECT Id FROM License_Class__c];

        Account licensee = TestDataHelper.createAccount('Test Licensee', 'Legal Entity', true);

        List<License_Policy__c> policyList = new List<License_Policy__c>();
        License_Policy__c policy0 = TestDataHelper.createLicensePolicy(fixedFeeLicenseClass.Id, Date.today().toStartOfMonth().addYears(-1), null, false);
        policy0.Licensee__c = licensee.Id;
        policy0.Other_Content_Type__c = Label.Billing_Frequency_Monthly;
        policyList.add(policy0);

        License_Policy__c policy1 = TestDataHelper.createLicensePolicy(fixedFeeLicenseClass.Id, Date.today().addMonths(1).toStartOfMonth().addYears(-1), null, false);
        policy1.Licensee__c = licensee.Id;
        policy1.Other_Content_Type__c = Label.Billing_Frequency_Quarterly;
        policyList.add(policy1);

        License_Policy__c policy2 = TestDataHelper.createLicensePolicy(fixedFeeLicenseClass.Id, Date.today().addMonths(2).toStartOfMonth().addYears(-1), null, false);
        policy2.Licensee__c = licensee.Id;
        policy2.Other_Content_Type__c = Label.Billing_Frequency_Semi_Annually;
        policyList.add(policy2);

        License_Policy__c policy3 = TestDataHelper.createLicensePolicy(fixedFeeLicenseClass.Id, Date.today().addMonths(3).toStartOfMonth().addYears(-1), null, false);
        policy3.Licensee__c = licensee.Id;
        policy3.Other_Content_Type__c = Label.Billing_Frequency_Annually;
        policyList.add(policy3);
        insert policyList;

        List<Fixed_Fee_Definition__c> fixedFeeDefList = new List<Fixed_Fee_Definition__c>();
        Fixed_Fee_Definition__c fixedFeeDef00 = new Fixed_Fee_Definition__c(
                License_Policy__c = policy0.Id,
                Start_Date__c = Date.newInstance(Date.today().year(), 1, 1),
                End_Date__c = Date.newInstance(Date.today().year(), 12, 31),
                Billing_Frequency__c = Label.Billing_Frequency_Monthly,
                Total_Amount__c = 12000000,
                Payment_Due_Date_Type__c = Label.Due_Date_Type_Date,
                Offset_Day_of_Month__c = '27'
        );
        fixedFeeDefList.add(fixedFeeDef00);
        Fixed_Fee_Definition__c fixedFeeDef01 = new Fixed_Fee_Definition__c(
                License_Policy__c = policy0.Id,
                Start_Date__c = Date.newInstance(Date.today().year() + 1, 1, 1),
                End_Date__c = Date.newInstance(Date.today().year() + 1, 12, 31),
                Billing_Frequency__c = Label.Billing_Frequency_Monthly,
                Total_Amount__c = 12000000,
                Payment_Due_Date_Type__c = Label.Due_Date_Type_Date,
                Offset_Day_of_Month__c = '27'
        );
        fixedFeeDefList.add(fixedFeeDef01);
        Fixed_Fee_Definition__c fixedFeeDef10 = new Fixed_Fee_Definition__c(
                License_Policy__c = policy1.Id,
                Start_Date__c = Date.newInstance(Date.today().year(), 1, 1),
                End_Date__c = Date.newInstance(Date.today().year(), 12, 31),
                Billing_Frequency__c = Label.Billing_Frequency_Quarterly,
                Total_Amount__c = 12000000,
                Payment_Due_Date_Type__c = Label.Due_Date_Type_Offset,
                Offset_Day_of_Month__c = '30'
        );
        fixedFeeDefList.add(fixedFeeDef10);
        Fixed_Fee_Definition__c fixedFeeDef11 = new Fixed_Fee_Definition__c(
                License_Policy__c = policy1.Id,
                Start_Date__c = Date.newInstance(Date.today().year() + 1, 1, 1),
                End_Date__c = Date.newInstance(Date.today().year() + 1, 12, 31),
                Billing_Frequency__c = Label.Billing_Frequency_Quarterly,
                Total_Amount__c = 12000000,
                Payment_Due_Date_Type__c = Label.Due_Date_Type_Offset,
                Offset_Day_of_Month__c = '30'
        );
        fixedFeeDefList.add(fixedFeeDef11);
        Fixed_Fee_Definition__c fixedFeeDef20 = new Fixed_Fee_Definition__c(
                License_Policy__c = policy2.Id,
                Start_Date__c = Date.newInstance(Date.today().year(), 1, 1),
                End_Date__c = Date.newInstance(Date.today().year(), 12, 31),
                Billing_Frequency__c = Label.Billing_Frequency_Semi_Annually,
                Total_Amount__c = 12000000,
                Payment_Due_Date_Type__c = Label.Due_Date_Type_Offset,
                Offset_Day_of_Month__c = '45'
        );
        fixedFeeDefList.add(fixedFeeDef20);
        Fixed_Fee_Definition__c fixedFeeDef21 = new Fixed_Fee_Definition__c(
                License_Policy__c = policy2.Id,
                Start_Date__c = Date.newInstance(Date.today().year() + 1, 1, 1),
                End_Date__c = Date.newInstance(Date.today().year() + 1, 12, 31),
                Billing_Frequency__c = Label.Billing_Frequency_Semi_Annually,
                Total_Amount__c = 12000000,
                Payment_Due_Date_Type__c = Label.Due_Date_Type_Offset,
                Offset_Day_of_Month__c = '45'
        );
        fixedFeeDefList.add(fixedFeeDef21);
        Fixed_Fee_Definition__c fixedFeeDef30 = new Fixed_Fee_Definition__c(
                License_Policy__c = policy3.Id,
                Start_Date__c = Date.newInstance(Date.today().year(), 1, 1),
                End_Date__c = Date.newInstance(Date.today().year(), 12, 31),
                Billing_Frequency__c = Label.Billing_Frequency_Annually,
                Total_Amount__c = 12000000,
                Payment_Due_Date_Type__c = Label.Due_Date_Type_Offset,
                Offset_Day_of_Month__c = '60'
        );
        fixedFeeDefList.add(fixedFeeDef30);
        Fixed_Fee_Definition__c fixedFeeDef31 = new Fixed_Fee_Definition__c(
                License_Policy__c = policy3.Id,
                Start_Date__c = Date.newInstance(Date.today().year() + 1, 1, 1),
                End_Date__c = Date.newInstance(Date.today().year() + 1, 12, 31),
                Billing_Frequency__c = Label.Billing_Frequency_Annually,
                Total_Amount__c = 12000000,
                Payment_Due_Date_Type__c = Label.Due_Date_Type_Offset,
                Offset_Day_of_Month__c = '60'
        );
        fixedFeeDefList.add(fixedFeeDef31);
        insert fixedFeeDefList;

        Test.startTest();
        new MonthlyFixedFeeScheduler().execute(null);
//        Database.executeBatch(new MonthlyFixedFeeBatch(8, 2020));
        Test.stopTest();

        License_Policy_Time_Period__c[] timePeriodList = [
                SELECT Billing_Entity__c, Financially_Responsible__c, Start_Date__c, End_Date__c,
                        License_Policy__r.Other_Content_Type__c
                FROM License_Policy_Time_Period__c
        ];
        System.assertEquals(4, timePeriodList.size());
        for (License_Policy_Time_Period__c timePeriod : timePeriodList) {
            System.assertEquals(licensee.Id, timePeriod.Billing_Entity__c);
            System.assertEquals(licensee.Id, timePeriod.Financially_Responsible__c);

            System.debug('Billing Frequency: ' + timePeriod.License_Policy__r.Other_Content_Type__c);
            System.debug('Start Date: ' + timePeriod.Start_Date__c);
            System.debug('End Date: ' + timePeriod.End_Date__c);
        }

        Transaction__c[] tranList = [
                SELECT License_Policy_Time_Period__r.License_Policy__c, License_Policy_Time_Period__r.End_Date__c,
                        Amount__c, Transaction_Date__c
                FROM Transaction__c
        ];

        System.assertEquals(4, tranList.size());
        for (Transaction__c tran : tranList) {
            if (tran.License_Policy_Time_Period__r.License_Policy__c == policy0.Id) {
                System.assertEquals(1000000, tran.Amount__c);
                Date tranDate = Date.newInstance(tran.License_Policy_Time_Period__r.End_Date__c.year(), tran.License_Policy_Time_Period__r.End_Date__c.month(), 27);
                System.assertEquals(tranDate, tran.Transaction_Date__c);
            }
            else if (tran.License_Policy_Time_Period__r.License_Policy__c == policy1.Id) {
                System.assertEquals(3000000, tran.Amount__c);
                Date tranDate = tran.License_Policy_Time_Period__r.End_Date__c.addDays(30);
                System.assertEquals(tranDate, tran.Transaction_Date__c);
            }
            else if (tran.License_Policy_Time_Period__r.License_Policy__c == policy2.Id) {
                System.assertEquals(6000000, tran.Amount__c);
                Date tranDate = tran.License_Policy_Time_Period__r.End_Date__c.addDays(45);
                System.assertEquals(tranDate, tran.Transaction_Date__c);
            }
            else if (tran.License_Policy_Time_Period__r.License_Policy__c == policy3.Id) {
                System.assertEquals(12000000, tran.Amount__c);
                Date tranDate = tran.License_Policy_Time_Period__r.End_Date__c.addDays(60);
                System.assertEquals(tranDate, tran.Transaction_Date__c);
            }
        }
    }
    @isTest
    static void testIrregularTimePeriod() {
        LicensePolicyHelper.doCreateRetroTimePeriods = false;

        License_Class__c fixedFeeLicenseClass = [SELECT Id FROM License_Class__c];

        Account licensee = TestDataHelper.createAccount('Test Licensee', 'Legal Entity', true);

        License_Policy__c policy1 = TestDataHelper.createLicensePolicy(fixedFeeLicenseClass.Id, Date.newInstance(2019, 12, 1), Date.newInstance(2020, 11, 30), false);
        policy1.Licensee__c = licensee.Id;
        insert policy1;

        List<Fixed_Fee_Definition__c> fixedFeeDefList = new List<Fixed_Fee_Definition__c>();
        Fixed_Fee_Definition__c fixedFeeDef10 = new Fixed_Fee_Definition__c(
                License_Policy__c = policy1.Id,
                Start_Date__c = Date.newInstance(2019, 12, 1),
                End_Date__c = Date.newInstance(2019, 12, 31),
                Billing_Frequency__c = Label.Billing_Frequency_Quarterly,
                Total_Amount__c = 4000,
                Payment_Due_Date_Type__c = Label.Due_Date_Type_Offset,
                Offset_Day_of_Month__c = '30'
        );
        fixedFeeDefList.add(fixedFeeDef10);
        Fixed_Fee_Definition__c fixedFeeDef11 = new Fixed_Fee_Definition__c(
                License_Policy__c = policy1.Id,
                Start_Date__c = Date.newInstance(2020, 1, 1),
                End_Date__c = Date.newInstance(2020, 11, 30),
                Billing_Frequency__c = Label.Billing_Frequency_Quarterly,
                Total_Amount__c = 8000,
                Payment_Due_Date_Type__c = Label.Due_Date_Type_Offset,
                Offset_Day_of_Month__c = '30'
        );
        fixedFeeDefList.add(fixedFeeDef11);
        insert fixedFeeDefList;

        License_Policy_Time_Period__c timePeriod = TestDataHelper.createLicensePolicyTimePeriod(policy1.Id, Date.newInstance(2019, 12, 1), Date.newInstance(2020, 2, 29), true);
        Transaction__c trans = TestDataHelper.createTransaction(timePeriod.Id, 1000, 'Billing Item', true);

        Test.startTest();
        Database.executeBatch(new MonthlyFixedFeeBatch(1, 2020));
        Test.stopTest();

        trans = [SELECT Amount__c FROM Transaction__c WHERE Id = :trans.Id];
        System.assertEquals(1000, trans.Amount__c);
    }

    @isTest
    static void testCreateRetroTimePeriods() {
        License_Class__c fixedFeeLicenseClass = [SELECT Id FROM License_Class__c];

        Account licensee = TestDataHelper.createAccount('Test Licensee', 'Legal Entity', true);

        Date today = Date.today();
        Date policyStartDate = today.toStartOfMonth().addMonths(-3);
        Date policyEndDate = policyStartDate.addYears(1).addDays(-1);

        License_Policy__c policy = TestDataHelper.createLicensePolicy(fixedFeeLicenseClass.Id, policyStartDate, policyEndDate, false);
        policy.Status__c = 'Draft';
        policy.Licensee__c = licensee.Id;
        insert policy;

        Fixed_Fee_Definition__c fixedFeeDef = new Fixed_Fee_Definition__c(
                License_Policy__c = policy.Id,
                Start_Date__c = policyStartDate,
                End_Date__c = policyEndDate,
                Billing_Frequency__c = Label.Billing_Frequency_Monthly,
                Total_Amount__c = 12000,
                Payment_Due_Date_Type__c = Label.Due_Date_Type_Date,
                Offset_Day_of_Month__c = '1'
        );
        insert fixedFeeDef;

        Test.startTest();
        policy.Status__c = GlobalConstant.ACTIVE_LITERAL;
        update policy;
        Test.stopTest();

        System.assertEquals(4, [SELECT COUNT() FROM License_Policy_Time_Period__c]);

        Transaction__c[] transList = [SELECT Transaction_Date__c FROM Transaction__c];
        System.assertEquals(4, transList.size());
        for (Transaction__c trans : transList) {
            System.assert(trans.Transaction_Date__c > today);
        }
    }

    @isTest
    static void testCreateRetroTimePeriodsError() {
        ASCAP_Global_Properties__c ascapProps = ASCAP_Global_Properties__c.getOrgDefaults();
        ascapProps.Error_Logging__c = true;
        ascapProps.Error_Log_Retention_Days__c = 1;
        update ascapProps;

        License_Class__c fixedFeeLicenseClass = [SELECT Id FROM License_Class__c];

        Account licensee = TestDataHelper.createAccount('Test Licensee', 'Legal Entity', true);

        Date today = Date.today();
        Date policyStartDate = today.toStartOfMonth().addMonths(-3);
        Date policyEndDate = policyStartDate.addYears(1).addDays(-1);

        License_Policy__c policy = TestDataHelper.createLicensePolicy(fixedFeeLicenseClass.Id, policyStartDate, policyEndDate, false);
        policy.Status__c = 'Draft';
        policy.Licensee__c = licensee.Id;
        insert policy;

        Test.startTest();
        policy.Status__c = GlobalConstant.ACTIVE_LITERAL;
        update policy;
        Test.stopTest();

        System.assertEquals(1, [SELECT COUNT() FROM Error_Log__c]);
    }
}