/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer              Description                                                  *
*   ----------  ---------------------  -------------------------------------------------------------*
*   2019-05-30  Xede Consulting Group  Initial Creation                                             *
*   2019-05-31  Xede Consulting Group  Adjusted class to be without sharing.                        *
*   2019-10-01  Xede Consulting Group  Qualified OrgWideEmail query to use custom label.            *
*                                                                                                   *
*   Code Coverage: 100.0%                                                                           *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This class contains an invocable method that is called from a process builder job.
* @author Xede Consulting Group
* @see SendSelfLicensingEmailActionTest
* @date 2019-05-30 
*/
//noinspection ApexUnresolvableReference,ApexUnusedDeclaration
public without sharing class SendSelfLicensingEmailAction {

    /**
     * @description This invocable method sends and email to a lead to encourage them to license on-line.
     * @param leadIdList
     * @param templateDevName
     */
    @InvocableMethod(Label='Send Self-Licensing Email' Description='Sends an email to encourage self-licensing')
    public static void sendSelfLicensingEmail(List<Id> leadIdList){

        /*
        Setup: get email template id, organizational wide email address, and instantiate list of email messages
        */
        EmailTemplate emailTemplateId    = [
                SELECT Id, Name, DeveloperName
                  FROM EmailTemplate
                 WHERE DeveloperName =: Label.Lead_Qualification_Email_Template LIMIT 1];

        OrgWideEmailAddress orgWideEmail = [
                SELECT Id, Address, DisplayName
                  FROM OrgWideEmailAddress
                 WHERE DisplayName =: label.Licensing_OrgWide_Email
                 LIMIT 1];

        List<Messaging.SingleEmailMessage> selfLicensingEmailList = new List<Messaging.SingleEmailMessage>();

        /*
        For each lead passed in, instantiate an email message an add it to the list to be sent.
        */
        for (Id leadId: leadIdList){
            Messaging.SingleEmailMessage selfLicensingEmail = new Messaging.SingleEmailMessage();
            selfLicensingEmail.setOrgWideEmailAddressId(orgWideEmail.Id);
            selfLicensingEmail.setTargetObjectId(leadId);
            selfLicensingEmail.setTemplateId(emailTemplateId.Id);
            selfLicensingEmailList.add(selfLicensingEmail);
        }

        //Send emails to list of Leads passed-in
        try {
            Messaging.sendEmail(selfLicensingEmailList);
        }
        catch(Exception ex){
            System.debug('ERROR: An Error occurred while trying to send list of emails.');
            System.debug('ERROR: Email List> ' + selfLicensingEmailList);
            System.debug('ERROR MESSAGE: ' + ex.getMessage());
        }
    }
}