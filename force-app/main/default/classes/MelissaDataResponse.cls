/**
 * Created by johnreedy on 2019-01-24.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  --------------------------  --------------------------------------------------------*
*   2019-01-24   Xede Consulting Group 		Initial Creation                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description <description>
* @author Xede Consulting Group
* @see MelissaDataResponseTest
* @date 2019-01-24 
*/
public with sharing class MelissaDataResponse {

    public List<Records> Records;
    public String TotalRecords;
    public String TransmissionReference;
    public String TransmissionResults;
    public String Version;

    public class Records {
        public String AddressExtras;
        public String AddressKey;
        public String AddressLine1;
        public String AddressLine2;
        public String City;
        public String CompanyName;
        public String EmailAddress;
        public String MelissaAddressKey;
        public String MelissaAddressKeyBase;
        public String NameFull;
        public String PhoneNumber;
        public String PostalCode;
        public String RecordExtras;
        public String RecordID;
        public String Reserved;
        public String Results;
        public String State;
    }


    public static MelissaDataResponse parse(String json) {
        return (MelissaDataResponse) System.JSON.deserialize(json, MelissaDataResponse.class);
    }
}