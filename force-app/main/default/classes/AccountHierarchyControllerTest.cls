/*****************************************************************************************************
 *                            M O D I F I C A T I O N   L O G                                        *
 *****************************************************************************************************
 *   Date        Developer               Description                                                 *
 *   ----------  -----------             ------------------------------------------------------------*
 *   2020-01-06  Xede Consulting Group   Initial Creation                                            *
 *                                                                                                   *
 *                                                                                                   *
 *****************************************************************************************************/

/**
 * @description Test class for AccountHierarchyController
 * @author Xede (Jason Burke)
 * @date 2020-01-06
 */
@IsTest
private class AccountHierarchyControllerTest {

    /**
     * @description creates an account hierarchy with 3 children and 9 grandchildren (3 grandchildren per child)
     * then calls the AccountHierarchyController.getAccountHierarchy method on one of the grandchildren.
     * Asserts that the AccountHierarchy list that is returned mirrors the actual account hierarchy.
     */
    @isTest
    static void testBehavior() {
        Account rootAccount = TestDataHelper.createAccount('Root Account', 'Legal Entity', false);
        rootAccount.Music_Use_Type__c = 'Social Media';
        rootAccount.Status__c = 'Licensed';
        insert rootAccount;

        List<Account> childAccounts = new List<Account>();
        for (Integer i = 0; i < 3; i++) {
            Account childAccount = TestDataHelper.createAccount('Child ' + i, 'Legal Entity', false);
            childAccount.Music_Use_Type__c = 'Audio Player';
            childAccount.Status__c = 'License Not Required';
            childAccount.ParentId = rootAccount.Id;
            childAccounts.add(childAccount);
        }
        insert childAccounts;

        List<Account> grandchildAccounts = new List<Account>();
        for (Account childAccount : childAccounts) {
            for (Integer i = 0; i < 3; i++) {
                Account grandchildAccount = TestDataHelper.createAccount('Grandchild ' + i, 'Broadcast Television', false);
                grandchildAccount.Music_Use_Type__c = 'Independent';
                grandchildAccount.Status__c = 'Prospect';
                grandchildAccount.ParentId = childAccount.Id;
                grandchildAccounts.add(grandchildAccount);
            }
        }
        insert grandchildAccounts;

        Test.startTest();
        AccountHierarchyController.AccountHierarchy[] hierarchyList = AccountHierarchyController.getAccountHierarchy(grandchildAccounts[5].Id);
        System.debug('NUMBER OF SOQL QUERIES: ' + Limits.getQueries());
        Test.stopTest();

        System.debug(hierarchyList);
        System.assertEquals(1, hierarchyList.size());
        System.assertEquals('Root Account', hierarchyList[0].accountName);

        System.assertEquals(3, hierarchyList[0].children.size());
        System.assertEquals('Child 0', hierarchyList[0].children[0].accountName);
        System.assertEquals('Child 1', hierarchyList[0].children[1].accountName);
        System.assertEquals('Child 2', hierarchyList[0].children[2].accountName);

        System.assertEquals(3, hierarchyList[0].children[0].children.size());
        System.assertEquals('Grandchild 0', hierarchyList[0].children[0].children[0].accountName);
        System.assertEquals('Grandchild 1', hierarchyList[0].children[0].children[1].accountName);
        System.assertEquals('Grandchild 2', hierarchyList[0].children[0].children[2].accountName);

        System.assertEquals(3, hierarchyList[0].children[1].children.size());
        System.assertEquals('Grandchild 0', hierarchyList[0].children[1].children[0].accountName);
        System.assertEquals('Grandchild 1', hierarchyList[0].children[1].children[1].accountName);
        System.assertEquals('Grandchild 2', hierarchyList[0].children[1].children[2].accountName);

        System.assertEquals(3, hierarchyList[0].children[2].children.size());
        System.assertEquals('Grandchild 0', hierarchyList[0].children[2].children[0].accountName);
        System.assertEquals('Grandchild 1', hierarchyList[0].children[2].children[1].accountName);
        System.assertEquals('Grandchild 2', hierarchyList[0].children[2].children[2].accountName);
    }
}