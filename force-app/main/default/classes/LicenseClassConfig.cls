/**
 * Created by Xede
 */

/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer   Description                                                             *
*   ----------  ----------- ------------------------------------------------------------------------*
*   2018-10-06  Xede  		Initial Creation                                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description Converts the  the JSON metadata information info apex class
* @author Xede Consulting Group
* @see LicenseClassConfigTest for test class
* @date 2018-10-06
*/

public class LicenseClassConfig {

    public class Payment_schedule_list {
        public Object payment_amount_rule {get;set;}
        public String payment_amount_type {get;set;}
        public String payment_due_date_day_type {get;set;}
        public String payment_period_number {get;set;}

        public Payment_schedule_list(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'payment_amount_rule') {
                            //payment_amount_rule = parser.readValueAs(Object.class);
                            payment_amount_rule = parser.getText();
                        } else if (text == 'payment_amount_type') {
                            payment_amount_type = parser.getText();
                        } else if (text == 'payment_due_date_day_type') {
                            payment_due_date_day_type = parser.getText();
                        } else if (text == 'payment_period_number') {
                            payment_period_number = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Payment_schedule_list consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }

    public class Properties_Z {
        public Id type_Z {get;set;} // in json: type
        public Id period {get;set;}

        public Properties_Z(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'type') {
                            type_Z =  parser.getText();
                        } else if (text == 'period') {
                            period =  parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Properties_Z consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }

    public class Per_container_rate_schedule_parse_results {
        public List<String> line_list {get;set;}
        public String name {get;set;}
        public String schedule_string {get;set;}
        public List<List<String>> value_matrix {get;set;}
        public List<String> x_axis_rules {get;set;}
        public List<String> y_axis_rules {get;set;}

        public Per_container_rate_schedule_parse_results(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'line_list') {
                            line_list = arrayOfString(parser);
                        } else if (text == 'name') {
                            name = parser.getText();
                        } else if (text == 'schedule_string') {
                            schedule_string = parser.getText();
                        } else if (text == 'value_matrix') {
                            value_matrix = arrayOfList(parser);
                        } else if (text == 'x_axis_rules') {
                            x_axis_rules = arrayOfString(parser);
                        } else if (text == 'y_axis_rules') {
                            y_axis_rules = arrayOfString(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Per_container_rate_schedule_parse_results consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }

    public class Policy_parameter_list {
        public String container_name {get;set;}
        public String data_type {get;set;}
        public String name {get;set;}
        public String parameter_type {get;set;}

        public Policy_parameter_list(){

        }
        public Policy_parameter_list(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'container_name') {
                            container_name = parser.getText();
                        } else if (text == 'data_type') {
                            data_type = parser.getText();
                        } else if (text == 'name') {
                            name = parser.getText();
                        } else if (text == 'parameter_type') {
                            parameter_type = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Policy_parameter_list consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }

    public class Container_min_max_dict {
        public License_Policy License_Policy {get;set;}

        public Container_min_max_dict(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'License_Policy') {
                            License_Policy = new License_Policy(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Container_min_max_dict consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }

    public String name {get;set;}
    public String desc_Z {get;set;} // in json: desc
    public Boolean auditable {get;set;}
    public Object refundable {get;set;}
    public Derived_value_dict derived_value_dict {get;set;}
    public Boolean prorate_license_fee {get;set;}
    public Object proration_type {get;set;}
    public List<Derived_value_dict> derived_variable_formula_list {get;set;}
    public List<Derived_value_dict> fee_adjustment_list {get;set;}
    public List<Derived_value_dict> fee_adjustment_rule_items {get;set;}
    public String fee_adjustment_formula_text {get;set;}
    public List<Derived_value_dict> fee_adjustment_parse_results {get;set;}
    public List<Derived_value_dict> admin_fee_list {get;set;}
    public List<Derived_value_dict> admin_fee_rule_items {get;set;}
    public Object admin_fee_formula_text {get;set;}
    public List<Derived_value_dict> admin_fee_parse_results {get;set;}
    public List<Per_container_rate_schedule_item_list> per_container_rate_schedule_item_list {get;set;}
    public List<Per_container_rate_schedule_item_list> per_container_rate_schedule_rule_items {get;set;}
    public List<Derived_value_dict> per_container_rate_schedule_rule_item_values {get;set;}
    public Object per_container_formula_text {get;set;}
    public String container_rate_schedule_item_formula_text {get;set;}
    public List<Per_container_rate_schedule_parse_results> per_container_rate_schedule_parse_results {get;set;}
    public Derived_value_dict policy_parameter_lp_child_dict {get;set;}
    public List<Policy_parameter_list> policy_parameter_list {get;set;}
    public List<Derived_value_dict> policy_parameter_constant_list {get;set;}
    public List<Cpi_fee_adjustment_list> cpi_fee_adjustment_list {get;set;}
    public List<Payment_schedule_list_Z> payment_schedule_list {get;set;}
    public Boolean previous_period_license_fee_CPI_adjustment {get;set;}
    public String previous_period_license_fee_CPI_adjustment_index {get;set;}
    public String previous_period_license_fee_CPI_adjustment_adjustment_month {get;set;}
    public String previous_period_license_fee_CPI_rounding_type {get;set;}
    public String next_period_license_fee_CPI_only_if_true {get;set;}
    public Container_min_max_dict container_min_max_dict {get;set;}
    public List<String> required_instance_attributes {get;set;}
    public List<Derived_value_dict> validation_issues {get;set;}

    public LicenseClassConfig(JSONParser parser) {
        while (parser.nextToken() != System.JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                    if (text == 'name') {
                        name = parser.getText();
                    } else if (text == 'desc') {
                        desc_Z = parser.getText();
                    } else if (text == 'auditable') {
                        auditable = parser.getBooleanValue();
                    } else if (text == 'refundable') {
                        //refundable = parser.readValueAs(Object.class);
                        refundable = parser.getBooleanValue();
                    } else if (text == 'derived_value_dict') {
                        derived_value_dict = new Derived_value_dict(parser);
                    } else if (text == 'prorate_license_fee') {
                        prorate_license_fee = parser.getBooleanValue();
                    } else if (text == 'proration_type') {
                        proration_type = parser.getText();
                    } else if (text == 'derived_variable_formula_list') {
                        derived_variable_formula_list = arrayOfDerived_value_dict(parser);
                    } else if (text == 'fee_adjustment_list') {
                        fee_adjustment_list = arrayOfDerived_value_dict(parser);
                    } else if (text == 'fee_adjustment_rule_items') {
                        fee_adjustment_rule_items = arrayOfDerived_value_dict(parser);
                    } else if (text == 'fee_adjustment_formula_text') {
                        fee_adjustment_formula_text = parser.getText();
                    } else if (text == 'fee_adjustment_parse_results') {
                        fee_adjustment_parse_results = arrayOfDerived_value_dict(parser);
                    } else if (text == 'admin_fee_list') {
                        admin_fee_list = arrayOfDerived_value_dict(parser);
                    } else if (text == 'admin_fee_rule_items') {
                        admin_fee_rule_items = arrayOfDerived_value_dict(parser);
                    } else if (text == 'admin_fee_formula_text') {
                        admin_fee_formula_text = parser.readValueAs(Object.class);
                    } else if (text == 'admin_fee_parse_results') {
                        admin_fee_parse_results = arrayOfDerived_value_dict(parser);
                    } else if (text == 'per_container_rate_schedule_item_list') {
                        per_container_rate_schedule_item_list = arrayOfPer_container_rate_schedule_item_list(parser);
                    } else if (text == 'per_container_rate_schedule_rule_items') {
                        per_container_rate_schedule_rule_items = arrayOfPer_container_rate_schedule_item_list(parser);
                    } else if (text == 'per_container_rate_schedule_rule_item_values') {
                        per_container_rate_schedule_rule_item_values = arrayOfDerived_value_dict(parser);
                    } else if (text == 'per_container_formula_text') {
                        per_container_formula_text = parser.readValueAs(Object.class);
                    } else if (text == 'container_rate_schedule_item_formula_text') {
                        container_rate_schedule_item_formula_text = parser.getText();
                    } else if (text == 'per_container_rate_schedule_parse_results') {
                        per_container_rate_schedule_parse_results = arrayOfPer_container_rate_schedule_parse_results(parser);
                    } else if (text == 'policy_parameter_lp_child_dict') {
                        policy_parameter_lp_child_dict = new Derived_value_dict(parser);
                    } else if (text == 'policy_parameter_list') {
                        policy_parameter_list = arrayOfPolicy_parameter_list(parser);
                    } else if (text == 'policy_parameter_constant_list') {
                        policy_parameter_constant_list = arrayOfDerived_value_dict(parser);
                    } else if (text == 'cpi_fee_adjustment_list') {
                        cpi_fee_adjustment_list = arrayOfCPI_fee_adjustment_list(parser);
                    }
                    else if (text == 'payment_schedule_list') {
                        payment_schedule_list = arrayOfPayment_schedule_list_Z(parser);
                    } else if (text == 'previous_period_license_fee_CPI_adjustment') {
                        previous_period_license_fee_CPI_adjustment = parser.getBooleanValue();
                    } else if (text == 'previous_period_license_fee_CPI_adjustment_index') {
                        previous_period_license_fee_CPI_adjustment_index = parser.getText();
                    } else if (text == 'previous_period_license_fee_CPI_adjustment_adjustment_month') {
                        previous_period_license_fee_CPI_adjustment_adjustment_month = parser.getText();
                    } else if (text == 'previous_period_license_fee_CPI_rounding_type') {
                        previous_period_license_fee_CPI_rounding_type = parser.getText();
                    } else if (text == 'next_period_license_fee_CPI_only_if_true') {
                        next_period_license_fee_CPI_only_if_true = parser.getText();
                    } else if (text == 'container_min_max_dict') {
                        //Commenting because issue with Restaurants_Bars_Nightclubs_and_Similiar_Establishments_License_Agreement
                        //container_min_max_dict = new Container_min_max_dict(parser);

                    } else if (text == 'required_instance_attributes') {
                        required_instance_attributes = arrayOfString(parser);
                    } else if (text == 'validation_issues') {
                        validation_issues = arrayOfDerived_value_dict(parser);
                    } else {
                        System.debug(LoggingLevel.WARN, 'LicenseClassConfig consuming unrecognized property: '+text);
                        consumeObject(parser);
                    }
                }
            }
        }
    }

    public class Derived_value_dict {

        public Derived_value_dict(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        {
                            System.debug(LoggingLevel.WARN, 'Derived_value_dict consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    public class Cpi_fee_adjustment_list {
        public String adjustment_month {get;set;}
        public String context {get;set;}
        public Boolean enabled {get;set;}
        public String index {get;set;}
        public String only_if_true {get;set;}
        public String rounding_type {get;set;}
        public Cpi_fee_adjustment_list(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        {
                            System.debug(LoggingLevel.WARN, 'CPI unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }

    }
    public class Properties {
        public Id Id {get;set;}
        public Id Name {get;set;}
        public Id License_Type {get;set;}
        public Id Location_Count {get;set;}

        public Properties(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'Id') {
                            Id =  parser.getText();
                        } else if (text == 'Name') {
                            Name =  parser.getText();
                        } else if (text == 'License_Type') {
                            License_Type =  parser.getText();
                        } else if (text == 'Location_Count') {
                            Location_Count =  parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Properties consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }

    public class Payment_schedule_list_Z {
        public String payment_frequency {get;set;}
        public List<Payment_schedule_list> payment_schedule_list {get;set;}
        public String payment_schedule_name {get;set;}
        public List<Payment_schedule_rate_schedule_list> payment_schedule_rate_schedule_list {get;set;}

        public Payment_schedule_list_Z(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'payment_frequency') {
                            payment_frequency = parser.getText();
                        } else if (text == 'payment_schedule_list') {
                            payment_schedule_list = arrayOfPayment_schedule_list(parser);
                        } else if (text == 'payment_schedule_name') {
                            payment_schedule_name = parser.getText();
                        } else if (text == 'payment_schedule_rate_schedule_list') {
                            payment_schedule_rate_schedule_list = arrayOfPayment_schedule_rate_schedule_list(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Payment_schedule_list_Z consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }

    public class License_Policy {
        public String container_name {get;set;}
        public Boolean max_dv {get;set;}
        public Boolean max_fee_is_numeric {get;set;}
        public String max_only_if_true {get;set;}
        public Object maximum_annual_fee_amount {get;set;}
        public Object maximum_fee_amount {get;set;}
        public Boolean min_dv {get;set;}
        public Boolean min_fee_is_numeric {get;set;}
        public String min_only_if_true {get;set;}
        public Double minimum_annual_fee_amount {get;set;}
        public Double minimum_fee_amount {get;set;}

        public License_Policy(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'container_name') {
                            container_name = parser.getText();
                        } else if (text == 'max_dv') {
                            max_dv = parser.getBooleanValue();
                        } else if (text == 'max_fee_is_numeric') {
                            max_fee_is_numeric = parser.getBooleanValue();
                        } else if (text == 'max_only_if_true') {
                            max_only_if_true = parser.getText();
                        } else if (text == 'maximum_annual_fee_amount') {
                            maximum_annual_fee_amount = parser.readValueAs(Object.class);
                        } else if (text == 'maximum_fee_amount') {
                            maximum_fee_amount = parser.readValueAs(Object.class);
                        } else if (text == 'min_dv') {
                            min_dv = parser.getBooleanValue();
                        } else if (text == 'min_fee_is_numeric') {
                            min_fee_is_numeric = parser.getBooleanValue();
                        } else if (text == 'min_only_if_true') {
                            min_only_if_true = parser.getText();
                        } else if (text == 'minimum_annual_fee_amount') {
                            minimum_annual_fee_amount = parser.getDoubleValue();
                        } else if (text == 'minimum_fee_amount') {
                            minimum_fee_amount = parser.getDoubleValue();
                        } else {
                            System.debug(LoggingLevel.WARN, 'License_Policy consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }

    public class Per_container_rate_schedule_item_list {
        public String container_name {get;set;}
        public Boolean include_in_proration {get;set;}
        public Boolean included_in_CPI {get;set;}
        public String name {get;set;}
        public String only_if_true {get;set;}
        public Boolean only_if_true_boolean {get;set;}
        public String rule_string {get;set;}
        public String schedule_string {get;set;}

        public Per_container_rate_schedule_item_list(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'container_name') {
                            container_name = parser.getText();
                        } else if (text == 'include_in_proration') {
                            include_in_proration = parser.getBooleanValue();
                        } else if (text == 'included_in_CPI') {
                            included_in_CPI = parser.getBooleanValue();
                        } else if (text == 'name') {
                            name = parser.getText();
                        } else if (text == 'only_if_true') {
                            only_if_true = parser.getText();
                        } else if (text == 'only_if_true_boolean') {
                            only_if_true_boolean = parser.getBooleanValue();
                        } else if (text == 'rule_string') {
                            rule_string = parser.getText();
                        } else if (text == 'schedule_string') {
                            schedule_string = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Per_container_rate_schedule_item_list consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }


    public class Id {
        public String type_Z {get;set;} // in json: type

        public Id(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'type') {
                            type_Z = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Id consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }

    public class Payment_schedule_rate_schedule_list {
        public String schedule_string {get;set;}
        public String type_Z {get;set;} // in json: type

        public Payment_schedule_rate_schedule_list(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'schedule_string') {
                            schedule_string = parser.getText();
                        } else if (text == 'type') {
                            type_Z = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Payment_schedule_rate_schedule_list consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }


    public static LicenseClassConfig parse(String json) {
        System.JSONParser parser = System.JSON.createParser(json);
        return new LicenseClassConfig(parser);
    }

    public static void consumeObject(System.JSONParser parser) {
        Integer depth = 0;
        do {
            System.JSONToken curr = parser.getCurrentToken();
            if (curr == System.JSONToken.START_OBJECT ||
                    curr == System.JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == System.JSONToken.END_OBJECT ||
                    curr == System.JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }



    private static List<List<String>> arrayOfList(System.JSONParser p) {
        List<List<String>> res = new List<List<String>>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(arrayOfString(p));
        }
        return res;
    }




    private static List<Payment_schedule_rate_schedule_list> arrayOfPayment_schedule_rate_schedule_list(System.JSONParser p) {
        List<Payment_schedule_rate_schedule_list> res = new List<Payment_schedule_rate_schedule_list>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Payment_schedule_rate_schedule_list(p));
        }
        return res;
    }





    private static List<Policy_parameter_list> arrayOfPolicy_parameter_list(System.JSONParser p) {
        List<Policy_parameter_list> res = new List<Policy_parameter_list>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Policy_parameter_list(p));
        }
        return res;
    }

    private static List<Cpi_fee_adjustment_list> arrayOfCPI_fee_adjustment_list(System.JSONParser p) {
        List<Cpi_fee_adjustment_list> res = new List<Cpi_fee_adjustment_list>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Cpi_fee_adjustment_list(p));
        }
        return res;
    }

    private static List<Per_container_rate_schedule_parse_results> arrayOfPer_container_rate_schedule_parse_results(System.JSONParser p) {
        List<Per_container_rate_schedule_parse_results> res = new List<Per_container_rate_schedule_parse_results>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Per_container_rate_schedule_parse_results(p));
        }
        return res;
    }


    private static List<Payment_schedule_list> arrayOfPayment_schedule_list(System.JSONParser p) {
        List<Payment_schedule_list> res = new List<Payment_schedule_list>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Payment_schedule_list(p));
        }
        return res;
    }



    private static List<String> arrayOfString(System.JSONParser p) {
        List<String> res = new List<String>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(p.getText());
        }
        return res;
    }


    private static List<Derived_value_dict> arrayOfDerived_value_dict(System.JSONParser p) {
        List<Derived_value_dict> res = new List<Derived_value_dict>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Derived_value_dict(p));
        }
        return res;
    }

    private static List<Payment_schedule_list_Z> arrayOfPayment_schedule_list_Z(System.JSONParser p) {
        List<Payment_schedule_list_Z> res = new List<Payment_schedule_list_Z>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Payment_schedule_list_Z(p));
        }
        return res;
    }


    private static List<Per_container_rate_schedule_item_list> arrayOfPer_container_rate_schedule_item_list(System.JSONParser p) {
        List<Per_container_rate_schedule_item_list> res = new List<Per_container_rate_schedule_item_list>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Per_container_rate_schedule_item_list(p));
        }
        return res;
    }

}