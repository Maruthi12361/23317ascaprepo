/**
 * Created by Xede
 */

/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer   Description                                                             *
*   ----------  ----------- ------------------------------------------------------------------------*
*   2018-10-28  Xede  		Initial Creation                                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description Test class for QueryUtility apex class
* @author Xede Consulting Group
* @date 2018-10-28
*/

@isTest class QueryUtilityTest {
    @testSetup static void testSetup() {
        LicenseClassTestDataHelper.setupTestData();
    }

    @isTest static void testQueryUtility() {

        LicenseClassTestDataHelper.loadData();
        System.assert(LicenseClassTestDataHelper.account != null, 'Test account is created');
        System.assert(LicenseClassTestDataHelper.contact != null, 'Test contact is created');
        System.assert(LicenseClassTestDataHelper.opportunity != null, 'Test opportunity is created');
        System.assert(LicenseClassTestDataHelper.licenseClass != null, 'Test license class is created');
        System.assert(LicenseClassTestDataHelper.licensePolicy != null, 'Test license policy is created');

    }
}