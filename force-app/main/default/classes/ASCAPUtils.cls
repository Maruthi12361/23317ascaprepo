/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer   Description                                                             *
*   ----------  ----------- ------------------------------------------------------------------------*
*   2018-11-27   Xede  		Initial Creation                                                        *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This utility class contains common methods used to facilitate common operations.
* @author Xede Consulting Group
* @see ASCAPUtilsTest
* @date 2018-11-27
*/
public with sharing class ASCAPUtils {

    /**
    * @description This method is used to get the trigger/class name from within a trigger/class itself.
    * @return Trigger or Class Name
    */
    public static String getExecutableName() {
        //force a division by zero error
        try {
            Integer i = 10 / 0;
        }
        catch (Exception e) {
            /*
            Iterate over list of lines in stack trace and determine the apex type (Trigger, Class) and return the
            appropriate name.
            */
            String[] lines = e.getStackTraceString().split('\n');
            for (Integer i = lines.size()-1; i >= 0; i--) {
                system.debug('Debug: lines[i]>' + lines[i]);
                //if the source is AnonymousBlock continue to next record
                if (lines[i].startsWith('AnonymousBlock:')){
                    continue;
                }
                //if the line starts with 'Trigger' then return the trigger name
                if (lines[i].startsWith('Trigger.')) {
                    return lines[i].subStringBetween('Trigger.', ':');
                }
                //if the line starts with 'Class.', the return both the class name and the method name
                else if (lines[i].startsWith('Class.')){
                    string className  = lines[i].substringAfter('.').substringBeforeLast('.');
                    string methodName = lines[i].substringBefore(':').substringAfterLast('.');
                    return className + '-' + methodName;
                }
                else{
                    system.debug('Info: no class or trigger key words found.');
                }
            }
        }
        return null;
    }

    /**
    * @description This method is used to verify the current status (enabled/disabled) of a given trigger.
    * @param triggerName The triggers whose status needs to be evaluated
    * @return triggerDisabled boolean vale depending on the current Disabled__c value
    */
    public static boolean triggerDisabled(string triggerName) {
        //initialize the return value and get the custom setting for the given trigger if it exists.
        boolean triggerDisabled = false;
        DisableTrigger__c triggerProperty = DisableTrigger__c.getValues(triggerName);

        //if there is a trigger entry in the Disable Trigger Custom Setting, and it is disabled, update the return value
        //to true.
        if (triggerProperty != null && triggerProperty.Disabled__c) {
            triggerDisabled = true;
        }
        return triggerDisabled;
    }

    public static LicenseClassField__mdt getGetLicenseClassMDTByName(String licenseClassName){

        String retQuery=QueryUtility.getObjectFieldsQuery('LicenseClassField__mdt') +' From LicenseClassField__mdt where LicenseClassName__c=:licenseClassName  limit 1' ;
        System.debug('\n licPolicyQuery=='+retQuery);
        List<LicenseClassField__mdt> mdtList = (List<LicenseClassField__mdt>)Database.Query(retQuery);



        //List<LicenseClass__mdt> licClasses = [SELECT MasterLabel,Label,Station_Code__c,Period_End_Yr_Month_Txt__c,Station_Code_Allowed__c,QualifiedApiName,LicenseClassName__c,Predominant_Programming_Type__c FROM LicenseClass__mdt where LicenseClassName__c=:licenseClassName limit 1];
        LicenseClassField__mdt retInstance = mdtList != null && !mdtList.isEmpty() ? mdtList[0] : null;
        return retInstance;
    }
    public static List<String> getLicenseClassFieldValues(String licenseClassName, String fieldName){
        List<String> retValsFinal;

        LicenseClassField__mdt licClass = getGetLicenseClassMDTByName(licenseClassName);
        if(licClass != null){
            String fieldValue;
            if(licClass.Field1__c != null && licClass.Field1__c.equalsIgnoreCase(fieldName)){
                fieldValue = licClass.Field1Value__c;
            }else if(licClass.Field2__c != null && licClass.Field2__c.equalsIgnoreCase(fieldName)){
                fieldValue = licClass.Field2Value__c;
            }else if(licClass.Field3__c != null && licClass.Field3__c.equalsIgnoreCase(fieldName))
            {
                fieldValue = licClass.Field3Value__c;
            }else if(licClass.Field4__c != null && licClass.Field4__c.equalsIgnoreCase(fieldName))
            {
                fieldValue = licClass.Field4Value__c;
            }
            retValsFinal = fieldValue != null ? fieldValue.split(',') : null;

        }
        return retValsFinal;
    }

    /**
     * @description This method parses a string of picklist values and converts to list of strings.
     * @param possibleValues
     * @return List of Picklist values
    */
    public static List<String> getLicenseClassPicklistValue(String pickListValueString){
        List<String> returnList = new List<String>();
        List<String> pickListValueList = pickListValueString != null ? pickListValueString.split(',') : null;

        if(pickListValueString != null){
            //Remove leading and trailing whitespace
            for (String each: pickListValueList){
                returnList.add(each.trim().normalizeSpace());
            }
        }
        return returnList;
    }
    /**
    * @description This method uses a string to generate encrypted string based n AES256 encoding
    * @param string value
    * @return string value
    */
    @AuraEnabled
    public static String getEncryptedValue(String inToEncypt) {
        Blob data = EncodingUtil.base64Decode(inToEncypt);
        Blob keyBlob = EncodingUtil.base64Decode(Label.AES_Encryption_Key);
        Blob encryptedData = Crypto.encryptWithManagedIV('AES256', keyBlob, data);
        String encodedString = EncodingUtil.urlEncode(EncodingUtil.base64Encode(encryptedData), 'UTF-8');
        return encodedString;
    }
    /**
     * @description This method uses a string to generate encrypted string based n AES256 encoding
     * @param string value
     * @return string value
    */
    @AuraEnabled
    public static String getCleanedEncryptedValue(String inToEncypt) {

        String encodedString = getEncryptedValue(inToEncypt);

        return encodedString;
    }
    @AuraEnabled
    public static String getDecryptedId(String encryptedId){
        String retString = getResetEncryptedValue(encryptedId);
        String testEncryptID =EncodingUtil.urlDecode(encryptedId,'UTF-8');

        if(encryptedId != null && encryptedId.length() > 18){
            try{
                Blob data = Test.isRunningTest() ? Blob.valueOf(testEncryptID) : EncodingUtil.base64Decode(encryptedId);
                Blob keyBlob = EncodingUtil.base64Decode(Label.AES_Encryption_Key);
                Blob decryptedData = Crypto.decryptWithManagedIV('AES256', keyBlob, data);
                retString = decryptedData.toString();

            }catch(Exception ex){
            }
        }
        return retString;
    }
    @AuraEnabled
    public static String getDecrypted18Id(String encryptedId){
        String retString = getResetEncrypted18Value(encryptedId);


        if(encryptedId != null && encryptedId.length() > 18){
            try{
                Blob data = EncodingUtil.base64Decode(retString);
                Blob keyBlob = EncodingUtil.base64Decode(Label.AES_Encryption_Key);
                Blob decryptedData = Crypto.decryptWithManagedIV('AES256', keyBlob, data);
                retString = EncodingUtil.base64Encode(decryptedData);
                retString = retString.replaceAll('==','');
                system.debug('\n Debug: variable>' + retString);

            }catch(Exception ex){
                //retString=encryptedId;
                system.debug('\n Debug: Exception>' + ex);
            }
        }
        return retString;
    }
    /**
     * @description This method uses a string to generate encrypted string based on AES256 encoding
     * @param string value
     * @return string value
    */
    @AuraEnabled
    public static String getResetEncryptedValue(String inToEncypt) {

        String resetString = EncodingUtil.urlEncode(inToEncypt,'UTF-8');
        return resetString;
    }
    /**
     * @description This method uses a string to generate encrypted string based on AES256 encoding
     * @param string value
     * @return string value
    */
    @AuraEnabled
    public static String getResetEncrypted18Value(String inToEncypt) {

        String resetString=inToEncypt;
        resetString =EncodingUtil.urlDecode(inToEncypt,'UTF-8');

        return resetString;
    }
    /**
     * @description This method accepts a list of sObjectIds, determines the object type and returns a list of one to
     * the calling process.
     * @param recordIdList list of sObject record ids
     * @return objectTypeList list of sObjectType names
    */
    @InvocableMethod
    public static List<String> getObjectTypeFromId(list<String> recordIdList){
        List<String> objectTypeList = new List<String>();

        //iterate over list of record ids, determine the sObject type and add to list to be returned.
        for (string eachId: recordIdList) {
            String sobjectType = Id.valueOf(eachId).getSobjectType().getDescribe().getName();
            objectTypeList.add(sobjectType);
        }

        return objectTypeList;
    }

    public static String getAllFieldNamesSelectString(String sObjectName) {
        Schema.DescribeSObjectResult[] results = Schema.describeSObjects(new String[] { sObjectName });
        List<String> fieldNames = new List<String>(results[0].fields.getMap().keySet());
        fieldNames.sort();
        return String.join(fieldNames, ', ');
    }
    
}