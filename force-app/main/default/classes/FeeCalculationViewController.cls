/**
 * Created by johnreedy on 2019-05-16.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer         Description                                                       *
*   ----------  ----------------  ----------------------------------------------------------------- *
*   2019-05-16  Xede Consulting     Initial Creation                                                *
*   2019-10-16  Xede Consulting     Added condition to check for null inputJSON and replace with    *
*                                   empty string if found.                                          *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This controller support the FeeCalculationViewForm, which renders a read-only version of the
* Parameters and Rate Fee Amounts.
* @author Xede Consulting Group
* @see FeeCalculationViewControllerTest
* @date 2019-05-16 
*/
public with sharing class FeeCalculationViewController {
    @AuraEnabled
    public static License_Policy__c getLicensePolicy(String recordId) {
        String licPolicyQuery = QueryUtility.getObjectFieldsQuery(GlobalConstant.OBJ_LICENSE_POLICY) + ' From License_Policy__c where Id=:recordId  ' ;
        List<License_Policy__c> licensePolicyList = (List<License_Policy__c>) Database.query(licPolicyQuery);
        License_Policy__c licensePolicy = licensePolicyList != null && !licensePolicyList.isEmpty() ? licensePolicyList[0] : null;
        return licensePolicy;
    }
    /**
     * @description This method builds the fieldMetadataWrapper and returns to the view component.
     * @param recordId
     * @return formWrapper
     */
    @AuraEnabled
    public static String getFeeAndInputFromObject(String recordId){
        FieldMetadataWrapper formWrapper = getFieldMetadataWrapper(recordId);
        return JSON.serialize(formWrapper, true);
    }
    /**
     * @description This method builds the fieldMetadataWrapper and returns to the view component.
     * @param recordId
     * @return formWrapper
     */
    @AuraEnabled
    public static FieldMetadataWrapper getFieldMetadataWrapper(String recordId){

        String sobjectType = Id.valueOf(recordId).getSobjectType().getDescribe().getName();

        //get license class name
        String inputJSON;
        String licenseClassName;
        String licenseClassId;
        Date coverageStartDate;
        Date coverageEndDate;
        Boolean isConvertedLead = false;
        Boolean isClosedOpportunity = false;
        Boolean hasRateScheduleFees = false;
        boolean authorizedUser = True;
        Decimal licenseFeeAmount = 0.0;
        Decimal prorateLicenseFeeAmount = 0.0;

        //if lead or opportunity set fields values appropriately so they can be rendered on the page, and or used to get other values.
        if(sobjectType == GlobalConstant.OBJ_LEAD){
            Lead currLead = [
                    SELECT Id, License_Class_Relationship__c, License_Class_Relationship__r.Name, Estimated_Value__c
                         , Fee_Calculation_Request__c, isConverted, Fee_Calculation_Response__c, License_Coverage_Start_Date__c
                         , License_Coverage_End_Date__c
                      FROM Lead
                     WHERE Id =: recordId
                     LIMIT 1];

            licenseClassName        = currLead.License_Class_Relationship__r.Name;
            licenseClassId          = currLead.License_Class_Relationship__c;
            inputJSON               = string.isNotBlank(currLead.Fee_Calculation_Request__c)? currLead.Fee_Calculation_Request__c :label.Empty_JSON_Request_String;
            licenseFeeAmount        = currLead.Estimated_Value__c;
            prorateLicenseFeeAmount = 0.0;
            isConvertedLead         = currLead.isConverted;
            coverageStartDate       = currLead.License_Coverage_Start_Date__c;
            coverageEndDate         = currLead.License_Coverage_End_Date__c;

            if (string.isNotBlank(currLead.Fee_Calculation_Response__c)){
                if (currLead.Fee_Calculation_Response__c.contains('"fee_detail_dict": {}')) {
                    hasRateScheduleFees = false;

                } else {
                    hasRateScheduleFees = true;
                }
            }


        }else if (sobjectType == GlobalConstant.OBJ_OPPORTUNITY){
            authorizedUser = False;
            Opportunity currOpportunity = [
                    SELECT Id, License_Class_Relationship__c, License_Class_Relationship__r.Name, Amount, Fee_Calculation_Request__c
                         , Fee_Calculation_Response__c, ForecastCategory, ownerId, License_Coverage_Start_Date__c
                            , License_Coverage_End_Date__c
                      FROM Opportunity
                     WHERE Id =: recordId
                     LIMIT 1];

            licenseClassName        = currOpportunity.License_Class_Relationship__r.Name;
            licenseClassId          = currOpportunity.License_Class_Relationship__c;
            inputJSON               = string.isNotBlank(currOpportunity.Fee_Calculation_Request__c)? currOpportunity.Fee_Calculation_Request__c: label.Empty_JSON_Request_String;
            licenseFeeAmount        = currOpportunity.Amount;
            prorateLicenseFeeAmount = 0.0;
            coverageStartDate       = currOpportunity.License_Coverage_Start_Date__c;
            coverageEndDate         = currOpportunity.License_Coverage_End_Date__c;

            isClosedOpportunity = currOpportunity.ForecastCategory == GlobalConstant.FORECAST_CLOSED_CATEGORY || currOpportunity.ForecastCategory == GlobalConstant.FORECAST_OMITTED_CATEGORY?True:False;
            isClosedOpportunity = currOpportunity.ForecastCategory == GlobalConstant.FORECAST_CLOSED_CATEGORY || currOpportunity.ForecastCategory == GlobalConstant.FORECAST_OMITTED_CATEGORY?True:False;

            set<string> authorizedUserList = FeeCalculationFormHelper.getAuthorizedUserList(currOpportunity.OwnerId, currOpportunity.id);
            string runningUserId = UserInfo.getUserId();
            if (authorizedUserList != null && authorizedUserList.contains(runningUserId)){
                authorizedUser = True;
            }

            if (string.isNotBlank(currOpportunity.Fee_Calculation_Response__c)){
                if (currOpportunity.Fee_Calculation_Response__c.contains('"fee_detail_dict": {}')) {
                    hasRateScheduleFees = false;

                } else {
                    hasRateScheduleFees = true;
                }
            }
        }
        else if (sobjectType == GlobalConstant.OBJ_LICENSE_POLICY){

            License_Policy__c currPolicy = [
                    SELECT Id, License_Class_Type__c, License_Class_Type__r.Name
                         , Current_Transaction_Request__c, Current_Transaction_Response__c
                      FROM License_Policy__c
                     WHERE Id =: recordId
                     LIMIT 1];

            List<License_Policy_Time_Period__c> currTimePeriod =[
                    SELECT Id, License_Policy_Fee_Amount__c, Fee_Calculation_Request_String__c, Fee_Calculation_Response_String__c
                         , Start_Date__c, End_Date__c
                      FROM License_Policy_Time_Period__c
                     WHERE License_Policy__c =: currPolicy.Id];

            //Should only every be one time period record (to avoid List has no rows: https://help.salesforce.com/articleView?id=000328824&type=1&mode=1)
            if (currTimePeriod.size()>0){
                inputJSON         = currTimePeriod[0].Fee_Calculation_Request_String__c;
                licenseFeeAmount  = currTimePeriod[0].License_Policy_Fee_Amount__c !=null? currTimePeriod[0].License_Policy_Fee_Amount__c:0.00;
                coverageStartDate = currTimePeriod[0].Start_Date__c;
                coverageEndDate   = currTimePeriod[0].End_Date__c;

                if (string.isNotBlank(currTimePeriod[0].Fee_Calculation_Response_String__c)){
                    if (currTimePeriod[0].Fee_Calculation_Response_String__c.contains('"fee_detail_dict": {}')) {
                        hasRateScheduleFees = false;

                    } else {
                        hasRateScheduleFees = true;
                    }
                }
            }

            licenseClassName = currPolicy.License_Class_Type__r.Name;
            licenseClassId   = currPolicy.License_Class_Type__c;

            prorateLicenseFeeAmount = 0.0;


        }
        Map<String,License_Class_Field_Info__c> licenseClassFieldMap = new Map<String,License_Class_Field_Info__c>();
        for(License_Class_Field_Info__c licenseClassField : [SELECT Id,Name,DSL_Parameter_Name__c, Help_Text__c, License_Class__c
                                                                  , Self_Service_Label__c, Internal_Field_Label__c, Order__c, Value__c
                                                               FROM License_Class_Field_Info__c
                                                              WHERE License_Class__c=:licenseClassId
                                                                 OR License_Class__r.Name=:licenseClassName]){
            //add fields to map
            licenseClassFieldMap.put(licenseClassField.DSL_Parameter_Name__c,licenseClassField);
        }

        //build Field Metadata Wrapper
        FieldMetadataWrapper formWrapper = new FieldMetadataWrapper(inputJSON,licenseClassFieldMap, isConvertedLead, isClosedOpportunity, authorizedUser);
        formWrapper.licenseClassName        = licenseClassName;
        formWrapper.licenseFeeAmount        = licenseFeeAmount;
        formWrapper.coverageStartDate       = coverageStartDate;
        formWrapper.coverageEndDate         = coverageEndDate;
        formWrapper.isConvertedLead         = isConvertedLead;
        formWrapper.isClosedOpportunity     = isClosedOpportunity;
        formWrapper.hasRateScheduleFees     = hasRateScheduleFees;
        formWrapper.prorateLicenseFeeAmount = prorateLicenseFeeAmount;
        formWrapper.authorizedUser = sobjectType == GlobalConstant.OBJ_OPPORTUNITY?authorizedUser:true;
        return formWrapper;
    }

    //noinspection ApexUnusedDeclaration -- this class may be used for container lists
    public class FieldMetadataWrapperList {
        public List<FieldMetadataWrapper> license_policy_list {get;set;}
        public String error_message {get;set;}
        public String licenseClassId{get;set;}
        public String licenseClassName { get; set; }
        public String coverageStartDate { get; set; }
        public String coverageEndDate { get; set; }
        public String containerListName { get; set; }
        public List<String> license_policy_column_names{get;set;}

        public FieldMetadataWrapperList(FieldMetadataWrapper wrapper){
            if(license_policy_list == null){
                license_policy_list = new List<FieldMetadataWrapper>();
            }
            license_policy_list.add(wrapper);
        }
        public FieldMetadataWrapperList(){

        }
    }

    /**
    * @description This wrapper class defines the object used to render the pages for the Fee Calculation App.
    */
    public class FieldMetadataWrapper {
        public List<FieldMetadata> column_license_policy {get;set;}
        public List<List<FieldMetadata>> column_container {get;set;}
        public List<FieldMetadata> column_timeperiod {get;set;}
        public List<String> column_names{get;set;}
        public String error_message {get;set;}
        public String licenseClassName { get; set; }
        public Date coverageStartDate { get; set; }
        public Date coverageEndDate { get; set; }
        public String containerListName { get; set; }
        public Double licenseFeeAmount { get; set; }
        public Boolean isConvertedLead { get;set; }
        public Boolean isClosedOpportunity { get;set; }
        public Boolean authorizedUser { get;set; }
        public Boolean readOnly { get;set; }
        public Boolean hasRateScheduleFees { get;set; }
        public Double prorateLicenseFeeAmount { get; set; }

        public FieldMetadataWrapper(String inputJSON,Map<String,License_Class_Field_Info__c> licenseClassFieldMap, Boolean isConvertedLead, Boolean isClosedOpportunity, Boolean authorizedUser ){

            try {
                column_license_policy = new List<FieldMetadata>();
                column_container = new List<List<FieldMetadata>>();
                column_timeperiod = new List<FieldMetadata>();
                column_names = new List<String>();
                readOnly = False;

                isConvertedLead = isConvertedLead;
                isClosedOpportunity = isClosedOpportunity;
                authorizedUser = authorizedUser;

                if (isConvertedLead || isClosedOpportunity || authorizedUser == False){
                    readOnly = True;
                }


                Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(inputJSON);

                Map<String,Object> licensePolicyObj = (Map<String,Object>)m.get('License_Policy');
                Map<String,Object> timePeriodObj = (Map<String,Object>)m.get('Time_Period');
                Boolean firstRow = true;

                for(String licensePolicyFieldKey: licensePolicyObj.keySet()){

                    Object  valueObject = licensePolicyObj.get(licensePolicyFieldKey);
                    Boolean listInstance = (valueObject instanceof  List<Object>);
                    FieldMetadata field = null;
                    if(!listInstance){
                        //TODO: remove hard coded set below and move into custom label to be maintained.
                        Set<String> DoNotDisplaySet = new Set<String>{'Id', 'License_Type', 'Name'};
                        //if field should not be displayed skip altogether (creates and issue with field alignment on page if there are an odd number of fields in set
                        if (DoNotDisplaySet.contains(licensePolicyFieldKey)){
                            continue;
                        }
                        field = createFieldMetadataFromJSON(valueObject,licensePolicyFieldKey,'License_Policy',null,licenseClassFieldMap);
                        column_license_policy.add(field);
                    }else if(listInstance){
                        String listName = licensePolicyFieldKey.replaceFirst('_List','');
                        containerListName = listName;
                        List<Object> containerList = (List<Object>) valueObject;
                        for(Object containerObject : containerList){

                            Map<String,Object> containerFieldMap = (Map<String,Object>)containerObject;

                            List<FieldMetadata> rowFieldList = new List<FieldMetadata>();
                            for(String containerFieldKey: containerFieldMap.keySet()) {
                                Object  containerValueObject = containerFieldMap.get(containerFieldKey);
                                field = createFieldMetadataFromJSON(containerValueObject,containerFieldKey,licensePolicyFieldKey,listName,licenseClassFieldMap);
                                rowFieldList.add(field);
                                if(firstRow){
                                    column_names.add(field.field_label);
                                }
                            }
                            rowFieldList.sort();
                            if(firstRow){
                                firstRow = false;
                            }
                            column_container.add(rowFieldList);
                        }
                    }
                    column_license_policy.sort();
                }
                for(String timePeriodFieldKey: timePeriodObj.keySet()){

                    Object  valueObject = timePeriodObj.get(timePeriodFieldKey);
                    Boolean listInstance = (valueObject instanceof  List<Object>);
                    FieldMetadata field = null;
                    if(!listInstance){
                        field = createFieldMetadataFromJSON(valueObject,timePeriodFieldKey,'Time_Period',null,licenseClassFieldMap);
                        column_timeperiod.add(field);
                    }
                }
            } catch (Exception e){
                error_message = e.getMessage();
                reinitializeAfterException(column_container,column_license_policy,column_timeperiod);
            }
        }
    }
    public static void reinitializeAfterException(List<List<FieldMetadata>> column_container,List<FieldMetadata>column_license_policy,List<FieldMetadata> column_timeperiod){
        column_container = new List<List<FieldMetadata>>();
        column_license_policy = new List<FieldMetadata>();
        column_timeperiod = new List<FieldMetadata>();
    }
    /**
    * @description This methods creates a FieldMetadata record
    * @param valueObject
    * @param fieldKey
    * @param containerName
    * @param listName
    * @param licenseClassFieldMap
    * @return FieldMetadata
    */
    public static FieldMetadata createFieldMetadataFromJSON(Object valueObject, String fieldKey, String containerName, String listName,Map<String,License_Class_Field_Info__c> licenseClassFieldMap){
        FieldMetadata field = new FieldMetadata();
        field.list_name = listName;
        field.field_api_name = fieldKey;
        field.field_label =  fieldKey.replaceAll('_',' ');
        field.displayable_field = true;
        Set<String> DoNotDisplaySet = new Set<String>{'Id', 'License_Type', 'Name'};

        if (DoNotDisplaySet.contains(fieldKey)){
            field.displayable_field = false;
        }

        //if a license class field info record exists, the use its label, help text and order fields
        if(licenseClassFieldMap != null && licenseClassFieldMap.containsKey(field.field_api_name)){
            field.field_label = licenseClassFieldMap.get(field.field_api_name).Internal_Field_Label__c;
            field.field_help_text = licenseClassFieldMap.get(field.field_api_name).Help_Text__c;
            Decimal orderValue = licenseClassFieldMap.get(field.field_api_name).Order__c ;
            if(orderValue != null){
                field.field_order = orderValue.intValue();
            }
        }

        //set the field value (picklist values), and datatype/field type
        field.container_name=containerName;
        if(valueObject instanceof  String){
            field.field_value = (String)valueObject;
            field.field_type = 'STRING';
        }
        else if(valueObject instanceof  Integer){
            field.field_value = (Integer)valueObject;
            field.field_type = 'INTEGER';
        }
        else if(valueObject instanceof  Decimal){
            field.field_value = (Decimal)valueObject;
            field.field_type = 'DECIMAL';
        }
        else if(valueObject instanceof  Boolean){
            field.field_value = (Boolean)valueObject;
            field.field_type = 'BOOLEAN';
        }
        else if(valueObject instanceof  Double){
            field.field_value = (Double)valueObject;
            field.field_type = 'DOUBLE';
        }
        else if(valueObject instanceof  List<Object>){
            return null;
        }
        else{
            System.debug('\n ERROR: Unknown instance of a data type - List>' + (valueObject instanceof  List<Object>));
            System.debug('\n ERROR: Unknown instance of a data type - Map>' + (valueObject instanceof  Map<String,Object>));
            return null;
        }
        return field;
    }
    /**
    * @description This inner class defines a FieldMetadata record (implements comparable to sort order of fields on page)
    */
    public class FieldMetadata implements Comparable {
        public String field_api_name;
        public String field_help_text;
        public String field_label;
        public Object field_value;
        public String field_type;
        public String container_name;
        public String list_name;
        public Boolean displayable_field;
        private Integer field_order;


        public FieldMetadata(){
        }
        public Integer compareTo(Object objToCompare) {
            //Sort by API Name Alphabetically
            FieldMetadata that = (FieldMetadata)objToCompare;
            if (this.field_order == that.field_order) return 0;
            if (this.field_order == null) return 1;
            if (that.field_order == null) return -1;
            if (this.field_order > that.field_order) return 1;
            return -1;
            //return field_order.compareTo(((ColumnMetadata)objToCompare).field_order);
        }
    }
}