/**
 * Created by johnreedy on 2019-10-29.
 */
/****************************************************************************************************
*                            M O D I F I C A T I O N   L O G                                        *
*****************************************************************************************************
*   Date        Developer                   Description                                             *
*   ----------  -------------  ---------------------------------------------------------------------*
*   2019-10-29   Xede Consulting  Initial Creation                                                     *
*                                                                                                   *
*****************************************************************************************************
*/
/**
* @description This test class exercises the methods in the LicensePolicyTimePeriodHelper class.
* @author Xede Consulting Group
* @date 2019-10-29 
*/
@IsTest
private class LicensePolicyTimePeriodHelperTest {
    static Account legalEntityObj;
    static Account reportingEntityObj;
    static Account venueObj;
    static Account financialRespObj;
    static Account billingEntityObj;
    static License_Class__c licenseClassObj;
    static License_Policy__c policyObj;

    /**
    * @description This method initializes setup data.
    */
    static void init(){
        legalEntityObj       = TestDataHelper.createAccount('WZYX', 'Legal Entity',True);
        reportingEntityObj  = TestDataHelper.createAccount('Reporting Entity', 'Vendor', True);
        venueObj             = TestDataHelper.createAccount('WZYX Station', 'Station',True);
        financialRespObj         = TestDataHelper.createAccount('Financially Responsible Account', 'Legal Entity',True);
        billingEntityObj     = TestDataHelper.createAccountWithAddresses('Billing Entity Account', 'Legal Entity', true, false, true);
        licenseClassObj      = TestDataHelper.createLicenseClass('Test License Class', GlobalConstant.INDUSTRY_TYPE, True);
    }

    /**
    * @description Given a License Policy with a Financially Responsible account, and a Reporting Entity, create a Time
    * Period record and verify that the Financially Responsible and Reporting Entity from the License Policy have been
    * populated on the Time Period Record.
    */
    static testMethod void testFinanciallyResponsiblePresent(){
        init();

        //create a policy with a Financially Responsible Account
        policyObj = TestDataHelper.createLicensePolicy(licenseClassObj.Id, System.Today(), null, False);
        policyObj.Licensee__c                = legalEntityObj.Id;
        policyObj.Financially_Responsible__c = financialRespObj.Id;
        policyObj.Venue__c                   = venueObj.Id;
        policyObj.Reporting_Entity__c       = reportingEntityObj.Id;
        policyObj.Billing_Entity__c          = billingEntityObj.Id;
        insert policyObj;

        //create time period record
        Test.startTest();
            Date firstOfMonth = System.today().toStartOfMonth();
            Date lastOfMonth  = System.today().addMonths(1).toStartOfMonth().addDays(-1);
            License_Policy_Time_Period__c timePeriodObj = TestDataHelper.createLicensePolicyTimePeriod(policyObj.Id, firstOfMonth, lastOfMonth, True);
        Test.stopTest();

        //verify that the time period's Financially Responsible Account and Reporting Entity are properly updated
        timePeriodObj = [SELECT Id, Billing_Entity__c, Financially_Responsible__c, Reporting_Entity__c FROM License_Policy_Time_Period__c WHERE Id = :timePeriodObj.Id];

        System.assertEquals(financialRespObj.Id, timePeriodObj.Financially_Responsible__c);
        System.assertEquals(reportingEntityObj.Id, timePeriodObj.Reporting_Entity__c);
        System.assertEquals(billingEntityObj.Id, timePeriodObj.Billing_Entity__c);

    }

    /**
    * @description Given a License Policy WITHOUT a Financially Responsible account, and a Reporting Entity, create a Time
    * Period record and verify that the Financially Responsible field on the Time Period is set to the Licensee (Legal Entity).
    */
    static testMethod void testFinanciallyResponsibleNOTPresent(){
        init();

        //create a policy WITHOUT a Financially Responsible Account
        policyObj = TestDataHelper.createLicensePolicy(licenseClassObj.Id, System.Today(), null, False);
        policyObj.Licensee__c          = legalEntityObj.Id;
        policyObj.Venue__c             = venueObj.Id;
        insert policyObj;

        //create time period record
        Test.startTest();
            Date firstOfMonth = System.today().toStartOfMonth();
            Date lastOfMonth  = System.today().addMonths(1).toStartOfMonth().addDays(-1);
            License_Policy_Time_Period__c timePeriodObj = TestDataHelper.createLicensePolicyTimePeriod(policyObj.Id, firstOfMonth, lastOfMonth, True);
        Test.stopTest();

        //verify that the time period's Financially Responsible Account and Reporting Entity are properly updated
        timePeriodObj = [SELECT Id, Billing_Entity__c, Financially_Responsible__c, Reporting_Entity__c FROM License_Policy_Time_Period__c WHERE Id = :timePeriodObj.Id];

        System.assertEquals(legalEntityObj.Id, timePeriodObj.Financially_Responsible__c);
        System.assertEquals(null, timePeriodObj.Reporting_Entity__c);
        System.assertEquals(legalEntityObj.Id, timePeriodObj.Billing_Entity__c);

    }
}