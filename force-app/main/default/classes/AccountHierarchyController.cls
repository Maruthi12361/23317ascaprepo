/*****************************************************************************************************
 *                            M O D I F I C A T I O N   L O G                                        *
 *****************************************************************************************************
 *   Date        Developer               Description                                                 *
 *   ----------  -----------             ------------------------------------------------------------*
 *   2020-01-06  Xede Consulting Group   Initial Creation                                            *
 *                                                                                                   *
 *   Code Coverage: 89%                                                                             *
 *                                                                                                   *
 *****************************************************************************************************/

/**
 * @description Constructs the entire account hierarchy tree for a given account and returns it in
 * the format used by the Lightning TreeGrid component.
 * @author Xede (Jason Burke)
 * @date 2020-01-06
 */
global with sharing class AccountHierarchyController {

    /**
     * @description builds the entire account hierarchy tree
     *
     * @param accountId the Id of an account in the tree
     *
     * @return The account hierarchy tree
     */
    @AuraEnabled
    global static AccountHierarchy[] getAccountHierarchy(Id accountId) {
        Account rootAccount = getRootAccount(accountId);
        AccountHierarchy[] hierarchyList = buildAccountHierarchy(rootAccount);
        return hierarchyList;
    }

    /**
     * @description finds the root (top-level) account of the account hierarchy tree
     *
     * @param accountId the Id of an account in the tree
     *
     * @return the root account
     */
    private static Account getRootAccount(Id accountId) {
        // query the max # of levels up (5) in order to minimize the number of queries that have to be made
        Account acct = [
                SELECT ParentId, Parent.ParentId, Parent.Parent.ParentId, Parent.Parent.Parent.ParentId,
                        Parent.Parent.Parent.Parent.ParentId, Parent.Parent.Parent.Parent.Parent.ParentId,
                        Name, RecordType.Name, Account_Classification__c, Music_Use_Type__c, Status__c
                FROM Account
                WHERE Id = :accountId
        ];

        if (acct.Parent.Parent.Parent.Parent.Parent.ParentId != null) {
            acct = getRootAccount(acct.Parent.Parent.Parent.Parent.Parent.ParentId);
        }
        else if (acct.Parent.Parent.Parent.Parent.ParentId != null) {
            acct = getRootAccount(acct.Parent.Parent.Parent.Parent.ParentId);
        }
        else if (acct.Parent.Parent.Parent.ParentId != null) {
            acct = getRootAccount(acct.Parent.Parent.Parent.ParentId);
        }
        else if (acct.Parent.Parent.ParentId != null) {
            acct = getRootAccount(acct.Parent.Parent.ParentId);
        }
        else if (acct.Parent.ParentId != null) {
            acct = getRootAccount(acct.Parent.ParentId);
        }
        else if (acct.ParentId != null) {
            acct = getRootAccount(acct.ParentId);
        }

        return acct;
    }

    /**
     * @description convenience method for buildAccountHierarchy() that takes a list
     *
     * @param rootAccount the root of the account hierarchy tree
     *
     * @return the account hierarchy tree
     */
    private static AccountHierarchy[] buildAccountHierarchy(Account rootAccount) {
        return buildAccountHierarchy(new Account[] { rootAccount });
    }

    /**
     * @description recursively builds the account hierarchy tree from the given account list downwards
     *
     * @param accountList the given top-level account list
     *
     * @return the account hierarchy for the account list
     */
    private static AccountHierarchy[] buildAccountHierarchy(Account[] accountList) {
        List<AccountHierarchy> hierarchyList = new List<AccountHierarchy>();
        for (Account acct : accountList) {
            hierarchyList.add(new AccountHierarchy(acct));
        }

        Account[] childAccountList = [
                SELECT ParentId, Name, RecordType.Name, Account_Classification__c, Music_Use_Type__c, Status__c
                FROM Account
                WHERE ParentId IN :accountList
                ORDER BY Name
        ];

        if (!childAccountList.isEmpty()) {
            AccountHierarchy[] childHierarchyList = buildAccountHierarchy(childAccountList);
            for (AccountHierarchy childHierarchy : childHierarchyList) {
                for (AccountHierarchy hierarchy : hierarchyList) {
                    if (hierarchy.accountId == childHierarchy.parentId) {
                        hierarchy.children.add(childHierarchy);
                        break;
                    }
                }
            }
        }

        return hierarchyList;
    }

    /**
     * @description data structure for each node in the tree, for use by the lightning TreeGrid component
     */
    global class AccountHierarchy {
        @AuraEnabled
        global Id accountId;
        @AuraEnabled
        global Id parentId;
        @AuraEnabled
        global String accountName;
        @AuraEnabled
        global String accountLink;
        @AuraEnabled
        global String accountRecordType;
        @AuraEnabled
        global String accountClassification;
        @AuraEnabled
        global String musicUseType;
        @AuraEnabled
        global String status;
        @AuraEnabled
        global List<AccountHierarchy> children;

        global AccountHierarchy(Account acct) {
            accountId = acct.Id;
            parentId = acct.ParentId;
            accountName = acct.Name;
            // if Network.getNetworkId() != null we are in a community site, otherwise we're in internal Salesforce
            accountLink = Network.getNetworkId() != null ? '/account/' + accountId : '/' + accountId;
            accountRecordType = acct.RecordType.Name;
            accountClassification = acct.Account_Classification__c;
            musicUseType = acct.Music_Use_Type__c;
            status = acct.Status__c;
            children = new List<AccountHierarchy>();
        }
    }
}