({
	populateNextFewYears : function(component) {
        var todaysDate = new Date();
        var thisYear = todaysDate.getFullYear();
        var nextFewYears = [];
        for(var i= thisYear; i < thisYear + 6; i++){
            nextFewYears.push(i);
        }
        component.set("v.nextFewYears", nextFewYears);
    },

    // Expiration Date must not be in the past using month and year since prior years aren't selectable
    validate : function(component) {
        // See if the fields are valid first
        var expirationDateYearField = component.find("expirationDateYearField");
        var expirationDateMonthField = component.find("expirationDateMonthField");
        var piecesValid = expirationDateYearField.checkValidity() && expirationDateMonthField.checkValidity();

        var todaysDate = new Date();
        // Because we use a picklist that starts with the current year, we don't need to check if the year is in the past
        // Note getMonth() returns 0 for January, so we need to add one to compare it accurately to the expiry month
        var priorMonthThisYear = expirationDateYearField.get("v.value") == todaysDate.getFullYear() && expirationDateMonthField.get("v.value") < todaysDate.getMonth() + 1;

        // Validity is the combination of the two factors.
        var isValid = piecesValid && !priorMonthThisYear;

        // Set the validity and the style on the component
        var expiryDate = component.find('expiryDate');
        component.set("v.valid", isValid);
        if(isValid) {
            $A.util.removeClass(expiryDate, 'slds-has-error');
        } else {
        	$A.util.addClass(expiryDate, 'slds-has-error');
        }
        return isValid;
    }
})