/*
 * Copyright (c) 2018, salesforce.com, inc.
 * All rights reserved.
 * Licensed under the BSD 3-Clause license.
 * For full license text, see LICENSE.txt file in the repo root  or https://opensource.org/licenses/BSD-3-Clause
 */
 
 ({

	invoke : function(component, event, helper) {
        
        // Get the record id passed into from the flow
        var destObject = component.get("v.SObject");
        // Opens the record based on the passed in ID, in EDIT mode
        var navEvt = $A.get("e.force:editRecord");
        navEvt.setParams({
          "recordId": destObject,
        });
        navEvt.fire();
     
}
     
 })