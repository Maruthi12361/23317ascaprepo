/**
 * Created by Xede Consulting Group (Jason Burke) on 1/2/2020.
 */

({
    handleInit : function(component, event, helper) {
        var columns = [
            {
                type: "url",
                fieldName: "accountLink",
                label: "ACCOUNT NAME",
                typeAttributes: {
                    label: { fieldName: "accountName"}
                }
            },
            {
                type: "text",
                fieldName: "accountRecordType",
                label: "ACCOUNT RECORD TYPE"
            },
            {
                type: "text",
                fieldName: "accountClassification",
                label: "ACCOUNT CLASSIFICATION"
            },
            {
                type: "text",
                fieldName: "musicUseType",
                label: "MUSIC USE TYPE"
            },
            {
                type: "text",
                fieldName: "status",
                label: "STATUS"
            }
        ];

        component.set("v.gridColumns", columns);

        helper.callGetAccountHierarchy(component);
    }
});