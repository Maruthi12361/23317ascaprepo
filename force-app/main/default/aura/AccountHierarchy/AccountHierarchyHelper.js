/**
 * Created by Xede Consulting Group (Jason Burke) on 1/2/2020.
 */

({
    callGetAccountHierarchy : function(component) {
        var action = component.get("c.getAccountHierarchy");
        var recordId = component.get("v.recordId");
        console.log("recordId: " + recordId);
        action.setParam("accountId", recordId);

        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var hierarchyList = response.getReturnValue();
                
                var accountIdList = this.processHierarchyList(hierarchyList);
                //console.log(JSON.stringify(hierarchyList));
                //console.log(accountIdList);

                component.set("v.gridData", hierarchyList);
                component.set("v.gridExpandedRows", accountIdList);
                component.set("v.gridSelectedRows", [recordId]);
            }
            else {
                this.handleErrors(response);
            }
        });

        $A.enqueueAction(action);
    },

    processHierarchyList : function(hierarchyList) {
        var accountIdList = [];

        for (var i = 0; i < hierarchyList.length; i++) {
            accountIdList.push(hierarchyList[i].accountId);
            //console.log("accountIdList: " + accountIdList);

            hierarchyList[i]._children = hierarchyList[i].children;
            delete hierarchyList[i].children;

            var childrenAccountIdList = this.processHierarchyList(hierarchyList[i]._children);
            //console.log("childrenAccountIdList: " + childrenAccountIdList);

            accountIdList = accountIdList.concat(childrenAccountIdList);
        }

        return accountIdList;
    },

    handleErrors : function(response) {
        var errors = response.getError();
        console.log("errors", errors);

        var errorMsg = "";
        if (errors) {
            errors.forEach(function(e) {
                errorMsg += e.message + "\r\n";
            });
        } else {
            errorMsg = "An unknown error has occurred";
        }

        this.createToastAction(errorMsg, "error").fire();
    },

    createToastAction : function(message, type) {
        var toastAction = $A.get("e.force:showToast");
        toastAction.setParam("title", "View Account Hierarchy");
        toastAction.setParam("message", message);
        toastAction.setParam("type", type);
        if (type == "error") {
            toastAction.setParam("mode", "sticky");
        }
        return toastAction;
    }
});