/**
 * Created by johnreedy on 2019-06-03.
 */
({
    doInit : function(component, event, helper) {
    	    var id = component.get("v.recordId");
            if(id== null || id=='undefined'){
                return;
            }
            let action = component.get('c.getRateScheduleFees');
            action.setParams({
                "recordId" : id
            });
            action.setCallback(this, function(response){
                let state = response.getState();
                if(state === 'SUCCESS'){
                    let ColumnMetaData = JSON.parse(response.getReturnValue());
                    //alert(feeCalculated);
                    component.set('v.ColumnMetaData', ColumnMetaData)
                 }

            });
                $A.enqueueAction(action);
        }
})