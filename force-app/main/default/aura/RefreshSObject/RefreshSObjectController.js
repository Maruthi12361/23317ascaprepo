({
	invoke : function(component, event, helper) {

		// Get the value passed into the component from the flow
		var refresh = component.get ("v.RefreshView");
		// Evaluate the value passed into the component
		if (refresh == true){
			$A.get('e.force:refreshView').fire();
		} else {
			//Do Nothing
			}
		}
	})