({
// Invoke allows this to used by a flow and we are grabbing the values passed into the component via flow
invoke : function (component, event, helper) {
    var objectAPI = component.get("v.entityApiName");
    var recordType = component.get("v.recordTypeId");
    var parentRecordId = component.get("v.ParentId");
    console.log("parentRecordId", parentRecordId);

// This opens the record creation page for the object passed in and record type defined
    $A.get('e.force:refreshView').fire();
    
    window.setTimeout(
    	$A.getCallback(function() {
            var createRecordEvent = $A.get("e.force:createRecord");
            createRecordEvent.setParams({
                "entityApiName": objectAPI,
                "recordTypeId": recordType,
                // Any fields that need to be defaulted are set below        
                "defaultFieldValues": {
                    "ParentId": parentRecordId
                } 
            });
            createRecordEvent.fire();
        })
    , 500);
          
                       
}
     
 })