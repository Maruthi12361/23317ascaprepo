/**
 * Created by Xede Consulting Group (Jason Burke) on 11/13/2019.
 */

({
    createToastAction : function(message, type) {
        var toastAction = $A.get("e.force:showToast");
        toastAction.setParam("title", "Available Record Type Select");
        toastAction.setParam("message", message);
        toastAction.setParam("type", type);
        if (type == "error") {
            toastAction.setParam("mode", "sticky");
        }
        return toastAction;
    },

    handleErrors : function(response) {
        var errors = response.getError();
        console.log("errors", errors);

        var errorMsg = "";
        if (errors) {
            errors.forEach(function(e) {
                errorMsg += e.message + "\r\n";
            });
        } else {
            errorMsg = "An unknown error has occurred";
        }

        this.createToastAction(errorMsg, "error").fire();
    }
});