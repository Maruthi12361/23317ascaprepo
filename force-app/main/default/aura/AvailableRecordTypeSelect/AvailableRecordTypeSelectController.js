/**
 * Created by Xede Consulting Group (Jason Burke) on 11/13/2019.
 */

({
    handleInit : function(component, event, helper) {
        var action = component.get("c.getAvailableRecordTypes");
        action.setParam("sObjectType", component.get("v.sObjectType"));
        action.setParam("excludedRecordTypes", component.get("v.excludedRecordTypes"));

        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var myRecordTypeInfos = response.getReturnValue();
                console.log("myRecordTypeInfos", myRecordTypeInfos);

                component.set("v.myRecordTypeInfos", myRecordTypeInfos);
                if (myRecordTypeInfos.length) {
                    component.set("v.selectedId", myRecordTypeInfos[0].id);
                }
            }
            else {
                helper.handleErrors(response);
            }
        });

        $A.enqueueAction(action);
    },

    handleSelect : function(component, event, helper) {
        console.log("selectedId", component.get("v.selectedId"));
    }
});