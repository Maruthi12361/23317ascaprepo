({
    subscribe : function(component, event, helper) {
        // Get the empApi component
        const empApi = component.find('empApi');

        // Get the channel from component
        const channel = component.find('channel').get('v.value');

        // Replay option to get new events
        const replayId = -1;

        // Subscribe to an event
        empApi.subscribe(channel, replayId, $A.getCallback(eventReceived => {
            console.log('Received event ', JSON.stringify(eventReceived));

            // Process event (this is called each time we receive an event
            var obj = JSON.parse(JSON.stringify(eventReceived));

            //check record id the
            if (component.get('v.recordId') == obj.data.payload.Object_Id__c){


                var message = obj.data.payload.Address_Type__c + " has been standardized.";
                //var message = 'Address(es) have been standardized.';
                //var message = obj.data.payload.Address_Type__c + " has changed.";
                    //message += "\nPremise Address Status: " + obj.data.sobject.Premise_Address_Status__c;

                //fire toast message
                let action = $A.get('e.force:showToast');

                action.setParams({
                    title: 'Address Standardization',
                    message: message,
                    type: 'success',
                    duration: "5000"
                });
                action.fire();
                $A.get('e.force:refreshView').fire();
            }

        }))
        .then(subscription => {
            // Confirm that we have subscribed to the event channel.
            console.log('Subscribed to channel ', subscription.channel);
            
        });
    }
})