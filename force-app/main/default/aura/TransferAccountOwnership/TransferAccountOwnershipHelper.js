/**
 * Created by Xede Consulting Group (Jason Burke) on 10/24/2019.
 */

({
    callGetTmlcLicensePolicyAction : function(component) {
        var action = component.get("c.getTmlcLicensePolicy");
        action.setParam("venueId", component.get("v.recordId"));

        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var licensePolicy = response.getReturnValue();
                component.set("v.licensePolicy", licensePolicy);
                window.setTimeout(
                    $A.getCallback(function() {
                        component.set("v.licenseType", licensePolicy.License_Type__c);
                        component.set("v.licenseClassId", licensePolicy.License_Class_Type__c);
                    })
                );
            }
            else {
                this.handleErrors(response);
            }
        });

        $A.enqueueAction(action);
    },

    callGetLicenseClassesAction : function(component) {
        var action = component.get("c.getLicenseClasses");

        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                component.set("v.licenseClasses", response.getReturnValue());
                window.setTimeout(
                    $A.getCallback(function() {
                        component.set("v.licenseClassId", component.get("v.licensePolicy.License_Class_Type__c"));
                    })
                );
            }
            else {
                this.handleErrors(response);
            }
        });

        $A.enqueueAction(action);
    },

    callTransferOwnershipAction : function(component) {
        var action = component.get("c.transferOwnership");

        var offAirDateList = [];
        component.get("v.selectedOffAirDates").forEach(function(dateStr) {
            console.log(dateStr);
            var newDate = new Date(dateStr);
            newDate.setUTCHours(0, 0, 0, 0);
            offAirDateList.push(newDate);
        });
        console.log(offAirDateList);

        var params = {
             "venueId" : component.get("v.recordId"),
             "parentId" : component.get("v.parentId"),
             "effectiveDate" : component.get("v.effectiveDate"),
             "offAirDateList" : offAirDateList,
             "licenseType" : component.get("v.licenseType"),
             "policyId" : component.get("v.licensePolicy.Id")
        };
        console.log("params", params);

        action.setParams(params);

        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                this.createToastAction("Successful!", "success").fire();
                $A.get("e.force:refreshView").fire();
            }
            else {
                this.handleErrors(response);
            }

            $A.get("e.force:closeQuickAction").fire();
        });

        $A.enqueueAction(action);
    },

    createToastAction : function(message, type) {
        var toastAction = $A.get("e.force:showToast");
        toastAction.setParam("title", "Transfer Venue Ownership");
        toastAction.setParam("message", message);
        toastAction.setParam("type", type);
        if (type == "error") {
            toastAction.setParam("mode", "sticky");
        }
        return toastAction;
    },

    handleErrors : function(response) {
        var errors = response.getError();
        console.log("errors", errors);

        var errorMsg = "";
        if (errors) {
            errors.forEach(function(e) {
                errorMsg += e.message + "\r\n";
            });
        } else {
            errorMsg = "An unknown error has occurred";
        }

        this.createToastAction(errorMsg, "error").fire();
    },

    formatDateString : function(dateStr) {
        var dateArray = dateStr.split(" ");
        var day = dateArray[2].charAt(0) == '0' ? dateArray[2].charAt(1) : dateArray[2];
        return dateArray[1] + " " + day + ", " + dateArray[3];
    }
});