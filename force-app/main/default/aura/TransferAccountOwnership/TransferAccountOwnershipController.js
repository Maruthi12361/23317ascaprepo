/**
 * Created by Xede Consulting Group (Jason Burke) on 10/24/2019.
 */

({
    handleInit : function(component, event, helper) {
        helper.callGetTmlcLicensePolicyAction(component);
        helper.callGetLicenseClassesAction(component);
    },

    handleEffectiveDate : function(component, event, helper) {
        var effectiveDateArray = component.get("v.effectiveDate").split("-");
        console.log(effectiveDateArray);
        var effectiveDate = new Date(effectiveDateArray[0], effectiveDateArray[1] - 1);
        console.log(effectiveDate);
        var endDate = new Date().setDate(1);
        console.log(endDate);

        var offAirDates = [];
        for (var curDate = effectiveDate; curDate <= endDate; curDate.setMonth(curDate.getMonth() + 1)) {
            console.log("curDate", curDate);
            offAirDates.push({
                "label": helper.formatDateString(curDate.toDateString()),
                "value": curDate.toISOString()
            });
        }

        console.log(offAirDates);
        component.set("v.offAirDates", offAirDates);
    },

    handleOffAirDates : function(component, event, helper) {
        component.set("v.selectedOffAirDates", event.getParam("values"));
    },

    handleTransferOwnership : function(component, event, helper) {
        helper.callTransferOwnershipAction(component);
    },

    handleCancel : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
});