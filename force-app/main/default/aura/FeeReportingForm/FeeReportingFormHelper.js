/**
 * Created by johnreedy on 2020-01-02.
 */
({
    //initializes the Revenue Reporting form with the fields required to calculate the fee for the given license policy
    initializeColumnMetaData : function(component,event) {
        var id = component.get("v.recordId");
        if(id==null || id=='undefined'){
            return;
        }

        let action = component.get('c.getLicenseClassConfiguration');

        action.setParams({
            "recordId" : id
        });

        action.setCallback(this, function(response){
            let state = response.getState();
            if(state === 'SUCCESS'){
                let field_metadata_wrapper = JSON.parse(response.getReturnValue());
                //alert(JSON.stringify(field_metadata_wrapper));

                component.set('v.FieldMetadata', field_metadata_wrapper.field_level_metadata);

                var licenseClassName = field_metadata_wrapper.licenseClassName;
                component.set('v.licenseClassName', licenseClassName);

                var LicensePolicyName = field_metadata_wrapper.LicensePolicyName;
                component.set('v.LicensePolicyName', LicensePolicyName);

                var LicenseClassConfig = field_metadata_wrapper.LicenseClassConfig;
                component.set('v.LicenseClassConfig', LicenseClassConfig);

                var licenseClassId = field_metadata_wrapper.licenseClassId;
                component.set('v.licenseClassId', licenseClassId);

                var policyEndDate = field_metadata_wrapper.policyEndDate;
                component.set('v.policyEndDate', policyEndDate);

               /* var contentType = field_metadata_wrapper.contentType;
                component.set('v.contentType', contentType);*/

                var defaultTimePeriod = field_metadata_wrapper.timePeriod;
                component.set('v.TimePeriod', defaultTimePeriod);

                var reportingColumns = field_metadata_wrapper.reportingColumns;
                component.set('v.reportingColumns', reportingColumns);

            }
            else{
                this.handleErrors(response);
            }

        });
        $A.enqueueAction(action);
    },
    findDependentFields : function(component, event, helper) {
        var selectedRecordName  = event.getSource().get("v.name");
        var isChecked           = event.getSource().get("v.checked");
        var LicPolicyFieldList  = component.get('v.FieldMetadata');

        /*
        Iterate over list of fields on form and when you find the field that was selected, check to see if there are
        any dependent fields, and if so, update the value field, and the checkbox accordingly.
        */
        if(LicPolicyFieldList != null && LicPolicyFieldList.length > 0){

            for (var i = 0; i < LicPolicyFieldList.length; i++) {
                var fieldAPIName        = LicPolicyFieldList[i].field_api_name;
                var fieldDisplayLogic   = LicPolicyFieldList[i].field_Condition;
                var dependentFieldNames = LicPolicyFieldList[i].dependent_fieldNames;

                //if the field api name equals the field selected, and the dependent fields has values, then continue
                if(fieldAPIName == selectedRecordName && typeof dependentFieldNames !== 'undefined'){
                    //iterate over list of fields on form and find the selected field and its dependent fields
                    for (var j = 0; j < LicPolicyFieldList.length; j++) {

                        var fieldAPIName  = LicPolicyFieldList[j].field_api_name;
                        var field_display = LicPolicyFieldList[j].field_display;
                        var field_type    = LicPolicyFieldList[j].field_type;
                        //alert(fieldAPIName +"\n" + field_display+"\n" +field_type + "\n"+ dependentFieldNames);

                        if(dependentFieldNames.includes(fieldAPIName)){
                            //if isChecked is now false, reset numeric fields
                            if(!isChecked && (field_type=="INTEGER" || field_type=="DOUBLE" || field_type=="NUMBER" )){
                                LicPolicyFieldList[j].value = '';
                                LicPolicyFieldList[j].integerValue=0;
                                LicPolicyFieldList[j].decimalValue=0;
                            }
                            //enable dependent field
                            LicPolicyFieldList[j].field_ConditionalDisplay = isChecked;
                            if(field_type=="BOOLEAN" && isChecked == false ){
                                LicPolicyFieldList[j].booleanValue = false;
                            }
                            else if(field_type=="BOOLEAN" && isChecked == true ){
                                LicPolicyFieldList[j].booleanValue = false;
                            }
                        }
                    }
                }
            }
        }
        component.set('v.FieldMetadata',LicPolicyFieldList);
        component.set('v.hideSubmitButton', true);
        component.set('v.hideCalcFeeButton', false);
    },
    //submit the request to the DSL to calculate the fee based on the values entereed into the form
    submitRequest : function(component, event, helper){
        component.set('v.refreshRateSchedule', false);
        component.set('v.hasFeeAmount', false);

        var recordId           = component.get("v.recordId");
        var LicensePolicyName  = component.get("v.LicensePolicyName");
        var LicPolicyFieldList = component.get("v.FieldMetadata");
        var licenseClassId     = component.get("v.licenseClassId");
        var LicenseClassConfig = component.get("v.LicenseClassConfig");
        var TimePeriodObj      = component.get("v.TimePeriod");
        var policyEndDate      = component.get("v.policyEndDate");

        //validate that all fields are not null, 'undefined' , or empty
        for (var i = 0; i < LicPolicyFieldList.length; i++) {
            //alert(JSON.stringify(LicPolicyFieldList[i]));
            var fieldValue = LicPolicyFieldList[i].decimalValue;

            //remove trailing decimals
            //LicPolicyFieldList[i].decimalValue = Math.trunc(LicPolicyFieldList[i].fieldValue);

            //if any of the field values are not set, display a message
            if (fieldValue === null || fieldValue === 'undefined' || fieldValue === ''){
                var msg = 'Please enter a value for the ' + LicPolicyFieldList[i].field_label + ' field.\n Field value must be numeric.';
                this.createToastAction(msg,'error').fire();
                return;
            }
        }

        var getFormAction = component.get('c.sendForm');
        getFormAction.setParams({
            "recordId" : recordId,
            "LicensePolicyName" : LicensePolicyName,
            "licenseClassId" : licenseClassId,
            "LicenseClassConfig" : LicenseClassConfig,
            "licensePolicy" : JSON.stringify(LicPolicyFieldList),
            "policyTimePeriod" : JSON.stringify(TimePeriodObj),
            "policyEndDate" : policyEndDate,

        });

        getFormAction.setCallback(this, function(response) {
            let state = response.getState();

            if(state === 'SUCCESS'){
                let fee_calc_response = JSON.parse(response.getReturnValue());

                var fee_amount = this.formatNumberWithCommas(fee_calc_response.feeAmount);

                //if the fee amount does not have a decimal add it
                if (fee_amount.indexOf('.') == -1) {
                    fee_amount += '.';
                }
                //right pad the fee amount with zeros
                while (fee_amount.length < fee_amount.indexOf('.') + 3){
                    fee_amount += '0';
                }
                component.set('v.fee_amount', fee_amount);

                var fee_calc_request_string = fee_calc_response.feeRequest;
                component.set('v.fee_calc_request_string', fee_calc_request_string);

                var fee_calc_response_string = fee_calc_response.feeResponse;
                component.set('v.fee_calc_response_string', fee_calc_response_string);

                var hasRateScheduleFees = fee_calc_response.hasRateScheduleFees;
                component.set('v.hasRateScheduleFees', hasRateScheduleFees);

                component.set('v.hasFeeAmount',true);
                component.set('v.refreshRateSchedule', true);
                component.set('v.hideSubmitButton', false);
                component.set('v.hideCalcFeeButton', true);

                let refreshView = $A.get('e.force:refreshView');
            }
            else{
                this.handleErrors(response);
            }
        });
        $A.enqueueAction(getFormAction);
    },
    //upon report submission, create the time period and transaction records in Salesforce
    createRecords : function(component,event) {
       var recordId      = component.get("v.recordId");
       var feeAmount     = component.get("v.fee_amount");
       var feeRequest    = component.get("v.fee_calc_request_string");
       var feeResponse   = component.get("v.fee_calc_response_string");
       var TimePeriodObj = component.get("v.TimePeriod");

       if(recordId==null || recordId=='undefined'){
           return;
       }

       let action = component.get('c.saveReport');

       action.setParams({
           "recordId" : recordId,
           "feeAmount" : feeAmount,
           "feeRequest" : feeRequest,
           "feeResponse" : feeResponse,
           "policyTimePeriod" : JSON.stringify(TimePeriodObj),
       });

       action.setCallback(this, function(response){
           let state = response.getState();
           if(state === 'SUCCESS'){
               let response_string = JSON.parse(response.getReturnValue());

               //post toast message indicating that the report has been successfully subnitted
               this.createToastAction('Your report has been successfully submitted.', 'success').fire();

               $A.get("e.force:closeQuickAction").fire();
               $A.get("e.force:refreshView") ;

            }
            else{

                this.handleErrors(response);
            }

        });

        $A.enqueueAction(action);

    },
    //format number with thousands separator
    formatNumberWithCommas : function(numberToFormat){
        var formattedNumber = numberToFormat.toString();
        var pattern = /(-?\d+)(\d{3})/;

        while (pattern.test(formattedNumber))
            formattedNumber = formattedNumber.replace(pattern, "$1,$2");

        return formattedNumber;

    },
    //Error Handler function
    handleErrors : function(response) {
        var errorMsg = "";

        var errors = response.getError();
        if (errors) {
            errors.forEach(function(e) {
                errorMsg += e.message + "\r\n";
            });
        } else {
            errorMsg = "An unknown error has occurred";
        }

        this.createToastAction(errorMsg, "error").fire();
    },
    //toast message function
    createToastAction : function(message, type) {
       var toastAction = $A.get("e.force:showToast");
       if (type === 'error'){
         toastAction.setParam("title", "Revenue Reporting Error");
       }
       else{
           toastAction.setParam("title", "Revenue Reporting");
       }
       toastAction.setParam("message", message);
       toastAction.setParam("type", type);
       if (type == "error") {
           toastAction.setParam("mode", "sticky");
       }
       return toastAction;
    }
})