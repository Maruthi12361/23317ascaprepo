/**
 * Created by johnreedy on 2019-12-02.
 */
({

    doInit : function(component, event, helper) {
            var id = component.get("v.recordId");
            var licenseClassId = component.get("v.licenseClassId");
            var feeResponse = component.get("v.feeResponse");
            if(id== null || id=='undefined'){
                return;
            }
            let action = component.get('c.getRateScheduleFees');
            action.setParams({
                "recordId" : id,
                "licenseClassId" : licenseClassId,
                "feeResponse" : feeResponse
            });
            action.setCallback(this, function(response){
                //var hasMessage = component.get('v.hasMessage');
                let state = response.getState();
                if(state === 'SUCCESS'){
                    let ColumnMetaData = JSON.parse(response.getReturnValue());
                    //alert(feeCalculated);
                    component.set('v.ColumnMetaData', ColumnMetaData)
                    for(var i = 0; i < ColumnMetaData.length; i++){
                        if(ColumnMetaData[i].field_api_name == 'Warning_Message' && ColumnMetaData[i].field_value.length != 0){
                            var warningMessage = ColumnMetaData[i].field_value;

                            component.set('v.hasMessage', true);
                            component.set('v.warningMessage', warningMessage);
                        }
                    }
                    let refreshView = $A.get('e.force:refreshView');
                 }
                 else{
                     this.handleErrors(response);
                 }

            });
                $A.enqueueAction(action);
        },
        //Error Handler function
        handleErrors : function(response) {
            var errorMsg = "";

            var errors = response.getError();
            if (errors) {
               errors.forEach(function(e) {
                   errorMsg += e.message + "\r\n";
               });
            } else {
               errorMsg = "An unknown error has occurred";
            }

            this.createToastAction(errorMsg, "error").fire();
        },
        //toast message function
        createToastAction : function(message, type) {
            var toastAction = $A.get("e.force:showToast");
            if (type === 'error'){
             toastAction.setParam("title", "Rate Schedule Section Error");
            }
            else{
               toastAction.setParam("title", "Rate Schedule");
            }
            toastAction.setParam("message", message);
            toastAction.setParam("type", type);
            if (type == "error") {
               toastAction.setParam("mode", "sticky");
            }
            return toastAction;
        }

})