/**
 * Created by johnreedy on 2019-11-21.
 */
({
    initializeComponent : function(component, event, helper) {
        helper.initializeColumnMetaData(component,event);
    },
    calculateFee : function(component, event, helper) {
        helper.submitRequest(component,event);
    },
    submitReport : function(component, event, helper) {
        helper.createRecords(component,event);
    },
    //event handler to refresh the rate schedule section on the Revenue Report form
    handleRefreshRateSchedule : function(component, event) {
      var refreshRateSchedule = event.getParam('refreshRateSchedule');
      component.set('v.refreshRateSchedule', refreshRateSchedule);
    },
    //event handler to refresh the rate schedule section on the Revenue Report form
    handleHideSubmitButton : function(component, event) {
      //var hideSubmitButton = event.getParam('hideSubmitButton');
      component.set('v.hideSubmitButton', true);
      component.set('v.hideCalcFeeButton', false);
    },
    cancel : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
        $A.get("e.force:refreshView") ;
    }
})