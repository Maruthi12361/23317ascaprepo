/**
 * Created by johnreedy on 2019-08-02.
 */
({
	getPrimaryContact : function(component) {

        var action = component.get("c.getPrimaryContact");
        action.setParams({ recordId : component.get("v.recordId") });
        component.set('v.ErrorMsg', null);
        component.set('v.ContactName', null);
        
        // Create a callback that is executed after
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            // The action executed successfully
            //alert(state);
            if (state === "SUCCESS") {
                // Get response and if no records in list set renderMe to false
                //alert(JSON.stringify(response.getReturnValue()));
            	let primaryContact = response.getReturnValue();

                component.set('v.ContactId', primaryContact.ContactId);
                component.set("v.ContactName",primaryContact.ContactName);
                component.set("v.ContactStreet",primaryContact.MailingStreet);
                component.set("v.ContactCity",primaryContact.MailingCity);
                component.set("v.ContactState",primaryContact.MailingState);
                component.set("v.ContactZip",primaryContact.MailingPostalCode);
                component.set("v.ContactEmail",primaryContact.Email);
                component.set("v.ContactPhone",primaryContact.Phone);
                component.set("v.IsPrimary",primaryContact.IsPrimary);
                component.set("v.Role",primaryContact.Role);
            }
            else if (state === "ERROR") {
                // The server returned an error
                // generic error handler
                var errors = response.getError();
                if (errors = "List has no rows for assignment to SObject"){
                    component.set("v.ErrorMsg", "No Primary Contact has been set for this Opportunity")
                }
                else if (errors) {
                    $A.log("Errors", errors);
                    if (errors[0] && errors[0].message) {
                        throw new Error("Error: " + errors[0].message);
                    }
                    else {
                       throw new Error("Unknown Error");
                    }
                }
            }
        });
        $A.enqueueAction(action);
    }
})