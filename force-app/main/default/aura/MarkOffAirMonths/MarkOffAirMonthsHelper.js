/**
 * Created by Xede Consulting Group (Jason Burke) on 12/10/2019.
 */

({
    calculateOffAirDates : function(component) {
        var timePeriod = component.get("v.timePeriod");
        var startDateArray = timePeriod.Start_Date__c.split("-");
        console.log("startDateArray", startDateArray);
        var endDateArray = timePeriod.End_Date__c.split("-");
        console.log("endDateArray", endDateArray);

        var startDate = new Date(startDateArray[0], startDateArray[1] - 1);
        console.log("startDate", startDate);
        var endDate = new Date(endDateArray[0], endDateArray[1] - 1);
        console.log("endDate", endDate);

        var offAirDates = [];
        for (var curDate = startDate; curDate <= endDate; curDate.setMonth(curDate.getMonth() + 1)) {
            console.log("curDate", curDate);
            offAirDates.push({
                "label": this.formatDateString(curDate.toDateString()),
                "value": curDate.toISOString()
            });
        }

        console.log(offAirDates);
        component.set("v.offAirDates", offAirDates);
    },

    callMarkOffAirMonthsAction : function(component) {
        var action = component.get("c.markOffAirMonths");

        var offAirDateList = [];
        component.get("v.selectedOffAirDates").forEach(function(dateStr) {
            console.log(dateStr);
            var newDate = new Date(dateStr);
            newDate.setUTCHours(0, 0, 0, 0);
            offAirDateList.push(newDate);
        });
        console.log(offAirDateList);

        var params = {
             "timePeriodId" : component.get("v.recordId"),
             "offAirDateList" : offAirDateList
        };
        console.log("params", params);

        action.setParams(params);

        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                this.createToastAction("Successful!", "success").fire();
                $A.get("e.force:refreshView").fire();
            }
            else {
                this.handleErrors(response);
            }

            $A.get("e.force:closeQuickAction").fire();
        });

        $A.enqueueAction(action);
    },

    handleErrors : function(response) {
        var errors = response.getError();
        console.log("errors", errors);

        var errorMsg = "";
        if (errors) {
            errors.forEach(function(e) {
                errorMsg += e.message + "\r\n";
            });
        } else {
            errorMsg = "An unknown error has occurred";
        }

        this.createToastAction(errorMsg, "error").fire();
    },

    createToastAction : function(message, type) {
        var toastAction = $A.get("e.force:showToast");
        toastAction.setParam("title", "Mark Off Air Months");
        toastAction.setParam("message", message);
        toastAction.setParam("type", type);
        if (type == "error") {
            toastAction.setParam("mode", "sticky");
        }
        return toastAction;
    },

    formatDateString : function(dateStr) {
        var dateArray = dateStr.split(" ");
        var day = dateArray[2].charAt(0) == '0' ? dateArray[2].charAt(1) : dateArray[2];
        return dateArray[1] + " " + day + ", " + dateArray[3];
    }
});