/**
 * Created by Xede Consulting Group (Jason Burke) on 12/10/2019.
 */

({
    handleTimePeriodChange : function(component, event, helper) {
        helper.calculateOffAirDates(component);
    },

    handleOffAirDates : function(component, event, helper) {
        var selectedOffAirDates = event.getParam("values");
        console.log("selectedOffAirDates", selectedOffAirDates);
        component.set("v.selectedOffAirDates", selectedOffAirDates);
    },

    handleMarkOffAirMonths : function(component, event, helper) {
        helper.callMarkOffAirMonthsAction(component);
    },

    handleCancel : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
});